/*
 * Control GPIO pins on the fly
 *
 * Copyright (c) 2008-2011 Analog Devices Inc.
 *
 * Licensed under the GPL-2 or later.
 */

#include <common.h>
#include <command.h>

#include <hdmi.h>

#ifndef name_to_hdmi
#define name_to_hdmi(name) simple_strtoul(name, NULL, 10)
#endif

extern unsigned *hdmi_base;

static void dump_hdmi(void)
{
	int i;

	printf("************direct register*******************\n");
	for (i = 0x8; i <= 0x30; i += 4)
		printf("direct offset 0x%x is 0x%x\n", i, hdmi_direct_read(hdmi_base, i));
	printf("************indirect register*******************\n");
	for (i = 0; i < 0x13e; i++)
		printf("offset 0x%x is 0x%x\n", i, hdmi_read(hdmi_base, i));
	printf("************HDCP indirect register*******************\n");
	for (i = 0x1350; i < 0x1352; i++)
		printf("offset 0x%x is 0x%x\n", i, hdmi_read(hdmi_base, i));
	for (i = 0x1384; i < 0x1386; i++)
		printf("offset 0x%x is 0x%x\n", i, hdmi_read(hdmi_base, i));

}

static int do_hdmi(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int hdmi;
	ulong addr, value;
	const char *str_cmd;

	str_cmd = argv[1];
	if ((argc == 2 && *str_cmd == 'D') ||
	argc == 3 || argc == 4) {
		addr = simple_strtoul(argv[2], NULL, 16);
		value = simple_strtoul(argv[3], NULL, 16);
	} else {
		goto show_usage;
	}

	/* parse the behavior */
	switch (*str_cmd) {
		case 'm':
			pxa168fb_hdmi_set_mode(addr, value);
			break;
		case 'r':
			printf("0x%x: 0x%x\n", addr, hdmi_read(hdmi_base, addr));
			break;
		case 'w':
			printf("change 0x%x from 0x%x to 0x%x\n",
					addr, hdmi_read(hdmi_base, addr), value);
			hdmi_write(hdmi_base, addr, value);
			break;
		case 'R':
			printf("0x%x: 0x%x\n", addr, hdmi_direct_read(hdmi_base, addr));
			break;
		case 'W':
			printf("change 0x%x from 0x%x to 0x%x\n",
					addr, hdmi_direct_read(hdmi_base, addr, value), value);
			hdmi_direct_write(hdmi_base, addr, value);
			break;
		case 'D':
			dump_hdmi();
			break;
		default:  goto show_usage;
	}
	return 0;

show_usage:
		return cmd_usage(cmdtp);
}

static int do_lcd(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	ulong addr, value;
	const char *str_cmd;

	if (argc != 3 && argc != 4)
 show_usage:
		return cmd_usage(cmdtp);
	str_cmd = argv[1];
	addr = simple_strtoul(argv[2], NULL, 16);
	value = simple_strtoul(argv[3], NULL, 16);

	/* parse the behavior */
	switch (*str_cmd) {
		case 'r':
			printf("0x%x: 0x%x\n", addr, lcd_read(addr));
			break;
		case 'w':
			lcd_write(addr, value);
			break;
		default:  goto show_usage;
	}
	return 0;
}

static int do_vid(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	u32 enable, xres, yres;
	//const char *str_cmd;

	if (!strcmp(argv[1], "sync")) {
		return 0;
	}

	if (argc != 4)
		return cmd_usage(cmdtp);

	enable = simple_strtoul(argv[1], NULL, 16);
	xres = simple_strtoul(argv[2], NULL, 16);
	yres = simple_strtoul(argv[3], NULL, 16);
	pxa168fb_vid(enable, xres, yres);

	return 0;
}

U_BOOT_CMD(hdmi, 4, 0, do_hdmi,
	"read/write hdmi register",
	"r/w for indirect and R/W for direct, m for cea_mode_id\n"
	"4: 720p60 20:1080p24 (id is in hex format)\n");
U_BOOT_CMD(lcd, 4, 0, do_lcd,
	"read/write lcd register",
	"lcd r <addr> or lcd w <addr> <value>\n");
U_BOOT_CMD(video, 4, 0, do_vid,
	"video layer size config or 3d sync",
	"video <enable> <xres> <yres>\n"
	"video sync\n"
	"Hex format input is required\n");
