/*
 * (C) Copyright 2012
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/arch/cpu.h>
#include <mv_wtm.h>
struct wtm_cmd {
    unsigned int prim_cmd_parm0;          // 0x0
    unsigned int prim_cmd_parm1;          // 0x4
    unsigned int prim_cmd_parm2;          // 0x8
    unsigned int prim_cmd_parm3;          // 0xc
    unsigned int prim_cmd_parm4;          // 0x10
    unsigned int prim_cmd_parm5;          // 0x14
    unsigned int prim_cmd_parm6;          // 0x18
    unsigned int prim_cmd_parm7;          // 0x1c
    unsigned int prim_cmd_parm8;          // 0x20
    unsigned int prim_cmd_parm9;          // 0x24
    unsigned int prim_cmd_parm10;         // 0x28
    unsigned int prim_cmd_parm11;         // 0x2c
    unsigned int prim_cmd_parm12;         // 0x30
    unsigned int prim_cmd_parm13;         // 0x34
    unsigned int prim_cmd_parm14;         // 0x38
    unsigned int prim_cmd_parm15;         // 0x3c
    unsigned int secure_processor_cmd;    // 0x40
};

/*
+ * WTM register file for host communication
+ */
struct wtm_mail_box {
	unsigned int prim_cmd_parm0;          // 0x0
	unsigned int prim_cmd_parm1;          // 0x4
	unsigned int prim_cmd_parm2;          // 0x8
	unsigned int prim_cmd_parm3;          // 0xc
	unsigned int prim_cmd_parm4;          // 0x10
	unsigned int prim_cmd_parm5;          // 0x14
	unsigned int prim_cmd_parm6;          // 0x18
	unsigned int prim_cmd_parm7;          // 0x1c
	unsigned int prim_cmd_parm8;          // 0x20
	unsigned int prim_cmd_parm9;          // 0x24
	unsigned int prim_cmd_parm10;         // 0x28
	unsigned int prim_cmd_parm11;         // 0x2c
	unsigned int prim_cmd_parm12;         // 0x30
	unsigned int prim_cmd_parm13;         // 0x34
	unsigned int prim_cmd_parm14;         // 0x38
	unsigned int prim_cmd_parm15;         // 0x3c
	unsigned int secure_processor_cmd;    // 0x40
	unsigned char reserved_0x44[60];
	unsigned int cmd_return_status;       // 0x80
	unsigned int cmd_status_0;            // 0x84
	unsigned int cmd_status_1;            // 0x88
	unsigned int cmd_status_2;            // 0x8c
	unsigned int cmd_status_3;            // 0x90
	unsigned int cmd_status_4;            // 0x94
	unsigned int cmd_status_5;            // 0x98
	unsigned int cmd_status_6;            // 0x9c
	unsigned int cmd_status_7;            // 0xa0
	unsigned int cmd_status_8;            // 0xa4
	unsigned int cmd_status_9;            // 0xa8
	unsigned int cmd_status_10;           // 0xac
	unsigned int cmd_status_11;           // 0xb0
	unsigned int cmd_status_12;           // 0xb4
	unsigned int cmd_status_13;           // 0xb8
	unsigned int cmd_status_14;           // 0xbc
	unsigned int cmd_status_15;           // 0xc0
	unsigned int cmd_fifo_status;         // 0xc4
	unsigned int host_interrupt_register; // 0xc8
	unsigned int host_interrupt_mask;     // 0xcc
	unsigned int host_exception_address;  // 0xd0
	unsigned int sp_trust_register;       // 0xd4
	unsigned int wtm_identification;      // 0xd8
	unsigned int wtm_revision;            // 0xdc
};


#define WTM_BASE			0xD4290000
#define WTM_GET_SOC_STEPPING		0x1007
#define WTM_PRIM_CMD_COMPLETE_MASK	(1 << 0)
#define WTM_GET_SOC_POWER_POINT		0x1006
unsigned int mv_ack_from_wtm   = 0x0;
unsigned int mv_profile_adjust = 0x0;
unsigned int mv_profile	 = 0x0;
unsigned int mv_max_freq	 = 0x0;
unsigned int mv_ts_calibration = 0x0;

unsigned int mv_stepping	 = 0x0;
unsigned int mv_soc_stepping	 = 0x0;

static volatile struct wtm_mail_box *wtm_mb =
	(volatile struct wtm_mail_box *)(WTM_BASE);

static int wtm_exe_cmd(struct wtm_cmd *cmd)
{
	int i;

	unsigned int *pcmd = &cmd->prim_cmd_parm0;
	volatile unsigned int *phi = &wtm_mb->prim_cmd_parm0;

	for (i = 0; i <= 16; i++) {
		*phi++ = *pcmd++;
	}

	/* try 1000 times */
	for (i = 0; i < 10000; i++) {
		if (wtm_mb->host_interrupt_register &
		    WTM_PRIM_CMD_COMPLETE_MASK) {
			/* clean interrupt */
			wtm_mb->host_interrupt_register = 0xFFFFFFFF;
			return wtm_mb->cmd_return_status;
		}
	}

	/* read fail */
	return -1;
}


int wtm_read_profile(void)
{
	struct wtm_cmd cmd;
	int status;

	memset(&cmd, 0, sizeof(cmd));

	/* valid request */
	cmd.prim_cmd_parm0 = 0;
	cmd.secure_processor_cmd = WTM_GET_SOC_POWER_POINT;
	status = wtm_exe_cmd(&cmd);
	if (status < 0) {
		mv_ack_from_wtm = 0;
		goto out;
	}

	mv_ack_from_wtm   = 1;
	mv_profile_adjust = wtm_mb->cmd_status_0;
	mv_profile	    = wtm_mb->cmd_status_1;
	mv_max_freq	    = wtm_mb->cmd_status_2;
	mv_ts_calibration = wtm_mb->cmd_status_3;

out:
	return status;
}

static char *mv_stepping_string;
void wtm_read_stepping(void)
{
	struct wtm_cmd cmd;
	int status;

	unsigned int chip_id;
	unsigned int id;
	unsigned int soc_stepping;

	memset(&cmd, 0, sizeof(cmd));

	/* valid request */
	cmd.prim_cmd_parm0 = 0;
	cmd.secure_processor_cmd = WTM_GET_SOC_STEPPING;
	status = wtm_exe_cmd(&cmd);
	if (status < 0) {
		printf("wtm read steppping error: %d\n", status);
	}

	soc_stepping = wtm_mb->cmd_status_0;
	chip_id      = __raw_readl(0xd4282c00);
	mv_soc_stepping = soc_stepping;
	if ((chip_id & 0x00ff0000) == 0x00a00000) {

		if (soc_stepping == 0x4130) {
			mv_stepping = 0xa0;
			mv_stepping_string = "A0";
		}
		else if (soc_stepping == 0x4131) {
			mv_stepping = 0xa1;
			mv_stepping_string = "A1";
		}
		else if (soc_stepping == 0x4132) {
			mv_stepping = 0xa2;
			mv_stepping_string = "A2";
		}
	} else if ((chip_id & 0x00ff0000) == 0x00b00000) {
		if (soc_stepping == 0x4230) {
			mv_stepping = 0xb0;
			mv_stepping_string = "B0";
		} else if (soc_stepping == 0x423050) {
			mv_stepping_string = "B0P";
		} else
			printf("Unknow cpu stepping!\n");
	} else
		printf("Unknow cpu stepping! ");
}

void wtm_dump_info(void)
{
	printf("----- wtm info -----\n");

	if (!mv_ack_from_wtm)
		printf("wtm has NO ack.\n");
	else
		printf("get ack from wtm.\n");

	printf("profile_adjust = 0x%08x\n", mv_profile_adjust);
	printf("profile        = 0x%08x\n", mv_profile);
	printf("max_freq       = 0x%08x\n", mv_max_freq);
	printf("ts_calibration = 0x%08x\n", mv_ts_calibration);
	printf("stepping       = %s\n", mv_stepping_string);

	printf("-----   end    -----\n\n");

	return;
}
