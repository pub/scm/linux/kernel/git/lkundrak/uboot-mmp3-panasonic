/*
 * Copyright 2011, Marvell Semiconductor Inc.
 * Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 */
#include <common.h>

#include <command.h>
#include <environment.h>
#include <sparse_format.h>
#include <linux/stddef.h>
#include <malloc.h>//Add for malloc a buf space (nand to RAM)
#include <linux/string.h>//Add for cmp ram buf1 and nand_out_buf2

#define SPARSE_HEADER_MAJOR_VER 1
#define SECTOR_SIZE	512
#define READ_BUF_ADDR 0x20000000
#define RETRY_COUNT 20
#define UPDATE_FLAG_ADDR 0x30000000

int *update_flag= 0x30000000;       //flag to show if update fail 
//*update_flag = 0;

int unsparse(block_dev_desc_t *dev, uint64_t from, uint64_t to, uint64_t sz)
{
	sparse_header_t *header = (sparse_header_t *)from;
	u32 i;
    uint64_t outlen = 0;
        

	if ((header->total_blks * header->blk_sz) > sz) {
		printf("sparse: section size %d MB limit: exceeded\n",
				sz / (1024*1024));
		return 1;
	}

	if (header->magic != SPARSE_HEADER_MAGIC) {
		printf("sparse: bad magic\n");
		return 1;
	}

	if ((header->major_version != SPARSE_HEADER_MAJOR_VER) ||
			(header->file_hdr_sz != sizeof(sparse_header_t)) ||
			(header->chunk_hdr_sz != sizeof(chunk_header_t))) {
		printf("sparse: incompatible format\n");
		return 1;
	}
	/* todo: ensure image will fit */

	/* Skip the header now */
	from += header->file_hdr_sz;

	for (i=0; i < header->total_chunks; i++) {
		unsigned int len = 0;
		int r;
		int rd;//Add by Ares (flag to show if read from nandflash successfully)
		int read_buf = READ_BUF_ADDR;
		int ret;
		int retry_count = RETRY_COUNT;//Add by Ares retry_time
		chunk_header_t *chunk = (void*) from;

		printf(".");

		/* move to next chunk */
		from += sizeof(chunk_header_t);

		switch (chunk->chunk_type) {
		case CHUNK_TYPE_RAW:
			len = chunk->chunk_sz * header->blk_sz;

			if (chunk->total_sz != (len + sizeof(chunk_header_t))) {
				printf("sparse: bad chunk size for chunk %d, type Raw\n", i);
				return 1;
			}

			outlen += len;
			if (outlen > sz) {
				printf("sparse: section size %d MB limit: "
					"exceeded\n", sz /(1024*1024));
				return 1;
			}
#ifdef DEBUG
			printf("sparse: RAW blk=%d bsz=%d: write(sector=%d,len=%d)\n",
					chunk->chunk_sz, header->blk_sz, from, len);
#endif
			r = dev->block_write(dev->dev, to / SECTOR_SIZE,
				len / SECTOR_SIZE, (const void *)from);
				
			//++Add by Ares@Jul14:read and check,if not the same ,do retry
			rd = dev->block_read(dev->dev, to / SECTOR_SIZE,len / SECTOR_SIZE, read_buf);
				
			ret = memcmp(from,read_buf,len);
			while(ret&&retry_count!=0)
			{
				printf("chunk need rewrite...\n");
				r = dev->block_write(dev->dev, to / SECTOR_SIZE,len / SECTOR_SIZE, (const void *)from);
				rd = dev->block_read(dev->dev, to / SECTOR_SIZE,len / SECTOR_SIZE, read_buf);
				ret = memcmp(from,read_buf,len);
				retry_count--;
			}
			if(ret!=0)
			{
				printf("update and retry fail.ooo\n");
				return 1;
			}
			//--Add by Ares    
				
				
			if (r < 0) {
				printf("sparse: mmc write failed\n");
				return 1;
			}
			
			to += len;
			from += len;
			break;

		case CHUNK_TYPE_DONT_CARE:
			if (chunk->total_sz != sizeof(chunk_header_t)) {
				printf("sparse: bogus DONT CARE chunk\n");
				return 1;
			}
			len = chunk->chunk_sz * header->blk_sz;
#ifdef DEBUG
			printf("sparse: DONT_CARE blk=%d bsz=%d: skip(sector=%d,len=%d)\n",
					chunk->chunk_sz, header->blk_sz, to, len);
#endif

			outlen += len;
			if (outlen > sz) {
				printf("sparse: section size %d MB limit: "
						"exceeded\n", sz/(1024*1024));
				return 1;
			}
			to += len;
			break;

		default:
			printf("sparse: unknown chunk ID %04x\n", chunk->chunk_type);
			return 1;
		}
	}

	printf("\nsparse: out-length-0x%lld MB\n", outlen/(1024*1024));
	return 0;
}

static int do_unsparse(cmd_tbl_t *cmdtp, int flag,
               int argc, char * const argv[])
{
       uint64_t size;
       uint64_t addr;
       uint64_t offset;
       block_dev_desc_t *dev_desc = NULL;
       int dev = 0;
       char *ep;
	   *update_flag = 0;
	   int ret = 0;

       if (argc < 5) {
               printf("usage: unsparse <interface> <dev> "
                       "<ram addr> <block offset> <size>\n");
               *update_flag = 1;
               return 1;
       }

       dev = (int)simple_strtoul(argv[2], &ep, 16);
       dev_desc = get_dev(argv[1], dev);
       if (dev_desc == NULL) {
               puts("\n** Invalid boot device **\n");
               *update_flag = 1;
               return 1;
       }
       addr = simple_strtoull(argv[3], NULL, 16);
       offset = simple_strtoull(argv[4], NULL, 16);
       size = simple_strtoull(argv[5], NULL, 16);

       ret = unsparse(dev_desc, addr, offset, size);
       *update_flag = ret;
       return ret;
}

U_BOOT_CMD(
       unsparse,       6,      0,      do_unsparse,
       "write sparsed file into block device",
       "<interface> <dev> <ram addr> <block offset> <size>\n"
       "    - unsparse sparsed image from the address 'addr' in RAM\n"
       "      to 'dev' on 'interface'"
);
