/*
 * (C) Copyright 2011
 * Marvell Semiconductors Ltd. <www.marvell.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <mvmfp.h>
#include <i2c.h>
#include <asm/arch/mfp.h>
#include <asm/arch/cpu.h>
#include <asm/gpio.h>
#ifdef CONFIG_USB_ETHER
#include <asm/arch/usb.h>
#endif
#ifdef CONFIG_GENERIC_MMC
#include <sdhci.h>
#endif
DECLARE_GLOBAL_DATA_PTR;

/*
 * Basic support
 */
static u32 boardid;
static u8 boardrev, boardtype;
int board_early_init_f(void)
{
	u32 mfp_cfg[] = {
		/* Enable Console on UART3 */
		UART3_RXD,
		UART3_TXD,

		/* Enable TWSI5 */
		TWSI5_SCL,
		TWSI5_SDA,

		/* Enable TWSI6 */
		TWSI6_SCL,
		TWSI6_SDA,

		/* MMC1 */
		MMC1_DATA3,
		MMC1_DATA2,
		MMC1_DATA1,
		MMC1_DATA0,
		MMC1_CLK,
		MMC1_CMD,
		MMC1_CD,
		MMC1_WP,

		/* MMC3 */
		MMC3_DATA7_NDIO15,
		MMC3_DATA6_NDIO14,
		MMC3_DATA5_NDIO12,
		MMC3_DATA4_NDIO10,
		MMC3_DATA3_NDIO8,
		MMC3_DATA2_NDIO13,
		MMC3_DATA1_NDIO11,
		MMC3_DATA0_NDIO9,
		MMC3_CLK_SMNCS1,
		MMC3_CMD_SMNCS0,

		MFP_EOC		/*End of configureation*/
	};
	/* configure MFP's */
	mfp_config(mfp_cfg);

	return 0;
}

#define MACH_TYPE_YELLOWSTONE	3495
#define FASTBOOT_KEY	20
int board_init(void)
{
	/* OEM UniqueID in the NTIM
	 * 32bit format: NN NN NN TV
	 * NNNNNN: board name
	 *	0x594553 (YES, YellowStone)
	 *	0x414249 (ABI, Abilene)
	 * T: type
	 *	0x1 (Pop board)
	 *	0x0 (Discrete board)
	 * V: revision
	*/
	boardid = *(volatile unsigned int *)0xd1020010;
	boardrev = boardid & 0xF;
	boardtype = (boardid >> 4) & 0xF;
	boardid = boardid >> 8;

	if (boardid == 0x594553)
		gd->bd->bi_arch_number = MACH_TYPE_YELLOWSTONE;

	gd->bd->bi_boot_params = CONFIG_SYS_TEXT_BASE + 0x3c00;

	gpio_direction_input(FASTBOOT_KEY);

	return 0;
}

extern int pmic_init(void);
int misc_init_r(void)
{
	int i, sz;

	if (boardid == 0x594553)
		printf("\nBoard: YellowStone (%s)\n",
			boardtype ? "Pop" : "Discrete");

#if defined(CONFIG_MMP_POWER)
	set_volt(1230);	/* set 1.23v for vcc_core */
	setop(6); 	/* set op for 800MHz */
#endif

	setenv("fbenv", "mmc0");

	/* init pmic: BUCKS, LDOs, watch dog... */
	pmic_init();

	/* set bootargs */
	setenv("bootargs", CONFIG_BOOTARGS);
	/* reserve 192M pmem if the memory is larger than 512M
	 * reserve 96M otherwise */
	for (i = 0, sz = 0; i < CONFIG_NR_DRAM_BANKS; i++)
		sz += gd->bd->bi_dram[i].size;
	if (sz > 0x20000000)
		run_command("setenv bootargs ${bootargs} reserve_pmem=0xc000000", 0);
	else
		run_command("setenv bootargs ${bootargs} reserve_pmem=0xA000000", 0);

	/* If Volumn up key is pressed, launch fastboot */
	if (!gpio_get_value(FASTBOOT_KEY))
		run_command("fb", 0);

	return 0;
}

#ifdef CONFIG_GENERIC_MMC
int board_mmc_init(bd_t *bd)
{
	ulong mmc_base_address[CONFIG_SYS_MMC_NUM] = CONFIG_SYS_MMC_BASE;
	u8 i;

	for (i = 0; i < CONFIG_SYS_MMC_NUM; i++) {
		if (mv_sdh_init(mmc_base_address[i], 0, 0,
				SDHCI_QUIRK_32BIT_DMA_ADDR))
			return 1;
		writel(DIS_PAD_SD_CLK_GATE | CLK_GATE_ON | CLK_GATE_CTL \
		       | WTC(WTC_DEF) | RTC(RTC_DEF),
		       mmc_base_address[i] + SD_FIFO_PARAM);

		writew(SDCLK_DELAY(0x1f) | SDCLK_SEL | WR_ENDIAN | RD_ENDIAN \
		       | DMA_SIZE(DMA_FIFO_128) | BURST_SIZE(BURST_64),
		       mmc_base_address[i] + SD_CLOCK_AND_BURST_SIZE_SETUP);
	}
	return 0;
}
#endif

void reset_cpu(ulong ignored)
{
	unsigned int cur_bus = i2c_get_bus_num();
	u8 addr = 0x30, reg, data;

	i2c_set_bus_num(0);
	/* 1.Enable FAULT_WU and FAULT_WU_EN */
	reg = 0xE7;
	i2c_read(addr, reg, 1, &data, 1);
	data |= ((1 << 3) | (1 << 2));
	i2c_write(addr, reg, 1, &data, 1);
	/* 2.Issue SW power down */
	reg = 0x0D;
	data = 0x20;
	i2c_write(addr, reg, 1, &data,  1);
	i2c_set_bus_num(cur_bus);
}

/*
 * USB Ethernet
 */
#ifdef CONFIG_USB_ETHER
int usb_lowlevel_init(void)
{
	struct usb_file *file = (struct usb_file *)CONFIG_USB_PHY_BASE;
	int count;

	if (cpu_is_ax()) {
		writel(readl(&file->pll_reg0) & ~(FB_DIV_MASK | REFDIV_MASK),
		       &file->pll_reg0);
		writel(readl(&file->pll_reg0) | (VDD18(1) | VDD12(1) | REFDIV(0xd)
		       | FB_DIV(0xf0)), &file->pll_reg0);
	} else if (cpu_is_bx()){
		writel(readl(&file->pll_reg0) & ~(FB_DIV_MASK_B0 | REFDIV_MASK_B0),
		       &file->pll_reg0);
		writel(readl(&file->pll_reg0) | (VDD18(1) | REFDIV_B0(0xd)
		       | FB_DIV_B0(0xf0)), &file->pll_reg0);
	} else {
		printf("%s: Unknown CPU type.\n", __func__);
		return -1;
	}
	writel(readl(&file->pll_reg1) & ~(UTMI_PLL_PU | PLL_ICP_MASK
	       | PLL_KVCO_MASK | PLL_CALI12_MASK), &file->pll_reg1);
	writel(readl(&file->pll_reg1) | (UTMI_PLL_PU | PLL_ICP(2)
	       | PLL_KVCO(3) | PLL_CALI12(3)), &file->pll_reg1);


	writel(readl(&file->tx_reg0) & ~IMPCAL_VTH_MASK, &file->tx_reg0);
	writel(readl(&file->tx_reg0) | IMPCAL_VTH(2), &file->tx_reg0);

	writel(readl(&file->tx_reg1) & ~(CK60_PHSEL_MASK | AMP_MASK
	       | TX_VDD12_MASK), &file->tx_reg1);
	writel(readl(&file->tx_reg1) | (CK60_PHSEL(4) | AMP(4)
	       | TX_VDD12(3)), &file->tx_reg1);

	writel(readl(&file->tx_reg2) & ~DRV_SLEWRATE(3), &file->tx_reg2);
	writel(readl(&file->tx_reg2) | DRV_SLEWRATE(3), &file->tx_reg2);

	writel(readl(&file->rx_reg0) & ~(SQ_LENGTH_MASK | SQ_THRESH_MASK),
	       &file->rx_reg0);
	writel(readl(&file->rx_reg0) | (SQ_LENGTH(0x2) | SQ_THRESH(0xa)),
	       &file->rx_reg0);

	writel(readl(&file->ana_reg1) | ANA_PU, &file->ana_reg1);

	writel(readl(&file->otg_reg0) | PU_OTG, &file->otg_reg0);

	udelay(200);
	writel(readl(&file->pll_reg1) | VCOCAL_START, &file->pll_reg1);

	udelay(200);
	writel(readl(&file->tx_reg0) | RCAL_START, &file->tx_reg0);
	udelay(40);
	writel(readl(&file->tx_reg0) & ~RCAL_START, &file->tx_reg0);
	udelay(400);

	/* make sure phy is ready */
	count = 100;
	while(((readl(&file->pll_reg1) & PLL_READY)==0) && count--)
		udelay(1000);
	if (count <= 0) {
		printf("%s %d: calibrate timeout, UTMI_PLL %x\n",
				__func__, __LINE__, readl(&file->pll_reg1));
		return -1;
	}

	return 0;
}
#endif

#ifdef CONFIG_CMD_NET
int board_eth_init(bd_t *bis)
{
	int res = -1;

#if defined(CONFIG_MV_UDC)
	if (usb_eth_initialize(bis) >= 0)
		res = 0;
#endif
	return res;
}
#endif

