/*
 * (C) Copyright 2011
 * Marvell Semiconductors Ltd. <www.marvell.com>
 * Lei Wen <leiwen@marvell.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <mvmfp.h>
#include <i2c.h>
#include <asm/arch/mfp.h>
#include <asm/arch/cpu.h>
#include <asm/gpio.h>
#ifdef CONFIG_USB_ETHER
#include <asm/arch/usb.h>
#endif
#ifdef CONFIG_GENERIC_MMC
#include <sdhci.h>
#endif
#if defined(CONFIG_PXA168_FB)
#include <pxa168fb.h>
#include <video_fb.h>
#if defined(CONFIG_HDMI)
#include <hdmi.h>
#endif
#include "../common/marvell.h"
#endif
#ifdef CONFIG_ARMADA100_FEC
#include <net.h>
#include <netdev.h>
#include <miiphy.h>
#endif /* CONFIG_ARMADA100_FEC */
#include <mmp_freq.h>
DECLARE_GLOBAL_DATA_PTR;

#define MAX8925_SLAVE_ADDR		0x3c
#define MAX77601_SLAVE_ADDR 		0x1c

#define MAX77601_VDVSSD0_REG 		0x1B
#define MAX77601_CNFG1_L1_REG		0x25
#define MAX77601_CNFG1_L7_REG		0x31
#define MAX77601_CNFG_GPIO5		0x3B
#define MAX77601_AME_GPIO_REG 		0x40
#define MAX77601_FPS_L1_REG 		0x47
#define MAX77601_FPS_L7_REG 		0x4D

#define MAX77601_FPSSRC_NOTFPS		(0x3 << 6)
#define MAX77601_MODE_NORMAL		(0x3 << 6)
#define MAX77601_L1_1P2V		0x10
#define MAX77601_L7_1P2V		0x08
#define MAX77601_VSD3_REG		0x19
#define MAX77601_SD0_1P25V		0x34
#define MAX77601_SD3_2P8V		0xB0

#define MAX77601_ONOFFCNFG2		0x42
#define MAX77601_ONOFFCNFG1		0x41
#define MAX77601_SFT_RST_WK		0x80
#define MAX77601_SFT_RST		0x80

#if defined(CONFIG_PXA168_FB)
static struct dsi_info abilene_dsi = {
	.id			= 1,
	.lanes			= 2,
	.bpp			= 24,
	.burst_mode		= DSI_BURST_MODE_SYNC_EVENT,
	.hbp_en			= 1,
	.hfp_en			= 1,
};

static struct fb_videomode video_modes[] = {
	[0] = {
		.pixclock	= 62500,
		.refresh	= 60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 2,

		.left_margin	= 10,
		.right_margin	= 116,
		.vsync_len	= 2,
		.upper_margin	= 10,
		.lower_margin	= 4,
		.sync		= FB_SYNC_VERT_HIGH_ACT \
				  | FB_SYNC_HOR_HIGH_ACT,
	},
};

static struct pxa168fb_mach_info mmp2_mipi_lcd_info = {
	.id			= "GFX Layer",
	.sclk_src		= 500000000,
	.sclk_div		= 0xe0001210,
	.num_modes		= ARRAY_SIZE(video_modes),
	.modes			= video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.burst_len		= 16,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the panel is hard connected with lcd panel path
	 * and dsi1 output
	 */
	.panel_rgb_reverse_lanes = 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.active			= 1,
	.enable_lcd             = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.max_fb_size		= FB_XRES * FB_YRES * 8 + 4096,
	.phy_type		= DSI2DPI,
	.phy_info		= &abilene_dsi,
	.twsi_id		= 4,
};
#if defined(CONFIG_HDMI)
static struct pxa168fb_mach_info mmp2_hdmi_info = {
	.id			= "GFX Layer",
	.index			= 1,
	.sclk_src		= 500000000,
	.sclk_div		= 0xe0001210,
	.num_modes		= ARRAY_SIZE(video_modes),
	.modes			= &cea_modes[4],
	.pix_fmt		= PIX_FMT_RGB565,
	.burst_len		= 16,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the panel is hard connected with lcd panel path
	 * and dsi1 output
	 */
	.panel_rgb_reverse_lanes = 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.active			= 1,
	.enable_lcd             = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.max_fb_size		= FB_XRES * FB_YRES * 8 + 4096,
	.phy_type		= DPI,
};
#endif
#endif

#if defined(CONFIG_PXA168_FB)
static GraphicDevice ctfb;
void *lcd_init(void)
{
	void *ret;

	ret = (void *)pxa168fb_init(&mmp2_mipi_lcd_info);
#if defined(CONFIG_HDMI)
	pxa168fb_init(&mmp2_hdmi_info);
#endif

	return ret;
}

void *video_hw_init(void)
{
	struct pxa168fb_info *fbi;
	struct fb_var_screeninfo *var;
	unsigned long t1, hsynch, vsynch;
	fbi = lcd_init();
	var = fbi->var;

	ctfb.winSizeX = var->xres;
	ctfb.winSizeY = var->yres;

	/* calculate hsynch and vsynch freq (info only) */
	t1 = (var->left_margin + var->xres +
	      var->right_margin + var->hsync_len) / 8;
	t1 *= 8;
	t1 *= var->pixclock;
	t1 /= 1000;
	hsynch = 1000000000L / t1;
	t1 *= (var->upper_margin + var->yres +
	       var->lower_margin + var->vsync_len);
	vsynch = 1000000000L / t1;

	/* fill in Graphic device struct */
	sprintf(ctfb.modeIdent, "%dx%dx%d %ldkHz %ldHz", ctfb.winSizeX,
		ctfb.winSizeY, var->bits_per_pixel, (hsynch / 1000),
		vsynch);

	ctfb.frameAdrs = (unsigned int) fbi->fb_start;
	ctfb.plnSizeX = ctfb.winSizeX;
	ctfb.plnSizeY = ctfb.winSizeY;

	ctfb.gdfBytesPP = 2;
	ctfb.gdfIndex = GDF_16BIT_565RGB;

	ctfb.isaBase = 0x9000000;
	ctfb.pciBase = (unsigned int) fbi->fb_start;
	ctfb.memSize = fbi->fb_size;

	/* Cursor Start Address */
	ctfb.dprBase = (unsigned int) fbi->fb_start + (ctfb.winSizeX \
			* ctfb.winSizeY * ctfb.gdfBytesPP);
	if ((ctfb.dprBase & 0x0fff) != 0) {
		/* allign it */
		ctfb.dprBase &= 0xfffff000;
		ctfb.dprBase += 0x00001000;
	}
	ctfb.vprBase = (unsigned int) fbi->fb_start;
	ctfb.cprBase = (unsigned int) fbi->fb_start;

	return &ctfb;
}
#endif

static u32 boardid;
static u8 boardrev, boardtype;
int board_early_init_f(void)
{
	u32 mfp_cfg[] = {
		/* Enable Console on UART3 */
		UART3_RXD,
		UART3_TXD,

		/* Enable TWSI1 for PMIC */
		TWSI1_SCL,
		TWSI1_SDA,

		/* Enable TWSI5 */
		TWSI5_SCL,
		TWSI5_SDA,

		/* Enable TWSI6 */
		TWSI6_SCL,
		TWSI6_SDA,

		/* Enable LCD */
		LCD_RESET,

		/* MMC1 */
		MMC1_DATA3,
		MMC1_DATA2,
		MMC1_DATA1,
		MMC1_DATA0,
		MMC1_CLK,
		MMC1_CMD,
		MMC1_CD,
		MMC1_WP,
		MMC1_GPIO13, /* EN_3P3V_MMC_N */

		/* MMC3 */
		MMC3_DATA7,
		MMC3_DATA6,
		MMC3_DATA5,
		MMC3_DATA4,
		MMC3_DATA3,
		MMC3_DATA2,
		MMC3_DATA1,
		MMC3_DATA0,
		MMC3_CLK,
		MMC3_CMD,

		/* Fast Ethernet */
#if defined(CONFIG_ARMADA100_FEC)
		FE_LED0_N,
		FE_LED1_N,
		FE_LED2_N,
#endif

		MFP_EOC		/*End of configureation*/
	};
	/* configure MFP's */
	mfp_config(mfp_cfg);

	return 0;
}

#define LDOCTL17	0x14
#define LDO17VOUT	0x16
#define LDOCTL3		0x20
#define LDO3VOUT	0x22
#define FASTBOOT_KEY	20
int board_init(void)
{
	u8 data;

#if defined(CONFIG_PXA168_FB)
	/* TC358765 reset */
	gpio_direction_output(LCD_RST_GPIO, 0);
	udelay(100000);
#endif

	i2c_set_bus_num(0);

	/* set ame for gpio 4/5/7 */
	data = 0xb0;
	i2c_write(MAX77601_SLAVE_ADDR, MAX77601_AME_GPIO_REG, 1, &data, 1);

	/* Set GPIO5 active low, VCXO_EN is low when suspend */
	i2c_read(MAX77601_SLAVE_ADDR, MAX77601_CNFG_GPIO5, 1, &data, 1);
	data &= ~0x1;
	i2c_write(MAX77601_SLAVE_ADDR, MAX77601_CNFG_GPIO5, 1, &data, 1);

	/* Enable 1.2V pmic_1p2v_mipi (Max77601 LDO1) at first */
	/* not configured as part of a flexible power sequence */
	i2c_read(MAX77601_SLAVE_ADDR, MAX77601_FPS_L1_REG, 1, &data, 1);
	data |= MAX77601_FPSSRC_NOTFPS;
	i2c_write(MAX77601_SLAVE_ADDR, MAX77601_FPS_L1_REG, 1, &data, 1);

	data = MAX77601_MODE_NORMAL | MAX77601_L1_1P2V;
	i2c_write(MAX77601_SLAVE_ADDR, MAX77601_CNFG1_L1_REG, 1, &data, 1);

	/* Then enable 1.2V pmic_1p2v_mipi_logic (Max77601 LDO7)  */
	i2c_read(MAX77601_SLAVE_ADDR, MAX77601_FPS_L7_REG, 1, &data, 1);
	data |= MAX77601_FPSSRC_NOTFPS;
	i2c_write(MAX77601_SLAVE_ADDR, MAX77601_FPS_L7_REG, 1, &data, 1);

	data = MAX77601_MODE_NORMAL | MAX77601_L7_1P2V;
	i2c_write(MAX77601_SLAVE_ADDR, MAX77601_CNFG1_L7_REG, 1, &data, 1);

	gpio_direction_input(FASTBOOT_KEY);

	#define GPIO_EN_3P3V_MMC_N	13
	gpio_direction_output(GPIO_EN_3P3V_MMC_N, 0);

	/* OEM UniqueID in the NTIM
	 * 32bit format: NN NN NN TV
	 * NNNNNN: board name
	 *	0x594553 (YES, YellowStone)
	 *	0x414249 (ABI, Abilene)
	 * T: type
	 *	0x1 (Pop board)
	 *	0x0 (Discrete board)
	 * V: revision
	*/
	boardid = *(volatile unsigned int *)0xd1020010;
	boardrev = boardid & 0xF;
	boardtype = (boardid >> 4) & 0xF;
	boardid = boardid >> 8;

	if (boardid == 0x414249)
		gd->bd->bi_arch_number = MACH_TYPE_ABILENE;

	gd->bd->bi_boot_params = CONFIG_SYS_TEXT_BASE + 0x3c00;

	return 0;
}

#if defined(CONFIG_PXA168_FB)
void show_logo(void)
{
	char cmd[100];
	int wide, high;

	/* Show marvell logo */
	wide = 213;
	high = 125;
	sprintf(cmd, "bmp display %p %d %d", MARVELL, \
		(FB_XRES - wide) / 2, (FB_YRES - high) / 2);
	run_command(cmd, 0);
}
#endif

int misc_init_r(void)
{
	int i;
	ulong sz;
#if defined(CONFIG_PXA168_FB)
	show_logo();
#endif
	if (boardid == 0x414249)
		printf("\nBoard: Abilene (%s)\n",
			boardtype ? "Pop" : "Discrete");

#if defined(CONFIG_MMP_POWER)
	set_volt(1250);	/* set 1.25v for vcc_core */
	setop(18); 	/* set op for 1GHz */
#endif

	setenv("fbenv", "mmc0");

	/* If Volumn down key is pressed, launch fastboot */
	if (!gpio_get_value(FASTBOOT_KEY))
		run_command("fb", 0);

	for (i = 0, sz = 0; i < CONFIG_NR_DRAM_BANKS; i++)
		sz += gd->bd->bi_dram[i].size;

	setenv("bootargs", "rdinit=/busybox/rdinit androidboot.console=ttyS2 console=ttyS2,115200 emmc_boot fb_share");
	if (sz > 0x20000000)
		run_command("setenv bootargs ${bootargs} reserve_pmem=0xc000000", 0);
	else
		run_command("setenv bootargs ${bootargs} reserve_pmem=0xA000000", 0);

	return 0;
}

#ifdef CONFIG_GENERIC_MMC
#define I2CEN		0x7
#define LDOSEQ(x)	((x & 0x7) << 2)
#define LDO_DC		0x2
#define LDO_EN		0x1
#define LDOCTL_11	0x40
#define LDO_3V		0x2d
#define LDOVOUT_11	0x42
int board_mmc_init(bd_t *bd)
{
	ulong mmc_base_address[CONFIG_SYS_MMC_NUM] = CONFIG_SYS_MMC_BASE;
	u8 i, data;
	i2c_set_bus_num(0);

	/*
	 * Set 2.8V power (pmic_sdmmc - max77601_SD3) for sd/mmc
	 * The power domain is shared with other components' power
	 * the voltage should be 2.8V on B0, will default enabled when
	 * booting up
	 */
	data = MAX77601_SD3_2P8V;
	i2c_write(MAX77601_SLAVE_ADDR, MAX77601_VSD3_REG, 1, &data, 1);

	for (i = 0; i < CONFIG_SYS_MMC_NUM; i++) {
		if (mv_sdh_init(mmc_base_address[i], 0, 0,
				SDHCI_QUIRK_32BIT_DMA_ADDR))
			return 1;
		writel(DIS_PAD_SD_CLK_GATE | CLK_GATE_ON | CLK_GATE_CTL \
		       | WTC(WTC_DEF) | RTC(RTC_DEF),
		       mmc_base_address[i] + SD_FIFO_PARAM);
	}
	writew(SDCLK_DELAY(0x1f) | SDCLK_SEL | WR_ENDIAN | RD_ENDIAN \
	       | DMA_SIZE(DMA_FIFO_128) | BURST_SIZE(BURST_64),
	       mmc_base_address[1] + SD_CLOCK_AND_BURST_SIZE_SETUP);
	return 0;
}
#endif

#ifdef CONFIG_USB_ETHER
int usb_lowlevel_init(void)
{
	struct usb_file *file = (struct usb_file *)CONFIG_USB_PHY_BASE;
	int count;

	if (cpu_is_ax()) {
		writel(readl(&file->pll_reg0) & ~(FB_DIV_MASK | REFDIV_MASK),
		       &file->pll_reg0);
		writel(readl(&file->pll_reg0) | (VDD18(1) | VDD12(1) | REFDIV(0xd)
		       | FB_DIV(0xf0)), &file->pll_reg0);
	} else {
		writel(readl(&file->pll_reg0) & ~(FB_DIV_MASK_B0 | REFDIV_MASK_B0),
		       &file->pll_reg0);
		writel(readl(&file->pll_reg0) | (VDD18(1) | REFDIV_B0(0xd)
		       | FB_DIV_B0(0xf0)), &file->pll_reg0);
	}

	writel(readl(&file->pll_reg1) & ~(UTMI_PLL_PU | PLL_ICP_MASK
	       | PLL_KVCO_MASK | PLL_CALI12_MASK), &file->pll_reg1);
	writel(readl(&file->pll_reg1) | (UTMI_PLL_PU | PLL_ICP(2)
	       | PLL_KVCO(3) | PLL_CALI12(3)), &file->pll_reg1);


	writel(readl(&file->tx_reg0) & ~IMPCAL_VTH_MASK, &file->tx_reg0);
	writel(readl(&file->tx_reg0) | IMPCAL_VTH(2), &file->tx_reg0);

	writel(readl(&file->tx_reg1) & ~(CK60_PHSEL_MASK | AMP_MASK
	       | TX_VDD12_MASK), &file->tx_reg1);
	writel(readl(&file->tx_reg1) | (CK60_PHSEL(4) | AMP(4)
	       | TX_VDD12(3)), &file->tx_reg1);

	writel(readl(&file->tx_reg2) & ~DRV_SLEWRATE(3), &file->tx_reg2);
	writel(readl(&file->tx_reg2) | DRV_SLEWRATE(3), &file->tx_reg2);

	writel(readl(&file->rx_reg0) & ~(SQ_LENGTH_MASK | SQ_THRESH_MASK),
	       &file->rx_reg0);
	writel(readl(&file->rx_reg0) | (SQ_LENGTH(0x2) | SQ_THRESH(0xa)),
	       &file->rx_reg0);

	writel(readl(&file->ana_reg1) | ANA_PU, &file->ana_reg1);

	writel(readl(&file->otg_reg0) | PU_OTG, &file->otg_reg0);

	udelay(200);
	writel(readl(&file->pll_reg1) | VCOCAL_START, &file->pll_reg1);

	udelay(200);
	writel(readl(&file->tx_reg0) | RCAL_START, &file->tx_reg0);
	udelay(40);
	writel(readl(&file->tx_reg0) & ~RCAL_START, &file->tx_reg0);
	udelay(400);

	/* make sure phy is ready */
	count = 100;
	while(((readl(&file->pll_reg1) & PLL_READY)==0) && count--)
		udelay(1000);
	if (count <= 0) {
		printf("%s %d: calibrate timeout, UTMI_PLL %x\n",
				__func__, __LINE__, readl(&file->pll_reg1));
		return -1;
	}

	return 0;
}
#endif

#ifdef CONFIG_CMD_NET
int board_eth_init(bd_t *bis)
{
	int res = -1;

#if defined(CONFIG_ARMADA100_FEC)
	res = armada100_fec_register(AMAD6_FEC_BASE);
#endif
#if defined(CONFIG_MV_UDC)
	if (usb_eth_initialize(bis) >= 0)
		res = 0;
#endif
	return res;
}
#endif

#ifdef CONFIG_ARMADA100_FEC
void fe_phy_regs_set(char *name, int phy_adr)
{
	/*
         * MMP3_A0 Fast Ethernet PHY register settings which is to
         * adjust to basic mode voltage and auto-negotiation
         */
        miiphy_write(name, phy_adr, 0x10, 0x138);
        miiphy_write(name, phy_adr, 0x1D, 4);
        miiphy_write(name, phy_adr, 0x1E, 0x51C);
        miiphy_write(name, phy_adr, 0x1D, 9);
        miiphy_write(name, phy_adr, 0x1E, 0x2081);
        miiphy_write(name, phy_adr, 0x1C, 0xC03);
}
#endif

/*
 * Reset the cpu by set pmic register
 */
void reset_cpu(ulong ignored)
{
	u8 data;
	i2c_set_bus_num(0);

	i2c_read(MAX77601_SLAVE_ADDR, MAX77601_ONOFFCNFG2, 1, &data, 1);
	data |= MAX77601_SFT_RST_WK;
	i2c_write(MAX77601_SLAVE_ADDR, MAX77601_ONOFFCNFG2, 1, &data, 1);
	i2c_read(MAX77601_SLAVE_ADDR, MAX77601_ONOFFCNFG1, 1, &data, 1);
	data |= MAX77601_SFT_RST;
	i2c_write(MAX77601_SLAVE_ADDR, MAX77601_ONOFFCNFG1, 1, &data, 1);
	udelay(10*1000);

	while (1);
}

#ifdef CONFIG_DISPLAY_BOARDINFO
extern u32 smp_hw_cpuid(void);
extern u32 smp_config(void);
int checkboard(void)
{
	u32 val;

	val = smp_hw_cpuid();
	printf("Boot Core: %s\n"
			, ((val == 0)
			? ("MP1")
			: ((val == 2)
				? ("MM")
				: ((val == 1)
					? ("MP1")
					: ("UNKNOWN")))));
	val = smp_config();
	printf("Available Cores: %s %s %s\n"
			, (val & 0x1) ? ("MP1") : ("")
			, (val & 0x2) ? ("MP2") : ("")
			, (val & 0x4) ? ("MM") : ("")
	);

	printf("DRAM interleave size: 0x%08x\n", dram_interleave_size());

	return 0;
}
#endif
