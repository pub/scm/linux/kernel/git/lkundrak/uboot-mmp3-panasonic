/*
 * (C) Copyright 2011
 * Marvell Semiconductors Ltd. <www.marvell.com>
 * Lei Wen <leiwen@marvell.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <mvmfp.h>
#include <asm/arch/mfp.h>
#include <asm/arch/cpu.h>
#ifdef CONFIG_USB_ETHER
#include <asm/arch/usb.h>
#endif
#ifdef CONFIG_GENERIC_MMC
#include <sdhci.h>
#endif

DECLARE_GLOBAL_DATA_PTR;

int board_early_init_f(void)
{
	u32 mfp_cfg[] = {
		/* Enable Console on UART3 */
		UART3_RXD,
		UART3_TXD,

		/* MMC1 */
		MMC1_DATA3,
		MMC1_DATA2,
		MMC1_DATA1,
		MMC1_DATA0,
		MMC1_CLK,
		MMC1_CMD,
		MMC1_CD,
		MMC1_WP,

		/* MMC3 */
		MMC3_DATA7,
		MMC3_DATA6,
		MMC3_DATA5,
		MMC3_DATA4,
		MMC3_DATA3,
		MMC3_DATA2,
		MMC3_DATA1,
		MMC3_DATA0,
		MMC3_CLK,
		MMC3_CMD,

		MFP_EOC		/*End of configureation*/
	};
	/* configure MFP's */
	mfp_config(mfp_cfg);
	return 0;
}

int board_init(void)
{
	/* arch number of FPGA Board */
	gd->bd->bi_arch_number = MACH_TYPE_G50;
	gd->bd->bi_boot_params = 0x3c00;

	return 0;
}

int misc_init_r(void)
{
	setenv("fbenv", "mmc1");

	return 0;
}

#ifdef CONFIG_GENERIC_MMC
int board_mmc_init(bd_t *bd)
{
	ulong mmc_base_address[CONFIG_SYS_MMC_NUM] = CONFIG_SYS_MMC_BASE;
	u8 i;

	for (i = 0; i < CONFIG_SYS_MMC_NUM; i++) {
		if (mv_sdh_init(mmc_base_address[i], 0, 0,
				SDHCI_QUIRK_32BIT_DMA_ADDR))
			return 1;
	}
	return 0;
}
#endif

#ifdef CONFIG_USB_ETHER
int usb_lowlevel_init(void)
{
	struct usb_file *file = (struct usb_file *)CONFIG_USB_PHY_BASE;
	int count = 0;
	int reg;

	/* initialize the usb phy power */
	writel(readl(&file->ctrl_reg) | POWER_UP | PLL_POWER_UP |
			PU_REF | USB_CTL_29_28(1), &file->ctrl_reg);
	/* UTMI_PLL settings */
	writel(readl(&file->pll_reg) & ~(REFDIV_MASK | FBDIV_MASK | ICP_MASK |
				PLLVDD12_MASK | PLLVDD18_MASK |
				PLLCALLI12_MASK), &file->pll_reg);
	writel(readl(&file->pll_reg) | REFDIV(13) | FBDIV(240) | ICP(10) |
			PLL_READY | PLLVDD12(3) | PLLVDD18(3) | PLLCALLI12(3),
			&file->pll_reg);
	/* UTMI_TX */
	writel(readl(&file->tx_reg) & ~(IMPCAL_VTH_MASK | CK60_PHSEL_Mask |
				TXVDD12_MASK), &file->tx_reg);
	writel(readl(&file->tx_reg) | IMPCAL_VTH(5) | CK60_PHSEL(4) |
			TXVDD12(3) | TXDATA_BLOCK_EN , &file->tx_reg);
	/* calibrate */
	count = 10000;
	while (((readl(&file->pll_reg) & PLL_READY) == 0) && count--)
		;
	if (count <= 0)
		printf("Calibrate timeout, UTMI_PLL:%x\n",
				readl(&file->pll_reg));

	/* toggle VCOCAL_START bit of UTMI_PLL */
	udelay(200);
	reg = readl(&file->pll_reg);
	writel(reg | VCOCAL_START, &file->pll_reg);
	udelay(40);
	writel(reg & (~VCOCAL_START), &file->pll_reg);

	/* toggle REG_RCAL_START bit of UTMI_TX */
	udelay(200);
	reg = readl(&file->tx_reg);
	writel(reg | REG_RCAL_START, &file->tx_reg);
	udelay(40);
	writel(reg & (~REG_RCAL_START), &file->tx_reg);
	udelay(200);

	/* make sure phy is ready */
	count = 1000;
	while (((readl(&file->pll_reg) & PLL_READY) == 0) && count--)
		;
	if (count <= 0)
		printf("Calibrate timeout, UTMI_PLL %x\n",
				readl(&file->pll_reg));

	return 0;
}
#endif

#ifdef CONFIG_CMD_NET
int board_eth_init(bd_t *bis)
{
	int res = -1;

#if defined(CONFIG_MV_UDC)
	if (usb_eth_initialize(bis) >= 0)
		res = 0;
#endif
	return res;
}
#endif
#define GPIO10		10
void reset_cpu(ulong ignored)
{
	gpio_direction_output(GPIO10, 1);
	while (1)
		;
}
