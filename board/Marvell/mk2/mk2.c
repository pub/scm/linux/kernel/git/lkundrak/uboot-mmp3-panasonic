/*
 * (C) Copyright 2011
 * Marvell Semiconductors Ltd. <www.marvell.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <mvmfp.h>
#include <i2c.h>
#include <asm/arch/mfp.h>
#include <asm/arch/cpu.h>
#include <asm/gpio.h>
#ifdef CONFIG_USB_ETHER
#include <asm/arch/usb.h>
#endif
#ifdef CONFIG_GENERIC_MMC
#include <sdhci.h>
#endif


#ifdef CONFIG_MV_RECOVERY
#include <mv_recovery.h>
#define RECOVERY_KEY	150
#define RTC_CLK_REG		0xd4015000
#endif
#include <malloc.h>

#include <timestamp.h>

#include "../../../../../kernel/kernel/arch/arm/mach-mmp/include/mach/wistron.h"

#if defined(CONFIG_PXA168_FB)
#include <pxa168fb.h>
#include <video_fb.h>
#include "../common/marvell.h"
#endif

//for mk2
#define GPIO_V_SD_EN	 	138
#define GPIO_V_HDMI5V_EN 	160
#define GPIO_BACKLIGHT_EN	17
#define GPIO_LCD_3V3		152
#define GPIO_53_PWM3		53
#define GPIO_64_GPIO		64
#define GPIO_68_GPIO		68
#define GPIO_84_GPIO		84
#define GPIO_85_GPIO		85
#define GPIO_19_GPIO		19


DECLARE_GLOBAL_DATA_PTR;

#define MK_FB_XRES		1024
#define MK_FB_YRES		768
#define PWM3_BASE		0xD401A800
#define APB_CLK_REG_BASE	0xD4015000

#define APBC_PWM3_CLK_RST	(APB_CLK_REG_BASE + 0x44)
#define PWM_CR3		(PWM3_BASE + 0x00)
#define PWM_DCR		(PWM3_BASE + 0x04)
#define PWM_PCR		(PWM3_BASE + 0x08)

#if defined(CONFIG_PXA168_FB)
static struct dsi_info mk2_dsi = {
	.id = 1,
	.lanes = 2,
	.bpp = 24,
	.burst_mode = DSI_BURST_MODE_SYNC_EVENT,
	.hbp_en = 1,
	.hfp_en = 1,
};

static struct lvds_info lvdsinfo = {
	.src	= LVDS_SRC_PN,
	.fmt	= LVDS_FMT_18BIT,
};

static struct fb_videomode video_modes[] = {
	[0] = {
		/* FIXME */
		.pixclock = 67414,
		.refresh = 60,
		.xres = MK_FB_XRES,
		.yres = MK_FB_YRES,
		.hsync_len = 10,
		.left_margin = 160,
		.right_margin = 200,
		.vsync_len = 2,
		.upper_margin = 18,
		.lower_margin = 18,
		.sync = FB_SYNC_VERT_HIGH_ACT \
				  | FB_SYNC_HOR_HIGH_ACT,
	},
};

static struct pxa168fb_mach_info mmp_mipi_lcd_info = {
	.index = 0,
	.id = "GFX Layer",
	.sclk_src = 500000000,
	.sclk_div = 0xe0001210,
	.num_modes = ARRAY_SIZE(video_modes),
	.modes = video_modes,
	.pix_fmt = PIX_FMT_RGB565,
	.burst_len = 16,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the panel is hard connected with lcd panel path
	 * and dsi1 output
	 */
	.panel_rgb_reverse_lanes = 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena = 0,
	.invert_pixclock = 0,
	.panel_rbswap = 0,
	.active = 1,
	.enable_lcd = 1,
	.spi_gpio_cs = -1,
	.spi_gpio_reset = -1,
	.max_fb_size = MK_FB_XRES * MK_FB_YRES * 8 + 4096,
	.phy_type = DSI2DPI,
	.phy_info = &mk2_dsi,
	.twsi_id = 2,
};

static void lvds_hook(struct pxa168fb_mach_info *mi)
{
	mi->phy_type = LVDS;
	mi->phy_info = (void *)&lvdsinfo;
}

#define LCD_P_GPIO17		17
static void en_bl_power(int on)
{
	if (on)
		gpio_direction_output(LCD_P_GPIO17, 1);
	else
		gpio_direction_output(LCD_P_GPIO17, 0);
}

#define VLCD_3V3_GPIO152	152
static void lcd_power_en(int on)
{
	if (on) {
		gpio_direction_output(VLCD_3V3_GPIO152, 1);
		gpio_direction_output(LCD_RST_GPIO, 1);
	} else
		gpio_direction_output(VLCD_3V3_GPIO152, 0);
}

static void set_pwm_en(int en)
{
	unsigned long data;

	if (en) {
		data = __raw_readl(APBC_PWM3_CLK_RST) & ~(0x7 << 4);
		data |= 1 << 1;
		__raw_writel(data, APBC_PWM3_CLK_RST);

		/* delay two cycles of the solwest clock between the APB bus
		 * clock and the functional module clock. */
		udelay(10);

		/* enable APB bus clk */
		data |= 1;
		__raw_writel(data, APBC_PWM3_CLK_RST);

		/* release from reset */
		data &= ~(1 << 2);
		__raw_writel(data, APBC_PWM3_CLK_RST);
	} else {
		data = __raw_readl(APBC_PWM3_CLK_RST) & ~(0x1 << 1 | 0x7 << 4);
		__raw_writel(data, APBC_PWM3_CLK_RST);
		udelay(1000);

		data &= ~(1);
		__raw_writel(data, APBC_PWM3_CLK_RST);
		udelay(1000);

		data |= 1 << 2;
		__raw_writel(data, APBC_PWM3_CLK_RST);
	}
}

static void turn_off_backlight(void)
{
	__raw_writel(0, PWM_CR3);
	__raw_writel(0, PWM_DCR);

	/* disable pwm */
	set_pwm_en(0);

	/* disable backlight power */
	en_bl_power(0);
}

static void turn_on_backlight(void)
{
	int duty_ns = 1000000, period_ns = 2000000;
	unsigned long period_cycles, prescale, pv, dc;

	/* enable pwm */
	set_pwm_en(1);

	period_cycles = 52000;
	if (period_cycles < 1)
		period_cycles = 1;

	prescale = (period_cycles - 1) / 1024;
	pv = period_cycles / (prescale + 1) - 1;

	if (prescale > 63)
		return;

	if (duty_ns == period_ns)
		dc = (1 << 10);
	else
		dc = (pv + 1) * duty_ns / period_ns;

	__raw_writel(prescale, PWM_CR3);
	__raw_writel(dc, PWM_DCR);
	__raw_writel(pv, PWM_PCR);

	/* enable backlight power */
	en_bl_power(1);
}

static GraphicDevice ctfb;
void *lcd_init(void)
{
	void *ret;
	int lvds_en = 0;
	int mode = 1;

//	turn_off_backlight();
//	lcd_power_en(1);

	
	gpio_direction_input(GPIO_19_GPIO);
	mode = gpio_get_value(GPIO_19_GPIO);
	lvds_en = !mode;

	/* disable lvds interface by default */
	if (cpu_is_bx() && lvds_en)
		lvds_hook(&mmp_mipi_lcd_info);

	ret = (void *)pxa168fb_init(&mmp_mipi_lcd_info);

//	turn_on_backlight();
	return ret;
}

void *video_hw_init(void)
{
	struct pxa168fb_info *fbi;
	struct fb_var_screeninfo *var;
	unsigned long t1, hsynch, vsynch;
	fbi = lcd_init();
	var = fbi->var;

	ctfb.winSizeX = var->xres;
	ctfb.winSizeY = var->yres;

	/* calculate hsynch and vsynch freq (info only) */
	t1 = (var->left_margin + var->xres +
	      var->right_margin + var->hsync_len) / 8;
	t1 *= 8;
	t1 *= var->pixclock;
	t1 /= 1000;
	hsynch = 1000000000L / t1;
	t1 *= (var->upper_margin + var->yres +
	       var->lower_margin + var->vsync_len);
	vsynch = 1000000000L / t1;

	/* fill in Graphic device struct */
	sprintf(ctfb.modeIdent, "%dx%dx%d %ldkHz %ldHz", ctfb.winSizeX,
		ctfb.winSizeY, var->bits_per_pixel, (hsynch / 1000),
		vsynch);

	ctfb.frameAdrs = (unsigned int) fbi->fb_start;
	ctfb.plnSizeX = ctfb.winSizeX;
	ctfb.plnSizeY = ctfb.winSizeY;

	ctfb.gdfBytesPP = 2;
	ctfb.gdfIndex = GDF_16BIT_565RGB;

	ctfb.isaBase = 0x9000000;
	ctfb.pciBase = (unsigned int) fbi->fb_start;
	ctfb.memSize = fbi->fb_size;

	/* Cursor Start Address */
	ctfb.dprBase = (unsigned int) fbi->fb_start + (ctfb.winSizeX \
			* ctfb.winSizeY * ctfb.gdfBytesPP);
	if ((ctfb.dprBase & 0x0fff) != 0) {
		/* allign it */
		ctfb.dprBase &= 0xfffff000;
		ctfb.dprBase += 0x00001000;
	}
	ctfb.vprBase = (unsigned int) fbi->fb_start;
	ctfb.cprBase = (unsigned int) fbi->fb_start;

	return &ctfb;
}
#endif

#if 0
#if defined(CONFIG_PXA168_FB)
static struct dsi_info abilene_dsi = {
	.id			= 1,
	.lanes			= 2,
	.bpp			= 24,
	.burst_mode		= DSI_BURST_MODE_SYNC_EVENT,
	.hbp_en			= 1,
	.hfp_en			= 1,
};

static struct fb_videomode video_modes[] = {
	[0] = {
		.pixclock	= 62500,
		.refresh	= 60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 2,

		.left_margin	= 10,
		.right_margin	= 116,
		.vsync_len	= 2,
		.upper_margin	= 10,
		.lower_margin	= 4,
		.sync		= FB_SYNC_VERT_HIGH_ACT \
				  | FB_SYNC_HOR_HIGH_ACT,
	},
};

static struct pxa168fb_mach_info mmp2_mipi_lcd_info = {
	.id			= "GFX Layer",
	.sclk_src		= 500000000,
	.sclk_div		= 0xe0001210,
	.num_modes		= ARRAY_SIZE(video_modes),
	.modes			= video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.burst_len		= 16,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the panel is hard connected with lcd panel path
	 * and dsi1 output
	 */
	.panel_rgb_reverse_lanes = 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.active			= 1,
	.enable_lcd             = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.max_fb_size		= FB_XRES * FB_YRES * 8 + 4096,
	.phy_type		= DSI2DPI,
	.dsi			= &abilene_dsi,
};
#endif

#if defined(CONFIG_PXA168_FB)
static GraphicDevice ctfb;
void *lcd_init(void)
{
	void *ret;

	ret = (void *)pxa168fb_init(&mmp2_mipi_lcd_info);
	return ret;
}

void *video_hw_init(void)
{
	struct pxa168fb_info *fbi;
	struct fb_var_screeninfo *var;
	unsigned long t1, hsynch, vsynch;
	fbi = lcd_init();
	var = fbi->var;

	ctfb.winSizeX = var->xres;
	ctfb.winSizeY = var->yres;

	/* calculate hsynch and vsynch freq (info only) */
	t1 = (var->left_margin + var->xres +
	      var->right_margin + var->hsync_len) / 8;
	t1 *= 8;
	t1 *= var->pixclock;
	t1 /= 1000;
	hsynch = 1000000000L / t1;
	t1 *= (var->upper_margin + var->yres +
	       var->lower_margin + var->vsync_len);
	vsynch = 1000000000L / t1;

	/* fill in Graphic device struct */
	sprintf(ctfb.modeIdent, "%dx%dx%d %ldkHz %ldHz", ctfb.winSizeX,
		ctfb.winSizeY, var->bits_per_pixel, (hsynch / 1000),
		vsynch);

	ctfb.frameAdrs = (unsigned int) fbi->fb_start;
	ctfb.plnSizeX = ctfb.winSizeX;
	ctfb.plnSizeY = ctfb.winSizeY;

	ctfb.gdfBytesPP = 2;
	ctfb.gdfIndex = GDF_16BIT_565RGB;

	ctfb.isaBase = 0x9000000;
	ctfb.pciBase = (unsigned int) fbi->fb_start;
	ctfb.memSize = fbi->fb_size;

	/* Cursor Start Address */
	ctfb.dprBase = (unsigned int) fbi->fb_start + (ctfb.winSizeX \
			* ctfb.winSizeY * ctfb.gdfBytesPP);
	if ((ctfb.dprBase & 0x0fff) != 0) {
		/* allign it */
		ctfb.dprBase &= 0xfffff000;
		ctfb.dprBase += 0x00001000;
	}
	ctfb.vprBase = (unsigned int) fbi->fb_start;
	ctfb.cprBase = (unsigned int) fbi->fb_start;

	return &ctfb;
}
#endif
#endif

#include <mmp_freq.h>

static u32 boardid;
static u8 boardrev, boardtype;
int board_early_init_f(void)
{
	u32 mfp_cfg[] = {
		/* Enable Console on UART3 */
		UART3_RXD,
		UART3_TXD,

		/* Enable TWSI1 for PMIC */
		TWSI1_SCL,
		TWSI1_SDA,

		/* Enable TWSI3 for TC35876X*/
		TWSI3_SCL,
		TWSI3_SDA,

		/* Enable TWSI5 */
		TWSI5_SCL,
		TWSI5_SDA,

		/* Enable TWSI6 */
		TWSI6_SCL,
		TWSI6_SDA,

		/* MMC1 */
		MMC1_DATA3,
		MMC1_DATA2,
		MMC1_DATA1,
		MMC1_DATA0,
		MMC1_CLK,
		MMC1_CMD,
		MMC1_CD,
		MMC1_WP,
		MMC1_GPIO138, /* GPIO_V_SD_EN */

		/* Enable LCD, gpio128*/
		LCD_RESET,
		GPIO152_VLCD_3V3,

		/* Back light PWM3, gpio53, gpio17 */
//		BACK_LIGHT_PWM3,
//		BL_POWER_EN,

		/* MMC3 */
		MMC3_DATA7_NDIO15,
		MMC3_DATA6_NDIO14,
		MMC3_DATA5_NDIO12,
		MMC3_DATA4_NDIO10,
		MMC3_DATA3_NDIO8,
		MMC3_DATA2_NDIO13,
		MMC3_DATA1_NDIO11,
		MMC3_DATA0_NDIO9,
		MMC3_CLK_SMNCS1,
		MMC3_CMD_SMNCS0,

		MFP_GPIO150,

 		/* HDMI 5V enable define */
 		GPIO_160,
 
 		/* Backlight enable*/
 		GPIO_17,
		/* LVDS mode detection pin*/
		GPIO_19, 
 		/*PWM3 for brightness*/
 //		GPIO53_PWM3,
 		GPIO53_GPIO,
		GPIO_147,
		/*Modem power control pins, William Liu*/
		GPIO93_GPIO,
		GPIO94_GPIO,
		GPIO95_GPIO,
		GPIO129_GPIO,

		MFP_EOC		/*End of configureation*/
	};
	/* configure MFP's */
	mfp_config(mfp_cfg);

	return 0;
}

#define MACH_TYPE_MK2	3497
#define FASTBOOT_KEY	147
extern void pmic_init(void);
extern void pmic_sdmmc_init(void);
extern void pmic_reset_cpu(void);
int board_init(void)
{
#if defined(CONFIG_PXA168_FB)
	/* TC358765 reset */
	gpio_direction_output(LCD_RST_GPIO, 0);
	udelay(1000);
#endif
	/* OEM UniqueID in the NTIM
	 * 32bit format: NN NN NN TV
	 * NNNNNN: board name
	 *	0x594553 (YES, YellowStone)
	 *	0x414249 (ABI, Abilene)
	 *	0x4f5243 (ORC, Orchid)
	 *	0x4d4b32 (MK2, MK2)
	 * T: type
	 *	0x1 (Pop board)
	 *	0x0 (Discrete board)
	 * V: revision
	*/
	boardid = *(volatile unsigned int *)0xd1020010;
	boardrev = boardid & 0xF;
	boardtype = (boardid >> 4) & 0xF;
	boardid = boardid >> 8;

	if (boardid == 0x4d4b32)
		gd->bd->bi_arch_number = MACH_TYPE_MK2;

	pmic_init();

	gpio_direction_input(FASTBOOT_KEY);


#ifdef CONFIG_MV_RECOVERY
	gpio_direction_input(RECOVERY_KEY);
	/* release RTC from reset state to support recovery function */
	__raw_writel(0x81, RTC_CLK_REG);
	/* read magic key from WTM */
	magic_read();
#endif

	/* for MK2, it will use GPIO_138 for the V_SD_EN */
	gpio_direction_output(GPIO_V_SD_EN, 1);

	// for MK2, use GPIO_160 for th  V_HDMI5v
	gpio_direction_output(GPIO_V_HDMI5V_EN, 0);

	gpio_direction_output(GPIO_BACKLIGHT_EN, 0);

	gpio_direction_output(GPIO_53_PWM3, 0);

	gpio_direction_output(GPIO_LCD_3V3, 1);

/* Both GPIO129, GPIO95 and GPIO94 should be in low state
 * BB_WAKE should keep floating (there is an external pull high). 
 */
	gpio_direction_output(93, 0); // BB_WAKE
	gpio_direction_output(94, 0); // BB_ENABLE
	gpio_direction_output(95, 0); // BB_RST
	gpio_direction_output(129, 0); // 3.3V rail
	printf("Start modem power on delay");
	udelay(500000);
	return 0;
}

#if defined(CONFIG_PXA168_FB)
void show_logo(void)
{
#if 0
	char cmd[100];
	int wide, high;

	/* Show marvell logo */
	wide = 213;
	high = 125;
	sprintf(cmd, "bmp display %p %d %d", MARVELL, \
		(FB_XRES - wide) / 2, (FB_YRES - high) / 2);
	run_command(cmd, 0);
#endif
#define BASE_ADDR       0x02500000
#define NEW_BASE_ADDR   0x02800000
        struct mmc *mmc_0;
        unsigned char bmp_header[54];
        mmc_0 = find_mmc_device(0);
        if (mmc_0) {
                if (mmc_init(mmc_0)){
                        printf("MMC 0 card init failed!\n");
                        return 0;
                }
        }

        mmc_switch_part(0, 0);
	//mmc_0->block_dev.block_read(0, 0x2000, 0x1201, (const void *)BASE_ADDR);
        //mmc_0->block_dev.block_read(0, 0xcc00, 0xC01, (const void *)BASE_ADDR);
	mmc_0->block_dev.block_read(0, 0x2000, 0xC01, (const void *)BASE_ADDR);
        memcpy(bmp_header, (const void *)BASE_ADDR, 54);
        //memcpy((void *)DEFAULT_FB_BASE, (void const *)(BASE_ADDR + bmp_header[0xa]), 0x240000);//remove bmp header
        memcpy((void *)DEFAULT_FB_BASE, (void const *)(BASE_ADDR + bmp_header[0xa]), 0x18000c);//remove bmp header


}


void show_fastlogo(void)
{
        char cmd[100];
        int wide, high;

        /* Show marvell logo */
        wide = 213;
        high = 125;
	//printf("FB_XRES is %d\r\n", FB_XRES);
        sprintf(cmd, "bmp display %p %d %d", MARVELL, \
                (FB_XRES - wide) / 2, (FB_YRES - high) / 2);
        run_command(cmd, 0);
}

void lcd_blank(void)
{
	memset((unsigned short *) DEFAULT_FB_BASE, 0x0, ctfb.memSize);
}
#endif



#ifdef CONFIG_MV_WTM
static int profile_map[] = {0x0, 0xffff, 0x3fff, 0xfff, 0x3ff, 0xff, 0x3f, 0xf, 0x3, 0x1};
extern unsigned int mv_profile;
#endif

int misc_init_r(void)
{
	int i, sz;

#if defined(CONFIG_PXA168_FB)
	{
	//printf("147 is HIGH\r\n");
	show_logo();
	}
#endif

	if (boardid == 0x4d4b32)
		printf("\nBoard: MK2 (%s)\n",
			boardtype ? "Pop" : "Discrete");

	gpio_set_value(GPIO_BACKLIGHT_EN, 1);
	gpio_set_value(GPIO_53_PWM3, 1);

#ifdef CONFIG_MV_WTM
	dcache_disable();
	wtm_read_profile();
	wtm_read_stepping();
	dcache_enable();
	wtm_dump_info();
	for (i = 0; i < 10; i++) {
		if (profile_map[i] == mv_profile)
			mv_profile = i;
	}
	printf("Soc profile: p%d\n", mv_profile);
#endif

	gd->bd->bi_boot_params = (ulong) malloc(CONFIG_SYS_BOOTPARAMS_LEN);

#if defined(CONFIG_MMP_POWER)
	set_volt(1250);	/* set 1.25v for vcc_core */
	setop(18); 	/* set op for 1GHz */
#endif

	/* set bootargs */
	setenv("bootargs", CONFIG_BOOTARGS);
	/* reserve 192M pmem if the memory is larger than 512M
	 * reserve 96M otherwise */
	for (i = 0, sz = 0; i < CONFIG_NR_DRAM_BANKS; i++)
		sz += gd->bd->bi_dram[i].size;
	if (sz > 0x20000000)
		run_command("setenv bootargs ${bootargs} reserve_pmem=0xc000000", 0);
	else
		run_command("setenv bootargs ${bootargs} reserve_pmem=0xA000000", 0);

	setenv("fbenv", "mmc0");
#ifdef CONFIG_MV_RECOVERY
	mv_recovery();
#endif

	return 0;
}

#ifdef CONFIG_GENERIC_MMC
int board_mmc_init(bd_t *bd)
{
	ulong mmc_base_address[CONFIG_SYS_MMC_NUM] = CONFIG_SYS_MMC_BASE;
	u8 i;

	/*
	 * Set 2.8V power (pmic_sdmmc - max77601_SD3) for sd/mmc
	 * The power domain is shared with other components' power
	 * the voltage should be 2.8V on B0, will default enabled when
	 * booting up
	 */
	pmic_sdmmc_init();

	for (i = 0; i < CONFIG_SYS_MMC_NUM; i++) {
		if (mv_sdh_init(mmc_base_address[i], 0, 0,
				SDHCI_QUIRK_32BIT_DMA_ADDR))
			return 1;
		writel(CLK_GATE_ON | CLK_GATE_CTL \
		       | WTC(WTC_DEF) | RTC(RTC_DEF),
		       mmc_base_address[i] + SD_FIFO_PARAM);
		writew(WR_ENDIAN | RD_ENDIAN \
			| DMA_SIZE(DMA_FIFO_128) | BURST_SIZE(BURST_64),
			mmc_base_address[i] + SD_CLOCK_AND_BURST_SIZE_SETUP);
		writel(TUNING_DLY_INC(0x1f) | SDCLK_DELAY(0x1f),
			mmc_base_address[i] + RX_CFG_REG);
	}

	return 0;
}
#endif

void reset_cpu(ulong ignored)
{
	pmic_reset_cpu();
}

#ifdef CONFIG_MV_RECOVERY
inline int magic_key_detect_recovery(void)
{
	/* If Volumn down key is pressed, go recovery flow */
	if (!gpio_get_value(RECOVERY_KEY))
		return 1;
	else
		return 0;
}
#endif

void arch_preboot_os(void)
{
#if defined(CONFIG_PXA168_FB)
//	lcd_blank();
//	turn_off_backlight();
#endif
}

/*
 * USB Ethernet
 */
#ifdef CONFIG_USB_ETHER
int usb_lowlevel_init(void)
{
	struct usb_file *file = (struct usb_file *)CONFIG_USB_PHY_BASE;
	int count;

	if (cpu_is_ax()) {
		writel(readl(&file->pll_reg0) & ~(FB_DIV_MASK | REFDIV_MASK),
		       &file->pll_reg0);
		writel(readl(&file->pll_reg0) | (VDD18(1) | VDD12(1)
			   | REFDIV(0xd) | FB_DIV(0xf0)), &file->pll_reg0);
	} else {
		writel(readl(&file->pll_reg0) & ~(FB_DIV_MASK_B0 | REFDIV_MASK_B0),
		       &file->pll_reg0);
		writel(readl(&file->pll_reg0) | (VDD18(1) | REFDIV_B0(0xd)
		       | FB_DIV_B0(0xf0)), &file->pll_reg0);
	}

	writel(readl(&file->pll_reg1) & ~(UTMI_PLL_PU | PLL_ICP_MASK
	       | PLL_KVCO_MASK | PLL_CALI12_MASK), &file->pll_reg1);
	writel(readl(&file->pll_reg1) | (UTMI_PLL_PU | PLL_ICP(2)
	       | PLL_KVCO(3) | PLL_CALI12(3)), &file->pll_reg1);


	writel(readl(&file->tx_reg0) & ~IMPCAL_VTH_MASK, &file->tx_reg0);
	writel(readl(&file->tx_reg0) | IMPCAL_VTH(2), &file->tx_reg0);

	writel(readl(&file->tx_reg1) & ~(CK60_PHSEL_MASK | AMP_MASK
	       | TX_VDD12_MASK), &file->tx_reg1);
	writel(readl(&file->tx_reg1) | (CK60_PHSEL(4) | AMP(4)
	       | TX_VDD12(3)), &file->tx_reg1);

	writel(readl(&file->tx_reg2) & ~DRV_SLEWRATE(3), &file->tx_reg2);
	writel(readl(&file->tx_reg2) | DRV_SLEWRATE(3), &file->tx_reg2);

	writel(readl(&file->rx_reg0) & ~(SQ_LENGTH_MASK | SQ_THRESH_MASK),
	       &file->rx_reg0);
	writel(readl(&file->rx_reg0) | (SQ_LENGTH(0x2) | SQ_THRESH(0xa)),
	       &file->rx_reg0);

	writel(readl(&file->ana_reg1) | ANA_PU, &file->ana_reg1);

	writel(readl(&file->otg_reg0) | PU_OTG, &file->otg_reg0);

	udelay(200);
	writel(readl(&file->pll_reg1) | VCOCAL_START, &file->pll_reg1);

	udelay(200);
	writel(readl(&file->tx_reg0) | RCAL_START, &file->tx_reg0);
	udelay(40);
	writel(readl(&file->tx_reg0) & ~RCAL_START, &file->tx_reg0);
	udelay(400);

	/* make sure phy is ready */
	count = 100;
	while (((readl(&file->pll_reg1) & PLL_READY) == 0) && count--)
		udelay(1000);
	if (count <= 0) {
		printf("%s %d: calibrate timeout, UTMI_PLL %x\n",
				__func__, __LINE__, readl(&file->pll_reg1));
		return -1;
	}

	return 0;
}
#endif

#ifdef CONFIG_CMD_NET
int board_eth_init(bd_t *bis)
{
	int res = -1;

#if defined(CONFIG_MV_UDC)
	if (usb_eth_initialize(bis) >= 0)
		res = 0;
#endif
	return res;
}
#endif

int board_late_init(void){

	char kernel_parameter[512];

	gpio_direction_output(GPIO_64_GPIO, 1);
	gpio_set_value(GPIO_64_GPIO, 1);

	gpio_direction_output(GPIO_68_GPIO, 1);
	gpio_set_value(GPIO_68_GPIO, 1);

	gpio_direction_output(GPIO_84_GPIO, 0);
	gpio_set_value(GPIO_84_GPIO, 0);

	gpio_direction_output(GPIO_85_GPIO, 0);
	gpio_set_value(GPIO_85_GPIO, 0);

#if 1	//Read customer serial number and pass to kernel
	struct mmc *mmc_0;
	char nvs_block[EMMC_BLOCK_SIZE];  
	mmc_0 = find_mmc_device(0);
        if (mmc_0) {
                if (mmc_init(mmc_0)){
                        printf("MMC 0 card init failed!\n");
                        return 0;
                }
        }

	mmc_switch_part(0, 0);
	mmc_0->block_dev.block_read(0, NVS_OFFSET/EMMC_BLOCK_SIZE, 1, (struct WISTRON_NVS *)nvs_block);
	
	struct WISTRON_NVS *wis_nvs = nvs_block;

	//empty serial number will cause problem in Android. Work around to avoid this.
	if(strlen(wis_nvs->sn_customer) == 0)
		strcpy(wis_nvs->sn_customer, "FZA1B");
#endif
	sprintf(kernel_parameter, 
		"androidboot.console=ttyS2 console=ttyS2,115200 panic_debug reserve_pmem=0xc000000 emmc_boot fb_share \
androidboot.bootloader=%s  androidboot.serialno=%s androidboot.baseband=%s", U_BOOT_DATE, wis_nvs->sn_customer,wis_nvs->baseband);
	setenv("bootargs", kernel_parameter);
}
