#ifndef __ASM_ARCH_MMP2_PLAT_VER_H
#define __ASM_ARCH_MMP2_PLAT_VER_H
inline int board_is_mmp2_brownstone_rev5(void);
inline int board_is_mmp2_brownstone_rev4(void);
inline int board_is_mmp2_brownstone_rev2(void);
#endif
