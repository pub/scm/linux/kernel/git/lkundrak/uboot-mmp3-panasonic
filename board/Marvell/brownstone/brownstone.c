/*
 * (C) Copyright 2011
 * Marvell Semiconductors Ltd. <www.marvell.com>
 * Lei Wen <leiwen@marvell.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <mvmfp.h>
#include <i2c.h>
#include <asm/gpio.h>
#include <asm/arch/mfp.h>
#include <asm/arch/cpu.h>
#ifdef CONFIG_USB_ETHER
#include <asm/arch/usb.h>
#endif
#ifdef CONFIG_GENERIC_MMC
#include <sdhci.h>
#endif
#if defined(CONFIG_PXA168_FB)
#include <pxa168fb.h>
#include <video_fb.h>
#include "../common/marvell.h"
#endif
#include <mv_wtm.h>
#include "plat_ver.h"
#ifdef CONFIG_MV_RECOVERY
#include <mv_recovery.h>
#define RECOVERY_KEY	17
#define RTC_CLK_REG	0xD4015000
#endif	/* CONFIG_MV_RECOVERY */
DECLARE_GLOBAL_DATA_PTR;

#define MAX8925_SLAVE_ADDR	0x3c

void show_charge_logo(unsigned char *logo, int wide, int high, int offset)
{
	int i = 0;
	unsigned char *ptrBuf_bmp = NULL;

	ptrBuf_bmp = DEFAULT_FB_BASE+(FB_XRES*FB_YRES)
		- FB_XRES*high - wide - offset;

	/* ensure the base address is not a odd number */
	if ((int)ptrBuf_bmp & 1)
		ptrBuf_bmp += 1;

	for (i = 0; i < high; i++) {
		memcpy(ptrBuf_bmp, logo, wide*2);
		logo += wide*2;
		ptrBuf_bmp += FB_XRES * 2;
	}
}

void lcd_flush(void)
{
	memset((unsigned char *) (DEFAULT_FB_BASE),
			0x0, FB_XRES * FB_YRES*6);
}

#if defined(CONFIG_PXA168_FB)
static struct dsi_info brownstone_dsi = {
	.id			= 1,
	.lanes			= 4,
	.bpp			= 16,
	.burst_mode		= DSI_BURST_MODE_BURST,
	.hbp_en			= 1,
	.hfp_en			= 1,
};

static struct fb_videomode video_modes[] = {
	[0] = {
		.pixclock	= 62500,
		.refresh	= 60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 2,

		.left_margin	= 12,
		.right_margin	= 315,
		.vsync_len	= 2,
		.upper_margin	= 10,
		.lower_margin	= 4,
		.sync		= FB_SYNC_VERT_HIGH_ACT \
				  | FB_SYNC_HOR_HIGH_ACT,
	},
};

static struct pxa168fb_mach_info mmp2_mipi_lcd_info = {
	.id			= "GFX Layer",
	.sclk_src		= 260000000,
	.sclk_div		= 0x40000104,
	.num_modes		= ARRAY_SIZE(video_modes),
	.modes			= video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.burst_len		= 16,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the panel is hard connected with lcd panel path
	 * and dsi1 output
	 */
	.panel_rgb_reverse_lanes = 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.active			= 1,
	.enable_lcd             = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.max_fb_size		= FB_XRES * FB_YRES * 8 + 4096,
	.phy_type		= DSI2DPI,
	.phy_info		= &brownstone_dsi,
	.twsi_id		= 4,
};
#endif

#define PWM3_BASE	0xD401A800
#define APB_CLK_REG_BASE	0xD4015000

#define GPIO83_LCD_RST		83
#define GPIO128			128
#define GPIO89			89
#define GPIO82			82

#define PMUM_CGR_PJ		0x1024
#define MFP_GPIO53		(MV_MFPR_BASE + 0x128)

#define APBC_PWM3_CLK_RST	(APB_CLK_REG_BASE + 0x0044)
#define PWM_CR3			(PWM3_BASE + 0x00)
#define PWM_DCR			(PWM3_BASE + 0x04)
#define PWM_PCR			(PWM3_BASE + 0x08)

#if defined(CONFIG_PXA168_FB)
void set_LCD_5V_power(int on)
{
	if (on)
		gpio_direction_output(GPIO89, 1);
	else
		gpio_direction_output(GPIO89, 0);

}
void turn_off_backlight(void)
{
	__raw_writel(0, PWM_CR3);
	__raw_writel(0, PWM_DCR);

	/* set panel 5V power off */
	set_LCD_5V_power(0);
}

void turn_on_backlight(void)
{
	int duty_ns = 1000000, period_ns = 2000000;
	unsigned long period_cycles, prescale, pv, dc;

	period_cycles = 52000;
	if (period_cycles < 1)
		period_cycles = 1;

	prescale = (period_cycles - 1) / 1024;
	pv = period_cycles / (prescale + 1) - 1;

	if (prescale > 63)
		return ;

	if (duty_ns == period_ns)
		dc = (1 << 10);
	else
		dc = (pv + 1) * duty_ns / period_ns;

	__raw_writel(prescale, PWM_CR3);
	__raw_writel(dc, PWM_DCR);
	__raw_writel(pv, PWM_PCR);

	/* set panel 5V power */
	set_LCD_5V_power(1);
}

void close_lcd(void)
{
	turn_off_backlight();
}

static GraphicDevice ctfb;
void *lcd_init(void)
{
	void *ret;

	turn_off_backlight();

	ret = (void *)pxa168fb_init(&mmp2_mipi_lcd_info);

	turn_on_backlight();
	return ret;
}

void *video_hw_init(void)
{
	struct pxa168fb_info *fbi;
	struct fb_var_screeninfo *var;
	unsigned long t1, hsynch, vsynch;
	fbi = lcd_init();
	var = fbi->var;

	ctfb.winSizeX = var->xres;
	ctfb.winSizeY = var->yres;

	/* calculate hsynch and vsynch freq (info only) */
	t1 = (var->left_margin + var->xres +
	      var->right_margin + var->hsync_len) / 8;
	t1 *= 8;
	t1 *= var->pixclock;
	t1 /= 1000;
	hsynch = 1000000000L / t1;
	t1 *= (var->upper_margin + var->yres +
	       var->lower_margin + var->vsync_len);
	vsynch = 1000000000L / t1;

	/* fill in Graphic device struct */
	sprintf(ctfb.modeIdent, "%dx%dx%d %ldkHz %ldHz", ctfb.winSizeX,
		ctfb.winSizeY, var->bits_per_pixel, (hsynch / 1000),
		vsynch);

	ctfb.frameAdrs = (unsigned int) fbi->fb_start;
	ctfb.plnSizeX = ctfb.winSizeX;
	ctfb.plnSizeY = ctfb.winSizeY;

	ctfb.gdfBytesPP = 2;
	ctfb.gdfIndex = GDF_16BIT_565RGB;

	ctfb.isaBase = 0x9000000;
	ctfb.pciBase = (unsigned int) fbi->fb_start;
	ctfb.memSize = fbi->fb_size;

	/* Cursor Start Address */
	ctfb.dprBase = (unsigned int) fbi->fb_start + (ctfb.winSizeX \
			* ctfb.winSizeY * ctfb.gdfBytesPP);
	if ((ctfb.dprBase & 0x0fff) != 0) {
		/* allign it */
		ctfb.dprBase &= 0xfffff000;
		ctfb.dprBase += 0x00001000;
	}
	ctfb.vprBase = (unsigned int) fbi->fb_start;
	ctfb.cprBase = (unsigned int) fbi->fb_start;

	return &ctfb;
}
#endif

int board_early_init_f(void)
{
	u32 mfp_cfg[] = {
		/* Enable Console on UART3 */
		UART3_RXD,
		UART3_TXD,

		/* Enable TWSI5 */
		TWSI5_SCL,
		TWSI5_SDA,

		/* Enable TWSI6 */
		TWSI6_SCL,
		TWSI6_SDA,

		/* MMC1 */
		MMC1_DATA3,
		MMC1_DATA2,
		MMC1_DATA1,
		MMC1_DATA0,
		MMC1_CLK,
		MMC1_CMD,
		MMC1_CD,
		MMC1_WP,

		/* MMC3 */
		MMC3_DATA7,
		MMC3_DATA6,
		MMC3_DATA5,
		MMC3_DATA4,
		MMC3_DATA3,
		MMC3_DATA2,
		MMC3_DATA1,
		MMC3_DATA0,
		MMC3_CLK,
		MMC3_CMD,

		/* Back light PWM3 */
		BACK_LIGHT_PWM3,

		/* Enable volume up and down */
		VOLUME_UP,
		VOLUME_DOWN,

		/* VERS */
		PLAT_VERS_PIN0,
		PLAT_VERS_PIN1,
		PLAT_VERS_PIN2,
		PLAT_VERS_PIN3,

		MFP_EOC		/*End of configureation*/
	};
	/* configure MFP's */
	mfp_config(mfp_cfg);
	wtm_read_profile();
	wtm_read_stepping();
	return 0;
}

#define LDOCTL17	0x14
#define LDO17VOUT	0x16
#define LDOCTL3		0x20
#define LDO3VOUT	0x22
#define FASTBOOT_KEY	16

int display_marvell_banner (void)
{
	printf("\n");
	printf(" __  __                      _ _\n");
	printf("|  \\/  | __ _ _ ____   _____| | |\n");
	printf("| |\\/| |/ _` | '__\\ \\ / / _ \\ | |\n");
	printf("| |  | | (_| | |   \\ V /  __/ | |\n");
	printf("|_|  |_|\\__,_|_|    \\_/ \\___|_|_|\n");
	printf(" _   _     ____              _\n");
	printf("| | | |   | __ )  ___   ___ | |_ \n");
	printf("| | | |___|  _ \\ / _ \\ / _ \\| __| \n");
	printf("| |_| |___| |_) | (_) | (_) | |_ \n");
	printf(" \\___/    |____/ \\___/ \\___/ \\__| ");
	printf("\n\nMARVELL MMP2 AP.");

	if(cpu_is_armada610_a2())
		printf("\nBased on MMP2 A2 CPU.\n\n");
	if(cpu_is_armada610_a1())
		printf("\nBased on MMP2 A1 CPU.\n\n");
	if(cpu_is_armada610_a0())
		printf("\nBased on MMP2 A0 CPU.\n\n");
	else if(cpu_is_armada610_z1())
		printf("\nBased on MMP2 Z1 CPU.\n\n");
	else if(cpu_is_armada610_z0())
		printf("\nBased on MMP2 Z0 CPU.\n\n");

	if (board_is_mmp2_brownstone_rev5())
		printf("Board info: BROWNSTONE REV5\n");
	else if (board_is_mmp2_brownstone_rev4())
		printf("Board info: BROWNSTONE REV4\n");
	else if (board_is_mmp2_brownstone_rev2())
		printf("Board info: BROWNSTONE REV2(3)\n");

#if defined (CONFIG_RECOVERY_MODE) && defined (CONFIG_TRUST_BOOT)
	if (R_uboot)
		printf("\nEntering Maverll Recovery Uboot.\n");
#endif
	return 0;
}

static void vbus_en(unsigned int enable)
{
	if (board_is_mmp2_brownstone_rev5()) {
		if (!enable) {
			/* set GPIO82 output low, otherwise vbus
			* will pull 5v regualtor output high even
			* regulator not enabled. */
			gpio_direction_output(GPIO82, 0);
			udelay(10000);
		}
	}
	return;
}

int checkboard (void)
{
	display_marvell_banner();
	wtm_dump_info();
	return 0;
}

int board_init(void)
{
	u8 data;

	/* arch number of FPGA Board */
	gd->bd->bi_arch_number = MACH_TYPE_BROWNSTONE;
	gd->bd->bi_boot_params = 0x3c00;

#if defined(CONFIG_PXA168_FB)
	/* TC358765 reset */
	gpio_direction_output(LCD_RST_GPIO, 0);
	udelay(100000);
#endif

	i2c_set_bus_num(0);
	/* Set V_LDO17 to enable power to MMC1 */
	data = 0x1f;
	i2c_write(MAX8925_SLAVE_ADDR, LDOCTL17, 1, &data, 1);
	data = 0x16;
	i2c_write(MAX8925_SLAVE_ADDR, LDO17VOUT, 1, &data, 1);

	/* Set V_LDO3 to enable power to MMC1 */
	data = 0x1f;
	i2c_write(MAX8925_SLAVE_ADDR, LDOCTL3, 1, &data, 1);
	data = 0x16;
	i2c_write(MAX8925_SLAVE_ADDR, LDO3VOUT, 1, &data, 1);

	gpio_direction_input(FASTBOOT_KEY);
	gpio_direction_input(RECOVERY_KEY);

#ifdef CONFIG_MV_RECOVERY
	/* release RTC from reset state to support recovery function */
	__raw_writel(0x81, RTC_CLK_REG);
	/* read magic key from WTM */
	magic_read();
#endif

	return 0;
}

#if defined(CONFIG_PXA168_FB)
void show_logo(void)
{
	char cmd[100];
	int wide, high;

	/* Show marvell logo */
	wide = 213;
	high = 125;
	sprintf(cmd, "bmp display %p %d %d", MARVELL, \
		(FB_XRES - wide) / 2, (FB_YRES - high) / 2);
	run_command(cmd, 0);
}
#endif

void send_cmd_to_max8925(unsigned long addr, unsigned int val)
{
	/* send pmic slave address with start bit */
	*(volatile u32 *)0xD4011008 = 0x78;
	*(volatile u32 *)0xD4011010 = 0x69;

	while (!(*(volatile u32 *)0xD4011018 & 0x40))
		nop();
	*(volatile u32 *)0xD4011018 |= 0x40;
	if (*(volatile u32 *)0xD4011018 & 0x20)
		*(volatile u32 *)0xD4011018 |= 0x20;

	*(volatile u32 *)0xD4011008 = addr;
	*(volatile u32 *)0xD4011010 = 0x68;

	while (!(*(volatile u32 *)0xD4011018 & 0x40))
		nop();
	*(volatile u32 *)0xD4011018 |= 0x40;
	if (*(volatile u32 *)0xD4011018 & 0x20)
		*(volatile u32 *)0xD4011018 |= 0x20;

	*(volatile u32 *)0xD4011008 = val;
	*(volatile u32 *)0xD4011010 = 0x6A;

	while (!(*(volatile u32 *)0xD4011018 & 0x40))
		nop();
	*(volatile u32 *)0xD4011018 |= 0x40;
	if (*(volatile u32 *)0xD4011018 & 0x20)
		*(volatile u32 *)0xD4011018 |= 0x20;

	*(volatile u32 *)0xD4011010 = *(volatile u32 *)0xD4011010 & ~0x2;
	return;
}

void max8925_power_off(void)
{
	printf("\nSystem is shuting down\n");
	udelay(1000000);
	send_cmd_to_max8925(0x0f, 0x40);
	while (1);
}

#ifdef CONFIG_MMP_POWER
#define DVFM_BASE_ADDR		0xd1020000
#define DVFM_STACK_ADDR		0xd1028000
extern void freq_init_sram(int addr);
extern void freq_chg_seq(int vaddr, int vstack, int op, int flag);
extern int set_volt(u32 vol);
extern int cpu_is_armada610_z1(void);
extern int cpu_is_armada610_a0(void);

void init_freq(void)
{
	/* set voltage to 1350mV by default */
	set_volt(1350);
	udelay(10);

	dcache_disable();
	freq_init_sram(DVFM_BASE_ADDR);

	if (cpu_is_armada610_z1()) {
		freq_chg_seq(DVFM_BASE_ADDR, DVFM_STACK_ADDR, 2, 1);
	} else {
		if (cpu_is_armada610_a0())
			freq_chg_seq(DVFM_BASE_ADDR, DVFM_STACK_ADDR, 3, 2);
		else
			freq_chg_seq(DVFM_BASE_ADDR, DVFM_STACK_ADDR, 3, 3);
	}
	dcache_enable();
}
#endif

int misc_init_r(void)
{
#ifdef CONFIG_POWEROFF_CHARGE
	/* Turn off vbus. And 5v enable will be done
	 * in charge_detect(). Regulator's enable pin
	 * need a low=>high logic.*/
	vbus_en(0);
	if (board_is_mmp2_brownstone_rev5())
		charge_detect();
#endif

#if defined(CONFIG_PXA168_FB)
	show_logo();
#endif

#ifdef CONFIG_MMP_POWER
	init_freq();
#endif
	setenv("fbenv", "mmc0");
	setenv("autostart", "no");

	/* If Volume up key is pressed, launch fastboot */
	if (!gpio_get_value(FASTBOOT_KEY))
		run_command("fb", 0);

#ifdef CONFIG_MV_RECOVERY
	mv_recovery();
#endif

	return 0;
}

#ifdef CONFIG_GENERIC_MMC
#define MAX8925_I2C_SLAVE_ADDR 0x3c
#define I2CEN          0x7
#define LDOSEQ(x)      ((x & 0x7) << 2)
#define LDO_DC         0x2
#define LDO_EN         0x1
#define LDOCTL_11      0x40
#define LDO_3V         0x2d
#define LDOVOUT_11     0x42
int board_mmc_init(bd_t *bd)
{
	ulong mmc_base_address[CONFIG_SYS_MMC_NUM] = CONFIG_SYS_MMC_BASE;
	u8 i, data;
	i2c_set_bus_num(0);
	data = LDOSEQ(I2CEN) | LDO_DC | LDO_EN;
	i2c_write(MAX8925_I2C_SLAVE_ADDR, LDOCTL_11, 1, &data, 1);
	data = LDO_3V;
	i2c_write(MAX8925_I2C_SLAVE_ADDR, LDOVOUT_11, 1, &data, 1);

	for (i = 0; i < CONFIG_SYS_MMC_NUM; i++) {
		if (mv_sdh_init(mmc_base_address[i], 0, 0,
				SDHCI_QUIRK_32BIT_DMA_ADDR))
			return 1;
	}
	return 0;
}
#endif

#ifdef CONFIG_USB_ETHER
int usb_lowlevel_init(void)
{
	struct usb_file *file = (struct usb_file *)CONFIG_USB_PHY_BASE;
	int count = 0;
	int reg;

	/* initialize the usb phy power */
	writel(readl(&file->ctrl_reg) | POWER_UP | PLL_POWER_UP |
			PU_REF |USB_CTL_29_28(1), &file->ctrl_reg );
	/* UTMI_PLL settings */
	writel(readl(&file->pll_reg) & ~(REFDIV_MASK | FBDIV_MASK | ICP_MASK |
				PLLVDD12_MASK | PLLVDD18_MASK |
				PLLCALLI12_MASK), &file->pll_reg);
	writel(readl(&file->pll_reg) | REFDIV(13) | FBDIV(240) | ICP(10) |
			PLL_READY | PLLVDD12(3) | PLLVDD18(3) | PLLCALLI12(3),
			&file->pll_reg);
	/* UTMI_TX */
	writel(readl(&file->tx_reg) & ~(IMPCAL_VTH_MASK | CK60_PHSEL_Mask |
				TXVDD12_MASK), &file->tx_reg);
	writel(readl(&file->tx_reg) | IMPCAL_VTH(5) | CK60_PHSEL(4) |
			TXVDD12(3) | TXDATA_BLOCK_EN , &file->tx_reg);
	/* calibrate */
	count = 10000;
	while(((readl(&file->pll_reg) & PLL_READY)==0) && count--);
	if (count <= 0)
		printf("Calibrate timeout, UTMI_PLL:%x\n",
				readl(&file->pll_reg));

	/* toggle VCOCAL_START bit of UTMI_PLL */
	udelay(200);
	reg = readl(&file->pll_reg);
	writel(reg | VCOCAL_START, &file->pll_reg);
	udelay(40);
	writel(reg & (~VCOCAL_START), &file->pll_reg);

	/* toggle REG_RCAL_START bit of UTMI_TX */
	udelay(200);
	reg = readl(&file->tx_reg);
	writel(reg | REG_RCAL_START, &file->tx_reg);
	udelay(40);
	writel(reg & (~REG_RCAL_START), &file->tx_reg);
	udelay(200);

	/* make sure phy is ready */
	count = 1000;
	while(((readl(&file->pll_reg) & PLL_READY)==0) && count--);
	if (count <= 0)
		printf("Calibrate timeout, UTMI_PLL %x\n",
				readl(&file->pll_reg));

	return 0;
}
#endif

#ifdef CONFIG_CMD_NET
int board_eth_init(bd_t *bis)
{
	int res = -1;

#if defined(CONFIG_MV_UDC)
	if (usb_eth_initialize(bis) >= 0)
		res = 0;
#endif
	return res;
}
#endif

/*
 * Reset the cpu by set max8925 pmic register
 */
#define RESET_CNFG		0x0f
#define nBBPMUEN_DST		(1 << 2)
#define RSTIN_DLY		(1 << 4)
#define SFT_RST			(1 << 5)
void reset_cpu(ulong ignored)
{
	u8 data;
	data = nBBPMUEN_DST | RSTIN_DLY | SFT_RST;
	i2c_set_bus_num(0);
	i2c_write(MAX8925_SLAVE_ADDR, RESET_CNFG, 1, &data, 1);
	while (1)
		;
}

#ifdef CONFIG_MV_RECOVERY
inline int magic_key_detect_recovery(void)
{
	/* If Volumn down key is pressed, go recovery flow */
	if (!gpio_get_value(RECOVERY_KEY))
		return 1;
	else
		return 0;
}
#endif

void arch_preboot_os(void)
{
#if defined(CONFIG_PXA168_FB)
	turn_off_backlight();
#endif
}

