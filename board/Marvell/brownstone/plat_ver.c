/*
 * (C) Copyright 2011
 * Marvell Semiconductors Ltd. <www.marvell.com>
 * Lei Wen <leiwen@marvell.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 */

#include <asm/gpio.h>
#include <asm/mach-types.h>

#define GPIO125		125
#define GPIO126		126
#define GPIO127		127
#define GPIO128		128

static inline int mmp2_get_bs_vers(void)
{
	int vers = 0;
	int vers0 = !!gpio_get_value(GPIO125);
	int vers1 = !!gpio_get_value(GPIO126);
	int vers2 = !!gpio_get_value(GPIO127);
	int vers3 = !!gpio_get_value(GPIO128);

	if (!machine_is_brownstone()) {
		return 0;
	}

	vers = (vers3 << 3) | (vers2 << 2) |
			(vers1 << 1) | (vers0 << 0);
	return vers;
}

inline int board_is_mmp2_brownstone_rev1(void)
{
	return (0x9 == mmp2_get_bs_vers());
}

inline int board_is_mmp2_brownstone_rev2(void)
{
	return (0x7 == mmp2_get_bs_vers());
}

inline int board_is_mmp2_brownstone_rev4(void)
{
	return (0x3 == mmp2_get_bs_vers());
}

inline int board_is_mmp2_brownstone_rev5(void)
{
	return (0xe == mmp2_get_bs_vers());
}

