/*
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#ifndef __CONFIG_ORCHID_H
#define __CONFIG_ORCHID_H

/*
 * Version number information
 */
#define CONFIG_IDENT_STRING		"\nMarvell-Orchid"

/*
 * High Level Configuration Options
 */

#define CONFIG_SHEEVA_88SV584xV7	1	/* CPU Core subversion */
#define CONFIG_ARMV7				1	/* ARM926EJS cpu family */
#define CONFIG_ARMADA6XX			1	/* SOC Family Name */
#define CONFIG_MACH_ORCHID			1	/* Machine type */

#define CONFIG_SYS_INIT_SP_ADDR		(0xd1020000 + 0x1000)
#define CONFIG_NR_DRAM_BANKS_MAX	8

/*
 * Commands configuration
 */
#define CONFIG_SYS_NO_FLASH		/* Declare no flash (NOR/SPI) */
#include <config_cmd_default.h>
#define CONFIG_CMD_I2C
#define CONFIG_CMD_GPIO
#define CONFIG_USB_ETHER
#define CONFIG_CMD_FASTBOOT
#define CONFIG_CMD_MMC
#define CONFIG_MISC_INIT_R
#define CONFIG_CMD_AUTOSCRIPT
#define CONFIG_CMD_UNSPARSE
#ifdef CONFIG_TZ_HYPERVISOR
#define CONFIG_SYS_TEXT_BASE		0x00200000
#define CONFIG_SYS_SDRAM_BASE		0x00200000
#define CONFIG_TZ_HYPERVISOR_SIZE	0x00200000
#else
#define CONFIG_SYS_TEXT_BASE		0
#define CONFIG_SYS_SDRAM_BASE		0
#define CONFIG_TZ_HYPERVISOR_SIZE	0
#endif
#undef CONFIG_CMD_NET
#undef CONFIG_CMD_NFS

#define CONFIG_CMD_MIPS

/*
 * mv-common.h should be defined after CMD configs since it used them
 * to enable certain macros
 */
#include "mv-common.h"
#undef CONFIG_ARCH_MISC_INIT
#define CONFIG_L2_OFF

/*
 * Boot setting
 */
#define CONFIG_ZERO_BOOTDELAY_CHECK
#define CONFIG_SHOW_BOOT_PROGRESS
#define CONFIG_BOOTDELAY		3
#define CONFIG_BOOTCOMMAND		"mmc dev 0; mmc read $loadaddr" \
					" 0x4c00 0x2000; bootm $loadaddr"
#define CONFIG_BOOTARGS			"rdinit=/busybox/rdinit " \
					"androidboot.console=ttyS2 " \
					"console=ttyS2,115200 emmc_boot fb_share"

/*
 * Environment variables configurations
 */
#define CONFIG_ENV_IS_NOWHERE	1	/* if env in SDRAM */
#define CONFIG_ENV_SIZE	0x20000	/* 64k */
#define CONFIG_SYS_HUSH_PARSER
#define CONFIG_SYS_PROMPT_HUSH_PS2	"> "

#endif	/* __CONFIG_ORCHID_H */
