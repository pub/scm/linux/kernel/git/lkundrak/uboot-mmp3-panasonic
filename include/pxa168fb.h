/*
 * linux/arch/arm/mach-mmp/include/mach/pxa168fb.h
 *
 *  Copyright (C) 2009 Marvell International Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef __ASM_MACH_PXA168FB_H
#define __ASM_MACH_PXA168FB_H

#include <linux/types.h>
#include <linux/list.h>
#include <linux/fb.h>

/*
 * panel interface
 */
#define DPI             0
#define DSI2DPI         1
#define DSI             2
#define LVDS            4

#define fb_base         0
#define fb_dual         1
#define FB_MODE_DUP     ((fbi->id == fb_base) && fb_mode && \
		gfx_info.fbi[fb_dual])

/* DSI burst mode */
#define DSI_BURST_MODE_SYNC_PULSE                       0x0
#define DSI_BURST_MODE_SYNC_EVENT                       0x1
#define DSI_BURST_MODE_BURST                            0x2

/* ------------< LCD register >------------ */
struct lcd_regs {
	/* Video Frame 0/1 Y/U/V/Command Starting Addr */
	u32 v_y0;
	u32 v_u0;
	u32 v_v0;
	u32 v_c0;
	u32 v_y1;
	u32 v_u1;
	u32 v_v1;
	u32 v_c1;
	/* Video Y and C Line Length (Pitch) */
	u32 v_pitch_yc;
	/* Video U and V Line Length (Pitch) */
	u32 v_pitch_uv;
	/* Video Starting Point on Screen */
	u32 v_start;
	/* Video Source Size */
	u32 v_size;
	/* Video Destination Size (After Zooming) */
	u32 v_size_z;
	/* Graphic Frame 0/1 Starting Address */
	u32 g_0;
	u32 g_1;
	/* Graphic Line Length (Pitch) */
	u32 g_pitch;
	/* Graphic Starting Point on Screen */
	u32 g_start;
	/* Graphic Source Size */
	u32 g_size;
	/* Graphic Destination Size (After Zooming) */
	u32 g_size_z;
	/* Hardware Cursor */
	u32 hc_start;
	/* Hardware Cursor */
	u32 hc_size;
	/* Screen Total Size */
	u32 screen_size;
	/* Screen Active Size */
	u32 screen_active;
	/* Screen Horizontal Porch */
	u32 screen_h_porch;
	/* Screen Vertical Porch */
	u32 screen_v_porch;
	/* Screen Blank Color */
	u32 blank_color;
	/* Hardware Cursor Color1 */
	u32 hc_Alpha_color1;
	/* Hardware Cursor Color2 */
	u32 hc_Alpha_color2;
	/* Video Y Color Key Control */
	u32 v_colorkey_y;
	/* Video U Color Key Control */
	u32 v_colorkey_u;
	/* Video V Color Key Control */
	u32 v_colorkey_v;
	/* VSYNC PulsePixel Edge Control */
	u32 vsync_ctrl;
};

#define intf_ctrl(id)		((id) ? (((id) & 1) ? LCD_TVIF_CTRL \
			: LCD_DUMB2_CTRL) : LCD_SPU_DUMB_CTRL)
#define dma_ctrl0(id)           ((id) ? (((id) & 1) ? LCD_TV_CTRL0 \
			: LCD_PN2_CTRL0) : LCD_SPU_DMA_CTRL0)
#define dma_ctrl1(id)           ((id) ? (((id) & 1) ? LCD_TV_CTRL1 \
			: LCD_PN2_CTRL1) : LCD_SPU_DMA_CTRL1)
#define dma_ctrl(ctrl1, id)     (ctrl1 ? dma_ctrl1(id) : dma_ctrl0(id))

/* 32 bit       TV Path DMA Control 0*/
#define LCD_TV_CTRL0                     (0x0080)
/* 32 bit       TV Path DMA Control 1*/
#define LCD_TV_CTRL1                     (0x0084)
/* 32 bit TV Path TVIF Control  Register */
#define LCD_TVIF_CTRL                    (0x0094)

/*
 * Buffer pixel format
 * bit0 is for rb swap.
 * bit12 is for Y UorV swap
 */
#define PIX_FMT_RGB565		0
#define PIX_FMT_BGR565		1
#define PIX_FMT_RGB1555		2
#define PIX_FMT_BGR1555		3
#define PIX_FMT_RGB888PACK	4
#define PIX_FMT_BGR888PACK	5
#define PIX_FMT_RGB888UNPACK	6
#define PIX_FMT_BGR888UNPACK	7
#define PIX_FMT_RGBA888		8
#define PIX_FMT_BGRA888		9
#define PIX_FMT_YUV422PACK	10
#define PIX_FMT_YVU422PACK	11
#define PIX_FMT_YUV422PLANAR	12
#define PIX_FMT_YVU422PLANAR	13
#define PIX_FMT_YUV420PLANAR	14
#define PIX_FMT_YVU420PLANAR	15
#define PIX_FMT_PSEUDOCOLOR	20
#define PIX_FMT_UYVY422PACK	(0x1000|PIX_FMT_YUV422PACK)

struct dsi_phy {
	unsigned int hs_prep_constant;    /* Unit: ns. */
	unsigned int hs_prep_ui;
	unsigned int hs_zero_constant;
	unsigned int hs_zero_ui;
	unsigned int hs_trail_constant;
	unsigned int hs_trail_ui;
	unsigned int hs_exit_constant;
	unsigned int hs_exit_ui;
	unsigned int ck_zero_constant;
	unsigned int ck_zero_ui;
	unsigned int ck_trail_constant;
	unsigned int ck_trail_ui;
	unsigned int req_ready;
};

struct dsi_info {
	unsigned        id;
	unsigned        regs;
	unsigned        lanes;
	unsigned        bpp;
	unsigned        rgb_mode;
	unsigned        burst_mode;
	unsigned        lpm_line_en;
	unsigned        lpm_frame_en;
	unsigned        last_line_turn;
	unsigned        hex_slot_en;
	unsigned        all_slot_en;
	unsigned        hbp_en;
	unsigned        hact_en;
	unsigned        hfp_en;
	unsigned        hex_en;
	unsigned        hlp_en;
	unsigned        hsa_en;
	unsigned        hse_en;
	unsigned        eotp_en;
	struct dsi_phy  *phy;
};

/* LVDS info */
struct lvds_info {
#define LVDS_SRC_PN	0
#define LVDS_SRC_CMU	1
#define LVDS_SRC_PN2	2
#define LVDS_SRC_TV	3
	u32	src;
#define LVDS_FMT_24BIT	0
#define LVDS_FMT_18BIT	1
	u32	fmt;
};

struct cmu_calibration {
	int left;
	int right;
	int top;
	int bottom;
};


struct pxa168fb_mach_info {
	char    id[16];
	unsigned	index;
	unsigned int    sclk_src;
	unsigned int    sclk_div;

	int             num_modes;
	struct fb_videomode *modes;
	unsigned int max_fb_size;

	/*
	 * Pix_fmt
	 */
	unsigned        pix_fmt;

	/*
	 * Burst length
	 */
	unsigned        burst_len;

	/*
	 * I/O pin allocation.
	 */
	unsigned int    io_pin_allocation_mode;

	/*
	 * Dumb panel -- assignment of R/G/B component info to the 24
	 * available external data lanes.
	 */
	unsigned        dumb_mode:4;
	unsigned        panel_rgb_reverse_lanes:1;

	/*
	 * Dumb panel -- GPIO output data.
	 */
	unsigned        gpio_output_mask:8;
	unsigned        gpio_output_data:8;

	/*
	 * Dumb panel -- configurable output signal polarity.
	 */
	unsigned        invert_composite_blank:1;
	unsigned        invert_pix_val_ena:1;
	unsigned        invert_pixclock:1;
	unsigned        invert_vsync:1;
	unsigned        invert_hsync:1;
	unsigned        panel_rbswap:1;
	unsigned        active:1;
	unsigned        enable_lcd:1;
	/*
	 * SPI control
	 */
	unsigned int    spi_ctrl;
	unsigned int    spi_gpio_cs;
	unsigned int    spi_gpio_reset;

	/*
	 * panel interface
	 */
	unsigned int    phy_type;
	unsigned int    twsi_id;

	/*
	 * vdma option
	 */
	unsigned int vdma_enable;

	/* phy interface info */
	void *phy_info;

	/*CMU platform calibration*/
	struct cmu_calibration cmu_cal[3];
	struct cmu_calibration cmu_cal_letter_box[3];
};

#define MAX_QUEUE_NUM 30

#define DEFAULT_FB_BASE		0x19000000
#define BMP_DOWNLOAD_BASE	0x15000000
#define DEFAULT_FB_SIZE		(1024*768*4)
#define DEFAULT_VID_BASE	(0x19000000 + 0x67A4000)
#define DEFAULT_VID_SIZE	(0x67A4000)
#define DEFAULT_VID_BASE2	(0x19000000 + 2*0x67A4000)

struct pxa168fb_info {
	struct device           *dev;
	struct clk              *clk;
	int                     id;
	void                    *reg_base;
	void                    *dsi1_reg_base;
	void                    *dsi2_reg_base;
	unsigned long           new_addr[3];    /* three addr for YUV
						   planar */
	unsigned char           *filterBufList[MAX_QUEUE_NUM][3];
	unsigned char           *buf_freelist[MAX_QUEUE_NUM];
	unsigned char           *buf_waitlist[MAX_QUEUE_NUM];
	unsigned char           *buf_current;
	dma_addr_t              fb_start_dma;
	void                    *fb_start;
	int                     fb_size;
	dma_addr_t              fb_start_dma_bak;
	void                    *fb_start_bak;
	int                     fb_size_bak;
	int                     dma_ctrl0;
	int                     fixed_output;
	unsigned char           *hwc_buf;
	unsigned int            pseudo_palette[16];
	char                    *mode_option;
	struct fb_info          *fb_info;
	int                     io_pin_allocation;
	int                     pix_fmt;
	unsigned                is_blanked:1;
	unsigned                edid:1;
	unsigned                cursor_enabled:1;
	unsigned                cursor_cfg:1;
	unsigned                panel_rbswap:1;
	unsigned                debug:1;
	unsigned                active:1;
	unsigned                enabled:1;
	unsigned                edid_en:1;

	/*
	 * 0: DMA mem is from DMA region.
	 * 1: DMA mem is from normal region.
	 */
	unsigned                mem_status:1;
	unsigned                wait_vsync;

	struct pxa168fb_mach_info	*mi;
	struct fb_var_screeninfo	*var;
	struct fb_fix_screeninfo	*fix;
};

#define FB_VISUAL_TRUECOLOR             2       /* True color   */
#define FB_VISUAL_PSEUDOCOLOR           3       /* Pseudo color (like atari) */

#define FB_SYNC_HOR_HIGH_ACT    1       /* horizontal sync high active  */
#define FB_SYNC_VERT_HIGH_ACT   2       /* vertical sync high active    */

/*       DSI Controller Registers       */
struct dsi_lcd_regs {
	u32 ctrl0;
	u32 ctrl1;
	u32 reserved1[2];
	u32 timing0;
	u32 timing1;
	u32 timing2;
	u32 timing3;
	u32 wc0;
	u32 wc1;
	u32 wc2;
	u32 reserved[1];
	u32 slot_cnt0;
	u32 slot_cnt1;
};



struct dsi_regs {
	u32 ctrl0;
	u32 ctrl1;
	u32 reserved1[2];
	u32 irq_status;
	u32 irq_mask;
	u32 reserved2[2];
	u32 cmd0;
	u32 cmd1;
	u32 cmd2;
	u32 cmd3;
	u32 dat0;
	u32 reserved3[7];

	u32 smt_cmd;
	u32 smt_ctrl0;
	u32 smt_ctrl1;
	u32 reserved4[1];

	u32 rx0_status;
	u32 rx0_header;
	u32 rx1_status;
	u32 rx1_header;
	u32 rx_ctrl;
	u32 rx_ctrl1;
	u32 rx2_status;
	u32 rx2_header;
	u32 reserved5[1];

	u32 phy_ctrl1;
	u32 phy_ctrl2;
	u32 phy_ctrl3;
	u32 phy_status0;
	u32 reserved6[7];

	u32 phy_rcomp0;
	u32 reserved7[3];
	u32 phy_timing0;
	u32 phy_timing1;
	u32 phy_timing2;
	u32 phy_timing3;
	u32 phy_timing4;
	u32 phy_timing5;
	u32 reserved8[2];
	u32 mem_ctrl;
	u32 tx_timer;
	u32 rx_timer;
	u32 turn_timer;
	u32 reserved9[4];

	struct dsi_lcd_regs lcd1;
	u32 reserved10[18];
	struct dsi_lcd_regs lcd2;
};

#define DSI_CTRL_0_CFG_SOFT_RST				(1<<31)
#define DSI_CTRL_0_CFG_SOFT_RST_REG			(1<<30)
#define DSI_CTRL_0_CFG_LCD1_TX_EN			(1<<8)
#define DSI_CTRL_0_CFG_LCD1_SLV				(1<<4)
#define DSI_CTRL_0_CFG_LCD1_EN				(1<<0)

#define DSI_CTRL_1_CFG_EOTP				(1<<8)
#define DSI_CTRL_1_CFG_LCD2_VCH_NO_MASK			(3<<2)
#define DSI_CTRL_1_CFG_LCD2_VCH_NO_SHIFT		2
#define DSI_CTRL_1_CFG_LCD1_VCH_NO_MASK			(3<<0)
#define DSI_CTRL_1_CFG_LCD1_VCH_NO_SHIFT		0

/* LCD 1 Vsync Reset Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_VSYNC_RST_EN		(1<<31)
/* Long Blanking Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HLP_PKT_EN		(1<<22)
/* Front Porch Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HFP_PKT_EN		(1<<20)
/* hact Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HACT_PKT_EN		(1<<19)
/* Back Porch Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HBP_PKT_EN		(1<<18)
/* hse Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HSE_PKT_EN		(1<<17)
/* hsa Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HSA_PKT_EN		(1<<16)
/* Turn Around Bus at Last h Line */
#define	DSI_LCD1_CTRL_1_CFG_L1_LAST_LINE_TURN		(1<<10)
/* Go to Low Power Every Frame */
#define	DSI_LCD1_CTRL_1_CFG_L1_LPM_FRAME_EN		(1<<9)
/* Go to Low Power Every Line */
#define	DSI_LCD1_CTRL_1_CFG_L1_LPM_LINE_EN		(1<<8)
/* DSI Transmission Mode for LCD 1 */
#define DSI_LCD1_CTRL_1_CFG_L1_BURST_MODE_SHIFT		2
/* LCD 1 Input Data RGB Mode for LCD 1 */
#define DSI_LCD2_CTRL_1_CFG_L1_RGB_TYPE_SHIFT		0

/* DPHY Data Lane Enable */
#define	DSI_PHY_CTRL_2_CFG_CSR_LANE_EN_SHIFT		4

/*		Bit(s) DSI_CPU_CMD_1_RSRV_31_24 reserved */
/* LPDT TX Enable */
#define	DSI_CPU_CMD_1_CFG_TXLP_LPDT_SHIFT		20
/* Low Power TX Trigger Code */
#define	DSI_CPU_CMD_1_CFG_TXLP_TRIGGER_CODE_SHIFT	0

/* Length of HS Exit Period in tx_clk_esc Cycles */
#define	DSI_PHY_TIME_0_CFG_CSR_TIME_HS_EXIT_SHIFT	24
/* DPHY HS Trail Period Length */
#define	DSI_PHY_TIME_0_CFG_CSR_TIME_HS_TRAIL_SHIFT	16
/* DPHY HS Zero State Length */
#define	DSI_PHY_TIME_0_CDG_CSR_TIME_HS_ZERO_SHIFT	8

/* Time to Drive LP-00 by New Transmitter */
#define	DSI_PHY_TIME_1_CFG_CSR_TIME_TA_GET_SHIFT	24
/* Time to Drive LP-00 after Turn Request */
#define	DSI_PHY_TIME_1_CFG_CSR_TIME_TA_GO_SHIFT		16

/* DPHY CLK Exit Period Length */
#define	DSI_PHY_TIME_2_CFG_CSR_TIME_CK_EXIT_SHIFT	24
/* DPHY CLK Trail Period Length */
#define	DSI_PHY_TIME_2_CFG_CSR_TIME_CK_TRAIL_SHIFT	16
/* DPHY CLK Zero State Length */
#define	DSI_PHY_TIME_2_CFG_CSR_TIME_CK_ZERO_SHIFT	8

/* DPHY LP Length */
#define	DSI_PHY_TIME_3_CFG_CSR_TIME_LPX_SHIFT		8

/* DSI timings */
#define DSI_ESC_CLK                 66  /* Unit: Mhz */
#define DSI_ESC_CLK_T               15  /* Unit: ns */

/* LVDS */
#define LCD_LVDS_SCLK_DIV_WR		(0x01f4)
#define LCD_LVDS_SCLK_DIV_RD		(0x01fc)

#define LCD_2ND_BLD_CTL				(0x02fc)
#define LVDS_SRC_MASK				(3 << 30)
#define LVDS_SRC_SHIFT				(30)
#define LVDS_FMT_MASK				(1 << 28)
#define LVDS_FMT_SHIFT				(28)

/* LVDS_PHY_CTRL */
#define LVDS_PHY_CTL				0x2a4
#define LVDS_PLL_LOCK				(1 << 31)
#define LVDS_PHY_EXT_MASK			(7 << 28)
#define LVDS_PHY_EXT_SHIFT			(28)
#define LVDS_CLK_PHASE_MASK			(0x7f << 16)
#define LVDS_CLK_PHASE_SHIFT		(16)
#define LVDS_SSC_RESET_EXT			(1 << 13)
#define LVDS_SSC_MODE_DOWN_SPREAD	(1 << 12)
#define LVDS_SSC_EN					(1 << 11)
#define LVDS_PU_PLL					(1 << 10)
#define LVDS_PU_TX					(1 << 9)
#define LVDS_PU_IVREF				(1 << 8)
#define LVDS_CLK_SEL				(1 << 7)
#define LVDS_CLK_SEL_LVDS_PCLK		(1 << 7)
#define LVDS_PD_CH_MASK				(0x3f << 1)
#define LVDS_PD_CH(ch)				((ch) << 1)
#define LVDS_RST					(1 << 0)

#define LVDS_PHY_CTL_EXT			0x2A8

/* LVDS_PHY_CTRL_EXT1 */
#define LVDS_SSC_RNGE_MASK			(0x7ff << 16)
#define LVDS_SSC_RNGE_SHIFT			(16)
#define LVDS_RESERVE_IN_MASK		(0xf << 12)
#define LVDS_RESERVE_IN_SHIFT		(12)
#define LVDS_TEST_MON_MASK			(0x7 << 8)
#define LVDS_TEST_MON_SHIFT			(8)
#define LVDS_POL_SWAP_MASK			(0x3f << 0)
#define LVDS_POL_SWAP_SHIFT			(0)

/* LVDS_PHY_CTRL_EXT2 */
#define LVDS_TX_DIF_AMP_MASK		(0xf << 24)
#define LVDS_TX_DIF_AMP_SHIFT		(24)
#define LVDS_TX_DIF_CM_MASK			(0x3 << 22)
#define LVDS_TX_DIF_CM_SHIFT		(22)
#define LVDS_SELLV_TXCLK_MASK		(0x1f << 16)
#define LVDS_SELLV_TXCLK_SHIFT		(16)
#define LVDS_TX_CMFB_EN				(0x1 << 15)
#define LVDS_TX_TERM_EN				(0x1 << 14)
#define LVDS_SELLV_TXDATA_MASK		(0x1f << 8)
#define LVDS_SELLV_TXDATA_SHIFT		(8)
#define LVDS_SELLV_OP7_MASK			(0x3 << 6)
#define LVDS_SELLV_OP7_SHIFT		(6)
#define LVDS_SELLV_OP6_MASK			(0x3 << 4)
#define LVDS_SELLV_OP6_SHIFT		(4)
#define LVDS_SELLV_OP9_MASK			(0x3 << 2)
#define LVDS_SELLV_OP9_SHIFT		(2)
#define LVDS_STRESSTST_EN			(0x1 << 0)

/* LVDS_PHY_CTRL_EXT3 */
#define LVDS_KVCO_MASK				(0xf << 28)
#define LVDS_KVCO_SHIFT				(28)
#define LVDS_CTUNE_MASK				(0x3 << 26)
#define LVDS_CTUNE_SHIFT			(26)
#define LVDS_VREG_IVREF_MASK		(0x3 << 24)
#define LVDS_VREG_IVREF_SHIFT		(24)
#define LVDS_VDDL_MASK				(0xf << 20)
#define LVDS_VDDL_SHIFT				(20)
#define LVDS_VDDM_MASK				(0x3 << 18)
#define LVDS_VDDM_SHIFT				(18)
#define LVDS_FBDIV_MASK				(0xf << 8)
#define LVDS_FBDIV_SHIFT			(8)
#define LVDS_REFDIV_MASK			(0x7f << 0)
#define LVDS_REFDIV_SHIFT			(0)

/* LVDS_PHY_CTRL_EXT4 */
#define LVDS_SSC_FREQ_DIV_MASK		(0xffff << 16)
#define LVDS_SSC_FREQ_DIV_SHIFT		(16)
#define LVDS_INTPI_MASK				(0xf << 12)
#define LVDS_INTPI_SHIFT			(12)
#define LVDS_VCODIV_SEL_SE_MASK		(0xf << 8)
#define LVDS_VCODIV_SEL_SE_SHIFT	(8)
#define LVDS_RESET_INTP_EXT			(0x1 << 7)
#define LVDS_VCO_VRNG_MASK			(0x7 << 4)
#define LVDS_VCO_VRNG_SHIFT			(4)
#define LVDS_PI_EN					(0x1 << 3)
#define LVDS_ICP_MASK				(0x7 << 0)
#define LVDS_ICP_SHIFT				(0)

/* LVDS_PHY_CTRL_EXT5 */
#define LVDS_FREQ_OFFSET_MASK			(0x1ffff << 15)
#define LVDS_FREQ_OFFSET_SHIFT			(15)
#define LVDS_FREQ_OFFSET_VALID			(0x1 << 2)
#define LVDS_FREQ_OFFSET_MODE_CK_DIV4_OUT	(0x1 << 1)
#define LVDS_FREQ_OFFSET_MODE_EN		(0x1 << 0)

void *pxa168fb_init(struct pxa168fb_mach_info *mi);
void *pxa168fb_hdmi_set_mode(u32 cea_id, u32 enable_3d);
void pxa168fb_vid(u32 enable, u32 xres, u32 yres);


#endif /* __ASM_MACH_PXA168FB_H */
