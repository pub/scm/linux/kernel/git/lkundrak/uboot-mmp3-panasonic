/*
 * (C) Copyright 2010
 * Marvell Semiconductor <www.marvell.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

#ifndef __MV_RECOVERY_H__
#define __MV_RECOVERY_H__

inline void magic_read(void);
void mv_recovery(void);

int magic_key_detect_recovery(void);
#endif /* __MV_RECOVERY_H__ */

