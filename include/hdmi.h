#ifndef _LINUX_HDMI_H
#include <linux/list.h>
#include <linux/fb.h>

extern const struct fb_videomode cea_modes[65];

typedef u32 cea_mode_id;

struct hdmi_dev {
	struct fb_info          *fb_info;
	cea_mode_id		mode_id;
	u32                     enable;
	u32                     mode_3d;
	u32                     pixel_rept;
	u32                     ratio;
};



void hdmi_init(struct fb_var_screeninfo *var, u32 id, u32 enable_3d);
int hdmi_vender_info_frame (struct hdmi_dev *hd, char buf[]);
int hdmi_avi_info_frame (struct hdmi_dev *hd, char buf[]);
int hdmi_get_freq (struct hdmi_dev *hd, u32 *need_freq);
void *pxa168fb_hdmi_set_mode(u32 cea_id, u32 enable_3d);
#endif
