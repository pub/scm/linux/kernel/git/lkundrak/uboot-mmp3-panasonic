/*
 * Copyright 2011, Marvell Semiconductor Inc.
 * Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 * Back ported to the 8xx platform (from the 8260 platform) by
 * Murray.Jensen@cmst.csiro.au, 27-Jan-01.
 */

#include <common.h>
#include <fastboot.h>
#include <asm/errno.h>
#include <linux/usb/ch9.h>
#include <linux/usb/cdc.h>
#include <linux/usb/gadget.h>
#include "gadget_chips.h"
#include <mmc.h>
#include "../../../../../kernel/kernel/arch/arm/mach-mmp/include/mach/wistron.h"

#ifdef CONFIG_USB_GADGET_DUALSPEED
#define DEVSPEED	USB_SPEED_HIGH
#else
#define DEVSPEED	USB_SPEED_FULL
#endif

static char CUSTOMER_SERIAL_NUM[SN_SIZE] = "";

struct fb_dev {
	struct usb_gadget	*gadget;
	struct usb_request	*req;		/* for control responses */
	u8			config;
	struct usb_ep		*in_ep, *out_ep;
	const struct usb_endpoint_descriptor
				*in, *out;
	struct usb_request	*tx_req, *rx_req;
};

static u8 control_req[512];
struct fb_dev l_fbdev;
unsigned fb_packet_sent;
static void tx_complete(struct usb_ep *ep, struct usb_request *req)
{
	fb_packet_sent = 1;
}

void fb_tx_data(void *data, unsigned long len)
{
	struct fb_dev *dev = &l_fbdev;
	struct usb_request *req = dev->tx_req;
	struct usb_ep *tx_ep = dev->in_ep;

	while (len > 0) {
		req->length = (len > tx_ep->maxpacket)
			? tx_ep->maxpacket : len;
		req->buf = data;
		req->context = NULL;
		req->complete = tx_complete;
		fb_packet_sent = 0;
		usb_ep_queue(dev->in_ep, req, 0);
		while (!fb_packet_sent)
			usb_gadget_handle_interrupts();

		data += req->length;
		len -= req->length;
	}
}

void fb_tx_status(const char *status)
{
	fb_tx_data((void *)status, strlen(status));
}

static struct usb_device_descriptor device_desc = {
	.bLength = sizeof(struct usb_device_descriptor),
	.bDescriptorType =	USB_DT_DEVICE,
	.bcdUSB =		__constant_cpu_to_le16(0x0200),

	.idVendor =		__constant_cpu_to_le16(CONFIG_USBD_VENDORID),
	.idProduct =		__constant_cpu_to_le16(CONFIG_USBD_PRODUCTID),
	.iManufacturer =	0x1,
	.iProduct =		0x2,
	.iSerialNumber =	0x3,
	.bNumConfigurations =	1
};

static struct usb_config_descriptor fb_config = {
	.bLength = sizeof(struct usb_config_descriptor),
	.bDescriptorType = USB_DT_CONFIG,

	.bNumInterfaces = 1,
	.bConfigurationValue = 1,
	.iConfiguration = 4,
	.bmAttributes = USB_CONFIG_ATT_ONE,
	.bMaxPower = 0x80
};

static struct usb_interface_descriptor control_intf = {
	.bLength  = sizeof(struct usb_interface_descriptor),
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = 0,
	.bNumEndpoints = 0x2,
	.bInterfaceClass = USB_CLASS_VENDOR_SPEC,
	.bInterfaceSubClass = 0x42,
	.bInterfaceProtocol = USB_CLASS_HID,
};

static struct usb_endpoint_descriptor fb_source_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,

	.bEndpointAddress = USB_DIR_IN,
	.bmAttributes =	USB_ENDPOINT_XFER_BULK,
};

static struct usb_endpoint_descriptor fb_sink_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,

	.bEndpointAddress = USB_DIR_OUT,
	.bmAttributes =	USB_ENDPOINT_XFER_BULK,
};

static const struct usb_descriptor_header *fb_function[4] = {
	(struct usb_descriptor_header *)&control_intf,
	(struct usb_descriptor_header *)&fb_source_desc,
	(struct usb_descriptor_header *)&fb_sink_desc,
	NULL,
};

static struct usb_qualifier_descriptor dev_qualifier = {
	.bLength =		sizeof dev_qualifier,
	.bDescriptorType =	USB_DT_DEVICE_QUALIFIER,

	.bcdUSB =		__constant_cpu_to_le16(0x0200),
	.bDeviceClass =		USB_CLASS_VENDOR_SPEC,
	.bMaxPacketSize0 =	64,

	.bNumConfigurations =	1,
};

static struct usb_string strings[] = {
	{ 1,	CONFIG_USBD_MANUFACTURER, },
	{ 2,	CONFIG_USBD_PRODUCT_NAME, },
	{ 3,	CUSTOMER_SERIAL_NUM, },
	{ 4,	CONFIG_USBD_CONFIGURATION_STR, },
	{0, NULL},
};

static struct usb_gadget_strings	stringtab = {
	.language	= 0x0409,	/* en-us */
	.strings = strings,
};

static void fb_setup_complete(struct usb_ep *ep, struct usb_request *req)
{
}

static int fb_bind(struct usb_gadget *gadget)
{
	struct fb_dev *dev = &l_fbdev;
	struct usb_ep	*in_ep, *out_ep;
	int gcnum;
	gcnum = usb_gadget_controller_number(gadget);
	if (gcnum >= 0)
		device_desc.bcdDevice = cpu_to_le16(0x0300 + gcnum);
	else {
		/*
		 * can't assume CDC works.  don't want to default to
		 * anything less functional on CDC-capable hardware,
		 * so we fail in this case.
		 */
		error("controller '%s' not recognized",
			gadget->name);
		return -ENODEV;
	}

	/* all we really need is bulk IN/OUT */
	usb_ep_autoconfig_reset(gadget);
	in_ep = usb_ep_autoconfig(gadget, &fb_source_desc);
	if (!in_ep) {
		error("can't autoconfigure on %s\n",
			gadget->name);
		return -ENODEV;
	}
	in_ep->driver_data = in_ep;	/* claim */

	out_ep = usb_ep_autoconfig(gadget, &fb_sink_desc);
	if (!out_ep) {
		error("can't autoconfigure on %s\n",
			gadget->name);
		return -ENODEV;
	}
#ifdef CONFIG_USB_GADGET_DUALSPEED
	fb_source_desc.wMaxPacketSize = __constant_cpu_to_le16(512);
	fb_sink_desc.wMaxPacketSize = fb_source_desc.wMaxPacketSize;
#endif
	out_ep->driver_data = out_ep;	/* claim */
	device_desc.bMaxPacketSize0 = gadget->ep0->maxpacket;
	usb_gadget_set_selfpowered(gadget);
	dev->in_ep = in_ep;
	dev->out_ep = out_ep;

	dev->gadget = gadget;
	set_gadget_data(gadget, dev);
	gadget->ep0->driver_data = dev;
	dev->req = usb_ep_alloc_request(gadget->ep0, 0);
	dev->req->buf = control_req;
	dev->req->complete = fb_setup_complete;
	dev->tx_req = usb_ep_alloc_request(dev->in_ep, 0);
	dev->rx_req = usb_ep_alloc_request(dev->out_ep, 0);
	return 0;
}

static void fb_unbind(struct usb_gadget *gadget)
{
	return;
}

static void rx_complete(struct usb_ep *ep, struct usb_request *req)
{
	rcv_cmd();
	req->length = 512;
	req->complete = rx_complete;
	usb_ep_queue(ep, req, 0);
}

static int
fb_setup(struct usb_gadget *gadget, const struct usb_ctrlrequest *ctrl)
{
	struct fb_dev		*dev = get_gadget_data(gadget);
	struct usb_request	*req = dev->req;
	int			value = -EOPNOTSUPP;
	u16			wValue = le16_to_cpu(ctrl->wValue);
	u16			wLength = le16_to_cpu(ctrl->wLength);

	switch (ctrl->bRequest) {
	case USB_REQ_GET_DESCRIPTOR:
		if (ctrl->bRequestType != USB_DIR_IN)
			break;
		switch (wValue >> 8) {

		case USB_DT_DEVICE:
			value = min(wLength, (u16) sizeof device_desc);
			memcpy(req->buf, &device_desc, value);
			break;
		case USB_DT_CONFIG:
			value  = usb_gadget_config_buf(&fb_config, req->buf,
					512, fb_function);
			if (value >= 0)
				value = min(wLength, (u16) value);
			break;

		case USB_DT_STRING:
			value = usb_gadget_get_string(&stringtab,
					wValue & 0xff, req->buf);

			if (value >= 0)
				value = min(wLength, (u16) value);

			break;
		case USB_DT_DEVICE_QUALIFIER:
			value = min(wLength, (u16) sizeof dev_qualifier);
			memcpy(req->buf, &dev_qualifier, value);
			break;
		}
		break;
	case USB_REQ_SET_CONFIGURATION:
		dev->out = &fb_sink_desc;
		dev->in = &fb_source_desc;
		dev->rx_req->length = 512;
		dev->rx_req->complete = rx_complete;
		usb_ep_enable(dev->out_ep, dev->out);
		usb_ep_enable(dev->in_ep, dev->in);
		value = usb_ep_queue(dev->out_ep, dev->rx_req, 0);
		if (value)
			error("rx submit --> %d", value);
		value = 0;
		break;
	}

	/* respond with data transfer before status phase? */
	if (value >= 0) {
		req->length = value;
		req->zero = value < wLength
				&& (value % gadget->ep0->maxpacket) == 0;
		value = usb_ep_queue(gadget->ep0, req, 0);
		if (value < 0) {
			debug("ep_queue --> %d\n", value);
			req->status = 0;
		}
	}

	/* host either stalls (value < 0) or reports success */
	return value;
}

static void fb_disconnect(struct usb_gadget *gadget)
{
	return;
}

void fb_halt(void)
{
	struct fb_dev *dev = &l_fbdev;
	struct usb_gadget *gadget = dev->gadget;
	usb_gadget_disconnect(gadget);
}

static struct usb_gadget_driver fb_driver = {
	.speed		= DEVSPEED,

	.bind		= fb_bind,
	.unbind		= fb_unbind,

	.setup		= fb_setup,
	.disconnect	= fb_disconnect,
};

void fb_init(void)
{
	struct fb_dev *dev = &l_fbdev;
	struct usb_gadget *gadget;
	char *s;
	s = getenv("fb_serial");
	if (s != NULL) {
		printf("Fastboot serial no set as:%s\n", s);
		strings[2].s = s;
	}
	usb_gadget_register_driver(&fb_driver);
	gadget = dev->gadget;
	usb_gadget_connect(dev->gadget);


#if 1	//Read customer serial number for "fastboot devices"
	struct mmc *mmc_0;
	char nvs_block[EMMC_BLOCK_SIZE];
        mmc_0 = find_mmc_device(0);
        if (mmc_0) {
                if (mmc_init(mmc_0)){
                        printf("MMC 0 card init failed!\n");
                        return 0;
                }
        }
	mmc_0->block_dev.block_read(0, NVS_OFFSET/EMMC_BLOCK_SIZE, 1, (struct WISTRON_NVS *)nvs_block);
	
	struct WISTRON_NVS *wis_nvs = nvs_block;
	
	if (strlen(wis_nvs->sn_customer) == 0) {
		strcpy(CUSTOMER_SERIAL_NUM, "FZA1B");
	} else {		
		strcpy(CUSTOMER_SERIAL_NUM, wis_nvs->sn_customer);
	}
#endif
}

void fb_run(void)
{
	usb_gadget_handle_interrupts();
}

int fb_get_rcv_len(void)
{
	struct fb_dev *dev = &l_fbdev;
	struct usb_request *req = dev->rx_req;
	return req->length;
}

void *fb_get_buf(void)
{
	struct fb_dev *dev = &l_fbdev;
	struct usb_request *req = dev->rx_req;
	return req->buf;
}

void fb_set_buf(void *buf)
{
	struct fb_dev *dev = &l_fbdev;
	struct usb_request *req = dev->rx_req;
	req->buf = buf;
}
