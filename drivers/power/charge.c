/*
 * (C) Copyright 2000
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 */

#include <common.h>
#include "max17042_battery.h"
#include "battery00.h"
#include "green_left.h"
#include "green_mid.h"
#include "green_right.h"

#define REBOOT_CMD_REG		0xd4010018
#define REBOOT_CMD_REBOOT	0x01
#define REBOOT_CMD_PWROFF	0x02

#define ANIMATION_THRESHOLD	3
#define ANDROID_THRESHOLD	15
#define CHG_CURRENT_THRESHOLD	(-30000)
#define CHARGE_CYCLE_US		1000000
#define LCD_TIMEOUT_CYCLE	7
#define FORCED_BOOTUP_CYCLE	3

#define GPIO86 86

#ifdef CONFIG_PXA27X_KEYPAD
static enum key{
	HOME,
	MENU,
	BACK,
	VOL_DOWN,
	VOL_UP,
	DOWN_UP,
	NONE,
};
extern inline enum key pxa27x_key_read(void);
#endif

static int read_battery_capacity(void)
{
	if (board_is_mmp2_brownstone_rev5()) {
#ifdef CONFIG_MAX17042_BATTERY
		return max17042_get_capacity();
#else
		return 0;
#endif
	} else
	{
		return 0;
	}
}

static inline int battery_is_charging(void)
{
	int n = 0;
	int chg = 0;

#ifdef CONFIG_MAX17042_BATTERY
	/* judge whether current is smaller than CHG_CURRENT_THRESHOLD
	   for 5 times sampling */
	while (n < 5) {
		if (max17042_get_current() > CHG_CURRENT_THRESHOLD) {
			if (chg == 0) {
				n = 0;
				chg = 1;
			} else {
				udelay(10000);
				n++;
			}
		} else {
			if (chg == 1) {
				n = 0;
				chg = 0;
			} else {
				udelay(10000);
				n++;
			}
		}
	}
	return chg;
#else
	return 0;
#endif
}

static inline int onkey_is_pressed(void)
{
	u8 data, t;
	int status;

	if (machine_is_brownstone()) {
		i2c_set_bus_num(0);
		int res = i2c_read(0x3c, 0x03, 1, &data, 1);

		if (data & 0x80)
			return 1;
		else
			return 0;
	}
}

static inline int check_dc_irq(void)
{
	u8 data, t;
	int status;

	if (machine_is_brownstone()) {
		i2c_set_bus_num(0);
		int res = i2c_read(0x3c, 0x7e, 1, &data, 1);

		if (data & 0x04)
			return 1;
		else
			return 0;
	}
}

extern void show_charge_logo(unsigned char *logo, int wide, int high, int offset);

void show_capacity(int capacity)
{
	int i = 0, round = 1;

	if (capacity >= 0 && capacity < 20) {
		for(i = 0; i < round; i++) {
			show_charge_logo(RES_battery00_BIN, 173, 71, 0);
			udelay(CHARGE_CYCLE_US);
			udelay(CHARGE_CYCLE_US);
			show_charge_logo(RES_green_left_BIN, 25, 65, 138);
			udelay(CHARGE_CYCLE_US);
			udelay(CHARGE_CYCLE_US);
		}
	} else if (capacity >= 20 && capacity < 40) {
		for(i = 0; i < round; i++) {
			show_charge_logo(RES_battery00_BIN, 173, 71, 0);
			show_charge_logo(RES_green_left_BIN, 25, 65, 138);
			udelay(CHARGE_CYCLE_US);
			udelay(CHARGE_CYCLE_US);
			show_charge_logo(RES_green_mid_BIN, 27, 65, 76);
			udelay(CHARGE_CYCLE_US);
			udelay(CHARGE_CYCLE_US);
		}
	} else if (capacity >= 40 && capacity < 60) {
		for(i = 0; i < round; i++) {
			show_charge_logo(RES_battery00_BIN, 173, 71, 0);
			show_charge_logo(RES_green_left_BIN, 25, 65, 138);
			show_charge_logo(RES_green_mid_BIN, 27, 65, 76);
			udelay(CHARGE_CYCLE_US);
			udelay(CHARGE_CYCLE_US);
			show_charge_logo(RES_green_mid_BIN, 27, 65, 14);
			udelay(CHARGE_CYCLE_US);
			udelay(CHARGE_CYCLE_US);
			}
	} else if (capacity >= 60 && capacity < 80) {
		for(i = 0; i < round; i++) {
			show_charge_logo(RES_battery00_BIN, 173, 71, 0);
			show_charge_logo(RES_green_left_BIN, 25, 65, 138);
			show_charge_logo(RES_green_mid_BIN, 27, 65, 76);
			show_charge_logo(RES_green_mid_BIN, 27, 65, 14);
			udelay(CHARGE_CYCLE_US);
			udelay(CHARGE_CYCLE_US);
			show_charge_logo(RES_green_mid_BIN, 27, 65, -48);
			udelay(CHARGE_CYCLE_US);
			udelay(CHARGE_CYCLE_US);
		}
	} else if (capacity >= 80 && capacity < 100) {
		for(i = 0; i < round; i++) {
			show_charge_logo(RES_battery00_BIN, 173, 71, 0);
			show_charge_logo(RES_green_left_BIN, 25, 65, 138);
			show_charge_logo(RES_green_mid_BIN, 27, 65, 76);
			show_charge_logo(RES_green_mid_BIN, 27, 65, 14);
			show_charge_logo(RES_green_mid_BIN, 27, 65, -48);
			udelay(CHARGE_CYCLE_US);
			udelay(CHARGE_CYCLE_US);
			show_charge_logo(RES_green_right_BIN, 25, 65, -110);
			udelay(CHARGE_CYCLE_US);
			udelay(CHARGE_CYCLE_US);
		}

	} else if (capacity >= 100){
		for(i = 0; i < round; i++) {
			show_charge_logo(RES_battery00_BIN, 173, 71, 0);
			show_charge_logo(RES_green_left_BIN, 25, 65, 138);
			show_charge_logo(RES_green_mid_BIN, 27, 65, 76);
			show_charge_logo(RES_green_mid_BIN, 27, 65, 14);
			show_charge_logo(RES_green_mid_BIN, 27, 65, -48);
			show_charge_logo(RES_green_right_BIN, 25, 65, -110);
			udelay(CHARGE_CYCLE_US);
			udelay(CHARGE_CYCLE_US);
		}
	}
}

extern void *lcd_init(void);
extern void lcd_flush(void);
extern void *close_lcd(void);
void show_lcd_animation(int charge_state)
{
	static int n_cycles = 0;

	if (charge_state == 1) {
		/* need to show lcd annimation here */
		if (n_cycles == 0) {
			/* open lcd and display */
			lcd_init();
			udelay(CHARGE_CYCLE_US);
		}
		n_cycles = LCD_TIMEOUT_CYCLE;
	} else {
		/* need to close lcd to save power */
		if (n_cycles == 1) {
			/* close lcd */
			close_lcd();
		}
		/* keep lcd on/off and do nothing */
		if (n_cycles != 0) {
			n_cycles--;
			show_capacity(read_battery_capacity());
		}
	}
}

static int do_charge_cycle(void)
{
	int t;
	int charge_state = 0;	/* 0: normal; 1: show lcd animation; 2: force system to boot up */
				/* 3: force system to boot up unconditionally */
	unsigned char chg_data[2], data[2];

#ifdef CONFIG_MAX17042_BATTERY
	printf("%s: VOL:%duV CUR:%duA CAP:%d%", __func__, \
		max17042_get_voltage(), max17042_get_current(), max17042_get_capacity());
	/* align and clear charge info */
	printf("                \r");
#endif
	/* write charger reg */
	if (board_is_mmp2_brownstone_rev5()) {
		chg_data[0] = 0x00;
		chg_data[1] = 0x08;
		i2c_set_bus_num(0);
		int res = i2c_write(0x09, 0x14, 1, chg_data, 2);
	}

	if (onkey_is_pressed()) {
		for (t = 0; t < FORCED_BOOTUP_CYCLE; t++) {
			udelay(CHARGE_CYCLE_US);
			if (!onkey_is_pressed())
				break;
		}
		if (t == FORCED_BOOTUP_CYCLE)
			charge_state = 2;
		else
			charge_state = 1;
	}
#ifdef CONFIG_PXA27X_KEYPAD
	if (pxa27x_key_read() == DOWN_UP) {
		for (t = 0; t < FORCED_BOOTUP_CYCLE; t++) {
			udelay(CHARGE_CYCLE_US);
			if (pxa27x_key_read() != DOWN_UP)
				break;
		}
		if (t == FORCED_BOOTUP_CYCLE);
			charge_state = 3;
	}
#endif
	if (!charge_state)
		udelay(CHARGE_CYCLE_US);

	return charge_state;
}

static void led_enable(int en)
{
	unsigned int gpio;

	if (board_is_mmp2_brownstone_rev5()) {
		gpio_direction_output(GPIO86, !en);
	}
	return ;
}

void charge_reset(void)
{
	int n = 0;
	unsigned char chg_data[2];

#ifdef CONFIG_MAX17042_BATTERY
	/* abnormal case: current is always zero*/
	while (n < 10) {
		if (max17042_get_current() == 0) {
			n++;
			udelay(10000);
			continue;
		} else
			break;
	}
	if (n == 10) {
		/* charger needs to be reset here */
		printf("fuel-gauge reseting...");
		max17042_reset();
		printf("done\n");
	}
#else
	return;
#endif
}

void charge_detect(void)
{
	int cap, i;
	int chg_to_full = 1;	/* 0: try to boot up if threshold met; 1: charge until full capacity */
	int charge_state = 1;	/* 0: normal; 1: show lcd animation; 2: force system to boot up */
				/* 3: force system to boot up unconditionally */
	unsigned char chg_data[2], data[2];

	/* stop charger first */
	if (board_is_mmp2_brownstone_rev5()) {
		chg_data[0] = 0x00;
		chg_data[1] = 0x00;
		i2c_set_bus_num(0);
		int res = i2c_write(0x09, 0x14, 1, chg_data, 2);

		/* wait for charger stopped totally */
		udelay(100000);
	}

	/* return to normal flow if no battery */
	if (read_battery_capacity() < 0) {
		printf("No battery on board.\n");
		return ;
	}

	charge_reset();

	/* in such case, uboot will try to boot up android */
	/* 1) DC is in; 2) no DC plugging irq 3) not triggered by power off cmd */
	/* due to reset button pressed */
	if (battery_is_charging() && !check_dc_irq() && \
	    *(volatile unsigned int *)REBOOT_CMD_REG != REBOOT_CMD_PWROFF)
		chg_to_full = 0;

	/* try to boot up kernel
	   if it is triggered by reboot cmd in last power cycle */
	if (*(volatile unsigned int *)REBOOT_CMD_REG == REBOOT_CMD_REBOOT)
		chg_to_full = 0;

	*(volatile unsigned int *)REBOOT_CMD_REG = 0;

	if (!battery_is_charging()) {
		chg_to_full = 0;
		cap = read_battery_capacity();
		if (cap >= ANDROID_THRESHOLD) {
			/* return to normal flow if battery is not in charging
			   and with enough capacity */
			return;
		} else {
			/* show some notice and wait, or shut down system directly here */
			printf("Low battery! DC is needed.\n");
			lcd_init();
			for (i = 0; i < 20; i++) {
				if (battery_is_charging())
					break;
				show_charge_logo(RES_battery00_BIN, 173, 71, 0);
				udelay(CHARGE_CYCLE_US);
			}
		}
	}

	/* get here if battery is in charge */
	printf("Battery is in charging...\n");
	led_enable(1);

	while(1) {
		cap = read_battery_capacity();
		if (cap < 0)
			break;

		if (((charge_state == 2 || chg_to_full == 0) && cap >= ANDROID_THRESHOLD) || \
			charge_state == 3) {
			lcd_flush();
			break;
		}
		if (!battery_is_charging()) {
			/* shut down system */
			max8925_power_off();
		} else if (cap < ANIMATION_THRESHOLD) {
			close_lcd();
			charge_state = do_charge_cycle();
			continue;
		} else if (cap < ANDROID_THRESHOLD) {
			show_lcd_animation(charge_state);
			charge_state = do_charge_cycle();
			continue;
		} else if (chg_to_full) {
			/* continue charging since bootup reason is DC pulugged in */
			show_lcd_animation(charge_state);
			charge_state = do_charge_cycle();
			continue;
		} else {
			break;
		}
	}
	/* return to normal flow with enough battery capacity */
	led_enable(0);

	/* stop charger */
	if (board_is_mmp2_brownstone_rev5()) {
		chg_data[0] = 0x00;
		chg_data[1] = 0x00;
		i2c_set_bus_num(0);
		int res = i2c_write(0x09, 0x14, 1, chg_data, 2);
	}

	return;
}
