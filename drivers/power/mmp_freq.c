/*
 *  U-Boot command for frequency change support
 *
 *  Copyright (C) 2008, 2009 Marvell International Ltd.
 *  All Rights Reserved
 *  Ning Jiang <ning.jiang@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <common.h>
#include <command.h>
#include <asm/io.h>
#include "ddr.h"
#include <i2c.h>
DECLARE_GLOBAL_DATA_PTR;

#ifndef CONFIG_MACH_MMP2

#if defined(CONFIG_MACH_ABILENE)

struct platform_ddr_setting ddr_setting[] = {
	{
		.ddr_freq = 400,
		.setting = {
			.reg1 = 0x911403CF,
			.reg2 = 0x64660414,
			.reg3 = 0xC2003053,
			.reg4 = 0x34F4A187,
			.reg5 = 0x000F20C1,
			.reg6 = 0x04040200,
			.reg7 = 0x00005501,
		},
	},
	{
		.ddr_freq = 531,
		.setting = {
			.reg1 = 0x911B03CF,
			.reg2 = 0x74780564,
			.reg3 = 0xC200406C,
			.reg4 = 0x3694DA09,
			.reg5 = 0x00142101,
			.reg6 = 0x04040200,
			.reg7 = 0x00006601,
		},
	},
};

#elif defined(CONFIG_MACH_YELLOWSTONE)

struct platform_ddr_setting ddr_setting[] = {
	{
		.ddr_freq = 400,
		.setting = {
			.reg1 = 0x911403CF,
			.reg2 = 0x64660404,
			.reg3 = 0xC2004453,
			.reg4 = 0x34F4A187,
			.reg5 = 0x000F20C1,
			.reg6 = 0x04040200,
			.reg7 = 0x00005501,
		},
	},
	{
		.ddr_freq = 531,
		.setting = {
			.reg1 = 0x911B03CF,
			.reg2 = 0x84880564,
			.reg3 = 0xC2004B6C,
			.reg4 = 0x3694DA09,
			.reg5 = 0x00142101,
			.reg6 = 0x04040200,
			.reg7 = 0x00006601,
		},
	},
};

#elif defined(CONFIG_MACH_MK2) || defined(CONFIG_MACH_ORCHID)

struct platform_ddr_setting ddr_setting[] = {
	{
		.ddr_freq = 200,
		.setting = {
			.reg1 = 0x488E0232,
			.reg2 = 0x524301A5,
			.reg3 = 0x201C1C12,
			.reg4 = 0x3012804F,
			.reg5 = 0x0A0900A1,
			.reg6 = 0x04040200,
			.reg7 = 0x00005201,
			.reg8 = 0x00000033,
		},
	},
	{
		.ddr_freq = 400,
		.setting = {
			.reg1 = 0x4CDA00C5,
			.reg2 = 0x94860342,
			.reg3 = 0x2000381B,
			.reg4 = 0x3023009D,
			.reg5 = 0x20110142,
			.reg6 = 0x02424190,
			.reg7 = 0x00005501,
			.reg8 = 0x00000066,
		},
	},
};

#else
#error "unknown machine for DFC.\n"
#endif

#endif

#ifndef CONFIG_MACH_MMP2
#define PLL1_DIV_BY_2		400
#define PLL1			800
#define PLL1_CLKOUT_P		1063
#define PLL2			1066
#define VCXO                    26
#define REF_VOLT		1275
#define STEP			6
#define VHIGH			1299
/* A1 MMP3 currently operates vcore above 1200mV*/
#define VLOW			1299

#define	PMUA_BASE		0xD4282800
#define	PMUM_BASE		0xD4050000
#define MCU1_BASE		0xd0000000
#define MCU2_BASE		0xd0010000

#define	PMUA_DM_CC_MOH		(PMUA_BASE+0x000C)
#define	PMUA_DM2_CC_MOH		(PMUA_BASE+0x0158)
#define	PMUA_DM_CC_SEA		(PMUA_BASE+0x0008)
#define	PMUA_DM2_CC_SEA		(PMUA_BASE+0x0154)

#define	PMUA_CC_MOH		(PMUA_BASE+0x0004)
#define PMUA_CC2_MOH		(PMUA_BASE+0x0150)
#define PMUA_CC3_MOH		(PMUA_BASE+0x0188)

#define	PMUA_CC_SEA		(PMUA_BASE+0x0000)
#define	PMUA_CC2_SEA		(PMUA_BASE+0x014c)

#define	PMUA_PLL_SEL_STATUS	(PMUA_BASE+0x00C4)
#define PMUA_CORE_STATUS	(PMUA_BASE+0x0090)

#define	PMUA_MOH_IMR		(PMUA_BASE+0x0098)
#define	PMUA_MOH_ISR		(PMUA_BASE+0x00A0)
#define	PMUA_PJ_IRWC		(PMUA_BASE+0x009c)

#define PMUM_CGR_SP		(PMUM_BASE+0x0024)
#define PMUM_CGR_PJ		(PMUM_BASE+0x1024)

#define PMUM_DEBUG_1		(PMUA_BASE+0x0088)
#define PMUM_DEBUG_2		(PMUA_BASE+0x0190)

#define	PMUM_PLL2CR		(PMUM_BASE+0x0034)
#define	PMUM_PLL2CR1		(PMUM_BASE+0x0414)
#define	PMUM_PLL2CR2		(PMUM_BASE+0x0418)
#define	PMUM_PLL2CR3		(PMUM_BASE+0x041c)
#define PMUM_PLL_DIFF_CNTRL	(PMUM_BASE+0x0068)
#define PMUA_MC_SLP_REQ_PJ	(PMUA_BASE+0x00b4)
#define PMUA_MC_PAR_CTRL	(PMUA_BASE+0x011c)
#define PMUA_BUS_CLK_RES_CTRL	(PMUA_BASE+0x006c)
#define	PMUM_FCCR		(PMUM_BASE+0x0008)

struct operating_point {
	int op_id;
	u32 pclk;
	u32 pclk_mm;
	u32 baclk;
	u32 dclk;
	u32 dclk2;
	u32 dclk_lp;
	u32 dclk_ddr;
	u32 aclk;
	u32 aclk2;
	u32 phclk;
	u32 atclk;
	u32 cp_pclk;
	u32 cp_baclk;
	u32 cp_clk_src;
	u32 ap_clk_src;
	u32 ddr_clk_src;
	u32 ddr_clk_src_lp;
	u32 ddr_clk_src_ddr;
	u32 axi_clk_src;
	u32 pll2freq;
	u32 pll1clkoutp;
	u32 pdclk;
	u32 xpclk;
	u32 cp_pdclk;
	u32 gc_clk_src;
	u32 vcore;
};

#define OP(p, pd, ba, xp, d, a, v) \
	{			\
	.pclk	= p,		\
	.pdclk	= pd,		\
	.baclk	= ba,		\
	.xpclk	= xp,		\
	.dclk	= d,		\
	.aclk	= a,		\
	.vcore	= v,		\
	}

#define OP_MMP3(p, pm, ba, dlp, dddr3, a1, a2, ph, at) \
	{			\
	.pclk = p,		\
	.pclk_mm = pm,		\
	.baclk	= ba,		\
	.dclk = dlp,		\
	.dclk_ddr3 = dddr3,	\
	.aclk	= a1,		\
	.aclk2	= a2,		\
	.phclk = ph,		\
	.atclk = at,		\
	}

struct operating_point pxa2128_op_array[] = {
	{
		.op_id = 0,
		.pclk = 100,
		.pclk_mm = 100,
		.baclk = 100,
		.dclk_lp = 200,
		.dclk_ddr = 400,
		.aclk = 200,
		.aclk2 = 100,
		.phclk = 1, /*6.5MHz*/
		.atclk = 133,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_DIV_BY_2,
		.ddr_clk_src_lp = PLL1_DIV_BY_2,
		.ddr_clk_src_ddr = PLL1,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = -1,
		.vcore = VLOW,
	},

	{
		.op_id = 1,
		.pclk = 26,
		.pclk_mm = 26,
		.baclk = 50,
		.dclk_lp = 200,
		.dclk_ddr = 400,
		.aclk = 50,
		.aclk2 = 50,
		.phclk = 1, /*6.5MHz*/
		.atclk = 13,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = VCXO,
		.ddr_clk_src_lp = PLL1_DIV_BY_2,
		.ddr_clk_src_ddr = PLL1,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = -1,
		.vcore = VLOW,
	},

	{
		.op_id = 2,
		.pclk = 50,
		.pclk_mm = 50,
		.baclk = 50,
		.dclk_lp = 200,
		.dclk_ddr = 400,
		.aclk = 100,
		.aclk2 = 100,
		.phclk = 1, /*100MHz*/
		.atclk = 133,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_DIV_BY_2,
		.ddr_clk_src_lp = PLL1_DIV_BY_2,
		.ddr_clk_src_ddr = PLL1,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = -1,
		.vcore = VLOW,
	},

	{
		.op_id = 3,
		.pclk = 100,
		.pclk_mm = 50,
		.baclk = 50,
		.dclk_lp = 200,
		.dclk_ddr = 400,
		.aclk = 100,
		.aclk2 = 100,
		.phclk = 1, /*100MHz*/
		.atclk = 133,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_DIV_BY_2,
		.ddr_clk_src_lp = PLL1_DIV_BY_2,
		.ddr_clk_src_ddr = PLL1,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = -1,
		.vcore = VLOW,
	},

	{
		.op_id = 4,
		.pclk = 200,
		.pclk_mm = 200,
		.baclk = 200,
		.dclk_lp = 200,
		.dclk_ddr = 400,
		.aclk = 200,
		.aclk2 = 100,
		.phclk = 1, /*100MHz*/
		.atclk = 133,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_DIV_BY_2,
		.ddr_clk_src_lp = PLL1_DIV_BY_2,
		.ddr_clk_src_ddr = PLL1,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = -1,
		.vcore = VLOW,
	},

	{
		.op_id = 5,
		.pclk = 400,
		.pclk_mm = 200,
		.baclk = 400,
		.dclk_lp = 200,
		.dclk_ddr = 400,
		.aclk = 400,
		.aclk2 = 200,
		.phclk = 1, /*100MHz*/
		.atclk = 133,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_DIV_BY_2,
		.ddr_clk_src_lp = PLL1_DIV_BY_2,
		.ddr_clk_src_ddr = PLL1,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = -1,
		.vcore = VLOW,
	},

	{
		.op_id = 6,
		.pclk = 800,
		.pclk_mm = 400,
		.baclk = 400,
		.dclk_lp = 200,
		.dclk_ddr = 400,
		.aclk = 400,
		.aclk2 = 200,
		.phclk = 1, /*200MHz*/
		.atclk = 265,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1,
		.ddr_clk_src_lp = PLL1_DIV_BY_2,
		.ddr_clk_src_ddr = PLL1,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = -1,
		.vcore = VHIGH,
	},

	{
		.op_id = 7,
		.pclk = 531,
		.pclk_mm = 265,
		.baclk = 531,
		.dclk_lp = 200,
		.dclk_ddr = 400,
		.aclk = 400,
		.aclk2 = 200,
		.phclk = 2, /*177MHz*/
		.atclk = 354,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_CLKOUT_P,
		.ddr_clk_src_lp = PLL1_DIV_BY_2,
		.ddr_clk_src_ddr = PLL1,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = PLL1_CLKOUT_P,
		.vcore = VHIGH,
	},

	{
		.op_id = 8,
		.pclk = 1063,
		.pclk_mm = 265,
		.baclk = 531,
		.dclk_lp = 200,
		.dclk_ddr = 400,
		.aclk = 400,
		.aclk2 = 200,
		.phclk = 2, /*177MHz*/
		.atclk = 354,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_CLKOUT_P,
		.ddr_clk_src_lp = PLL1_DIV_BY_2,
		.ddr_clk_src_ddr = PLL1,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = PLL1_CLKOUT_P,
		.vcore = VHIGH,
	},

	{
		.op_id = 9,/*PP 5_2*/
		.pclk = 400,
		.pclk_mm = 400,
		.baclk = 400,
		.dclk_lp = 200,
		.dclk_ddr = 400,
		.aclk = 400,
		.aclk2 = 200,
		.phclk = 1, /*100MHz*/
		.atclk = 133,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_DIV_BY_2,
		.ddr_clk_src_lp = PLL1_DIV_BY_2,
		.ddr_clk_src_ddr = PLL1,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = -1,
		.vcore = VHIGH,
	},

	{
		.op_id = 10,
		.pclk = 100,
		.pclk_mm = 100,
		.baclk = 100,
		.dclk_lp = 400,
		.dclk_ddr = 531,
		.aclk = 200,
		.aclk2 = 100,
		.phclk = 1, /*6.5MHz*/
		.atclk = 133,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_DIV_BY_2,
		.ddr_clk_src_lp = PLL1,
		.ddr_clk_src_ddr = PLL1_CLKOUT_P,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = PLL1_CLKOUT_P,
		.vcore = VLOW,
	},

	{
		.op_id = 11,
		.pclk = 26,
		.pclk_mm = 26,
		.baclk = 50,
		.dclk_lp = 400,
		.dclk_ddr = 531,
		.aclk = 50,
		.aclk2 = 50,
		.phclk = 1, /*6.5MHz*/
		.atclk = 13,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = VCXO,
		.ddr_clk_src_lp = PLL1,
		.ddr_clk_src_ddr = PLL1_CLKOUT_P,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = PLL1_CLKOUT_P,
		.vcore = VLOW,
	},

	{
		.op_id = 12,
		.pclk = 50,
		.pclk_mm = 50,
		.baclk = 50,
		.dclk_lp = 400,
		.dclk_ddr = 531,
		.aclk = 100,
		.aclk2 = 100,
		.phclk = 1, /*100MHz*/
		.atclk = 133,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_DIV_BY_2,
		.ddr_clk_src_lp = PLL1,
		.ddr_clk_src_ddr = PLL1_CLKOUT_P,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = PLL1_CLKOUT_P,
		.vcore = VLOW,
	},

	{
		.op_id = 13,
		.pclk = 100,
		.pclk_mm = 50,
		.baclk = 50,
		.dclk_lp = 400,
		.dclk_ddr = 531,
		.aclk = 100,
		.aclk2 = 100,
		.phclk = 1, /*100MHz*/
		.atclk = 133,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_DIV_BY_2,
		.ddr_clk_src_lp = PLL1,
		.ddr_clk_src_ddr = PLL1_CLKOUT_P,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = PLL1_CLKOUT_P,
		.vcore = VLOW,
	},

	{
		.op_id = 14,
		.pclk = 200,
		.pclk_mm = 200,
		.baclk = 200,
		.dclk_lp = 400,
		.dclk_ddr = 531,
		.aclk = 200,
		.aclk2 = 100,
		.phclk = 1, /*100MHz*/
		.atclk = 133,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_DIV_BY_2,
		.ddr_clk_src_lp = PLL1,
		.ddr_clk_src_ddr = PLL1_CLKOUT_P,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = PLL1_CLKOUT_P,
		.vcore = VLOW,
	},

	{
		.op_id = 15,
		.pclk = 400,
		.pclk_mm = 200,
		.baclk = 400,
		.dclk_lp = 400,
		.dclk_ddr = 531,
		.aclk = 400,
		.aclk2 = 200,
		.phclk = 1, /*100MHz*/
		.atclk = 133,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_DIV_BY_2,
		.ddr_clk_src_lp = PLL1,
		.ddr_clk_src_ddr = PLL1_CLKOUT_P,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = PLL1_CLKOUT_P,
		.vcore = VLOW,
	},

	{
		.op_id = 16,
		.pclk = 800,
		.pclk_mm = 400,
		.baclk = 400,
		.dclk_lp = 400,
		.dclk_ddr = 531,
		.aclk = 400,
		.aclk2 = 200,
		.phclk = 1, /*200MHz*/
		.atclk = 265,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1,
		.ddr_clk_src_lp = PLL1,
		.ddr_clk_src_ddr = PLL1_CLKOUT_P,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = PLL2,
		.pll1clkoutp = PLL1_CLKOUT_P,
		.vcore = VHIGH,
	},

	{
		.op_id = 17,
		.pclk = 531,
		.pclk_mm = 265,
		.baclk = 531,
		.dclk_lp = 400,
		.dclk_ddr = 531,
		.aclk = 400,
		.aclk2 = 200,
		.phclk = 2, /*177MHz*/
		.atclk = 354,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_CLKOUT_P,
		.ddr_clk_src_lp = PLL1,
		.ddr_clk_src_ddr = PLL1_CLKOUT_P,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = PLL1_CLKOUT_P,
		.vcore = VHIGH,
	},

	{
		.op_id = 18,
		.pclk = 1063,
		.pclk_mm = 265,
		.baclk = 531,
		.dclk_lp = 400,
		.dclk_ddr = 531,
		.aclk = 400,
		.aclk2 = 200,
		.phclk = 2, /*177MHz*/
		.atclk = 354,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_CLKOUT_P,
		.ddr_clk_src_lp = PLL1,
		.ddr_clk_src_ddr = PLL1_CLKOUT_P,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = PLL1_CLKOUT_P,
		.vcore = VHIGH,
	},

	{
		.op_id = 19,/*PP 5_2*/
		.pclk = 400,
		.pclk_mm = 400,
		.baclk = 400,
		.dclk_lp = 400,
		.dclk_ddr = 531,
		.aclk = 400,
		.aclk2 = 200,
		.phclk = 1, /*100MHz*/
		.atclk = 133,
		.cp_pclk = 200,
		.cp_baclk = 100,
		.cp_clk_src = PLL1_DIV_BY_2,
		.ap_clk_src = PLL1_DIV_BY_2,
		.ddr_clk_src_lp = PLL1,
		.ddr_clk_src_ddr = PLL1_CLKOUT_P,
		.axi_clk_src = PLL1_DIV_BY_2,
		.pll2freq = -1,
		.pll1clkoutp = PLL1_CLKOUT_P,
		.vcore = VHIGH,
	},
};

/*struct operating_point pxa2128_op_array[] = {
	pclk_mp pclk_mm bclk lp_dclk ddr3_dclk aclk1 aclk2 phclk atclk
	OP_MMP3(100, 100, 100, 200, 200, 200, 100, 100, 133),
	OP_MMP3(400, 400, 400, 400, 533, 400, 200, 200, 133),
	OP_MMP3(400, 400, 400, 400, 533, 400, 200, 200, 133),
	OP_MMP3(400, 400, 400, 400, 533, 400, 200, 200, 133),
	OP_MMP3(400, 400, 400, 400, 533, 400, 200, 200, 133),
	OP_MMP3(800, 400, 400, 400, 533, 400, 200, 200, 266),
	OP_MMP3(1066, 533, 400, 400, 533, 400, 200, 176, 355),
	OP_MMP3(531, 531, 531, 400, 531, 400, 200, 177, 354),
	OP_MMP3(1063, 531, 531, 400, 531, 400, 200, 177, 354),
};*/

struct operating_point pxa920_op_array[] = {
	/* pclk pdclk baclk xpclk  dclk  aclk vcore */
	OP(156,   78,   78,  156,   78,   78, 1225), /* op0 */
	OP(78,   39,   39,   78,   78,   78, 1225),
	OP(156,   78,   78,  156,  104,  104, 1225),
	OP(312,  156,  156,  312,  156,  156, 1225),
	OP(624,  156,  156,  312,  156,  156, 1225),
	OP(0,    0,    0,    0,    0,    0,    0), /* op5 */
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0), /* op10 */
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(806,  201,  201,  403,  201,  208, 1300),
	OP(780,  195,  195,  390,  195,  208, 1300),
	OP(801,  200,  200,  400,  200,  208, 1300), /* op15 */
	OP(797,  199,  199,  398,  199,  208, 1300),
	OP(936,  156,  156,  312,  156,  156, 1400),
	OP(988,  164,  164,  329,  164,  197, 1400),
};

struct operating_point pxa921_op_array[] = {
	/* pclk pdclk baclk xpclk  dclk  aclk vcore */
	OP(156,   78,   78,  156,   78,   78, 1225), /* op0 */
	OP(78,   39,   39,   78,   78,   78, 1225),
	OP(156,  156,  156,  156,  156,  156, 1225),
	OP(312,  156,  156,  312,  156,  156, 1225),
	OP(500,  250,  250,  250,  250,  156, 1225),
	OP(0,    0,    0,    0,    0,    0,    0), /* op5 */
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(1001, 250,  250,  500,  250,  208, 1350), /* op10 */
};

struct operating_point pxa910_op_array[] = {
	/* pclk pdclk baclk xpclk  dclk  aclk vcore */
	OP(156,   78,   78,  156,   78,   78, 1225), /* op0 */
	OP(78,   39,   39,   78,   78,   78, 1225),
	OP(208,  104,  104,  104,  104,  104, 1225),
	OP(312,  156,  156,  156,  156,  156, 1225),
	OP(624,  156,  156,  312,  156,  156, 1225),
	OP(0,    0,    0,    0,    0,    0,    0), /* op5 */
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0), /* op10 */
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(0,    0,    0,    0,    0,    0,    0),
	OP(806,  201,  201,  403,  201,  208, 1300),
	OP(780,  195,  195,  390,  195,  208, 1300),
	OP(801,  200,  200,  400,  200,  208, 1300), /* op15 */
	OP(797,  199,  199,  398,  199,  208, 1300),
	OP(936,  156,  156,  312,  156,  156, 1400),
	OP(988,  164,  164,  329,  164,  197, 1400),
};

union pmum_pll2cr1 {
	struct {
		unsigned int reserved0:8;
		unsigned int en:1;
		unsigned int ctrl:1;
		unsigned int pll2fbd:9;
		unsigned int pll2refd:5;
		unsigned int reserved2:8;
	} b;
	unsigned int v;
};

union pmua_pllsel1 {
	struct {
		unsigned int cpclksel:3;
		unsigned int apclksel:3;
		unsigned int ddrclksel:3;
		unsigned int axiclksel:3;
		unsigned int reserved0:20;
	} b;
	unsigned int v;
};

union pmum_fccr1 {
	struct {
		unsigned int pll1fbd:9;
		unsigned int pll1refd:5;
		unsigned int pll1cen:1;
		unsigned int mfc:1;
		unsigned int pll1_mfc_rst_time:2;
		unsigned int rsrvd:5;
		unsigned int ddrclksel:3;
		unsigned int seaclksel:3;
		unsigned int mohclksel:3;
	} b;
	unsigned int v;
};

union pmua_bus_clk_ctrl1 {
	struct {
		unsigned int mc_rst:1;
		unsigned int mc2_rst:1;
		unsigned int rsrvd1:4;
		unsigned int saxi_clk_sel:3;
		unsigned int ddr2clksel:3;
		unsigned int rsrvd:20;
	} b;
	unsigned int v;
};

union pmua_cc1 {
	struct {
		unsigned int core_clk_div:3;
		unsigned int atclk_div:3;
		unsigned int rsrvd3:3;
		unsigned int phclk_div:3;
		unsigned int ddr_clk_div:3;
		unsigned int soc_axi_clk_div:3;
		unsigned int rsrvd2:4;
		unsigned int ddr2_freq_chg_req:1;
		unsigned int rsrvd1:1;
		unsigned int sp_freq_chg_req:1;
		unsigned int ddr_freq_chg_req:1;
		unsigned int saxi_freq_chg_req:1;
		unsigned int core_allow_spd_chg:1;
		unsigned int rsrvd:3;
		unsigned int core_rd_st_clear:1;
	} b;
	unsigned int v;
};

union pmua_cc2_sp1 {
	struct {
		unsigned int saxi2_clk_div:3;
		unsigned int rsrvd1:6;
		unsigned int ddr_clk2_div:3;
		unsigned int rsrvd:20;
	} b;
	unsigned int v;
};

union pmua_cc2_pj1 {
	struct {
		unsigned int saxi_clk2_div:3;
		unsigned int rsrvd3:6;
		unsigned int mp1_pclken_div:4;
		unsigned int mp2_pclken_div:4;
		unsigned int mm_pclken_div:4;
		unsigned int aclken_div:4;
		unsigned int mp2_sw_rstn:1;
		unsigned int mm_sw_rstn:1;
		unsigned int phclk_en:1;
		unsigned int mp1_pclken_dis:1;
		unsigned int mp2_pclken_dis:1;
		unsigned int mm_pclken_dis:1;
		unsigned int pj_force_pclkon:1;
	} b;
	unsigned int v;
};

union pmua_cc3_pj1 {
	struct {
		unsigned int rst_mp1_wdog:1;
		unsigned int rst_mp2_wdog:1;
		unsigned int rst_mm_wdog:1;
		unsigned int timer_sw_rst:1;
		unsigned int timer_clken:1;
		unsigned int rsrvd:3;
		unsigned int timer_clk_ratio:5;
		unsigned int preset_atresetn:1;
		unsigned int mp1_wd_rst_mask:1;
		unsigned int mp2_wd_rst_mask:1;
		unsigned int mm_wd_rst_mask:1;
		unsigned int ddr_clk2_div:3;
		unsigned int atclk_pclkdg_rtio:5;
		unsigned int rsrvd1:7;
	} b;
	unsigned int v;
};

union pmua_dm_cc1 {
	struct {
		unsigned int core_clk_div:3;
		unsigned int at_clk_div:3;
		unsigned int rsrvd1:3;
		unsigned int ddr_clk2_div:3;
		unsigned int ddr_clk_div:3;
		unsigned int soc_axi_clk_div:3;
		unsigned int rsrvd:6;
		unsigned int sea_rd_status:1;
		unsigned int moh_rd_status:1;
		unsigned int reserved:6;
	} b;
	unsigned int v;
};

union pmua_dm2_cc_pj1 {
	struct {
		unsigned int soc_axi2_div:3;
		unsigned int rsrvd:6;
		unsigned int mp1_pclen_div:4;
		unsigned int mp2_pclen_div:4;
		unsigned int mm_pclen_div:4;
		unsigned int aclken_div:4;
		unsigned int periphclk_div:3;
		unsigned int reserved:4;
	} b;
	unsigned int v;
};

union pmua_dm2_cc_sp1 {
	struct {
		unsigned int reserved:29;
		unsigned int soc_axi2_div:3;
	} b;
	unsigned int v;
};

#define CLK_SRC_VCTCXO		(1u<<0)
#define CLK_SRC_PLL1_312	(1u<<1)
#define CLK_SRC_PLL1_624	(1u<<2)
#define CLK_SRC_PLL2		(1u<<3)
#define CLK_SRC_PCLKOUT_P	(1u<<4)


static int fc_lock_ref_cnt;

static void get_fc_lock(void)
{
	union pmua_dm_cc1 dm_cc_ap;

	fc_lock_ref_cnt++;

	if (fc_lock_ref_cnt == 1) {
		int timeout = 100000;
		/* AP-CP FC mutual exclusion */
		dm_cc_ap.v = __raw_readl(PMUA_DM_CC_MOH);
		while (dm_cc_ap.b.sea_rd_status && timeout) {
			dm_cc_ap.v = __raw_readl(PMUA_DM_CC_MOH);
			timeout--;
		}
		if (timeout <= 0)
			printf("cp does not release its fc lock\n");
	}
}

static void put_fc_lock(void)
{
	union pmua_cc1 cc_ap;

	fc_lock_ref_cnt--;

	if (fc_lock_ref_cnt < 0)
		printf("unmatched put_fc_lock\n");

	if (fc_lock_ref_cnt == 0) {
		/* write 1 to MOH_RD_ST_CLEAR to clear MOH_RD_STATUS */
		cc_ap.v = __raw_readl(PMUA_CC_MOH);
		cc_ap.b.core_rd_st_clear = 1;
		__raw_writel(cc_ap.v, PMUA_CC_MOH);
		cc_ap.b.core_rd_st_clear = 0;
		__raw_writel(cc_ap.v, PMUA_CC_MOH);
	}
}
/*Turning all the clocks, AIB clocks and programing debug
*registers
*/

void freq_change_prep(void)
{
	__raw_writel(0x1, PMUM_DEBUG_1);
	__raw_writel(0x0, PMUM_DEBUG_2);
	__raw_writel(0x0818E23E, PMUM_CGR_SP);
	__raw_writel(0x0818E23E, PMUM_CGR_PJ);
}

static u32 get_pll2_freq(void)
{
	union pmum_pll2cr1 pll2cr;

	pll2cr.v = __raw_readl(PMUM_PLL2CR);
	if ((pll2cr.b.ctrl == 1) && (pll2cr.b.en == 0))
		return 0;
	return 13 * pll2cr.b.pll2fbd / pll2cr.b.pll2refd;
}

#if 0
static void turn_off_pll2(void)
{
	union pmum_pll2cr1 pll2cr;

	pll2cr.v = __raw_readl(PMUM_PLL2CR);
	pll2cr.b.ctrl = 1; /* Let SW control PLL2 */
	pll2cr.b.en = 0;   /* disable PLL2 by en bit */
	__raw_writel(pll2cr.v, PMUM_PLL2CR);
}
#endif

void turn_on_pll2(u32 pll2freq)
{
	union pmum_pll2cr1 pll2cr;
	u32 pll2cr2;
	pll2cr.v = 0;
	pll2cr.b.pll2refd = 3;
	pll2cr.b.pll2fbd = (2*pll2freq*pll2cr.b.pll2refd)/26;
	pll2cr.b.ctrl = 1;

	__raw_writel(pll2cr.v, PMUM_PLL2CR);
	__raw_writel(0x05310599, PMUM_PLL2CR1);

	pll2cr2 = __raw_readl(PMUM_PLL2CR2);
	__raw_writel(pll2cr2 | 1, PMUM_PLL2CR2);

	__raw_writel(0x25310599, PMUM_PLL2CR1);

	udelay(200);

	pll2cr.b.en = 1;
	__raw_writel(pll2cr.v, PMUM_PLL2CR);

	udelay(200);
	printf("PLL2 is initialized at %dMHz\n", pll2freq);
}

static unsigned int to_clk_src(unsigned int sel, u32 pll2freq)
{
	unsigned int clk = 0;

	switch (sel) {
	case 0:
		clk = PLL1_DIV_BY_2;
		break;
	case 1:
		clk = PLL1;
		break;
	case 2:
		clk = pll2freq;
		break;
	case 3:
		clk = PLL1_CLKOUT_P;
		break;
	case 4:
		clk = 26;
		break;
	default:
		printf("Wrong clock source\n");
		break;
	}

	/*printf("clock source was: %u\n",clk);*/
	return clk;
}

static void get_current_op(struct operating_point *cop)
{
	union pmua_pllsel1 pllsel;
	union pmua_dm_cc1 dm_cc_cp, dm_cc_ap;
	union pmua_dm2_cc_pj1 dm2_cc_ap;
	union pmua_dm2_cc_sp1 dm2_cc_cp;
	u32 temp = 0;

	u32 ddr_type = __raw_readl(MCU1_BASE + SDRAM_CTRL4) & 0x0000001C;

	get_fc_lock();
	dm_cc_ap.v = __raw_readl(PMUA_DM_CC_MOH);
	dm_cc_cp.v = __raw_readl(PMUA_DM_CC_SEA);
	dm2_cc_ap.v = __raw_readl(PMUA_DM2_CC_MOH);
	dm2_cc_cp.v = __raw_readl(PMUA_DM2_CC_SEA);
	pllsel.v = __raw_readl(PMUA_PLL_SEL_STATUS);

	cop->pll2freq = get_pll2_freq();
	cop->ap_clk_src = to_clk_src(pllsel.b.apclksel, cop->pll2freq);
	cop->cp_clk_src = to_clk_src(pllsel.b.cpclksel, cop->pll2freq);
	cop->axi_clk_src = to_clk_src(pllsel.b.axiclksel, cop->pll2freq);
	cop->ddr_clk_src = to_clk_src(pllsel.b.ddrclksel, cop->pll2freq);

	temp = cop->ap_clk_src/(dm_cc_ap.b.core_clk_div + 1);
	cop->pclk = temp / (dm2_cc_ap.b.mp1_pclen_div + 1);
	cop->pclk_mm = temp / (dm2_cc_ap.b.mm_pclen_div + 1);
	cop->baclk = temp / (dm2_cc_ap.b.aclken_div + 1);
	cop->cp_pclk = cop->cp_clk_src/(dm_cc_cp.b.core_clk_div+1);

	if (ddr_type == DDR3) {
		cop->dclk_ddr = cop->ddr_clk_src/(dm_cc_ap.b.ddr_clk_div + 1)/2;
		temp = cop->dclk_ddr;
	} else {
		cop->dclk_lp = cop->ddr_clk_src/(dm_cc_ap.b.ddr_clk_div + 1)/2;
		temp = cop->dclk_lp;
	}

	cop->aclk = cop->axi_clk_src/(dm_cc_ap.b.soc_axi_clk_div + 1);
	cop->aclk2 = cop->axi_clk_src/(dm2_cc_ap.b.soc_axi2_div + 1);

	#ifdef MMP3_DFC_DEBUG
	printf("dm_cc_ap.b.core_clk_div: %u\n", dm_cc_ap.b.core_clk_div);
	printf("dm2_cc_ap.b.mp1_pclen_div: %u\n", dm2_cc_ap.b.mp1_pclen_div);
	printf("dm2_cc_ap.b.mm_pclen_div: %u\n", dm2_cc_ap.b.mm_pclen_div);
	printf("dm2_cc_ap.b.aclken_div: %u\n", dm2_cc_ap.b.aclken_div);
	printf("cop->pclk: %u and cop->baclk :%u\n", cop->pclk, cop->baclk);
	printf("DDR: %u and current op->aclk:"
	"%u, current op->aclk2: %u\n", temp,
	cop->aclk, cop->aclk2);
	#endif

	put_fc_lock();
}
/*
static void set_ap_clk_sel(struct operating_point *top)
{
	union pmum_fccr1 fccr;

	fccr.v = __raw_readl(PMUM_FCCR);
	if (top->ap_clk_src == 312)
		fccr.b.mohclksel = 0;
	else if (top->ap_clk_src == 624)
		fccr.b.mohclksel = 1;
	else if (top->ap_clk_src == top->pll2freq)
		fccr.b.mohclksel = 2;
	else if (top->ap_clk_src == 26)
		fccr.b.mohclksel = 3;
	__raw_writel(fccr.v, PMUM_FCCR);
}

static void set_axi_clk_sel(struct operating_point *top)
{
	union pmum_fccr1 fccr;

	fccr.v = __raw_readl(PMUM_FCCR);
	if (top->axi_clk_src == 312) {
		fccr.b.axiclksel1 = 0;
		fccr.b.axiclksel0 = 0;
	} else if (top->axi_clk_src == 624) {
		fccr.b.axiclksel1 = 0;
		fccr.b.axiclksel0 = 1;
	} else if (top->axi_clk_src == top->pll2freq) {
		fccr.b.axiclksel1 = 1;
		fccr.b.axiclksel0 = 0;
	} else if (top->axi_clk_src == 26) {
		fccr.b.axiclksel1 = 1;
		fccr.b.axiclksel0 = 1;
	}
	__raw_writel(fccr.v, PMUM_FCCR);
}

static void set_ddr_clk_sel(struct operating_point *top)
{
	union pmum_fccr1 fccr;

	fccr.v = __raw_readl(PMUM_FCCR);
	if (top->ddr_clk_src == 312)
		fccr.b.ddrclksel = 0;
	else if (top->ddr_clk_src == 624)
		fccr.b.ddrclksel = 1;
	else if (top->ddr_clk_src == top->pll2freq)
		fccr.b.ddrclksel = 2;
	else if (top->ddr_clk_src == 26)
		fccr.b.ddrclksel = 3;
	__raw_writel(fccr.v, PMUM_FCCR);
}
*/

static void enable_fc_intr(void)
{
	u32 fc_int_msk;

	fc_int_msk = __raw_readl(PMUA_MOH_IMR);
	fc_int_msk &= ~(0xfff);
	/* fc_int_msk |= (7<<3); */
	/*
	 * enable AP FC done interrupt for one step,
	 * while not use three interrupts by three steps
	 */
	fc_int_msk |= (1<<3);
	fc_int_msk |= (1<<4);
	fc_int_msk |= (1<<5);
	__raw_writel(fc_int_msk, PMUA_MOH_IMR);
}

static void wait_for_fc_done(void)
{
	int timeout = 1000000;
	u32 check = __raw_readl(PMUA_MOH_ISR);
	u32 temp = 0x8;

	while ((!(temp & check)) && timeout) {
		check = __raw_readl(PMUA_MOH_ISR);
		timeout--;
	}
	if (timeout <= 0)
		panic("AP frequency change timeout!\n");
	__raw_writel(0x1fff, PMUA_MOH_ISR);
	__raw_writel(0x0, PMUA_MOH_ISR);
	__raw_writel(0x0, PMUA_MOH_IMR);
	udelay(1000);
}

/*
static void wait_for_ap_fc_done(void)
{
	while (!((1<<3) & __raw_readl(PMUA_MOH_ISR)))
		;
	__raw_writel(0x0, PMUA_MOH_ISR);
}

static void wait_for_axi_fc_done(void)
{
	while (!((1<<5) & __raw_readl(PMUA_MOH_ISR)))
		;
	__raw_writel(0x0, PMUA_MOH_ISR);
}

static void wait_for_ddr_fc_done(void)
{
	while (!((1<<4) & __raw_readl(PMUA_MOH_ISR)))

		;
	__raw_writel(0x0, PMUA_MOH_ISR);
}
*/

#define debug_wt_reg(val, reg)						\
do {									\
	__raw_writel(val, reg);						\
	/*printf(" %08x ==> [%x]\n", val, reg);*/				\
} while (0)

#define _insert_tbl(tbl_num, tbl_entry, base, reg, data, pause, end)	\
do {									\
	int tmp = 0;							\
	debug_wt_reg(data, base + REGISTER_TABLE_DATA_0);		\
	tmp |= reg;							\
	if (pause)							\
		tmp |= (1 << 16);					\
	if (end)							\
		tmp |= (1 << 17);					\
	debug_wt_reg(tmp, base + REGISTER_TABLE_DATA_1);		\
	tmp = (1 << 31);						\
	tmp |= (tbl_num << 5);						\
	tmp |= tbl_entry;						\
	debug_wt_reg(tmp, base + REGISTER_TABLE_CTRL_0);		\
	tbl_entry++;							\
} while (0)

#define tbl_add_com(num, ent, base, reg, data) \
	_insert_tbl(num, ent, base, reg, data, 0, 0)
#define tbl_add_pop(num, ent, base, reg, data) \
	_insert_tbl(num, ent, base, reg, data, 1, 0)
#define tbl_add_eop(num, ent, base, reg, data) \
	_insert_tbl(num, ent, base, reg, data, 1, 1)

static void mc4_prgm_tble_lpddr_h2l(u32 base, struct ddr_timing *timing)
{
	u32 entry = 0, data;

	tbl_add_com(0, entry, base, SDRAM_CTRL14, 0x2);	/* halt mc4 scheduler */

	tbl_add_com(0, entry, base, SDRAM_CTRL4,
		__raw_readl(base + SDRAM_CTRL4));	/* cas latency */
	tbl_add_com(0, entry, base, SDRAM_TIMING1, timing->reg1);
	tbl_add_com(0, entry, base, SDRAM_TIMING2, timing->reg2);
	tbl_add_com(0, entry, base, SDRAM_TIMING3, timing->reg3);
	tbl_add_com(0, entry, base, SDRAM_TIMING4, timing->reg4);
	tbl_add_com(0, entry, base, SDRAM_TIMING5, timing->reg5);
	tbl_add_com(0, entry, base, SDRAM_TIMING6, timing->reg6);
	tbl_add_com(0, entry, base, SDRAM_TIMING7, timing->reg7);
	tbl_add_com(0, entry, base, SDRAM_TIMING8, timing->reg8);

	tbl_add_com(0, entry, base, PHY_CTRL14, 0x20000000);	/* reset master dll */
	tbl_add_com(0, entry, base, PHY_CTRL14, 0x40000000);	/* update master dll */
	tbl_add_pop(0, entry, base, PHY_CTRL14, 0x80000000);	/* sync 2x clk */

	data = __raw_readl(base + SDRAM_CTRL1) | (0x40);
	tbl_add_com(0, entry, base, SDRAM_CTRL1, data);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND0, 0x03001000);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND1, 0x03020001);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND1, 0x03020002);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND1, 0x03020003);
	tbl_add_eop(0, entry, base, SDRAM_CTRL14, 0x0);
}

static void mc4_prgm_tble_lpddr_l2h(u32 base, struct ddr_timing *timing)
{
	u32 entry = 0, data;

	tbl_add_com(0, entry, base, SDRAM_CTRL14, 0x2);	/* halt mc4 scheduler */

	tbl_add_com(0, entry, base, SDRAM_CTRL4,
		__raw_readl(base + SDRAM_CTRL4));	/* cas latency */
	tbl_add_com(0, entry, base, SDRAM_TIMING1, timing->reg1);
	tbl_add_com(0, entry, base, SDRAM_TIMING2, timing->reg2);
	tbl_add_com(0, entry, base, SDRAM_TIMING3, timing->reg3);
	tbl_add_com(0, entry, base, SDRAM_TIMING4, timing->reg4);
	tbl_add_com(0, entry, base, SDRAM_TIMING5, timing->reg5);
	tbl_add_com(0, entry, base, SDRAM_TIMING6, timing->reg6);
	tbl_add_com(0, entry, base, SDRAM_TIMING7, timing->reg7);
	tbl_add_pop(0, entry, base, SDRAM_TIMING8, timing->reg8);

	tbl_add_com(0, entry, base, PHY_CTRL14, 0x20000000);	/* reset master dll */
	tbl_add_com(0, entry, base, PHY_CTRL14, 0x40000000);	/* update master dll */
	tbl_add_pop(0, entry, base, PHY_CTRL14, 0x80000000);	/* sync 2x clk */

	data = __raw_readl(base + SDRAM_CTRL1) | (0x40);
	tbl_add_com(0, entry, base, SDRAM_CTRL1, data);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND0, 0x03001000);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND1, 0x03020001);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND1, 0x03020002);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND1, 0x03020003);
	tbl_add_eop(0, entry, base, SDRAM_CTRL14, 0x0);
}

static void mc4_prgm_tble_ddr3_h2l(u32 base, struct ddr_timing *timing)
{
	u32 entry = 0, data;

	tbl_add_pop(0, entry, base, SDRAM_CTRL14, 0x2);	/* halt mc4 scheduler */

	tbl_add_com(0, entry, base, SDRAM_CTRL4,
		__raw_readl(base + SDRAM_CTRL4));	/* cas latency */
	tbl_add_com(0, entry, base, SDRAM_TIMING1, timing->reg1);
	tbl_add_com(0, entry, base, SDRAM_TIMING2, timing->reg2);
	tbl_add_com(0, entry, base, SDRAM_TIMING3, timing->reg3);
	tbl_add_com(0, entry, base, SDRAM_TIMING4, timing->reg4);
	tbl_add_com(0, entry, base, SDRAM_TIMING5, timing->reg5);
	tbl_add_com(0, entry, base, SDRAM_TIMING6, timing->reg6);
	tbl_add_com(0, entry, base, SDRAM_TIMING7, timing->reg7);

	tbl_add_com(0, entry, base, PHY_CTRL14, 0x20000000);	/* reset master dll */
	tbl_add_com(0, entry, base, PHY_CTRL14, 0x40000000);	/* update master dll */
	tbl_add_pop(0, entry, base, PHY_CTRL14, 0x80000000);	/* sync 2x clk */

	tbl_add_com(0, entry, base, SDRAM_CTRL14, 0x2);
	data = __raw_readl(base + SDRAM_CTRL1) & ~(0x40);
	tbl_add_com(0, entry, base, SDRAM_CTRL1, data);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND0, 0x01000100);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND0, 0x01000400);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND0, 0x01001000);
	tbl_add_eop(0, entry, base, SDRAM_CTRL14, 0x0);
}

static void mc4_prgm_tble_ddr3_l2h(u32 base, struct ddr_timing *timing)
{
	u32 entry = 0, data;

	tbl_add_com(0, entry, base, SDRAM_CTRL14, 0x2);	/* halt mc4 scheduler */

	tbl_add_com(0, entry, base, SDRAM_CTRL4,
		__raw_readl(base + SDRAM_CTRL4));	/* cas latency */
	tbl_add_com(0, entry, base, SDRAM_TIMING1, timing->reg1);
	tbl_add_com(0, entry, base, SDRAM_TIMING2, timing->reg2);
	tbl_add_com(0, entry, base, SDRAM_TIMING3, timing->reg3);
	tbl_add_com(0, entry, base, SDRAM_TIMING4, timing->reg4);
	tbl_add_com(0, entry, base, SDRAM_TIMING5, timing->reg5);
	tbl_add_com(0, entry, base, SDRAM_TIMING6, timing->reg6);
	tbl_add_pop(0, entry, base, SDRAM_TIMING7, timing->reg7);

	tbl_add_com(0, entry, base, PHY_CTRL14, 0x20000000);	/* reset master dll */
	tbl_add_com(0, entry, base, PHY_CTRL14, 0x40000000);	/* update master dll */
	tbl_add_pop(0, entry, base, PHY_CTRL14, 0x80000000);	/* sync 2x clk */

	tbl_add_com(0, entry, base, SDRAM_CTRL14, 0x2);
	data = __raw_readl(base + SDRAM_CTRL1) | (0x40);
	tbl_add_com(0, entry, base, SDRAM_CTRL1, data);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND0, 0x03000100);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND0, 0x03000400);
	tbl_add_com(0, entry, base, USER_INITIATED_COMMAND0, 0x03001000);
	tbl_add_eop(0, entry, base, SDRAM_CTRL14, 0x0);
}

void mc4_fc_prgm_tble_seq(u32 base, u32 crnt_frq, u32 nxt_frq, int dtyp)
{

	int i;

	for (i = 0; i < ARRAY_SIZE(ddr_setting); i++) {
		if (nxt_frq == ddr_setting[i].ddr_freq)
			break;
	}

	if (i == ARRAY_SIZE(ddr_setting)) {
		printf("ddr freq do not support, should not run here!\n");
		return;
	}

	if (dtyp == DDR3) {
		if (crnt_frq > nxt_frq) {
			mc4_prgm_tble_ddr3_h2l(base, &ddr_setting[i].setting);
		} else {
			mc4_prgm_tble_ddr3_l2h(base, &ddr_setting[i].setting);
		}
	}

	if (dtyp == LPDDR2) {
		if (crnt_frq > nxt_frq) {
			mc4_prgm_tble_lpddr_h2l(base, &ddr_setting[i].setting);
		} else {
			mc4_prgm_tble_lpddr_l2h(base, &ddr_setting[i].setting);
		}
	}

	return;
}

void ddr_fc_program_table(unsigned int base, unsigned int current_freq,
				unsigned int next_freq, u32 ddr_type)
{
	u32 temp = __raw_readl(PMUA_MC_SLP_REQ_PJ);

	mc4_fc_prgm_tble_seq(base, current_freq, next_freq, ddr_type);

	if (base == MCU1_BASE)
		temp &= ~(0x30);
	else
		temp &= ~(0xc0);

	if (ddr_type == DDR3) {
		if (current_freq < next_freq) {
			if (base == MCU1_BASE)
				temp |= 0x30;
			else
				temp |= 0xc0;
		} else {
			if (base == MCU1_BASE)
				temp |= 0x20;
			else
				temp |= 0x80;
		}
	} else {
		if (current_freq < next_freq)
			temp |= 0x50;
		else
			temp |= 0x0;
	}

	__raw_writel(temp, PMUA_MC_SLP_REQ_PJ);
}

void allow_speed_change(int i)
{
	union pmua_cc1 cc_ap;

	cc_ap.v = __raw_readl(PMUA_CC_MOH);
	cc_ap.b.core_allow_spd_chg = i;
	__raw_writel(cc_ap.v, PMUA_CC_MOH);
}

static void PMUcore2_fc_seq(struct operating_point *cop,
	struct operating_point *top)
{
	union pmua_cc1 cc_ap;
	union pmua_cc2_pj1 cc2_ap;
	union pmua_cc3_pj1 cc3_ap;
	union pmum_fccr1 fccr;
	union pmua_bus_clk_ctrl1 bus_clk_ctrl;

	int coreFlag = 0;
	int axiFlag = 0;
	int ddrFlag = 0;
	u32 ddr_type = 0;
	u32 dclk2x;
	u32 temp, temp1, ddr_src;

#ifdef CONFIG_MMP3_DVC
	if (top->vcore > cop->vcore) {
		if (set_volt(top->vcore) == 0)
			printf("Voltage is changed to: %dmV\n", top->vcore);
		else
			printf("Voltage change was unsuccessful\n");
	}
#endif

	ddr_type = __raw_readl(MCU1_BASE + SDRAM_CTRL4) & 0x0000001C;
	printf("DDR Type: %s\n", (ddr_type == DDR3) ? "DDR3" : "LPDDR");

	if (ddr_type == DDR3)
		dclk2x = top->dclk_ddr * 2;
	else
		dclk2x = top->dclk_lp * 2;

	__raw_writel(0x04, PMUA_MC_PAR_CTRL);

	bus_clk_ctrl.v = __raw_readl(PMUA_BUS_CLK_RES_CTRL);
	fccr.v = __raw_readl(PMUM_FCCR);
	cc_ap.v = __raw_readl(PMUA_CC_MOH);
	cc2_ap.v = __raw_readl(PMUA_CC2_MOH);
	cc3_ap.v = __raw_readl(PMUA_CC3_MOH);

	if (cop->pll2freq != PLL2 && top->pll2freq == PLL2)
		turn_on_pll2(top->pll2freq);
	if (cop->pll1clkoutp != PLL1_CLKOUT_P &&
		top->pll1clkoutp == PLL1_CLKOUT_P)
			__raw_writel(0x011, PMUM_PLL_DIFF_CNTRL);

	cc_ap.b.rsrvd = 0;
	cc_ap.b.rsrvd1 = 1;
	cc_ap.b.rsrvd2 = (1 << 3) | (1 << 2) | (1 << 1) | (1 << 0);
	cc_ap.b.rsrvd3 = 3;
	cc2_ap.b.rsrvd3 = 0;
	fccr.b.rsrvd = 0;
	bus_clk_ctrl.b.rsrvd = 0;
	bus_clk_ctrl.b.rsrvd1 = 0;

	cc_ap.b.atclk_div = top->ap_clk_src / top->atclk - 1;
	cc_ap.b.phclk_div = top->phclk;
	cc_ap.b.core_allow_spd_chg = 0;

	if (ddr_type == DDR3) {
		temp1 = top->dclk_ddr;
		temp = cop->dclk_ddr;
		ddr_src = top->ddr_clk_src_ddr;
	} else {
		temp1 = top->dclk_lp;
		temp = cop->dclk_lp;
		ddr_src = top->ddr_clk_src_lp;
	}

	if (temp != temp1) {

		if (ddr_src == PLL1) {
			fccr.b.ddrclksel = 1;
			bus_clk_ctrl.b.ddr2clksel = 1;
			cc_ap.b.ddr_clk_div = (ddr_src / dclk2x) - 1;
			cc3_ap.b.ddr_clk2_div = (ddr_src / dclk2x) - 1;
		}
		if (ddr_src == PLL1_DIV_BY_2) {
			fccr.b.ddrclksel = 0;
			bus_clk_ctrl.b.ddr2clksel = 0;
			cc_ap.b.ddr_clk_div = (ddr_src / dclk2x) - 1;
			cc3_ap.b.ddr_clk2_div = (ddr_src / dclk2x) - 1;
		}
		if (ddr_src == PLL1_CLKOUT_P) {
			fccr.b.ddrclksel = 3;
			bus_clk_ctrl.b.ddr2clksel = 3;
			cc_ap.b.ddr_clk_div = (ddr_src / dclk2x) - 1;
			cc3_ap.b.ddr_clk2_div = (ddr_src / dclk2x) - 1;
		}
		if (ddr_src == PLL2) {
			fccr.b.ddrclksel = 2;
			bus_clk_ctrl.b.ddr2clksel = 2;
			cc_ap.b.ddr_clk_div = (ddr_src / dclk2x) - 1;
			cc3_ap.b.ddr_clk2_div = (ddr_src / dclk2x) - 1;
		}
		if (ddr_src == VCXO) {
			fccr.b.ddrclksel = 4;
			bus_clk_ctrl.b.ddr2clksel = 4;
			cc_ap.b.ddr_clk_div = (ddr_src / dclk2x) - 1;
			cc3_ap.b.ddr_clk2_div = (ddr_src / dclk2x) - 1;
		}
		cc_ap.b.ddr_freq_chg_req = 1;
		cc_ap.b.ddr2_freq_chg_req = 1;
		ddrFlag = 1;
	}

	/* as part of work around for silicon bug
	*  we are triggering core freq change if ddrFlag is set
	*/
	if (cop->pclk != top->pclk || cop->pclk_mm != top->pclk_mm) {

		if (top->ap_clk_src == PLL1) {
			fccr.b.mohclksel = 1;
			cc_ap.b.core_clk_div = (top->ap_clk_src / top->pclk)
						- 1;
			temp = PLL1 / (cc_ap.b.core_clk_div + 1);
			cc2_ap.b.mp1_pclken_div = (temp / top->pclk) - 1;
			cc2_ap.b.mp2_pclken_div = (temp / top->pclk) - 1;
			cc2_ap.b.mm_pclken_div = (temp / top->pclk_mm) - 1;
			cc2_ap.b.aclken_div = (temp / top->baclk) - 1;
		}
		if (top->ap_clk_src == PLL1_DIV_BY_2) {
			fccr.b.mohclksel = 0;
			cc_ap.b.core_clk_div = (top->ap_clk_src / top->pclk)
						- 1;
			temp = PLL1_DIV_BY_2 / (cc_ap.b.core_clk_div + 1);
			cc2_ap.b.mp1_pclken_div = (temp / top->pclk) - 1;
			cc2_ap.b.mp2_pclken_div = (temp / top->pclk) - 1;
			cc2_ap.b.mm_pclken_div = (temp / top->pclk_mm) - 1;
			cc2_ap.b.aclken_div = (temp / top->baclk) - 1;
		}
		if (top->ap_clk_src == PLL1_CLKOUT_P) {
			fccr.b.mohclksel = 3;
			cc_ap.b.core_clk_div = (top->ap_clk_src / top->pclk)
						- 1;
			temp = PLL1_CLKOUT_P / (cc_ap.b.core_clk_div + 1);
			cc2_ap.b.mp1_pclken_div = (temp / top->pclk) - 1;
			cc2_ap.b.mp2_pclken_div = (temp / top->pclk) - 1;
			cc2_ap.b.mm_pclken_div = (temp / top->pclk_mm) - 1;
			cc2_ap.b.aclken_div = (temp / top->baclk) - 1;
		}
		if (top->ap_clk_src == PLL2) {
			fccr.b.mohclksel = 2;
			cc_ap.b.core_clk_div = (top->ap_clk_src / top->pclk)
						- 1;
			temp = PLL2 / (cc_ap.b.core_clk_div + 1);
			cc2_ap.b.mp1_pclken_div = (temp / top->pclk) - 1;
			cc2_ap.b.mp2_pclken_div = (temp / top->pclk) - 1;
			cc2_ap.b.mm_pclken_div = (temp / top->pclk_mm) - 1;
			cc2_ap.b.aclken_div = (temp / top->baclk) - 1;
		}
		if (top->ap_clk_src == VCXO) {
			fccr.b.mohclksel = 4;
			cc_ap.b.core_clk_div = (top->ap_clk_src / top->pclk)
						- 1;
			temp = VCXO / (cc_ap.b.core_clk_div + 1);
			cc2_ap.b.mp1_pclken_div = (temp / top->pclk) - 1;
			cc2_ap.b.mp2_pclken_div = (temp / top->pclk) - 1;
			cc2_ap.b.mm_pclken_div = (temp / top->pclk_mm) - 1;
			cc2_ap.b.aclken_div = (temp / top->baclk) - 1;
		}
		cc_ap.b.sp_freq_chg_req = 1;
		coreFlag = 1;
	}

	if (cop->aclk != top->aclk || cop->aclk2 != top->aclk2) {
		cc_ap.b.soc_axi_clk_div = (top->axi_clk_src / top->aclk) - 1;
		cc2_ap.b.saxi_clk2_div = (top->axi_clk_src / top->aclk2) - 1;
		if (top->axi_clk_src == PLL1)
			bus_clk_ctrl.b.saxi_clk_sel = 1;
		if (top->axi_clk_src == PLL1_DIV_BY_2)
			bus_clk_ctrl.b.saxi_clk_sel = 0;
		if (top->axi_clk_src == PLL2)
			bus_clk_ctrl.b.saxi_clk_sel = 2;
		if (top->axi_clk_src == PLL1_CLKOUT_P)
			bus_clk_ctrl.b.saxi_clk_sel = 3;
		if (top->axi_clk_src == VCXO)
			bus_clk_ctrl.b.saxi_clk_sel = 4;
		cc_ap.b.saxi_freq_chg_req = 1;
		axiFlag = 1;
	}

	__raw_writel(bus_clk_ctrl.v, PMUA_BUS_CLK_RES_CTRL);
	__raw_writel(fccr.v, PMUM_FCCR);
	__raw_writel(cc_ap.v, PMUA_CC_MOH);
	__raw_writel(cc2_ap.v, PMUA_CC2_MOH);
	__raw_writel(cc3_ap.v, PMUA_CC3_MOH);

	if (ddrFlag == 1) {

		if (ddr_type == DDR3) {

			ddr_fc_program_table(MCU1_BASE, cop->dclk_ddr,
				top->dclk_ddr, ddr_type);
			ddr_fc_program_table(MCU2_BASE, cop->dclk_ddr,
				top->dclk_ddr, ddr_type);
		} else {
			ddr_fc_program_table(MCU1_BASE, cop->dclk_lp,
				top->dclk_lp, ddr_type);
			ddr_fc_program_table(MCU2_BASE, cop->dclk_lp,
				top->dclk_lp, ddr_type);
		}

	}

	if (coreFlag == 1 || axiFlag == 1 || ddrFlag == 1) {
		allow_speed_change(1);
		wait_for_fc_done();
		allow_speed_change(0);
	}

	#ifdef CONFIG_MMP3_DVC
	if (top->vcore < cop->vcore) {
		if (set_volt(top->vcore) == 0)
			printf("Voltage is changed to: %dmV\n", top->vcore);
		else
			printf("Voltage change was unsuccessful\n");
	}
	#endif
}

struct proc_op_array {
	unsigned int cpuid;
	unsigned int chip_id;
	char *cpu_name;
	struct operating_point *op_array;
	unsigned int nr_op;
};

static struct proc_op_array proc_op_arrays[] = {
{0x8000, 0xc921, "PXA921", pxa921_op_array, ARRAY_SIZE(pxa921_op_array)},
{0x8000, 0xc920, "PXA920", pxa920_op_array, ARRAY_SIZE(pxa920_op_array)},
{0x8000, 0xc910, "PXA910", pxa910_op_array, ARRAY_SIZE(pxa910_op_array)},
{0x8000, 0x2128, "PXA2128", pxa2128_op_array, ARRAY_SIZE(pxa2128_op_array)},
};

int setop(int num)
{
	u32 ddr_type;
	struct proc_op_array *proc = NULL;
	struct operating_point *op;
	unsigned int chip_id;
	int i, j;
	/*u32 gc_clk_res, lcd_clk_res;*/
	struct operating_point cop;
	/* pmua_cc cc_cp, cc_ap; */

	ddr_type = __raw_readl(MCU1_BASE + SDRAM_CTRL4) & 0x0000001C;
	chip_id = __raw_readl(0xd4282c00) & 0xffff;

	for (i = 0; i < ARRAY_SIZE(proc_op_arrays); i++) {
		proc = proc_op_arrays + i;
		if (proc->chip_id == chip_id)
			break;
	}
	if (i >= ARRAY_SIZE(proc_op_arrays))
		return -1;

	printf("Total defined OPs:%d (%d ~ %d)\n",
		proc->nr_op, 0, (proc->nr_op - 1));
	op = proc->op_array;
	/*printf("in the set_op num: %d, op_num: %d, pclk: %d,
	pclk_src: %u\n",num, op[num].op_id, op[num].pclk,op[num].ap_clk_src);*/

	j = proc->nr_op - 1;
	if (op[num].pclk == 0 || op[num].op_id == -1 || num > j) {
		printf("op is not supported\n");
		return -1;
	}

	freq_change_prep();
	enable_fc_intr();
	get_fc_lock();
	get_current_op(&cop);
	PMUcore2_fc_seq(&cop, &op[num]);
	put_fc_lock();
	return 0;
}

void show_op(void)
{
	struct operating_point cop;
	u32 temp;
	get_current_op(&cop);
	if ((__raw_readl(MCU1_BASE + SDRAM_CTRL4) & 0x0000001C) == DDR3)
		temp = cop.dclk_ddr;
	else
		temp = cop.dclk_lp;
	printf("OP: MPs at %3uMHz, MM at %3uMHz, DDR at %3uMHz, "
		"AXI_1 at %3uMHz, AXI_2 at %3uMHz\n",
		cop.pclk, cop.pclk_mm, temp, cop.aclk, cop.aclk2);
}

void pj_cycle_monitor(void)
{
	u32 oldtimer, timer;
	ulong   cycles_this_interval;
	/* clear cycle counter and event counters */
	__asm__ __volatile__ (
	"mcr p15, 0, %[reg], c9, c12, 0"
		:
		:
		[reg] "r" (0x6)
		);
	/* disable cycle counter */
	__asm__ __volatile__ (
	"mcr p15, 0, %[reg], c9, c12, 2"
		:
		:
		[reg] "r" (0x80000000)
		);
	/* reset counter: FIXME - parameter of 0 OK? */
	__asm__ __volatile__ (
	"mcr p15, 0, %[reg], c9, c13, 0"
		:
		:
		[reg] "r" (0)
		);
	/* enable cycle counter */
	__asm__ __volatile__ (
	"mcr p15, 0, %[reg], c9, c12, 1"
		:
		:
		[reg] "r" (0x80000000)
		);
	/* start cycle counter and event counters */
	__asm__ __volatile__ (
	"mcr p15, 0, %[reg], c9, c12, 0"
		:
		:
		[reg] "r" (0x7)
		);
	/* wait 100 mS */
	__raw_writel(0x1, (CONFIG_SYS_TIMERBASE + 0xa4));
	oldtimer = __raw_readl(CONFIG_SYS_TIMERBASE + 0xa4);
	__raw_writel(0x1, (CONFIG_SYS_TIMERBASE + 0xa4));
	oldtimer = __raw_readl(CONFIG_SYS_TIMERBASE + 0xa4);
	while (1) {
		__raw_writel(0x1, (CONFIG_SYS_TIMERBASE + 0xa4));
		timer = __raw_readl(CONFIG_SYS_TIMERBASE + 0xa4);
		__raw_writel(0x1, (CONFIG_SYS_TIMERBASE + 0xa4));
		timer = __raw_readl(CONFIG_SYS_TIMERBASE + 0xa4);
		if (timer >= oldtimer)
			timer = timer - oldtimer;
		else
			timer = (0xffffffff - oldtimer) + timer;
		if (timer >= (CONFIG_SYS_HZ/10))
			break;
	}
	/* end of wait */
	/* pmu read   */
	/* cnt cycles */
	__asm__ __volatile__ (
	"mrc p15, 0, %[reg], c9, c13, 0" :
	[reg] "=r" (cycles_this_interval)
	);
	/* cycles_this_interval has the value */
	/* store the data in a globally visible space */
	/**(volatile unsigned long *)0xd4282c24 = cycles_this_interval;*/
	printf("%lu cycles\n", cycles_this_interval);
}

static const char *op_help_text[] = {
	"-----------------------------------------------------------------------------------------------------\n",
	"| op num | pclk_mp0 | pclk_mp1 | pclk_mm | bclk | lp_dclk | d3_dclk | aclk1 | aclk2 | phclk | atclk |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 0   | 100      | 100      | 100     | 100  | 200     | 400     | 200   | 100   | 100   | 133   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 1   | 26       | 26       | 26      | 50   | 200     | 400     | 50    | 50    | 6.5   | 13    |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 2   | 50       | 50       | 50      | 50   | 200     | 400     | 100   | 100   | 100   | 133   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 3   | 100      | 100      | 50      | 50   | 200     | 400     | 100   | 100   | 100   | 133   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 4   | 200      | 200      | 200     | 200  | 200     | 400     | 200   | 100   | 100   | 133   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 5   | 400      | 400      | 200     | 400  | 200     | 400     | 400   | 200   | 100   | 133   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 6   | 800      | 800      | 400     | 400  | 200     | 400     | 400   | 200   | 200   | 266   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 7   | 531      | 531      | 265     | 531  | 200     | 400     | 400   | 200   | 177   | 354   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 8   | 1063     | 1063     | 265     | 531  | 200     | 400     | 400   | 200   | 177   | 354   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 9   | 400      | 400      | 400     | 400  | 200     | 400     | 400   | 200   | 100   | 133   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 10  | 100      | 100      | 100     | 100  | 400     | 531     | 200   | 100   | 100   | 133   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 11  | 26       | 26       | 26      | 50   | 400     | 531     | 50    | 50    | 6.5   | 13    |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 12  | 50       | 50       | 50      | 50   | 400     | 531     | 100   | 100   | 100   | 133   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 13  | 100      | 100      | 50      | 50   | 400     | 531     | 100   | 100   | 100   | 133   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 14  | 200      | 200      | 200     | 200  | 400     | 531     | 200   | 100   | 100   | 133   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 15  | 400      | 400      | 200     | 400  | 400     | 531     | 400   | 200   | 100   | 133   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 16  | 800      | 800      | 400     | 400  | 400     | 531     | 400   | 200   | 200   | 266   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 17  | 531      | 531      | 265     | 531  | 400     | 531     | 400   | 200   | 177   | 354   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 18  | 1063     | 1063     | 265     | 531  | 400     | 531     | 400   | 200   | 177   | 354   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
	"| OP 19  | 400      | 400      | 400     | 400  | 400     | 531     | 400   | 200   | 100   | 133   |\n",
	"-----------------------------------------------------------------------------------------------------\n",
};

#endif /* non CONFIG_MACH_MMP2 */

#ifdef CONFIG_MACH_MMP2
#define DVFM_BASE_ADDR		0xd1020000
#define DVFM_STACK_ADDR		0xd1028000
extern void freq_init_sram(int addr);
extern void freq_chg_seq(int vaddr, int vstack, int op, int flag);

int do_op(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int op;

	if (argc != 2) {
		printf("usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	op = simple_strtoul(argv[1], NULL, 0);
	dcache_disable();
	freq_init_sram(DVFM_BASE_ADDR);
	freq_chg_seq(DVFM_BASE_ADDR, DVFM_STACK_ADDR, op, 3);
	dcache_enable();
	printf("op %d setting successfully\n", op);

	return 0;
}
#else /* non CONFIG_MACH_MMP2 */
int do_op(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	ulong num;
	printf("Performance before OP mode change: ");
	pj_cycle_monitor();
	if (argc == 2) {
		if (strict_strtoul(argv[1], 0, &num) == 0) {
			if (setop(num) < 0)
				return -1;
			printf("Freq change is done for OP: %lu\n", num);
			printf("Performance after OP mode change: ");
			pj_cycle_monitor();
			show_op();
		}
	}

	if (argc < 2) {
		int i;

		printf("Wrong argument, select op mode from below table: \n");
		for (i = 0; i < ARRAY_SIZE(op_help_text); i++)
			printf("%s", op_help_text[i]);
		printf("Current Operating freq is:\n");
		show_op();
	}

	return 0;
}
#endif /* CONFIG_MACH_MMP2 */

U_BOOT_CMD(
	op,	2,	1,	do_op,
	"change operating point",
	"[ op number ]"
);

#ifdef CONFIG_BROWNSTONE_VOLT

#define VBUCK1_CNT(x)	((x < 0) ? -1 :			\
				((x <= 1380) ? ((x - 750) / 10)	\
				: -1))
#define MAX8649_I2C_SLAVE_ADDR  0x60
#endif

#if defined(CONFIG_MMP_POWER) || defined(CONFIG_BROWNSTONE_VOLT)

int set_volt(u32 vol)
{
	int res = -1;
	u8 data;
	u32 cnt_steps;

#if defined(CONFIG_BROWNSTONE_VOLT)

	cnt_steps = VBUCK1_CNT(vol);
	if (cnt_steps < 0) {
		return -1;
	}
	i2c_set_bus_num(0);
	res = i2c_read(MAX8649_I2C_SLAVE_ADDR, 0x02, 1, &data, 1);
	printf("SD1 old raw val is 0x%x res %d\n", data, res);
	data &= ~0x3f;
	data |= cnt_steps;
	res = i2c_write(MAX8649_I2C_SLAVE_ADDR, 0x02, 1, &data, 1);
	res = i2c_read(MAX8649_I2C_SLAVE_ADDR, 0x02, 1, &data, 1);
	printf("SD1 new raw val is 0x%x res %d\n", data, res);

#elif defined(CONFIG_MACH_ABILENE) || defined(CONFIG_MACH_MK2)

#define MAX77601_SLAVE_ADDR 	0x1c
#define MAX77601_VDVSSD0_REG 	0x1b
#define MAX77601_SD0_REG 	0x16

#define VOL_BASE		600000
#define VOL_STEP		12500
#define VOL_HIGH		1350000

	vol *= 1000;
	if ((vol < VOL_BASE) || (vol > VOL_HIGH)) {
		printf("out of range when set voltage!\n");
		return -1;
	}

	data = (vol - VOL_BASE) / VOL_STEP;
	printf("Sending 0x%x to MAX77601\n", data);
	i2c_set_bus_num(0);
	res = i2c_write(MAX77601_SLAVE_ADDR, MAX77601_VDVSSD0_REG, 1, &data, 1);
	res = i2c_write(MAX77601_SLAVE_ADDR, MAX77601_SD0_REG, 1, &data, 1);

#elif defined(CONFIG_MACH_YELLOWSTONE)

#define FAIR_CHILD_SLAVE_ADDR 	0x60
#define FAIR_CHILD_VDVSSD0_REG 	0x1

#define VOL_BASE		600000
#define VOL_STEP		10000
#define VOL_HIGH		1230000

	vol *= 1000;
	if ((vol < VOL_BASE) || (vol > VOL_HIGH)) {
		printf("out of range when set voltage!\n");
		return -1;
	}

	data = (vol - VOL_BASE) / VOL_STEP;
	data |= 0x80;
	printf("Sending 0x%x to fair child pmic\n", data);
	i2c_set_bus_num(0);
	res = i2c_write(FAIR_CHILD_SLAVE_ADDR, FAIR_CHILD_VDVSSD0_REG, 1, &data, 1);

#elif defined(CONFIG_MACH_ORCHID)

#define PM812_SLAVE_ADDR 	0x31
#define PM812_VBUCK1_SET0_REG 	0x3c

#define VOL_BASE		600000
#define VOL_STEP		12500
#define VOL_HIGH		1350000

	vol *= 1000;
	if ((vol < VOL_BASE) || (vol > VOL_HIGH)) {
		printf("out of range when set voltage!\n");
		return -1;
	}

	data = (vol - VOL_BASE) / VOL_STEP;
	printf("Sending 0x%x to pm812\n", data);
	i2c_set_bus_num(0);
	res = i2c_write(PM812_SLAVE_ADDR, PM812_VBUCK1_SET0_REG, 1, &data, 1);

#endif

	return res;
}

int do_setvolt(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	ulong vol;
	int res = -1;
	if ((argc < 1) || (argc > 2))
		return -1;

	if (argc == 1) {
		printf("usage: setvolt xxxx\n"
			"for tavorevb and ttd_dkb, xxxx can be 725..1500, step 25\n"
			"for aspenite and zylonite2, xxxx can be\n"
			"493  521  550  578  606  635  663  691  720  748  776\n"
			"805  833  861  890  918  947  975  1003 1032 1060 1088\n"
			"1117 1145 1173 1202 1230 1258 1287 1315 1343 1372 1400\n"
			"1429 1457 1485 1514 1542 1570 1599\n"
			"for mmp2 bonnell, xxxx can be 750..1380, step 10\n"
			"for mmp2 brownstone V1/2/3/4, xxxx can be 750..1380, step 25\n"
			"for mmp2 brownstone V5, xxxx can be 790..1450, step 25\n"
			"for mmp2 g50, xxxx can be 1140 1210 1280 1350\n"
			"for mmp3 abliene/mk2, xxxx can be 600..1350, step 13 or 25\n"
			"for mmp3 yellowstone, xxxx can be 600..1230, step 10\n"
			"for mmp3 orchid, xxxx can be 600..1350, step 13 or 25\n"
			);

		return 0;
	}
	res = strict_strtoul(argv[1], 0, &vol);
	if (res == 0 && set_volt(vol) == 0)
		printf("Voltage change was successful\n");
	else
		printf("Voltage change was unsuccessful\n");

	return 0;
}

U_BOOT_CMD(
	setvolt, 6, 1, do_setvolt,
	"Setting voltages",
	""
);
#endif
