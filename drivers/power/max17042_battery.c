/*
 * (C) Copyright 2000
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 */

#include <common.h>
#include "max17042_battery.h"
#include <i2c.h>

enum max17042_register {
	MAX17042_STATUS		= 0x00,
	MAX17042_VALRT_Th	= 0x01,
	MAX17042_TALRT_Th	= 0x02,
	MAX17042_SALRT_Th	= 0x03,
	MAX17042_AtRate		= 0x04,
	MAX17042_RepCap		= 0x05,
	MAX17042_RepSOC		= 0x06,
	MAX17042_Age		= 0x07,
	MAX17042_TEMP		= 0x08,
	MAX17042_VCELL		= 0x09,
	MAX17042_Current	= 0x0A,
	MAX17042_AvgCurrent	= 0x0B,
	MAX17042_Qresidual	= 0x0C,
	MAX17042_SOC		= 0x0D,
	MAX17042_AvSOC		= 0x0E,
	MAX17042_RemCap		= 0x0F,
	MAX17402_FullCAP	= 0x10,
	MAX17042_TTE		= 0x11,
	MAX17042_V_empty	= 0x12,

	MAX17042_RSLOW		= 0x14,

	MAX17042_AvgTA		= 0x16,
	MAX17042_Cycles		= 0x17,
	MAX17042_DesignCap	= 0x18,
	MAX17042_AvgVCELL	= 0x19,
	MAX17042_MinMaxTemp	= 0x1A,
	MAX17042_MinMaxVolt	= 0x1B,
	MAX17042_MinMaxCurr	= 0x1C,
	MAX17042_CONFIG		= 0x1D,
	MAX17042_ICHGTerm	= 0x1E,
	MAX17042_AvCap		= 0x1F,
	MAX17042_ManName	= 0x20,
	MAX17042_DevName	= 0x21,
	MAX17042_DevChem	= 0x22,

	MAX17042_TempNom	= 0x24,
	MAX17042_TempCold	= 0x25,
	MAX17042_TempHot	= 0x26,
	MAX17042_AIN		= 0x27,
	MAX17042_LearnCFG	= 0x28,
	MAX17042_SHFTCFG	= 0x29,
	MAX17042_RelaxCFG	= 0x2A,
	MAX17042_MiscCFG	= 0x2B,
	MAX17042_TGAIN		= 0x2C,
	MAx17042_TOFF		= 0x2D,
	MAX17042_CGAIN		= 0x2E,
	MAX17042_COFF		= 0x2F,

	MAX17042_Q_empty	= 0x33,
	MAX17042_T_empty	= 0x34,

	MAX17042_RCOMP0		= 0x38,
	MAX17042_TempCo		= 0x39,
	MAX17042_Rx		= 0x3A,
	MAX17042_T_empty0	= 0x3B,
	MAX17042_TaskPeriod	= 0x3C,
	MAX17042_FSTAT		= 0x3D,

	MAX17042_SHDNTIMER	= 0x3F,

	MAX17042_VFRemCap	= 0x4A,

	MAX17042_QH		= 0x4D,
	MAX17042_QL		= 0x4E,
};

static int max17042_read_reg(u8 reg, u16 *data)
{
	int status;

	i2c_set_bus_num(0);
	status = i2c_read(MAX17042_I2C_ADDR, reg, 1, (u8 *)data, 2);
	if (status < 0)
		return status;
	else
		return 0;
}

static int max17042_write_reg(u8 reg, u16 data)
{
	int status;
	unsigned char buffer[2];

	buffer[0] = data & 0xff;
	buffer[1] = (data & 0xff00) >> 8;
	i2c_set_bus_num(0);
	status = i2c_write(MAX17042_I2C_ADDR, reg, 1, buffer, 2);
	if (status < 0)
		return status;
	else
		return 0;
}

/* Capacity: % */
int max17042_get_capacity(void)
{
	u16 data;
	int ret = 0;

	ret = max17042_read_reg(MAX17042_RepSOC, &data);
	if (ret < 0)
		return ret;
	ret = data >> 8;
	if (ret > 100)
		ret = 100;
	return ret;
}

/* Voltage: µV */
int max17042_get_voltage(void)
{
	u16 data;
	int ret = 0;

	ret = max17042_read_reg(MAX17042_VCELL, &data);
	if (ret < 0)
		return ret;
	return (data >> 3) * 625;
}

/* Current: µA */
int max17042_get_current(void)
{
	u16 data;
	int t;
	int ret = 0;

	ret = max17042_read_reg(MAX17042_Current, &data);
	if (ret < 0)
		return ret;
	if (0x8000 & data)
		t = ((~data & 0x7FFF) + 1) * -1;
	else
		t = data;
	return t * (1562500 / MAX17042_DEFAULT_R_SNS);
}

/* send SoftPOR cmd just only when the mode is in the unlocked state.
 * Do NOT call this reset function frequently. It would causes the
 * fuel-gauge to forget valuable information which is learned very
 * rarely.
 */
int max17042_reset(void)
{
	u16 reg62, reg63, status;
	int num;

	num = 0;
	do {
		if (max17042_write_reg(0x62, 0x0000) ||
			max17042_write_reg(0x63, 0x0000) ||
			max17042_write_reg(MAX17042_STATUS, 0x0000))
			goto err;
		if (max17042_read_reg(0x62, &reg62) ||
			max17042_read_reg(0x63, &reg63) ||
			max17042_read_reg(MAX17042_STATUS, &status))
			goto err;
		num++;
	} while ((reg62 || reg63 || status) && (num < 50));

	num = 0;
	do {
		/* send softPOR cmd */
		max17042_write_reg(0x60, 0x000f);
		udelay(2000);
		if (max17042_read_reg(MAX17042_STATUS, &status))
			goto err;
		num++;
	} while (((status & 0x2) == 0x0) && (num < 50));

	return 0;
err:
	return -1;
}
