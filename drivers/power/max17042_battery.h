/*
 * (C) Copyright 2000
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __MAX17042_BATTERY_H__
#define __MAX17042_BATTERY_H__

#define MAX17042_I2C_ADDR	(0x36)
#define MAX17042_DEFAULT_R_SNS	(10000) /* mirco-ohms */

/* function declarations */
int max17042_get_capacity(void);
int max17042_get_voltage(void);
int max17042_get_current(void);
int max17042_reset(void);

#endif
