/*
 * linux/drivers/video/pxa168fb.c -- Marvell PXA168 LCD Controller
 *
 *  Copyright (C) 2008 Marvell International Ltd.
 *  All rights reserved.
 *
 *  2009-02-16  adapted from original version for PXA168
 *		Green Wan <gwan@marvell.com>
 *              Jun Nie <njun@marvell.com>
 *		Kevin Liu <kliu5@marvell.com>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

#include <config.h>
#include <common.h>
#include <malloc.h>
#include <linux/types.h>
#include <hdmi.h>
#include <asm/io.h>
#include <common.h>
#include <i2c.h>
#include <asm/gpio.h>
#include "tc35876x.h"
#include <pxa168fb.h>
#include "pxa168fb.h"

#define tc_read16(reg, pval)	i2c_read(0xf, (unsigned int)reg,\
		2, (void *)pval, 2)
#define tc_read32(reg, pval)	i2c_read(0xf, (unsigned int)reg,\
		2, (void *)pval, 4)
#define tc_write32(reg, val)	do { u32 _val; _val = val; \
			i2c_write(0xf, reg, 2, (u8 *)&_val, 4); } while (0)

#define dsi_ex_pixel_cnt                0
#define dsi_hex_en                      0
/* (Unit: Mhz) */
#define dsi_lpclk                       3

#define to_dsi_bcnt(timing, bpp)        (((timing) * (bpp)) >> 3)

struct pxa168fb_info *pxa168_fbi[3];
static unsigned int dsi_lane[5] = {0, 0x1, 0x3, 0x7, 0xf};

void dsi_cclk_set(struct dsi_info *di, int en)
{
	struct dsi_regs *dsi = (struct dsi_regs *)di->regs;

	if (en)
		writel(0x1, &dsi->phy_ctrl1);
	else
		writel(0x0, &dsi->phy_ctrl1);

	udelay(100000);
}

/* dsi phy timing */
static struct dsi_phy phy = {
	.hs_prep_constant       = 60,    /* Unit: ns. */
	.hs_prep_ui             = 5,
	.hs_zero_constant       = 85,
	.hs_zero_ui             = 5,
	.hs_trail_constant      = 0,
	.hs_trail_ui            = 64,
	.hs_exit_constant       = 100,
	.hs_exit_ui             = 0,
	.ck_zero_constant       = 300,
	.ck_zero_ui             = 0,
	.ck_trail_constant      = 60,
	.ck_trail_ui            = 0,
	.req_ready              = 0x3c,
};
void dsi_set_dphy(struct pxa168fb_info *fbi)
{
	struct pxa168fb_mach_info *mi = fbi->mi;
	struct dsi_info *di = (struct dsi_info *)mi->phy_info;
	struct dsi_regs *dsi = (struct dsi_regs *)di->regs;
	u32 ui, lpx_clk, lpx_time, ta_get, ta_go, wakeup, reg;
	u32 hs_prep, hs_zero, hs_trail, hs_exit, ck_zero, ck_trail, ck_exit;

	/* in kernel: ui = 1000/dsi_hsclk + 1 */
	ui = 1000 / (mi->sclk_src / 1000000) + 1;
	lpx_clk = (DSI_ESC_CLK / dsi_lpclk / 2) - 1;
	lpx_time = (lpx_clk + 1) * DSI_ESC_CLK_T;
	/* Below is for NT35451 */
	ta_get = ((lpx_time * 10 + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;
	ta_go  = ((lpx_time * 4 + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;
	wakeup = 0xfff0;

	hs_prep = phy.hs_prep_constant + phy.hs_prep_ui * ui;
	hs_prep = ((hs_prep + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;

	/* Our hardware added 3-byte clk automatically.
	 * 3-byte 3 * 8 * ui.
	 */
	hs_zero = phy.hs_zero_constant + phy.hs_zero_ui * ui;
	if (hs_zero > (24 * ui))
		hs_zero -= (24 * ui);
	else
		hs_zero = DSI_ESC_CLK_T;

	if (hs_zero > (DSI_ESC_CLK_T * 2))
		hs_zero = ((hs_zero + (DSI_ESC_CLK_T >> 1)) \
				/ DSI_ESC_CLK_T) - 1;
	else
		hs_zero = 1;

	hs_trail = phy.hs_trail_constant + phy.hs_trail_ui * ui;
	hs_trail = ((hs_trail + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;

	hs_exit = phy.hs_exit_constant + phy.hs_exit_ui * ui;
	hs_exit = ((hs_exit + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;

	ck_zero = phy.ck_zero_constant + phy.ck_zero_ui * ui;
	ck_zero = ((ck_zero + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;

	ck_trail = phy.ck_trail_constant + phy.ck_trail_ui * ui;
	ck_trail = ((ck_trail + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;

	ck_exit = hs_exit;

	/* bandgap ref enable */
	reg = readl(&dsi->phy_rcomp0);
	reg |= (1<<9);
	writel(reg, &dsi->phy_rcomp0);
	/* timing_0 */
	reg = (hs_exit << DSI_PHY_TIME_0_CFG_CSR_TIME_HS_EXIT_SHIFT)
		| (hs_trail << DSI_PHY_TIME_0_CFG_CSR_TIME_HS_TRAIL_SHIFT)
		| (hs_zero << DSI_PHY_TIME_0_CDG_CSR_TIME_HS_ZERO_SHIFT)
		| (hs_prep);

	writel(reg, &dsi->phy_timing0);
	reg = (ta_get << DSI_PHY_TIME_1_CFG_CSR_TIME_TA_GET_SHIFT)
		| (ta_go << DSI_PHY_TIME_1_CFG_CSR_TIME_TA_GO_SHIFT)
		| wakeup;
	writel(reg, &dsi->phy_timing1);
	reg = (ck_exit << DSI_PHY_TIME_2_CFG_CSR_TIME_CK_EXIT_SHIFT)
		| (ck_trail << DSI_PHY_TIME_2_CFG_CSR_TIME_CK_TRAIL_SHIFT)
		| (ck_zero << DSI_PHY_TIME_2_CFG_CSR_TIME_CK_ZERO_SHIFT)
		| lpx_clk;
	writel(reg, &dsi->phy_timing2);

	reg = (lpx_clk << DSI_PHY_TIME_3_CFG_CSR_TIME_LPX_SHIFT) | \
	      phy.req_ready;
	writel(reg, &dsi->phy_timing3);
	/* calculated timing on brownstone:
	 * DSI_PHY_TIME_0 0x06080204
	 * DSI_PHY_TIME_1 0x6d2bfff0
	 * DSI_PHY_TIME_2 0x603130a
	 * DSI_PHY_TIME_3 0xa3c
	 */
}

void dsi_reset(struct dsi_info *di, int hold)
{
	struct dsi_regs *dsi = (struct dsi_regs *)di->regs;
	unsigned int reg;

	writel(0x0, &dsi->ctrl0);
	reg = readl(&dsi->ctrl0);
	reg |= DSI_CTRL_0_CFG_SOFT_RST | DSI_CTRL_0_CFG_SOFT_RST_REG;

	if (!hold) {
		writel(reg, &dsi->ctrl0);
		reg &= ~(DSI_CTRL_0_CFG_SOFT_RST | DSI_CTRL_0_CFG_SOFT_RST_REG);
		udelay(1000);
	}
	writel(reg, &dsi->ctrl0);
}

#if defined(DEBUG_PXA168FB)
#define DSIW(x, y)  do {writel(x, y); printf("address %x, \
		value %x\n", y, x); } while (0)
#else
#define DSIW(x, y)  do {writel(x, y); } while (0)
#endif
void dsi_set_controller(struct pxa168fb_info *fbi)
{
	struct fb_var_screeninfo *var = fbi->var;
	struct pxa168fb_mach_info *mi = fbi->mi;
	struct dsi_info *di = (struct dsi_info *)mi->phy_info;
	struct dsi_regs *dsi = (struct dsi_regs *)di->regs;
	struct dsi_lcd_regs *dsi_lcd = &dsi->lcd1;
	unsigned hsync_b, hbp_b, hact_b, hex_b, hfp_b, httl_b;
	unsigned hsync, hbp, hact, hfp, httl, h_total, v_total;
	unsigned hsa_wc, hbp_wc, hact_wc, hex_wc, hfp_wc, hlp_wc;
	int bpp = di->bpp, hss_bcnt = 4, hse_bct = 4, lgp_over_head = 6, reg;

	if (di->id & 2)
		dsi_lcd = &dsi->lcd2;

	h_total = var->xres + var->left_margin + var->right_margin
		+ var->hsync_len;
	v_total = var->yres + var->upper_margin + var->lower_margin
		+ var->vsync_len;

	hact_b = to_dsi_bcnt(var->xres, bpp);
	hfp_b = to_dsi_bcnt(var->right_margin, bpp);
	hbp_b = to_dsi_bcnt(var->left_margin, bpp);
	hsync_b = to_dsi_bcnt(var->hsync_len, bpp);
	hex_b = to_dsi_bcnt(dsi_ex_pixel_cnt, bpp);
	httl_b = hact_b + hsync_b + hfp_b + hbp_b + hex_b;

	hact = hact_b / di->lanes;
	hfp = hfp_b / di->lanes;
	hbp = hbp_b / di->lanes;
	hsync = hsync_b / di->lanes;
	httl = hact + hfp + hbp + hsync;
	/* word count in the unit of byte */
	hsa_wc = (di->burst_mode == DSI_BURST_MODE_SYNC_PULSE) ? \
		(hsync_b - hss_bcnt - lgp_over_head) : 0;

	/* Hse is with backporch */
	hbp_wc = (di->burst_mode == DSI_BURST_MODE_SYNC_PULSE) ? \
		(hbp_b - hse_bct - lgp_over_head) \
		: (hsync_b + hbp_b - hss_bcnt - lgp_over_head);

	hfp_wc = ((di->burst_mode == DSI_BURST_MODE_BURST) \
			&& (dsi_hex_en == 0)) ? \
		(hfp_b + hex_b - lgp_over_head - lgp_over_head) : \
		(hfp_b - lgp_over_head - lgp_over_head);

	hact_wc =  ((var->xres) * bpp) >> 3;

	/* disable Hex currently */
	hex_wc = 0;

	/*  There is no hlp with active data segment.  */
	hlp_wc = (di->burst_mode == DSI_BURST_MODE_SYNC_PULSE) ? \
		(httl_b - hsync_b - hse_bct - lgp_over_head) : \
		(httl_b - hss_bcnt - lgp_over_head);

	/* FIXME - need to double check the (*3) is bytes_per_pixel
	 * from input data or output to panel
	 */
	/* dsi_lane_enable - Set according to specified DSI lane count */
	DSIW(dsi_lane[di->lanes] << DSI_PHY_CTRL_2_CFG_CSR_LANE_EN_SHIFT, \
			&dsi->phy_ctrl2);
	DSIW(dsi_lane[di->lanes] << DSI_CPU_CMD_1_CFG_TXLP_LPDT_SHIFT, \
			&dsi->cmd1);

	/* SET UP LCD1 TIMING REGISTERS FOR DSI BUS */
	/* NOTE: Some register values were obtained by trial and error */
	DSIW((hact << 16) | httl, &dsi_lcd->timing0);
	DSIW((hsync << 16) | hbp, &dsi_lcd->timing1);
	/*
	 * For now the active size is set really low (we'll use 10) to allow
	 * the hardware to attain V Sync. Once the DSI bus is up and running,
	 * the final value will be put in place for the active size (this is
	 * done below). In a later stepping of the processor this workaround
	 * will not be required.
	 */
	DSIW(((var->yres)<<16) | (v_total), &dsi_lcd->timing2);

	DSIW(((var->vsync_len) << 16) | (var->upper_margin), \
			&dsi_lcd->timing3);
	/* SET UP LCD1 WORD COUNT REGISTERS FOR DSI BUS */
	/* Set up for word(byte) count register 0 */
	DSIW((hbp_wc << 16) | hsa_wc, &dsi_lcd->wc0);
	DSIW((hfp_wc << 16) | hact_wc, &dsi_lcd->wc1);
	DSIW((hex_wc << 16) | hlp_wc, &dsi_lcd->wc2);
	/* calculated value on brownstone:
	 * WC0: 0x1a0000
	 * WC1: 0x1500f00
	 * WC2: 0x1076 */

	/* Configure LCD control register 1 FOR DSI BUS */
	reg = ((di->rgb_mode << DSI_LCD2_CTRL_1_CFG_L1_RGB_TYPE_SHIFT)
		| (di->burst_mode << DSI_LCD1_CTRL_1_CFG_L1_BURST_MODE_SHIFT)
		| (di->lpm_line_en ? DSI_LCD1_CTRL_1_CFG_L1_LPM_LINE_EN : 0)
		| (di->lpm_frame_en ? DSI_LCD1_CTRL_1_CFG_L1_LPM_FRAME_EN : 0)
		| (di->last_line_turn ? DSI_LCD1_CTRL_1_CFG_L1_LAST_LINE_TURN \
			: 0)
		| (di->hex_slot_en ? 0 : 0)   /* disable Hex slot */
		| (di->all_slot_en ? 0 : 0)   /* disable all slots */
		| (di->hbp_en ? DSI_LCD1_CTRL_1_CFG_L1_HBP_PKT_EN : 0)
		| (di->hact_en ? DSI_LCD1_CTRL_1_CFG_L1_HACT_PKT_EN : 0)
		| (di->hfp_en ? DSI_LCD1_CTRL_1_CFG_L1_HFP_PKT_EN : 0)
		| (di->hex_en ? 0 : 0)      /* Hex packet is disabled */
		| (di->hlp_en ? DSI_LCD1_CTRL_1_CFG_L1_HLP_PKT_EN : 0));

	reg |= (di->burst_mode == DSI_BURST_MODE_SYNC_PULSE) ? \
		(((di->hsa_en) ? DSI_LCD1_CTRL_1_CFG_L1_HSA_PKT_EN : 0)
		| (DSI_LCD1_CTRL_1_CFG_L1_HSE_PKT_EN))  /* Hse is always
							    eabled */
		:
		(((di->hsa_en) ? 0 : 0)   /* Hsa packet is disabled */
		| ((di->hse_en) ? 0 : 0));     /* Hse packet is disabled */

	reg |=  DSI_LCD1_CTRL_1_CFG_L1_VSYNC_RST_EN;
	DSIW(reg, &dsi_lcd->ctrl1);
	/*Start the transfer of LCD data over the DSI bus*/
	/* DSI_CTRL_1 */
	reg = readl(&dsi->ctrl1);
	reg &= ~(DSI_CTRL_1_CFG_LCD2_VCH_NO_MASK \
			| DSI_CTRL_1_CFG_LCD1_VCH_NO_MASK);
	reg |= 0x1 << ((di->id & 1) ? DSI_CTRL_1_CFG_LCD2_VCH_NO_SHIFT \
			: DSI_CTRL_1_CFG_LCD1_VCH_NO_SHIFT);

	reg &= ~(DSI_CTRL_1_CFG_EOTP);
	if (di->eotp_en)
		reg |= DSI_CTRL_1_CFG_EOTP;	/* EOTP */

	DSIW(reg, &dsi->ctrl1);
	/* DSI_CTRL_0 */
	reg = DSI_CTRL_0_CFG_LCD1_SLV | DSI_CTRL_0_CFG_LCD1_TX_EN \
	      | DSI_CTRL_0_CFG_LCD1_EN;
	if (di->id & 2)
		reg = reg << 1;
	DSIW(reg, &dsi->ctrl0);
	udelay(100000);

	/* FIXME - second part of the workaround */
	DSIW(((var->yres)<<16) | (v_total), &dsi_lcd->timing2);
}

static int tc358765_reset(void)
{
	gpio_set_value(LCD_RST_GPIO, 0);
	udelay(100000);

	gpio_set_value(LCD_RST_GPIO, 1);
	udelay(100000);

	return 0;
}

#define TC358765_CHIPID_REG	0x0580
#define TC358765_CHIPID		0x6500
static int dsi_dump_tc358765(void)
{
#if 0
	u32 val;

	tc_read32(PPI_TX_RX_TA, &val);
	printf("tc35876x - PPI_TX_RX_TA = 0x%x\n", val);
	tc_read32(PPI_LPTXTIMECNT, &val);
	printf("tc35876x - PPI_LPTXTIMECNT = 0x%x\n", val);
	tc_read32(PPI_D0S_CLRSIPOCOUNT, &val);
	printf("tc35876x - PPI_D0S_CLRSIPOCOUNT = 0x%x\n", val);
	tc_read32(PPI_D1S_CLRSIPOCOUNT, &val);
	printf("tc35876x - PPI_D1S_CLRSIPOCOUNT = 0x%x\n", val);

	tc_read32(PPI_D2S_CLRSIPOCOUNT, &val);
	printf("tc35876x - PPI_D2S_CLRSIPOCOUNT = 0x%x\n", val);
	tc_read32(PPI_D3S_CLRSIPOCOUNT, &val);
	printf("tc35876x - PPI_D3S_CLRSIPOCOUNT = 0x%x\n", val);

	tc_read32(PPI_LANEENABLE, &val);
	printf("tc35876x - PPI_LANEENABLE = 0x%x\n", val);
	tc_read32(DSI_LANEENABLE, &val);
	printf("tc35876x - DSI_LANEENABLE = 0x%x\n", val);
	tc_read32(PPI_STARTPPI, &val);
	printf("tc35876x - PPI_STARTPPI = 0x%x\n", val);
	tc_read32(DSI_STARTDSI, &val);
	printf("tc35876x - DSI_STARTDSI = 0x%x\n", val);

	tc_read32(VPCTRL, &val);
	printf("tc35876x - VPCTRL = 0x%x\n", val);
	tc_read32(HTIM1, &val);
	printf("tc35876x - HTIM1 = 0x%x\n", val);
	tc_read32(HTIM2, &val);
	printf("tc35876x - HTIM2 = 0x%x\n", val);
	tc_read32(VTIM1, &val);
	printf("tc35876x - VTIM1 = 0x%x\n", val);
	tc_read32(VTIM2, &val);
	printf("tc35876x - VTIM2 = 0x%x\n", val);
	tc_read32(VFUEN, &val);
	printf("tc35876x - VFUEN = 0x%x\n", val);
	tc_read32(LVCFG, &val);
	printf("tc35876x - LVCFG = 0x%x\n", val);

	tc_read32(DSI_INTSTAUS, &val);
	printf("!! - DSI_INTSTAUS= 0x%x BEFORE\n", val);
	tc_write32(DSI_INTCLR, 0xFFFFFFFF);
	tc_read32(DSI_INTSTAUS, &val);
	printf("!! - DSI_INTSTAUS= 0x%x AFTER\n", val);

	tc_read32(DSI_LANESTATUS0, &val);
	printf("tc35876x - DSI_LANESTATUS0= 0x%x\n", val);
	tc_read32(DSIERRCNT, &val);
	printf("tc35876x - DSIERRCNT= 0x%x\n", val);
	tc_read32(DSIERRCNT, &val);
	printf("tc35876x - DSIERRCNT= 0x%x AGAIN\n", val);
	tc_read32(SYSSTAT, &val);
	printf("tc35876x - SYSSTAT= 0x%x\n", val);
#endif
	return 0;
}

static int dsi_set_tc358765(struct dsi_info *di, unsigned int twsi_id)
{
	unsigned int cur_bus = i2c_get_bus_num();
	u16 chip_id = 0;
	int status;

	/* Switch to TWSIx bus */
	i2c_set_bus_num(twsi_id);
	status = tc_read16(TC358765_CHIPID_REG, &chip_id);
	if ((status < 0) || (chip_id != TC358765_CHIPID)) {
		printf("tc358765 chip ID is 0x%x\n", chip_id);
		i2c_set_bus_num(cur_bus);
		return -1;
	}

	/* REG 0x13C,DAT 0x000C000F */
	tc_write32(PPI_TX_RX_TA, 0x00040004);
	/* REG 0x114,DAT 0x0000000A */
	tc_write32(PPI_LPTXTIMECNT, 0x00000004);

	/* get middle value of mim-max value
	 * 0-0x13 for 2lanes-rgb888, 0-0x26 for 4lanes-rgb888
	 * 0-0x21 for 2lanes-rgb565, 0-0x25 for 4lanes-rgb565
	 */
	if (di->lanes == 4)
		status = 0x13;
	else if (di->bpp == 24)
		status = 0xa;
	else
		status = 0x11;
	/* REG 0x164,DAT 0x00000005 */
	tc_write32(PPI_D0S_CLRSIPOCOUNT, status);
	/* REG 0x168,DAT 0x00000005 */
	tc_write32(PPI_D1S_CLRSIPOCOUNT, status);
	if (di->lanes == 4) {
		/* REG 0x16C,DAT 0x00000005 */
		tc_write32(PPI_D2S_CLRSIPOCOUNT, status);
		/* REG 0x170,DAT 0x00000005 */
		tc_write32(PPI_D3S_CLRSIPOCOUNT, status);
	}

	/* REG 0x134,DAT 0x00000007 */
	tc_write32(PPI_LANEENABLE, (di->lanes == 4) ? 0x1f : 0x7);
	/* REG 0x210,DAT 0x00000007 */
	tc_write32(DSI_LANEENABLE, (di->lanes == 4) ? 0x1f : 0x7);

	/* REG 0x104,DAT 0x00000001 */
	tc_write32(PPI_STARTPPI, 0x0000001);
	/* REG 0x204,DAT 0x00000001 */
	tc_write32(DSI_STARTDSI, 0x0000001);

	/* REG 0x450,DAT 0x00012020, VSDELAY = 8 pixels */
	tc_write32(VPCTRL, 0x00800020);

	/* REG 0x454,DAT 0x00200008*/
	tc_write32(HTIM1, 0x00200008);

	/* REG 0x45C,DAT 0x00040004*/
	tc_write32(VTIM1, 0x00040004);

        /* Test TSB */
        tc_write32(0x0464, 0x00000001); /* VFUEN */
        tc_write32(0x04A0, 0x00448006); /* LVPHY0 */
        /* Wait is needed heare */
        tc_write32(0x04A0, 0x00048006); /* LVPHY0*/
        tc_write32(0x0504, 0x00000004); /* SYSRST */

        tc_write32(0x0520, 0x0000001F); /* GPIOC */
        tc_write32(0x0524, 0x00000000); /* GPIOO */

	/* REG 0x49C,DAT 0x00000201 */
	tc_write32(LVCFG, 0x00000001);

	dsi_dump_tc358765();
	i2c_set_bus_num(cur_bus);

	return 0;
}

static int brownstone_dsi_init(struct pxa168fb_info *fbi)
{
	struct pxa168fb_mach_info *mi = fbi->mi;
	struct dsi_info *di = (struct dsi_info *)mi->phy_info;
	int ret = 0;

	/* reset DSI controller */
	dsi_reset(di, 1);
	udelay(1000);

	/* disable continuous clock */
	dsi_cclk_set(di, 0);

	/*  reset the bridge */
	tc358765_reset();

	/* dsi out of reset */
	dsi_reset(di, 0);

	/* set dphy */
	dsi_set_dphy(fbi);

	/* set dsi controller */
	dsi_set_controller(fbi);

	/* turn on DSI continuous clock */
	dsi_cclk_set(di, 1);

	/* set dsi to dpi conversion chip */
	if (mi->phy_type == DSI2DPI) {
		ret = dsi_set_tc358765(di, mi->twsi_id);
		if (ret < 0)
			printf("dsi2dpi_set error!\n");
	}

	return 0;
}

struct lcd_regs *get_regs(struct pxa168fb_info *fbi)
{
	struct lcd_regs *regs = (struct lcd_regs *)((unsigned)fbi->reg_base);

	if (fbi->id == 0)
		regs = (struct lcd_regs *)((unsigned)fbi->reg_base + 0xc0);
	if (fbi->id == 2)
		regs = (struct lcd_regs *)((unsigned)fbi->reg_base + 0x200);

	return regs;
}

u32 dma_ctrl_read(struct pxa168fb_info *fbi, int ctrl1)
{
	u32 reg = (u32)fbi->reg_base + dma_ctrl(ctrl1, fbi->id);
	return __raw_readl(reg);
}

void dma_ctrl_write(struct pxa168fb_info *fbi, int ctrl1, u32 value)
{
	u32 reg = (u32)fbi->reg_base + dma_ctrl(ctrl1, fbi->id);
	__raw_writel(value, reg);
}

void dma_ctrl_set(struct pxa168fb_info *fbi, int ctrl1, u32 mask, u32 value)
{
	u32 reg = (u32)fbi->reg_base + dma_ctrl(ctrl1, fbi->id);
	u32 tmp1, tmp2;

	tmp1 = tmp2 = __raw_readl(reg); tmp2 &= ~mask; tmp2 |= value;
	if (tmp1 != tmp2)
		__raw_writel(tmp2, reg);
}

static void set_pix_fmt(struct fb_var_screeninfo *var, int pix_fmt)
{
	switch (pix_fmt) {
	case PIX_FMT_RGB565:
		var->bits_per_pixel = 16;
		var->red.offset = 11;    var->red.length = 5;
		var->green.offset = 5;   var->green.length = 6;
		var->blue.offset = 0;    var->blue.length = 5;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		break;
	case PIX_FMT_BGR565:
		var->bits_per_pixel = 16;
		var->red.offset = 0;     var->red.length = 5;
		var->green.offset = 5;   var->green.length = 6;
		var->blue.offset = 11;   var->blue.length = 5;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		break;
	case PIX_FMT_RGB1555:
		var->bits_per_pixel = 16;
		var->red.offset = 10;    var->red.length = 5;
		var->green.offset = 5;   var->green.length = 5;
		var->blue.offset = 0;    var->blue.length = 5;
		var->transp.offset = 15; var->transp.length = 1;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 5 << 20;
		break;
	case PIX_FMT_BGR1555:
		var->bits_per_pixel = 16;
		var->red.offset = 0;     var->red.length = 5;
		var->green.offset = 5;   var->green.length = 5;
		var->blue.offset = 10;   var->blue.length = 5;
		var->transp.offset = 15; var->transp.length = 1;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 5 << 20;
		break;
	case PIX_FMT_RGB888PACK:
		var->bits_per_pixel = 24;
		var->red.offset = 16;    var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 0;    var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 6 << 20;
		break;
	case PIX_FMT_BGR888PACK:
		var->bits_per_pixel = 24;
		var->red.offset = 0;     var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 16;   var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 6 << 20;
		break;
	case PIX_FMT_RGB888UNPACK:
		var->bits_per_pixel = 32;
		var->red.offset = 16;    var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 0;    var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 7 << 20;
		break;
	case PIX_FMT_BGR888UNPACK:
		var->bits_per_pixel = 32;
		var->red.offset = 0;     var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 16;   var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 7 << 20;
		break;
	case PIX_FMT_RGBA888:
		var->bits_per_pixel = 32;
		var->red.offset = 16;    var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 0;    var->blue.length = 8;
		var->transp.offset = 24; var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 8 << 20;
		break;
	case PIX_FMT_BGRA888:
		var->bits_per_pixel = 32;
		var->red.offset = 0;     var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 16;   var->blue.length = 8;
		var->transp.offset = 24; var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 8 << 20;
		break;
	}
}

/*
 * The hardware clock divider has an integer and a fractional
 * stage:
 *
 *	clk2 = clk_in / integer_divider
 *	clk_out = clk2 * (1 - (fractional_divider >> 12))
 *
 * Calculate integer and fractional divider for given clk_in
 * and clk_out.
 */
static void set_clock_divider(struct pxa168fb_info *fbi)
{
	struct pxa168fb_mach_info *mi = fbi->mi;

	if (! fbi->id) {
		writel(mi->sclk_div, fbi->reg_base + clk_div(fbi->id));
		if (mi->phy_type & LVDS)
			writel(mi->sclk_div, fbi->reg_base + LCD_LVDS_SCLK_DIV_WR);
	} else
		writel(0x60010005, fbi->reg_base + clk_div(fbi->id));
}

static u32 dma_ctrl0_update(int active, struct pxa168fb_mach_info *mi,
		u32 x, u32 pix_fmt)
{
	if (active)
		x |= 0x00000100;
	else
		x &= ~0x00000100;

	/* If we are in a pseudo-color mode, we need to enable
	 * palette lookup  */
	if (pix_fmt == PIX_FMT_PSEUDOCOLOR)
		x |= 0x10000000;

	/* Configure hardware pixel format */
	x &= ~(0xF << 16);
	x |= (pix_fmt >> 1) << 16;

	/* Check YUV422PACK */
	x &= ~((1 << 9) | (1 << 11) | (1 << 10) | (1 << 12));
	if (((pix_fmt >> 1) == 5) || (pix_fmt & 0x1000)) {
		x |= 1 << 9;
		x |= (mi->panel_rbswap) << 12;
		if (pix_fmt == 11)
			x |= 1 << 11;
		if (pix_fmt & 0x1000)
			x |= 1 << 10;
	} else {
		/* Check red and blue pixel swap.
		 * 1. source data swap. BGR[M:L] rather than RGB[M:L] is
		 * stored in memeory as source format.
		 * 2. panel output data swap
		 */
		x |= (((pix_fmt & 1) ^ 1) ^ (mi->panel_rbswap)) << 12;
	}
	/* enable horizontal smooth filter for both graphic and video layers */
	x |= CFG_GRA_HSMOOTH(1) | CFG_DMA_HSMOOTH(1);

	return x;
}

static void set_dma_control0(struct pxa168fb_info *fbi)
{
	struct pxa168fb_mach_info *mi;
	u32 x = 0, active, pix_fmt = fbi->pix_fmt;

	/* Set bit to enable graphics DMA */
	if (fbi->id != 1)
		dma_ctrl_set(fbi, 0, CFG_ARBFAST_ENA(1), CFG_ARBFAST_ENA(1));

	mi = fbi->mi;
	active = fbi->active;
	x = dma_ctrl_read(fbi, 0);
	active = 1;
	x = dma_ctrl0_update(active, mi, x, pix_fmt);
	dma_ctrl_write(fbi, 0, x);
	/* enable multiple burst request in DMA AXI bus arbiter for
	 * faster read
	 */
	x = readl(fbi->reg_base + LCD_SPU_DMA_CTRL0);
	x |= CFG_ARBFAST_ENA(1);
	writel(x, fbi->reg_base + LCD_SPU_DMA_CTRL0);
}

static void set_dma_control1(struct pxa168fb_info *fbi, int sync)
{
	u32 x;

	/* Configure default bits: vsync triggers DMA, gated clock
	 * enable, power save enable, configure alpha registers to
	 * display 100% graphics, and set pixel command.
	 */
	x = dma_ctrl_read(fbi, 1);
	/* We trigger DMA on the falling edge of vsync if vsync is
	 * active low, or on the rising edge if vsync is active high.
	 */
	if (!(sync & FB_SYNC_VERT_HIGH_ACT))
		x |= 0x08000000;
	else
		x &= ~0x08000000;
	dma_ctrl_write(fbi, 1, x);
}

static void set_graphics_start(struct pxa168fb_info *fb, int xoffset,
		int yoffset)
{
	struct pxa168fb_info *fbi = fb;
	struct fb_var_screeninfo *var = fb->var;
	int pixel_offset;
	unsigned long addr;
	static int debugcount;
	struct lcd_regs *regs;

	if (debugcount < 10)
		debugcount++;

	pixel_offset = (yoffset * var->xres_virtual) + xoffset;
	addr = fbi->fb_start_dma + (pixel_offset * (var->bits_per_pixel >> 3));

	regs = get_regs(fbi);
	writel(addr, &regs->g_0);
}

static void set_screen(struct pxa168fb_info *fbi, struct pxa168fb_mach_info *mi)
{
	struct fb_var_screeninfo *var = fbi->var;
	struct lcd_regs *regs = get_regs(fbi);
	struct dsi_info *di = NULL;
	u32 x, h_porch, vsync_ctrl, vec = 10;
	u32 xres = var->xres, yres = var->yres;
	u32 xres_z = var->xres, yres_z = var->yres;
	u32 xres_virtual = var->xres_virtual;
	u32 bits_per_pixel = var->bits_per_pixel;

	if (mi->phy_type & (DSI2DPI | DSI)) {
		di = (struct dsi_info *)mi->phy_info;
		vec = ((di->lanes <= 2) ? 1 : 2) * 10 * di->bpp / 8 / di->lanes;
	}
	/* resolution, active */
	writel((var->yres << 16) | var->xres, &regs->screen_active);
	/* pitch, pixels per line */
	x = readl(&regs->g_pitch);
	if (mi->index)
		x = (x & ~0xFFFF) | ((pxa168_fbi[0]->var->xres_virtual * bits_per_pixel) >> 3);
	else
		x = (x & ~0xFFFF) | ((xres_virtual * bits_per_pixel) >> 3);
	writel(x, &regs->g_pitch);

	/* resolution, src size */
	if (mi->index) {
		/* share same content on pn0 and tv path */
		writel((pxa168_fbi[0]->var->yres << 16) | pxa168_fbi[0]->var->xres,
				&regs->g_size);
	} else {
		writel((yres << 16) | xres, &regs->g_size);
	}
	/* resolution, dst size */
	writel((yres_z << 16) | xres_z, &regs->g_size_z);

	/* h porch, left/right margin */
	if (mi->phy_type & (DSI2DPI | DSI)) {
		h_porch = (var->xres + var->right_margin) * vec / 10
			- var->xres;
		h_porch = (var->left_margin * vec / 10) << 16 | h_porch;
	} else
		h_porch = (var->left_margin) << 16 | var->right_margin;
	writel(h_porch, &regs->screen_h_porch);
	/* v porch, upper/lower margin */
	writel((var->upper_margin << 16) | var->lower_margin,
		&regs->screen_v_porch);

	/* vsync ctrl */
	if (mi->phy_type & (DSI2DPI | DSI))
		vsync_ctrl = 0x01330133;
	else {
		if ((fbi->id == 0) || (fbi->id == 2))
			vsync_ctrl = ((var->width + var->left_margin) << 16)
				| (var->width + var->left_margin);
		else
			vsync_ctrl = ((var->xres + var->right_margin) << 16)
				| (var->xres + var->right_margin);
	}
	writel(vsync_ctrl, &regs->vsync_ctrl);  /* FIXME */

	/* blank color */
	writel(0x00000000, &regs->blank_color);
}

static void set_dumb_panel_control(struct pxa168fb_info *fbi,
		struct pxa168fb_mach_info *mi)
{
	u32 x;

	x = readl(fbi->reg_base + intf_ctrl(fbi->id)) & 0x00000001;
	x |= (fbi->is_blanked ? 0x7 : mi->dumb_mode) << 28;
	x |= (fbi->var->sync & 2) ? 0 : 0x00000008;
	x |= (fbi->var->sync & 1) ? 0 : 0x00000004;
	x |= (fbi->var->sync & 8) ? 0 : 0x00000020;
	if (fbi->id == 1) {
		/* enable AXI urgent flag */
		x |= (1 << 12) | (0xff << 16);
		writel(x, fbi->reg_base + intf_ctrl(fbi->id));  /* FIXME */
		return;
	} else {
		x |= mi->gpio_output_data << 20;
		x |= mi->gpio_output_mask << 12;
	}
	x |= mi->panel_rgb_reverse_lanes ? 0x00000080 : 0;
	x |= mi->invert_composite_blank ? 0x00000040 : 0;
	x |= mi->invert_pix_val_ena ? 0x00000010 : 0;
	x |= mi->invert_pixclock ? 0x00000002 : 0;

	writel(x, fbi->reg_base + intf_ctrl(fbi->id));  /* FIXME */
}

static void set_dumb_screen_dimensions(struct pxa168fb_info *fbi,
		struct pxa168fb_mach_info *mi)
{
	struct lcd_regs *regs = get_regs(fbi);
	struct fb_var_screeninfo *v = fbi->var;
	struct dsi_info *di = NULL;
	int x;
	int y;
	int vec = 10;

	/* FIXME - need to double check (*3) and (*2) */
	if (mi->phy_type & (DSI2DPI | DSI)) {
		di = (struct dsi_info *)mi->phy_info;
		vec = ((di->lanes <= 2) ? 1 : 2) * 10 * di->bpp / 8 / di->lanes;
	}

	x = v->xres + v->right_margin + v->hsync_len + v->left_margin;
	x = x * vec / 10;
	y = v->yres + v->lower_margin + v->vsync_len + v->upper_margin;

	writel((y << 16) | x, &regs->screen_size);
}

static int pxa168fb_set_par(struct pxa168fb_info *fb,
		struct pxa168fb_mach_info *mi)
{
	struct pxa168fb_info *fbi = fb;
	struct fb_var_screeninfo *var = fb->var;
	int pix_fmt;
	u32 x;

	/* Determine which pixel format we're going to use */
	pix_fmt = mi->pix_fmt;    /* choose one */

	if (pix_fmt < 0)
		return pix_fmt;
	fbi->pix_fmt = pix_fmt;

	/* Set additional mode info */
	if (pix_fmt == PIX_FMT_PSEUDOCOLOR)
		fb->fix->visual = FB_VISUAL_PSEUDOCOLOR;
	else
		fb->fix->visual = FB_VISUAL_TRUECOLOR;

	/* convet var to video mode */
	set_pix_fmt(var, pix_fmt);
	if (!var->xres_virtual)
		var->xres_virtual = var->xres;
	if (!var->yres_virtual)
		var->yres_virtual = var->yres * 2;
	/* Calculate clock divisor. */
	set_clock_divider(fbi);
	/* Configure dma ctrl regs. */
	set_dma_control1(fbi, fb->var->sync);

	/* Configure graphics DMA parameters.
	 * Configure global panel parameters.
	 */
	set_screen(fbi, mi);
	/* Configure dumb panel ctrl regs & timings */
	set_dumb_panel_control(fbi, mi);
	set_dumb_screen_dimensions(fbi, mi);
	x = readl(fbi->reg_base + intf_ctrl(fbi->id));
	if ((x & 1) == 0)
		writel(x | 1, fbi->reg_base + intf_ctrl(fbi->id));

	set_graphics_start(fbi, fbi->var->xoffset, fbi->var->yoffset);
	set_dma_control0(fbi);
	return 0;
}

static void pxa168fb_init_mode(struct pxa168fb_info *fbi,
			      struct pxa168fb_mach_info *mi)
{
	struct fb_var_screeninfo *var = fbi->var;

	/* Init mode */
	var->xres = mi->modes->xres;
	var->yres = mi->modes->yres;
	var->xres_virtual = mi->modes->xres;
	var->yres_virtual = mi->modes->yres;
	var->xoffset = 0;
	var->yoffset = 0;
	var->pixclock = mi->modes->pixclock;
	var->left_margin = mi->modes->left_margin;
	var->right_margin = mi->modes->right_margin;
	var->upper_margin = mi->modes->upper_margin;
	var->lower_margin = mi->modes->lower_margin;
	var->hsync_len = mi->modes->hsync_len;
	var->vsync_len = mi->modes->vsync_len;
	var->sync = mi->modes->sync;

	/* Init settings. */
	var->xres_virtual = var->xres;
	var->yres_virtual = var->yres * 2;
}

static void pxa168fb_set_default(struct pxa168fb_info *fbi,
		struct pxa168fb_mach_info *mi)
{
	struct lcd_regs *regs = get_regs(fbi);
	u32 dma_ctrl1 = 0x2012ff81;
	u32 burst_length = (mi->burst_len == 16) ?
		CFG_CYC_BURST_LEN16 : CFG_CYC_BURST_LEN8;
	/* Configure default register values */
	writel(mi->io_pin_allocation_mode | burst_length,
			fbi->reg_base + SPU_IOPAD_CONTROL);
	/* enable 16 cycle burst length to get better formance */
	writel(0x00000000, &regs->blank_color);
	writel(0x00000000, &regs->g_1);
	writel(0x00000000, &regs->g_start);

	/* Configure default bits: vsync triggers DMA,
	 * power save enable, configure alpha registers to
	 * display 100% graphics, and set pixel command.
	 */
	if (fbi->id == 1) {
		writel(0x60010005, fbi->reg_base + LCD_TCLK_DIV);

		if (mi->phy_type & (DSI2DPI | DSI))
			dma_ctrl1 = 0xa03eff00;
		else
			dma_ctrl1 = 0x203eff00;	/* FIXME */
	}
	dma_ctrl_write(fbi, 1, dma_ctrl1);
}

static void rect_fill(unsigned short *addr, int left, int up, int right,
		      int down, int width, unsigned short color)
{
	int i, j;
	for (j = up; j < down; j++)
		for (i = left; i < right; i++)
			*(addr + j * width + i) = color;
}

void test_panel(int xres, int yres)
{
	int w = xres, h = yres;
	int x = w / 8, y = h / 8;

	printf("panel test: white background, test RGB color\r\n");
	memset((unsigned short *) DEFAULT_FB_BASE, 0xff,
	       w * h * 2);
	udelay(50 * 1000);
	rect_fill((unsigned short *) DEFAULT_FB_BASE, x, y, w - x,
		  h - y, w, 0x1f);
	udelay(50 * 1000);
	rect_fill((unsigned short *) DEFAULT_FB_BASE, x * 2, y * 2,
		  w - x * 2, h - y * 2, w, 0x7e0);
	udelay(50 * 1000);
	rect_fill((unsigned short *) DEFAULT_FB_BASE, x * 3, y * 3,
		  w - x * 3, h - y * 3, w, 0xf800);
	udelay(50 * 1000);
}

void test_vid(int xres, int yres)
{
	u32 length = xres*yres*3 >> 1;
	int x, y;
	char *px;

	memset((void *)DEFAULT_VID_BASE, 0, length);
	memset((void *)DEFAULT_VID_BASE2, 0xf0, length);

	for(y = 0; y < yres; y++) {
		px = (char *)(DEFAULT_VID_BASE2 + y * xres+ xres - 50);
		for(x = 0; x < 50; x++)
			*px++ = 0xcf;
	}
	//green
	for(y = 0; y < yres; y++) {
		px = (char *)(DEFAULT_VID_BASE + y * xres);
		for(x = 0; x < 50; x++)
			*px++ = 0xcf;
	}
}

/* select LVDS_PHY_CTL_EXTx */
#define lvds_ext_select(ext, tmp, reg) do {				\
	reg = DISPLAY_CONTROLLER_BASE + LVDS_PHY_CTL;		\
	if (ext) {							\
		/* select LVDS_PHY_CTL_EXTx */				\
		tmp = readl(reg) & (~LVDS_PHY_EXT_MASK);		\
		writel(tmp | (ext - 1) << LVDS_PHY_EXT_SHIFT, reg);	\
		/* switch to LVDS_PHY_CTL_EXTx */			\
		reg -= LVDS_PHY_CTL; reg += LVDS_PHY_CTL_EXT;		\
	}								\
} while (0)

static u32 lvds_get(int ext)
{
	u32 reg, tmp;

	lvds_ext_select(ext, tmp, reg);

	return readl(reg);
}

static int lvds_set(int ext, u32 mask, u32 val)
{
	u32 reg, tmp, tmp2;

	lvds_ext_select(ext, tmp, reg);

	tmp = tmp2 = readl(reg);
	tmp2 &= ~mask; tmp2 |= val;
	if (tmp != tmp2)
		writel(tmp2, reg);

	return 0;
}

static void lvds_dump(struct lvds_info *lvds)
{
	u32 reg = DISPLAY_CONTROLLER_BASE + LCD_2ND_BLD_CTL;
	char *str;

	switch (lvds->src) {
	case LVDS_SRC_PN:
		str = "PN";
		break;
	case LVDS_SRC_CMU:
		str = "CMU";
		break;
	case LVDS_SRC_PN2:
		str = "PN2";
		break;
	case LVDS_SRC_TV:
		str = "TV";
		break;
	default:
		str = "?";
		break;
	};

	printf("lvds_info: src %s fmt %s\n", str,
		(lvds->fmt & LVDS_FMT_18BIT) ? "18bit" : "24bit");
	printf("LCD_2ND_BLD_CTL(0x%x): 0x%x\n\n", reg & 0xfff, readl(reg));

	printf("LVDS_PHY_CTL: 0x%x\n", lvds_get(0));
	printf("        EXT1: 0x%x\n", lvds_get(1));
	printf("        EXT2: 0x%x\n", lvds_get(2));
	printf("        EXT3: 0x%x\n", lvds_get(3));
	printf("        EXT4: 0x%x\n", lvds_get(4));
	printf("        EXT5: 0x%x\n", lvds_get(5));
}

static int pxa688_lvds_config(struct lvds_info *lvds)
{
	u32 reg = DISPLAY_CONTROLLER_BASE + LCD_2ND_BLD_CTL;
	u32 val = readl(reg) & ~(LVDS_SRC_MASK | LVDS_FMT_MASK);

	val |= (lvds->src << LVDS_SRC_SHIFT) | (lvds->fmt << LVDS_FMT_SHIFT);
	writel(val, reg);

	return 0;
}

static int pxa688_lvds_init(struct lvds_info *lvds)
{
	u32 mask, val;
	int count = 100000;

	/* configure lvds src and fmt */
	pxa688_lvds_config(lvds);

	/* release LVDS PHY from reset */
	lvds_set(0, LVDS_RST, 0);
	udelay(100);

	/* disable LVDS channel 0-5 power-down */
	lvds_set(0, LVDS_PD_CH_MASK, 0);

	/* select LVDS_PCLK instead of REFCLK as LVDS PHY clock */
	lvds_set(0, LVDS_CLK_SEL, LVDS_CLK_SEL_LVDS_PCLK);

	/* power up IP */
	lvds_set(0, LVDS_PU_IVREF, LVDS_PU_IVREF);

	/* REFDIV = 0x3, reference clock divider
	 * FBDIV = 0xa, feedback clock divider
	 * KVCO = 0x4, 1.7G - 1.9G */
	mask = LVDS_REFDIV_MASK | LVDS_FBDIV_MASK | LVDS_REFDIV_MASK
		| LVDS_CTUNE_MASK | LVDS_VREG_IVREF_MASK
		| LVDS_VDDL_MASK | LVDS_VDDM_MASK;
	val = (0x6 << LVDS_REFDIV_SHIFT) | (0x1 << LVDS_FBDIV_SHIFT)
		| (0x4 << LVDS_KVCO_SHIFT | (0x2 << LVDS_CTUNE_SHIFT)
		| (0x2 << LVDS_VREG_IVREF_SHIFT) | (0x9 << LVDS_VDDL_SHIFT)
		| (0x1 << LVDS_VDDM_SHIFT));
	lvds_set(3, mask, val);

	/* VCO_VRNG = 0x3, LVDS PLL V to I gain control, for KVCO[3:0] = 0x4 */
	mask = LVDS_VCO_VRNG_MASK | LVDS_ICP_MASK | LVDS_PI_EN
		| LVDS_VCODIV_SEL_SE_MASK | LVDS_INTPI_MASK;
	val = (0x3 << LVDS_VCO_VRNG_SHIFT) | (0x1 << LVDS_ICP_SHIFT)
		| LVDS_PI_EN | (0xd << LVDS_VCODIV_SEL_SE_SHIFT)
		| (0x3 << LVDS_INTPI_SHIFT);
	lvds_set(4, mask, val);

	/* enable PUPLL/PUTX to power up rest of PLL and TX */
	lvds_set(0, LVDS_PU_TX | LVDS_PU_PLL, LVDS_PU_TX | LVDS_PU_PLL);

	/* poll on lock bit until LVDS PLL locks */
	while (!(lvds_get(0) & LVDS_PLL_LOCK) && count--);
	if (count <= 0) {
		printf("lvds init failed\n");
		lvds_dump(lvds);
	}

	/* enable common mode feedback circuit */
	mask = LVDS_SELLV_OP9_MASK | LVDS_SELLV_OP7_MASK | LVDS_SELLV_OP6_MASK
		| LVDS_SELLV_TXDATA_MASK | LVDS_SELLV_TXCLK_MASK | LVDS_TX_DIF_CM_MASK
		| LVDS_TX_DIF_AMP_MASK | LVDS_TX_TERM_EN | LVDS_TX_CMFB_EN;
	val = (0x1 << LVDS_SELLV_OP9_SHIFT) | (0x1 << LVDS_SELLV_OP7_SHIFT)
		| (0x1 << LVDS_SELLV_OP6_SHIFT) | (0xa << LVDS_SELLV_TXDATA_SHIFT)
		| (0xa << LVDS_SELLV_TXCLK_SHIFT) | (0x3 << LVDS_TX_DIF_CM_SHIFT)
		| (0x8 << LVDS_TX_DIF_AMP_SHIFT) | LVDS_TX_CMFB_EN;
	lvds_set(2, mask, val);

	/* Flip all the N\P pins in order to get correct display,
	 * the pins might be inverted in the chip */
	lvds_set(1, LVDS_POL_SWAP_MASK, 0x3f << LVDS_POL_SWAP_SHIFT);

	return 0;
}

void *pxa168fb_init(struct pxa168fb_mach_info *mi)
{
	struct pxa168fb_info *fbi = malloc(sizeof(struct pxa168fb_info));
	struct dsi_info *di = NULL;
	struct lvds_info *lvds = NULL;
	u32 dma0, i;
	u32 cea_id = 4; /* 720p60 */
	u32 enable_3d = 0;

	memset(fbi, 0, sizeof(struct pxa168fb_info));
	/* Initialize private data */
	fbi->panel_rbswap = mi->panel_rbswap;
	fbi->id = mi->index;
	pxa168_fbi[mi->index] = fbi;
	fbi->mi = mi;
	fbi->var = malloc(sizeof(struct fb_var_screeninfo));
	fbi->fix = malloc(sizeof(struct fb_fix_screeninfo));

	memset(fbi->var, 0, sizeof(struct fb_var_screeninfo));
	memset(fbi->fix, 0, sizeof(struct fb_fix_screeninfo));
	fbi->is_blanked = 0;
	fbi->debug = 0;
	fbi->active = mi->active;

	/* Map LCD controller registers */
	fbi->reg_base = (void *)DISPLAY_CONTROLLER_BASE;
	if (!fbi->id) {
		if (mi->phy_type & (DSI2DPI | DSI)) {
			di = (struct dsi_info *)mi->phy_info;
			di->regs = DSI1_REG_BASE;     /* DSI 1 */
		} else if (mi->phy_type & LVDS)
			lvds = (struct lvds_info *)mi->phy_info;
#ifdef CONFIG_HDMI
	} else {
		mi->modes = &cea_modes[cea_id];
#endif
	}

	/*
	 * Allocate framebuffer memory
	 */
	fbi->fb_size = DEFAULT_FB_SIZE;
	fbi->fb_start = (unsigned int *) DEFAULT_FB_BASE;
	fbi->fb_start_dma = DEFAULT_FB_BASE;
	memset(fbi->fb_start, 0x0, fbi->fb_size);

	/*
	 * init video mode data
	 */
	pxa168fb_init_mode(fbi, mi);

	/* LCD_TOP_CTRL control reg */
	if (! fbi->id)
		writel(VDMA_ENABLE, fbi->reg_base + LCD_TOP_CTRL);

	/*
	 * Fill in sane defaults
	 */
	pxa168fb_set_default(fbi, mi);	/* FIXME */
	pxa168fb_set_par(fbi, mi);


	dma0 = dma_ctrl_read(fbi, 0);
	/* toggle left/right for 3d */
	if (enable_3d && 1 == fbi->id)
		dma0 |= 1 << 7;
	else
		dma0 &= ~(1 << 7);
	dma_ctrl_write(fbi, 0, dma0);

	if(!fbi->id) {
		if (mi->phy_type & (DSI2DPI | DSI))
			/* phy interface init */
			brownstone_dsi_init(fbi);
		else if (mi->phy_type & LVDS)
			pxa688_lvds_init(lvds);
	}
#ifdef CONFIG_HDMI
	else
		hdmi_init(fbi->var, cea_id, enable_3d); /* 4: 720P60 32:1920@24 */
#endif
	/* dump all lcd and dsi registers for debug purpose */
#if defined(DEBUG_PXA168FB)
	printf("lcd regs:\n");
	for (i = 0; i < 0x300; i += 4) {
		if (!(i % 16) && i)
			printf("\n0x%3x: ", i);
		printf(" %8x", readl(fbi->reg_base + i));
	}
	if (mi->phy_type & (DSI2DPI | DSI)) {
		printf("\n dsi regs:");
		for (i = 0x0; i < 0x200; i += 4) {
			if (!(i % 16))
				printf("\n0x%3x: ", i);
			printf(" %8x", readl(di->regs + i));
		}
	}
	printf("\n");
	test_panel(mi->modes->xres, mi->modes->yres);
#endif

	return (void *)fbi;
}

#ifdef CONFIG_HDMI
extern void hdmi_3d_sync_view(void);
#endif
void pxa168fb_vid(u32 enable, u32 xres, u32 yres)
{
	struct pxa168fb_info *fbi = pxa168_fbi[1];
	struct lcd_regs *regs = get_regs(fbi);
	u32 v, dma0, dma1, len = xres*yres;

	if (! enable) {
		dma0 = dma_ctrl_read(fbi, 0);
		dma0 &= ~0x1;
		dma_ctrl_write(fbi, 0, dma0);
		return;
	}

	printf("video xres %d yres %d\n", xres, yres);
	test_vid(xres, yres);

	writel(DEFAULT_VID_BASE, &regs->v_y0);
	writel(DEFAULT_VID_BASE2, &regs->v_y1);
	writel(DEFAULT_VID_BASE + len, &regs->v_u0);
	writel(DEFAULT_VID_BASE + len + (len >> 2), &regs->v_v0);
	writel(DEFAULT_VID_BASE2 + len, &regs->v_u1);
	writel(DEFAULT_VID_BASE2 + len + (len >> 2), &regs->v_v1);
	/* start address on screen */
	writel((80 << 16) | 80, &regs->v_start);
	//writel((top << 16) | left, &regs->v_start);
	/* pitch, pixels per line */
	writel(xres & 0xFFFF, &regs->v_pitch_yc);
	writel((xres/4) << 16 | (xres/4), &regs->v_pitch_uv);
	/* resolution, src size */
	writel((yres << 16) | xres, &regs->v_size);
	/* resolution, dst size */
	writel((yres << 16) | xres, &regs->v_size_z);

	dma1 = dma_ctrl_read(fbi, 1);
	dma1 |= CFG_ALPHA_MODE(2) | CFG_ALPHA(0xff);
	dma_ctrl_write(fbi, 1, dma1);

	dma0 = dma_ctrl_read(fbi, 0);
	/* vid dma, 420p and yuv2rgb */
	dma0 |= 0x1 | 0x2 | (7 << 20);
	v = readl(fbi->reg_base + TV_FRAMEDONE_ENA_MASK);
	writel(0, fbi->reg_base + SPU_IRQ_ISR);
	/* enable video layer in vblank period */
	while (! (TV_FRAMEDONE_ENA_MASK & readl(fbi->reg_base + SPU_IRQ_ISR)));
#ifdef CONFIG_HDMI
	hdmi_3d_sync_view();
#endif
	dma_ctrl_write(fbi, 0, dma0);
}

void pxa168fb_vid_sync(void)
{
	struct pxa168fb_info *fbi = pxa168_fbi[1];
	u32 dma0;

	dma0 = dma_ctrl_read(fbi, 0);
	/* we only sync if vid dma is on */
	if (!(dma0 & 0x1))
		return;

	writel(0, fbi->reg_base + SPU_IRQ_ISR);
	/* wait for vid layer frame 1 eof */
	while (! (TV_DMA_FRAME_IRQ1_ENA_MASK & readl(fbi->reg_base + SPU_IRQ_ISR)));
	writel(0, fbi->reg_base + SPU_IRQ_ISR);
	/* load after eof of this frame */
	/* TVSYNC_IRQ_ENA_MASK */
	while (! (TV_FRAMEDONE_ENA_MASK & readl(fbi->reg_base + SPU_IRQ_ISR)));
#ifdef CONFIG_HDMI
	hdmi_3d_sync_view();
#endif
}

u32 lcd_read(u32 addr)
{
	return readl(pxa168_fbi[0]->reg_base + addr);
}

void lcd_write(u32 addr, u32 v)
{
	writel(v, pxa168_fbi[0]->reg_base + addr);
}

#ifdef CONFIG_HDMI
void *pxa168fb_hdmi_set_mode(u32 cea_id, u32 enable_3d)
{
	struct pxa168fb_info *fbi = pxa168_fbi[1];
	struct pxa168fb_mach_info *mi = fbi->mi;
	u32 dma0;

	mi->modes = &cea_modes[cea_id];
	/*
	 * init video mode data
	 */
	pxa168fb_init_mode(fbi, mi);

	/*
	 * Fill in sane defaults
	 */
	pxa168fb_set_default(fbi, mi);	/* FIXME */
	pxa168fb_set_par(fbi, mi);

	dma0 = dma_ctrl_read(fbi, 0);
	/* toggle */
	if (enable_3d)
		dma0 |= 1 << 7;
	else
		dma0 &= ~(1 << 7);
	dma_ctrl_write(fbi, 0, dma0);

	hdmi_init(fbi->var, cea_id, enable_3d);

	/* dump all lcd and dsi registers for debug purpose */
#if defined(DEBUG_PXA168FB)
	printf("lcd regs:\n");
	for (i = 0; i < 0x300; i += 4) {
		if (!(i % 16) && i)
			printf("\n0x%3x: ", i);
		printf(" %8x", readl(fbi->reg_base + i));
	}
	printf("\n dsi regs:");
	for (i = 0x0; i < 0x200; i += 4) {
		if (!(i % 16))
			printf("\n0x%3x: ", i);
		printf(" %8x", readl(di->regs + i));
	}
	printf("\n");
	//test_panel(mi->modes->xres, mi->modes->yres);
#endif

}
#endif
