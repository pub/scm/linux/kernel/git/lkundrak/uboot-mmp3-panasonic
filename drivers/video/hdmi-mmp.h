#define	HDMI_PHY_CFG_0	(0x0008)
	/* 32 bit HDMI PHY Config 0 Register */
#define	HDMI_PHY_CFG_1	(0x000C)
	/* 32 bit HDMI PHY Config 1 Register */
#define	HDMI_PHY_CFG_2	(0x0010)
	/* 32 bit HDMI PHY Config 2 Register */
#define	HDMI_PHY_CFG_3	(0x0014)
	/* 32 bit HDMI PHY Config 3 Register */
#define	HDMI_AUDIO_CFG	(0x0018)
	/* 32 bit HDMI Audio Contror Register */
#define	HDMI_CLOCK_CFG	(0x001C)
	/* 32 bit HDMI Clock Control Register */
#define	HDMI_PLL_CFG_0	(0x0020)
	/* 32 bit HDMI PLL Control 0 Register */
#define	HDMI_PLL_CFG_1	(0x0024)
	/* 32 bit HDMI PLL Control 1 Register */
#define	HDMI_PLL_CFG_2	(0x0028)	/* 32 bit HDMI PLL Control 2 Register */
#define	HDMI_PLL_CFG_3	(0x002C)	/* 32 bit HDMI PLL Control 3 Register */
#define	HDMI_3D_CTRL	(0x30)

/*
 *	THE BIT DEFINES
 */
/*	HDMI_PHY_CFG_0		0x0008	HDMI PHY Config 0 Register */
#define	HDMI_PHY_CFG_0_CP_MSK	((0xff) << 24)	/* HDMI PHY CP Config */
#define	HDMI_PHY_CFG_0_CP_BASE	24
#define	HDMI_PHY_CFG_0_EAMP_MSK	((0xfff) << 12)	/* HDMI PHY EAMP Config */
#define	HDMI_PHY_CFG_0_EAMP_BASE	12
#define	HDMI_PHY_CFG_0_DAMP_MSK		(0xfff)/* HDMI PHY DAMP Config */
#define	HDMI_PHY_CFG_0_DAMP_BASE	0
/*	HDMI_PHY_CFG_1			0x000C	HDMI PHY Config 1 Register */
#define	HDMI_PHY_CFG_1_AJ_D_MSK		((0xf) << 28)	/* HDMI PHY AJ_D Config */
#define	HDMI_PHY_CFG_1_AJ_D_BASE	28
#define	HDMI_PHY_CFG_1_SVTX_MSK		((0xfff) << 16)/* HDMI PHY SVTX Config */
#define	HDMI_PHY_CFG_1_SVTX_BASE	16
#define	HDMI_PHY_CFG_1_IDRV_MSK		(0xffff)	/* HDMI PHY IDRV Config */
#define	HDMI_PHY_CFG_1_IDRV_BASE	0
/*	HDMI_PHY_CFG_2		0x0010	HDMI PHY Config 2 Register */
/* HDMI PHY TP_EN Config */
#define	HDMI_PHY_CFG_2_TP_EN_MSK	((0xf) << 28)
#define	HDMI_PHY_CFG_2_TP_EN_BASE	28
/* HDMI PHY RESET_TX Config */
#define	HDMI_PHY_CFG_2_RESET_TX		(1 << 27)
/* HDMI PHY POLSWAP_TX Config */
#define	HDMI_PHY_CFG_2_POLSWAP_TX_MSK	((0xf) << 23)
#define	HDMI_PHY_CFG_2_POLSWAP_TX_BASE	23
/* HDMI PHY PD_TX Config */
#define	HDMI_PHY_CFG_2_PD_TX_MSK	((0xf) << 19)
#define	HDMI_PHY_CFG_2_PD_TX_BASE	19
/* HDMI PHY PD_IREF Config */
#define	HDMI_PHY_CFG_2_PD_IREF		(1 << 18)
/* HDMI PHY Loopback Config */
#define	HDMI_PHY_CFG_2_LOOPBACK_MSK	((0xf) << 14)
#define	HDMI_PHY_CFG_2_LOOPBACK_BASE	14
/* HDMI PHY INV_CK20T Config */
#define	HDMI_PHY_CFG_2_INV_CK20T_MSK	((0xf) << 10)
#define	HDMI_PHY_CFG_2_INV_CK20T_BASE	10
#define	HDMI_PHY_CFG_2_AUX_MSK		((0x3f)	<< 4)/* HDMI PHY AUS Config */
#define	HDMI_PHY_CFG_2_AUX_BASE		4
/* HDMI PHY AJ_EN Config */
#define	HDMI_PHY_CFG_2_AJ_EN_MSK	(0xf)
#define	HDMI_PHY_CFG_2_AJ_EN_BASE	0

/*	HDMI_PHY_CFG_3	0x0014	HDMI PHY Config 3 Register */
/*		Bit(s) HDMI_PHY_CFG_3_RSRV_31_11 reserved */
/* HDMI Hot Plug Detection Status */
#define	HDMI_PHY_CFG_3_HPD	(1 << 10)
/* HDMI PHY TXDRVX2 Config */
#define	HDMI_PHY_CFG_3_TXDRVX2_MSK	((0xf) << 6)
#define	HDMI_PHY_CFG_3_TXDRVX2_BASE	6
/* HDMI PHY TPC Config */
#define	HDMI_PHY_CFG_3_TPC_MSK	((0x1f) << 1)
#define	HDMI_PHY_CFG_3_TPC_BASE	1
/* HDMI PHY SYNC Config */
#define	HDMI_PHY_CFG_3_SYNC	1

/*	HDMI_AUDIO_CFG			0x0018	HDMI Audio Control Register */
/*		Bit(s) HDMI_AUDIO_CFG_RSRV_31_12 reserved */
/* HDMI Audio I<super 2>S Config */
#define	HDMI_AUDIO_CFG_I2S	(1 << 11)
/* HDMI Audio wsp Config */
#define	HDMI_AUDIO_CFG_WSP	(1 << 10)
/* HDMI Audio fsp Config */
#define	HDMI_AUDIO_CFG_FSP	(1 << 9)
/* HDMI Audio clkp Config */
#define	HDMI_AUDIO_CFG_CLKP	(1 << 8)
/* HDMI Audio xdatdly Config */
#define	HDMI_AUDIO_CFG_XDATDLY_MSK	((0x3) << 6)
#define	HDMI_AUDIO_CFG_XDATDLY_BASE	6
/* HDMI Audio xwdlen Config */
#define	HDMI_AUDIO_CFG_XWDLEN_MSK	((0x7) << 3)
#define	HDMI_AUDIO_CFG_XWDLEN_BASE	3
/* HDMI Audio xfrlen Config */
#define	HDMI_AUDIO_CFG_XFRLEN_MSK	(0x7)
#define	HDMI_AUDIO_CFG_XFRLEN_BASE	0
/*	HDMI_CLOCK_CFG			0x001C	HDMI Clock Control Register */
/*		Bit(s) HDMI_CLOCK_CFG_RSRV_31_17 reserved */
/* HDMI prclk Config */
#define	HDMI_CLOCK_CFG_HDMI_PRCLK_DIV_MSK	((0xf) << 13)
#define	HDMI_CLOCK_CFG_HDMI_PRCLK_DIV_BASE	13
#define	HDMI_CLOCK_CFG_HDMI_TCLK_DIV_MSK	((0xf) << 9 )		/* HDMI tclk Config */
#define	HDMI_CLOCK_CFG_HDMI_TCLK_DIV_BASE	9
#define	HDMI_CLOCK_CFG_HDMI_MCLK_DIV_MSK	((0xf) << 5)/* HDMI mclk Config */
#define	HDMI_CLOCK_CFG_HDMI_MCLK_DIV_BASE	5
/* HDMI Enable Config */
#define	HDMI_CLOCK_CFG_HDMI_EN	(1 << 4)
#define	HDMI_CLOCK_CFG_RTC_MSK	((0x3) << 2)/* HDMI RTC Config */
#define	HDMI_CLOCK_CFG_RTC_BASE	2
#define	HDMI_CLOCK_CFG_WTC_MSK	(0x3)		/* HDMI WTC Config */
#define	HDMI_CLOCK_CFG_WTC_BASE	0

/*HDMI_PLL_CFG_0	0x0020	HDMI PLL Control 0 Register */
#define HDMI_PLL_CFG_0_RESET_INTP_EXT_MSK ((0x1) << 31)
#define HDMI_PLL_CFG_0_RESET_INTP_EXT_BASE 31
/* HDMI PLL INTPI Config, INTPI [1_0] */
#define	HDMI_PLL_CFG_0_INTPI_MSK	((0xf) << 27)
#define	HDMI_PLL_CFG_0_INTPI_BASE	27
/* HDMI PLL CLK_DET_EN Config */
#define	HDMI_PLL_CFG_0_CLK_DET_EN	(1 << 26)
/* HDMI PLL reset offset Config */
#define	HDMI_PLL_CFG_0_RESET_OFFSET	(1 << 25)
#define HDMI_PLL_CFG_0_KVCO_EXT_EN	(1 << 24)
/* HDMI PLL KVCO Config */
#define	HDMI_PLL_CFG_0_KVCO_MSK		((0xf) << 20)
#define	HDMI_PLL_CFG_0_KVCO_BASE	20
/* HDMI PLL CLKOUT TST ENABLE Config */
#define	HDMI_PLL_CFG_0_CLKOUT_TST_EN	(1 << 19)
/* HDMI PLL VREG IVREF config */
#define HDMI_PLL_CFG_0_VREG_IVREF_MSK	((0x3) << 17)
#define HDMI_PLL_CFG_0_VREG_IVREF_BASE	17
/* HDMI PLL POSTDIV Config */
#define	HDMI_PLL_CFG_0_POSTDIV_MSK	((0x7) << 14)
#define	HDMI_PLL_CFG_0_POSTDIV_BASE	14
#define	HDMI_PLL_CFG_0_ICP_MSK	((0xf) << 10)	/* HDMI PLL ICP Config */
#define	HDMI_PLL_CFG_0_ICP_BASE	10
/* HDMI PLL VDDL Config */
#define	HDMI_PLL_CFG_0_VDDL_MSK	((0xf) << 6)
#define	HDMI_PLL_CFG_0_VDDL_BASE	6
/* HDMI PLL VDDM Config */
#define	HDMI_PLL_CFG_0_VDDM_MSK	((0x3) << 4)
#define	HDMI_PLL_CFG_0_VDDM_BASE	4
/* HDMI PLL VPLL CAL START config */
#define HDMI_PLL_CFG_0_VPLL_CAL_START	(1 << 3)
/* HDMI PLL VPLL_CALCLK_DIV Config */
#define	HDMI_PLL_CFG_0_VPLL_CALCLK_DIV_MSK	((0x3) << 1)
#define	HDMI_PLL_CFG_0_VPLL_CALCLK_DIV_BASE	1

/*HDMI_PLL_CFG_1 0x0024	HDMI PLL Control 1 Register */
/*Bit(s) HDMI_PLL_CFG_1_RSRV_31 reserved */
/* HDMI PLL VTH_VPLL_CA config */
#define HDMI_PLL_CFG_1_VTH_VPLL_CA_MSK	((0x3) << 29)
#define HDMI_PLL_CFG_1_VTH_VPLL_CA_BASE 29
/* HDMI PLL TEST_MON Config */
#define	HDMI_PLL_CFG_1_TEST_MON_MSK	((0xf) << 25)
#define	HDMI_PLL_CFG_1_TEST_MON_BASE	25
/* HDMI PLL EN_PANNEL Config */
#define	HDMI_PLL_CFG_1_EN_PANNEL	(1 << 24)
/* HDMI PLL EN_HDMI Config */
#define	HDMI_PLL_CFG_1_EN_HDMI	(1 << 23)
/* HDMI PLL FREQ_OFFSET_READY_ADJ Config */
#define	HDMI_PLL_CFG_1_FREQ_OFFSET_READY_ADJ	(1 << 22)
/* HDMI PLL FREQ_OFFSET_INNER Config */
#define	HDMI_PLL_CFG_1_FREQ_OFFSET_INNER_MSK	((0x1ffff) << 4)
#define	HDMI_PLL_CFG_1_FREQ_OFFSET_INNER_BASE	4
/* HDMI PLL Mode Config, MODE [1_0] */
#define	HDMI_PLL_CFG_1_MODE		(1 << 3)
/* HDMI PLL CTUNE Config */
#define	HDMI_PLL_CTUNE_MSK	0x7
#define HDMI_PLL_CTUNE_BASE	0x0


/*HDMI_PLL_CFG_2 0x0028	HDMI PLL Control 2 Register */
/*Bit(s) HDMI_PLL_CFG_2_RSRV_31_17 reserved */
/* HDMI PLL FREQ_OFFSET_ADJ Config */
#define	HDMI_PLL_CFG_2_FREQ_OFFSET_ADJ_MSK	(0x1ffff)
#define	HDMI_PLL_CFG_2_FREQ_OFFSET_ADJ_BASE	0

/*HDMI_PLL_CFG_3 0x002C	HDMI PLL Control 3 Register */
/*Bit(s) HDMI_PLL_CFG_3_RSRV_31_27 reserved */
/* HDMI KVCO_RD Config */
#define	HDMI_PLL_CFG_3_KVCO_RD_MSK	((0xf) << 23)
#define	HDMI_PLL_CFG_3_KVCO_RD_BASE	23
/* HDMI PLL_LOCK Config */
#define	HDMI_PLL_CFG_3_PLL_LOCK		(1 << 22)
/* HDMI CTUNE_RD Config */
#define	HDMI_PLL_CFG_3_CTUNE_RD_MSK	((0x7) << 19)
#define	HDMI_PLL_CFG_3_CTUNE_RD_BASE	19
/* HDMI PLL VPLL_CAL_DONE config */
#define HDMI_PLL_CFG_3_VPLL_CAL_DONE	(1 << 18)
/* HDMI PLL VPLL_CAL_ENABLE config */
#define HDMI_PLL_CFG_3_VPLL_CAL_ENABLE    (1 << 17)
/* HDMI PLL hdmi_test_mode Config */
#define	HDMI_PLL_CFG_3_HDMI_TEST_MODE (1 << 16)
/* HDMI PLL hdmi_pll_fbdiv Config */
#define	HDMI_PLL_CFG_3_HDMI_PLL_FBDIV_MSK	((0x1ff) << 7)
#define	HDMI_PLL_CFG_3_HDMI_PLL_FBDIV_BASE	7
/* HDMI PLL hdmi_pll_refdiv Config */
#define	HDMI_PLL_CFG_3_HDMI_PLL_REFDIV_MSK	((0x1f) << 7)
#define	HDMI_PLL_CFG_3_HDMI_PLL_REFDIV_BASE	2
/* HDMI PLL hdmi_pll_rstb Config */
#define	HDMI_PLL_CFG_3_HDMI_PLL_RSTB	(1 << 1)
/* HDMI PLL hdmi_pll_on PU Config */
#define	HDMI_PLL_CFG_3_HDMI_PLL_ON	1

/*HDTX_ACR_N0	0x0000	ACR N Value Bits [7_0] Register */
#define	HDTX_ACR_N0_ACR_N_7_0_MSK (0xff)	/* ACR_N[7_0] */
#define	HDTX_ACR_N0_ACR_N_7_0_BASE 0
/*HDTX_ACR_N1	0x0001	ACR N Value Bits [15_8] Register */
#define	HDTX_ACR_N1_ACR_N_15_8_MSK (0xff)	/* ACR_N[15_8] */
#define	HDTX_ACR_N1_ACR_N_15_8_BASE 0
/*HDTX_ACR_N2	0x0002	ACR N Value Bits [19_16] Register */
/*Bit(s) HDTX_ACR_N2_RSRV_7_4 reserved */
#define	HDTX_ACR_N2_ACR_N_19_16_MSK (0xf)	/* ACR_N[19_16] */
#define	HDTX_ACR_N2_ACR_N_19_16_BASE 0
/*
 *HDTX_ACR_CTS0 0x0004	Software-Calculated ACR CTS Value Bits [7_0] Register
 */
#define	HDTX_ACR_CTS0_ACR_CTS_7_0_MSK (0xff)	/* ACR_CTS[7_0] */
#define	HDTX_ACR_CTS0_ACR_CTS_7_0_BASE 0
/*
 * HDTX_ACR_CTS1 0x0005	Software-Calculated ACR CTS Value Bits[15_8] Register
 */
#define	HDTX_ACR_CTS1_ACR_CTS_15_8_MSK (0xff)	/* ACR_CTS[15_8] */
#define	HDTX_ACR_CTS1_ACR_CTS_15_8_BASE 0
/*
 *HDTX_ACR_CTS2	0x0006	Software-Calculated ACR CTS Value Bits[19_16] Register
 */
/*Bit(s) HDTX_ACR_CTS2_RSRV_7_4 reserved */
#define	HDTX_ACR_CTS2_ACR_CTS_19_16_MSK (0xf)	/* ACR_CTS[19_16] */
#define	HDTX_ACR_CTS2_ACR_CTS_19_16_BASE 0
/*HDTX_ACR_CTRL	0x0007	ACR Control Register */
/*Bit(s) HDTX_ACR_CTRL_RSRV_7_4 reserved */
#define	HDTX_ACR_CTRL_MCLK_SEL_MSK	((0x3) << 2)	/* MCLK Select */
#define	HDTX_ACR_CTRL_MCLK_SEL_BASE	2
#define	HDTX_ACR_CTRL_CTS_SEL	(1 < 1)	/* CTS Select */
#define	HDTX_ACR_CTRL_ACR_EN	1		/* ACR Enable */
/*
 * HDTX_ACR_STS0 0x0008	Hardware Computed ACR CTS Bits [7_0]Register
 */
#define	HDTX_ACR_STS0_ACR_CTS_VAL_7_0_MSK	(0xff)	/* ACR_CTS_VAL[7_0] */
#define	HDTX_ACR_STS0_ACR_CTS_VAL_7_0_BASE	0

/*	HDTX_ACR_STS1			0x0009	Hardware Computed ACR CTS Bits [15_8]
 *									Register
 */
/* ACR_CTS_VAL[15_8] */
#define	HDTX_ACR_STS1_ACR_CTS_VAL_15_8_MSK	(0xff)
#define	HDTX_ACR_STS1_ACR_CTS_VAL_15_8_BASE	0

/*	HDTX_ACR_STS2			0x000A	Hardware Computed ACR CTS Bits [19_16]
 *									Register
 */
/*		Bit(s) HDTX_ACR_STS2_RSRV_7_4 reserved */
/* ACR_CTS_VAL[19_16] */
#define	HDTX_ACR_STS2_ACR_CTS_VAL_19_16_MSK	(0xf)
#define	HDTX_ACR_STS2_ACR_CTS_VAL_19_16_BASE	0

/*	HDTX_AUD_CTRL			0x000B	Audio Control Register */
#define	HDTX_AUD_CTRL_VALIDITY	(1 << 7)		/* Validity */
#define	HDTX_AUD_CTRL_I2S_DBG_EN	(1 << 6)		/* I<super 2>S Debug Enable */
#define	HDTX_AUD_CTRL_I2S_MODE	(1 << 5)	/* I<super 2>S Mode */
/* I<super 2>S Channel Enable */
#define	HDTX_AUD_CTRL_I2S_CH_EN_MSK	((0xf) << 1)
#define	HDTX_AUD_CTRL_I2S_CH_EN_BASE	1
#define	HDTX_AUD_CTRL_AUD_EN	1		/* Audio Enable */

/*	HDTX_I2S_CTRL			0x000C	I<super 2>S Control Register */
/*		Bit(s) HDTX_I2S_CTRL_RSRV_7_5 reserved */
#define	HDTX_I2S_CTRL_I2S_CTL_b4	(1 << 4) /* I<super 2>S Control [4] */
#define	HDTX_I2S_CTRL_I2S_CTL_b3	(1 << 3) /* I<super 2>S Control [3] */
#define	HDTX_I2S_CTRL_I2S_CTL_b2	(1 << 7) /* I<super 2>S Control [2] */
#define	HDTX_I2S_CTRL_I2S_CTL_b1	(1 << 1)		/* I<super 2>S Control [1] */
#define	HDTX_I2S_CTRL_I2S_CTL_b0	1		/* I<super 2>S Control [0] */

/*	HDTX_I2S_DLEN			0x000D	I<super 2>S Valid Data Length Register */
/*		Bit(s) HDTX_I2S_DLEN_RSRV_7_5 reserved */
/* I<super 2>S Data Len [4_0] */
#define	HDTX_I2S_DLEN_I2S_DATALEN_4_0_MSK	(0x1f)
#define	HDTX_I2S_DLEN_I2S_DATALEN_4_0_BASE	0

/*	HDTX_I2S_DBG_LFT0		0x0010	I<super 2>S Left Channel Debug Bits [7_0]
 *									Register
 */
/* Left Debug [7_0] */
#define	HDTX_I2S_DBG_LFT0_LEFT_DBG_7_0_MSK	(0xff)
#define	HDTX_I2S_DBG_LFT0_LEFT_DBG_7_0_BASE	0

/*	HDTX_I2S_DBG_LFT1		0x0011	I<super 2>S Left Channel Debug Bits [15_8]
 *									Register
 */
/* Left Debug [15_8] */
#define	HDTX_I2S_DBG_LFT1_LEFT_DBG_15_8_MSK	(0xff)
#define	HDTX_I2S_DBG_LFT1_LEFT_DBG_15_8_BASE	0

/*	HDTX_I2S_DBG_LFT2		0x0012	I<super 2>S Left Channel Debug Bits
 *									[23_16] Register
 */
/* Left Debug [23_16] */
#define	HDTX_I2S_DBG_LFT2_LEFT_DBG_23_16_MSK	(0xff)
#define	HDTX_I2S_DBG_LFT2_LEFT_DBG_23_16_BASE	0

/*	HDTX_I2S_DBG_LFT3		0x0013	I<super 2>S Left Channel Debug Bits
 *									[31_24] Register
 */
/* Left Debug [31_24] */
#define	HDTX_I2S_DBG_LFT3_LEFT_DBG_31_24_MSK	(0xff)
#define	HDTX_I2S_DBG_LFT3_LEFT_DBG_31_24_BASE	0

/*	HDTX_I2S_DBG_RIT0		0x0014	I<super 2>S Right Channel Debug Bits [7_0]
 *									Register
 */
/* Right Debug [7_0] */
#define	HDTX_I2S_DBG_RIT0_RIT_DBG_7_0_MSK	(0xff)
#define	HDTX_I2S_DBG_RIT0_RIT_DBG_7_0_BASE	0

/*	HDTX_I2S_DBG_RIT1		0x0015	I<super 2>S Right Channel Debug Bits
 *									[15_8] Register
 */
/* Right Debug [15_8] */
#define	HDTX_I2S_DBG_RIT1_RIT_DBG_15_8_MSK	(0xff)
#define	HDTX_I2S_DBG_RIT1_RIT_DBG_15_8_BASE	0

/*	HDTX_I2S_DBG_RIT2		0x0016	I<super 2>S Right Channel Debug Bits
 *									[23_16] Register
 */
/* Right Debug [23_16] */
#define	HDTX_I2S_DBG_RIT2_RIT_DBG_23_16_MSK	(0xff)
#define	HDTX_I2S_DBG_RIT2_RIT_DBG_23_16_BASE	0

/*	HDTX_I2S_DBG_RIT3		0x0017	I<super 2>S Right Channel Debug Bits
 *									[31_24] Register
 */
/* Right Debug [31_24] */
#define	HDTX_I2S_DBG_RIT3_RIT_DBG_31_24_MSK	(0xff)
#define	HDTX_I2S_DBG_RIT3_RIT_DBG_31_24_BASE	0

/*	HDTX_CHSTS_0			0x0018	Channel Status Bits [7_0] Register */
#define	HDTX_CHSTS_0_CHSTS_7_0_MSK	(0xff)	/* Channel Status [7_0] */
#define	HDTX_CHSTS_0_CHSTS_7_0_BASE	0

/*	HDTX_CHSTS_1			0x0019	Channel Status Bits [15_8] Register */
/* Channel Status [15_8] */
#define	HDTX_CHSTS_1_CHSTS_15_8_MSK	(0xff)
#define	HDTX_CHSTS_1_CHSTS_15_8_BASE	0

/*	HDTX_CHSTS_2			0x001A	Channel Status Bits [23_16] Register */
/* Channel Status [23_16] */
#define	HDTX_CHSTS_2_CHSTS_23_16_MSK	(0xff)
#define	HDTX_CHSTS_2_CHSTS_23_16_BASE	0

/*	HDTX_CHSTS_3			0x001B	Channel Status Bits [31_24] Register */
/* Channel Status [31_24] */
#define	HDTX_CHSTS_3_CHSTS_31_24_MSK	(0xff)
#define	HDTX_CHSTS_3_CHSTS_31_24_BASE	0

/*	HDTX_CHSTS_4			0x001C	Channel Status Bits [39_32] Register */
/* Channel Status [40_32] */
#define	HDTX_CHSTS_4_CHSTS_40_32_MSK	(0xff)
#define	HDTX_CHSTS_4_CHSTS_40_32_BASE	0

/*	HDTX_FIFO_CTRL			0x001D	FIFO Control Register */
/*		Bit(s) HDTX_FIFO_CTRL_RSRV_7_2 reserved */
#define	HDTX_FIFO_CTRL_IRST	(1 << 1)			/* Internal FIFO Reset */
#define	HDTX_FIFO_CTRL_FIFO_RST	1			/* FIFO Reset */

/*	HDTX_MEMSIZE_L			0x001E	FIFO Memory Size Bits [7_0] Register */
/* Memory Size [7_0] */
#define	HDTX_MEMSIZE_L_MEMSIZE_7_0_MSK	(0xff)
#define	HDTX_MEMSIZE_L_MEMSIZE_7_0_BASE	0

/*	HDTX_MEMSIZE_H			0x001F	FIFO Memory Size Bit [8] Register */
/*		Bit(s) HDTX_MEMSIZE_H_RSRV_7_1 reserved */
#define	HDTX_MEMSIZE_H_MEMSIZE_b8			1			/* Memory Size [8] */

/*	HDTX_GCP_CFG0			0x0020	GCP Packet Configure Register 0 */
/*		Bit(s) HDTX_GCP_CFG0_RSRV_7_4 reserved */
#define	HDTX_GCP_CFG0_PP_SW_VAL	(1 << 3)		/* PP Software Value */
#define	HDTX_GCP_CFG0_DEF_PHASE	(1 << 2)		/* Default Phase */
#define	HDTX_GCP_CFG0_AVMUTE	(1 << 1)		/* AV Mute */
/* GCP Packet Transmission Enable */
#define	HDTX_GCP_CFG0_GCP_EN	1

/*	HDTX_GCP_CFG1			0x0021	GCP Packet Configure Register 1 */
#define	HDTX_GCP_CFG1_COL_DEPTH_MSK	SHIFT4(0xf)	/* Color Depth */
#define	HDTX_GCP_CFG1_COL_DEPTH_BASE	4
#define	HDTX_GCP_CFG1_PP_3_0_MSK	(0xf)	/* Pixel Packing [3_0] */
#define	HDTX_GCP_CFG1_PP_3_0_BASE	0

/*HDTX_AUD_STS 0x0022	Audio Status Register */
/*Bit(s) HDTX_AUD_STS_RSRV_7_4 reserved */
#define	HDTX_AUD_STS_UNDERFLOW	(1 << 3)		/* Underflow */
#define	HDTX_AUD_STS_OVERFLOW	(1 << 2)		/* Overflow */
/*Bit(s) HDTX_AUD_STS_RSRV_1_0 reserved */
/*HDTX_HTOT_L 0x0024	HTOTAL Bits [7_0] Register */
/* Total Horizontal Pixels per Line [7_0] */
#define	HDTX_HTOT_L_HTOT_7_0_MSK	(0xff)
#define	HDTX_HTOT_L_HTOT_7_0_BASE	0

/* HDTX_HTOT_H 0x0025	HTOTAL Bits [15_8] Register */
/* Total Horizontal Pixels per Line [15_8] */
#define	HDTX_HTOT_H_HTOT_15_8_MSK	(0xff)
#define	HDTX_HTOT_H_HTOT_15_8_BASE	0

/* HDTX_HBLANK_L 0x0026	HBLANK Bits [7_0] Register */
/* Horizontal Blanking Period [7_0] */
#define	HDTX_HBLANK_L_HBLANK_7_0_MSK	(0xff)
#define	HDTX_HBLANK_L_HBLANK_7_0_BASE	0

/* HDTX_HBLANK_H 0x0027	HBLANK Bits [15_8] Register */
/* Horizontal Blanking Period [15_8] */
#define	HDTX_HBLANK_H_HBLANK_15_8_MSK	(0xff)
#define	HDTX_HBLANK_H_HBLANK_15_8_BASE		0

/* HDTX_VTOT_L 0x0028	VTOTAL Bits [7_0] Register */
/* Vertical Resolution [7_0] */
#define	HDTX_VTOT_L_VTOT_7_0_MSK	(0xff)
#define	HDTX_VTOT_L_VTOT_7_0_BASE	0

/* HDTX_VTOT_H 0x0029	VTOTAL Bits [15_8] Register */
/* Vertical Resolution [15_8] */
#define	HDTX_VTOT_H_VTOT_15_8_MSK	(0xff)
#define	HDTX_VTOT_H_VTOT_15_8_BASE	0

/* HDTX_VRES_L 0x002A	VRES Bits [7_0] Register */
/* Active Vertical Resolution [7_0] */
#define	HDTX_VRES_L_VRES_7_0_MSK	(0xff)
#define	HDTX_VRES_L_VRES_7_0_BASE	0

/* HDTX_VRES_H 0x002B VRES Bits [15_8] Register */
/* Active Vertical Resolution [15_8] */
#define	HDTX_VRES_H_VRES_15_8_MSK	(0xff)
#define	HDTX_VRES_H_VRES_15_8_BASE	0

/* HDTX_VSTART_L 0x002C	VSTART Bits [7_0] Register */
/* First Active Line of Frame [7_0] */
#define	HDTX_VSTART_L_VSTART_7_0_MSK	(0xff)
#define	HDTX_VSTART_L_VSTART_7_0_BASE	0

/* HDTX_VSTART_H 0x002D	VSTART Bits [15_8] Register */
/* First Active Line of Frame [15_8] */
#define	HDTX_VSTART_H_VSTART15_8_MSK	(0xff)
#define	HDTX_VSTART_H_VSTART15_8_BASE	0

/* HDTX_HTOT_STS_L 0x002E HTOTAL Bits [7_0] Register */
/* Total Horizontal Pixels per Line [7_0] */
#define	HDTX_HTOT_STS_L_HTOT_VAL_7_0_MSK	(0xff)
#define	HDTX_HTOT_STS_L_HTOT_VAL_7_0_BASE	0

/* HDTX_HTOT_STS_H 0x002F	HTOTAL Bits [15_8] Register */
/* Total Horizontal Pixels per Line [15_8] */
#define	HDTX_HTOT_STS_H_HTOT_VAL_15_8_MSK	(0xff)
#define	HDTX_HTOT_STS_H_HTOT_VAL_15_8_BASE	0

/* HDTX_HBLANK_STS_L 0x0030 HBLANK Bits [7_0] Register */
/* Horizontal Blanking Period [7_0] */
#define	HDTX_HBLANK_STS_L_HBLANK_VAL_7_0_MSK	(0xff)
#define	HDTX_HBLANK_STS_L_HBLANK_VAL_7_0_BASE	0

/* HDTX_HBLANK_STS_H 0x0031 HBLANK Bits [15_8] Register */
/* Horizontal Blanking Period [15_8] */
#define	HDTX_HBLANK_STS_H_HBLANK_VAL_15_8_MSK	(0xff)
#define	HDTX_HBLANK_STS_H_HBLANK_VAL_15_8_BASE	0

/* HDTX_VTOT_STS_L 0x0032 VTOTAL Bits [7_0] Register */
/* Vertical Resolution of Frame [7_0] */
#define	HDTX_VTOT_STS_L_VTOT_VAL_7_0_MSK	(0xff)
#define	HDTX_VTOT_STS_L_VTOT_VAL_7_0_BASE	0

/* HDTX_VTOT_STS_H 0x0033 VTOTAL Bits [15_8] Register */
/* Vertical Resolution of Frame [15_8] */
#define	HDTX_VTOT_STS_H_VTOT_VAL_15_8_MSK	(0xff)
#define	HDTX_VTOT_STS_H_VTOT_VAL_15_8_BASE	0

/* HDTX_VRES_STS_L 0x0034 VRES Bits [7_0] Register */
/* Active Vertical Resolution of Frame [7_0] */
#define	HDTX_VRES_STS_L_VRES_VAL_7_0_MSK	(0xff)
#define	HDTX_VRES_STS_L_VRES_VAL_7_0_BASE	0

/* HDTX_VRES_STS_H 0x0035 VRES Bits [15_8] Register */
/* Active Vertical Resolution of Frame [15_8] */
#define	HDTX_VRES_STS_H_VRES_VAL_15_8_MSK	(0xff)
#define	HDTX_VRES_STS_H_VRES_VAL_15_8_BASE	0

/* HDTX_VSTART_STS_L 0x0036 VSTART Bits [7_0] Register */
/* First Active Line of Frame [7_0] */
#define	HDTX_VSTART_STS_L_VSTART_VAL_7_0_MSK	(0xff)
#define	HDTX_VSTART_STS_L_VSTART_VAL_7_0_BASE	0

/* HDTX_VSTART_STS_H 0x0037 VSTART Bits [15_8] Register */
/* First Active Line of Frame [15_8] */
#define	HDTX_VSTART_STS_H_VSTART_VAL_15_8_MSK	(0xff)
#define	HDTX_VSTART_STS_H_VSTART_VAL_15_8_BASE	0

/* HDTX_VIDEO_STS 0x0038 Video Status Register */
/* Bit(s) HDTX_VIDEO_STS_RSRV_7_1 reserved */
#define	HDTX_VIDEO_STS_INIT_OVER	1	/* Init Over */

/* HDTX_VIDEO_CTRL 0x0039 Video Control Register */
/* Bit(s) HDTX_VIDEO_CTRL_RSRV_7 reserved */
/* Internal Logic Video Format Detect */
#define	HDTX_VIDEO_CTRL_INT_FRM_SEL	(1 << 6)
/* Bit(s) HDTX_VIDEO_CTRL_RSRV_5_4 reserved */
#define	HDTX_VIDEO_CTRL_ACR_PRI_SEL_I	(1 << 3)		/* ACR priority selection */
#define	HDTX_VIDEO_CTRL_DEBUG_CTRL	(1 << 2)		/* Debug control */
#define	HDTX_VIDEO_CTRL_FLD_POL	(1 << 1)		/* Filed polarity */
#define	HDTX_VIDEO_CTRL_IN_YC	1		/* Input Video YC */

/* HDTX_HDMI_CTRL 0x003A HDMI Control Register */
/* Bit(s) HDTX_HDMI_CTRL_RSRV_7 reserved */
#define	HDTX_HDMI_CTRL_PIX_RPT_MSK	SHIFT4(0x7)	/* Pixel Repetition Values */
#define	HDTX_HDMI_CTRL_PIX_RPT_BASE	4
#define	HDTX_HDMI_CTRL_BCH_ROT	(1 << 2)		/* BCH Value */
#define	HDTX_HDMI_CTRL_LAYOUT	(1 << 1)		/* Layout */
#define	HDTX_HDMI_CTRL_HDMI_MODE	1		/* HDMI Mode */

/* HDTX_PP_HW 0x0046 Hardware Computed Pixel Packing Value Register */
/* Bit(s) HDTX_PP_HW_RSRV_7_4 reserved */
#define	HDTX_PP_HW_PP_REG_MSK	(0xf)	/* Pixel Packing Phase */
#define	HDTX_PP_HW_PP_REG_BASE	0

/* HDTX_DC_FIFO_SFT_RST	0x0047	Deep Color FIFO Soft Reset Register */
/* Bit(s) HDTX_DC_FIFO_SFT_RST_RSRV_7_1 reserved */
/* FIFO Soft Reset */
#define	HDTX_DC_FIFO_SFT_RST_FIFO_SFT_RST_REG	1

/* HDTX_DC_FIFO_WR_PTR 0x0048 FIFO Write Pointer Register */
/* Bit(s) HDTX_DC_FIFO_WR_PTR_RSRV_7_6 reserved */
/* FIFO Write Pointer */
#define	HDTX_DC_FIFO_WR_PTR_FIFO_WR_PTR__MSK	(0x3f)
#define	HDTX_DC_FIFO_WR_PTR_FIFO_WR_PTR__BASE	0

/* HDTX_DC_FIFO_RD_PTR 0x0049 FIFO Read Pointer Register */
/* Bit(s) HDTX_DC_FIFO_RD_PTR_RSRV_7_6 reserved */
/* FIFO Read Pointer */
#define	HDTX_DC_FIFO_RD_PTR_FIFO_RD_PTR_REG_MSK	(0x3f)
#define	HDTX_DC_FIFO_RD_PTR_FIFO_RD_PTR_REG_BASE	0

/* HDTX_TDATA0_0 0x004C	Test Data [7_0] for TMDS Channel 0 Register */
#define	HDTX_TDATA0_0_TDATA0_REG_7_0_MSK	(0xff)	/* TDATA0 [7_0] */
#define	HDTX_TDATA0_0_TDATA0_REG_7_0_BASE	0

/* HDTX_TDATA0_1 0x004D	Test Data [15_8] for TMDS Channel 0 Register */
#define	HDTX_TDATA0_1_TDATA0_REG_15_8_MSK	(0xff)	/* TDATA0 [15_8] */
#define	HDTX_TDATA0_1_TDATA0_REG_15_8_BASE	0

/* HDTX_TDATA0_2 0x004E	Test Data [19_16] for TMDS Channel 0 Register */
/* Bit(s) HDTX_TDATA0_2_RSRV_7_4 reserved */
#define	HDTX_TDATA0_2_TDATA0_REG_19_16_MSK	(0xf)	/* TDATA0 [19_16] */
#define	HDTX_TDATA0_2_TDATA0_REG_19_16_BASE	0

/* HDTX_TDATA1_0 0x0050	Test Data [7_0] for TMDS Channel 1 Register */
#define	HDTX_TDATA1_0_TDATA1_REG_7_0_MSK	(0xff)	/* TDATA1 [7_0] */
#define	HDTX_TDATA1_0_TDATA1_REG_7_0_BASE	0

/* HDTX_TDATA1_1 0x0051	Test Data [15_8] for TMDS Channel 1 Register */
#define	HDTX_TDATA1_1_TDATA1_REG_15_8_MSK	(0xff)	/* TDATA1 [15_8] */
#define	HDTX_TDATA1_1_TDATA1_REG_15_8_BASE	0

/* HDTX_TDATA1_2 0x0052	Test Data [19_16] for TMDS Channel 1 Register */
/* Bit(s) HDTX_TDATA1_2_RSRV_7_4 reserved */
#define	HDTX_TDATA1_2_TDATA1_REG_19_16_MSK	(0xf)	/* TDATA1 [19_16] */
#define	HDTX_TDATA1_2_TDATA1_REG_19_16_BASE	0

/* HDTX_TDATA2_0 0x0054	Test Data [7_0] for TMDS Channel 2 Register */
#define	HDTX_TDATA2_0_TDATA2_REG_7_0_MSK	(0xff)	/* TDATA2 [7_0] */
#define	HDTX_TDATA2_0_TDATA2_REG_7_0_BASE	0

/* HDTX_TDATA2_1 0x0055 Test Data [15_8] for TMDS Channel 2 Register */
#define	HDTX_TDATA2_1_TDATA2_REG_15_8_MSK	(0xff)	/* TDATA2 [15_8] */
#define	HDTX_TDATA2_1_TDATA2_REG_15_8_BASE	0

/* HDTX_TDATA2_2 0x0056	Test Data [19_16] for TMDS Channel 2 Register */
/* Bit(s) HDTX_TDATA2_2_RSRV_7_4 reserved */
#define	HDTX_TDATA2_2_TDATA2_REG_19_16_MSK	(0xf)	/* TDATA2 [19_16] */
#define	HDTX_TDATA2_2_TDATA2_REG_19_16_BASE	0

/* HDTX_TDATA3_0 0x0058	Test Data [7_0] for TMDS Channel 3 Register */
#define	HDTX_TDATA3_0_TDATA3_REG_7_0_MSK	(0xff)	/* TDATA3 [7_0] */
#define	HDTX_TDATA3_0_TDATA3_REG_7_0_BASE	0

/* HDTX_TDATA3_1 0x0059 Test Data [15_8] for TMDS Channel 3 Register */
#define	HDTX_TDATA3_1_TDATA3_REG_15_8_MSK	(0xff)	/* TDATA3 [15_8] */
#define	HDTX_TDATA3_1_TDATA3_REG_15_8_BASE	0

/* HDTX_TDATA3_2 0x005A	Test Data [19_16] for TMDS Channel 3 Register */
/* Bit(s) HDTX_TDATA3_2_RSRV_7_4 reserved */
#define	HDTX_TDATA3_2_TDATA3_REG_19_16_MSK	(0xf)	/* TDATA3 [19_16] */
#define	HDTX_TDATA3_2_TDATA3_REG_19_16_BASE	0

/* HDTX_TDATA_SEL 0x005B Test Data Selection Control Register */
/* Bit(s) HDTX_TDATA_SEL_RSRV_7 reserved */
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_b6	(1 << 6)		/* TDATA Select [6] */
/* TDATA Select [5_4] */
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_5_4_MSK	((0x3) << 4)
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_5_4_BASE	4
/* TDATA Select [3_2] */
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_3_2_MSK	((0x3) << 2)
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_3_2_BASE	2
/* TDATA Select [1_0] */
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_1_0_MSK	(0x3)
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_1_0_BASE	0

/* HDTX_SWAP_CTRL 0x005C	TMDS Data Swap Control Register */
/* Bit(s) HDTX_SWAP_CTRL_RSRV_7_2 reserved */
#define	HDTX_SWAP_CTRL_CHANNEL_SWAP	(1 << 1)			/* Channel Swap */
#define	HDTX_SWAP_CTRL_BIT_SWAP	1			/* Bit Swap */

/* HDTX_AVMUTE_CTRL 0x005D AVMUTE Control Register */
/* Bit(s) HDTX_AVMUTE_CTRL_RSRV_7_2 reserved */
#define	HDTX_AVMUTE_CTRL_AVMUTE_CTRL_REG_MSK	(0x3)		/* Audio Mute */
#define	HDTX_AVMUTE_CTRL_AVMUTE_CTRL_REG_BASE	0

/* HDTX_HOST_PKT_CTRL0 0x005E Host Packet Control Register 0 */
/* Bit(s) HDTX_HOST_PKT_CTRL0_RSRV_7_6 reserved */
#define	HDTX_HOST_PKT_CTRL0_PKT5_EN	(1 << 5)		/* Packet Type 5 Enable */
#define	HDTX_HOST_PKT_CTRL0_PKT4_EN	(1 << 4)		/* Packet Type 4 Enable */
#define	HDTX_HOST_PKT_CTRL0_PKT3_EN	(1 << 3)		/* Packet Type 3 Enable */
#define	HDTX_HOST_PKT_CTRL0_PKT2_EN	(1 << 2)		/* Packet Type 2 Enable */
#define	HDTX_HOST_PKT_CTRL0_PKT1_EN	(1 << 1)		/* Packet Type 1 Enable */
#define	HDTX_HOST_PKT_CTRL0_PKT0_EN	1		/* Packet Type 0 Enable */

/* HDTX_HOST_PKT_CTRL1 0x005F Host Packet Control Register 1 */
/* Bit(s) HDTX_HOST_PKT_CTRL1_RSRV_7_6 reserved */
/* Packet Type 5 Tx Mode */
#define	HDTX_HOST_PKT_CTRL1_PKT5_TX_MODE	(1 << 5)
/* Packet Type 4 Tx Mode */
#define	HDTX_HOST_PKT_CTRL1_PKT4_TX_MODE	(1 << 4)
/* Packet Type 3 Tx Mode */
#define	HDTX_HOST_PKT_CTRL1_PKT3_TX_MODE	(1 << 3)
/* Packet Type 2 Tx Mode */
#define	HDTX_HOST_PKT_CTRL1_PKT2_TX_MODE	(1 << 2)
/* Packet Type 1 Tx Mode */
#define	HDTX_HOST_PKT_CTRL1_PKT1_TX_MODE	(1 << 1)
/* Packet Type 0 Tx Mode */
#define	HDTX_HOST_PKT_CTRL1_PKT0_TX_MODE	1

/* HDTX_PKT0_BYTE0_30 0x0060 PKT0 Byte 0 to 30 Registers */
#define	HDTX_PKT0_BYTE0_30_PKT0_255_0_MSK	(0xff)	/* PKT0 [255_0] */
#define	HDTX_PKT0_BYTE0_30_PKT0_255_0_BASE	0

/* HDTX_PKT1_BYTE0_30 0x0080	PKT1 Byte 0 to 30 Registers */
#define	HDTX_PKT1_BYTE0_30_PKT1_255_0_MSK	(0xff)	/* PKT1 [255_0] */
#define	HDTX_PKT1_BYTE0_30_PKT1_255_0_BASE	0

/* HDTX_PKT2_BYTE0_30 0x00A0	PKT2 Byte 0 to 30 Registers */
#define	HDTX_PKT2_BYTE0_30_PKT2_255_0_MSK	(0xff)	/* PKT2 [255_0] */
#define	HDTX_PKT2_BYTE0_30_PKT2_255_0_BASE	0

/* HDTX_PKT3_BYTE0_30 0x00C0 PKT3 Byte 0 to 30 Registers */
#define	HDTX_PKT3_BYTE0_30_PKT3_255_0_MSK	(0xff)	/* PKT3 [255_0] */
#define	HDTX_PKT3_BYTE0_30_PKT3_255_0_BASE	0

/* HDTX_PKT4_BYTE0_30 0x00E0 PKT4 Byte 0 to 30 Registers */
#define	HDTX_PKT4_BYTE0_30_PKT4_255_0_MSK	(0xff)	/* PKT4 [255_0] */
#define	HDTX_PKT4_BYTE0_30_PKT4_255_0_BASE	0

/* HDTX_PKT5_BYTE0_30 0x0100	PKT5 Byte 0 to 30 Registers */
#define	HDTX_PKT5_BYTE0_30_PKT5_255_0_MSK	(0xff)	/* PKT5 [255_0] */
#define	HDTX_PKT5_BYTE0_30_PKT5_255_0_BASE	0

/* HDTX_UBITS_0 0x0120 User Data Bits of SPDIF Header Register 0 */
/* User Data Bits of SPDIF Header [7_0] */
#define	HDTX_UBITS_0_UBITS_7_0_MSK	(0xff)
#define	HDTX_UBITS_0_UBITS_7_0_BASE	0

/* HDTX_UBITS_1 0x0121 User Data Bits of SPDIF Header Register 1 */
/* User Data Bits of SPDIF Header [15_8] */
#define	HDTX_UBITS_1_UBITS_15_8_MSK	(0xff)
#define	HDTX_UBITS_1_UBITS_15_8_BASE	0

/* HDTX_UBITS_2	0x0122 User Data Bits of SPDIF Header Register 2 */
/* User Data Bits of SPDIF Header [23_16] */
#define	HDTX_UBITS_2_UBITS_23_16_MSK	(0xff)
#define	HDTX_UBITS_2_UBITS_23_16_BASE	0

/* HDTX_UBITS_3 0x0123 User Data Bits of SPDIF Header Register 3 */
/* User Data Bits of SPDIF Header [31_24] */
#define	HDTX_UBITS_3_UBITS_31_24_MSK	(0xff)
#define	HDTX_UBITS_3_UBITS_31_24_BASE	0

/* HDTX_UBITS_4	0x0124 User Data Bits of SPDIF Header Register 4 */
/* User Data Bits of SPDIF Header [39_32] */
#define	HDTX_UBITS_4_U_BITS_39_32_MSK	(0xff)
#define	HDTX_UBITS_4_U_BITS_39_32_BASE	0

/* HDTX_UBITS_5 0x0125 User Data Bits of SPDIF Header Register 5 */
/* User Data Bits of SPDIF Header [47_40] */
#define	HDTX_UBITS_5_UBITS_47_40_MSK	(0xff)
#define	HDTX_UBITS_5_UBITS_47_40_BASE	0

/* HDTX_UBITS_6	0x0126	User Data Bits of SPDIF Header Register 6 */
/* User Data Bits of SPDIF Header [55_48] */
#define	HDTX_UBITS_6_UBITS_55_48_MSK	(0xff)
#define	HDTX_UBITS_6_UBITS_55_48_BASE	0

/* HDTX_UBITS_7 0x0127 User Data Bits of SPDIF Header Register 7 */
/* User Data Bits of SPDIF Header [63_56] */
#define	HDTX_UBITS_7_UBITS_63_56_MSK	(0xff)
#define	HDTX_UBITS_7_UBITS_63_56_BASE	0

/* HDTX_UBITS_8	0x0128	User Data Bits of SPDIF Header Register 8 */
/* User Data Bits of SPDIF Header [71_64] */
#define	HDTX_UBITS_8_UBITS_71_64_MSK	(0xff)
#define	HDTX_UBITS_8_UBITS_71_64_BASE	0

/* HDTX_UBITS_9	0x0129 User Data Bits of SPDIF Header Register 9 */
/* User Data Bits of SPDIF Header [79_72] */
#define	HDTX_UBITS_9_UBITS_79_72_MSK	(0xff)
#define	HDTX_UBITS_9_UBITS_79_72_BASE	0

/* HDTX_UBITS_10 0x012A User Data Bits of SPDIF Header Register 10 */
/* User Data Bits of SPDIF Header [87_80] */
#define	HDTX_UBITS_10_UBITS_87_80_MSK	(0xff)
#define	HDTX_UBITS_10_UBITS_87_80_BASE	0

/* HDTX_UBITS_11 0x012B	User Data Bits of SPDIF Header Register 11 */
/* User Data Bits of SPDIF Header [95_88] */
#define	HDTX_UBITS_11_UBITS_95_88_MSK	(0xff)
#define	HDTX_UBITS_11_UBITS_95_88_BASE	0

/* HDTX_UBITS_12 0x012C User Data Bits of SPDIF Header Register 12 */
/* User Data Bits of SPDIF Header [103_96] */
#define	HDTX_UBITS_12_UBITS_103_96_MSK	(0xff)
#define	HDTX_UBITS_12_UBITS_103_96_BASE	0

/* HDTX_UBITS_13 0x012D	User Data Bits of SPDIF Header Register 13 */
/* MSB 4 Bits [107_104] of SPDIF Header */
#define	HDTX_UBITS_13_UBITS_107_104_MSK	(0xff)
#define	HDTX_UBITS_13_UBITS_107_104_BASE	0

/* HDTX_HBR_PKT 0x012E HBR Packet Transmission Control Register */
/* Bit(s) HDTX_HBR_PKT_RSRV_7_1 reserved */
/* Audio Sample/HBR Packet Transmission */
#define	HDTX_HBR_PKT_HDTX_HBR_PKT	1

/* HDTX_PHY_FIFO_SOFT_RST 0x013 HDMI Tx PHY FIFO Soft Reset Register */
/* Bit(s) HDTX_PHY_FIFO_SOFT_RST_RSRV_7_1 reserved */
/* PHY FIFO Soft Reset */
#define	HDTX_PHY_FIFO_SOFT_RST_HDTX_PHY_FIFO_SOFT_RST	1

/* HDTX_PHY_FIFO_PTRS 0x0131 HDMITX PHY FIFO Read and Write Pointers Register */
/* LSB 4 Bits [7_4] for Putting Read Pointer */
#define	HDTX_PHY_FIFO_PTRS_HDTX_PHY_FIFO_RD_PTRS_7_4_MSK	((0xf) << 4)
#define	HDTX_PHY_FIFO_PTRS_HDTX_PHY_FIFO_RD_PTRS_7_4_BASE	4
/* MSB 4 Bits [3_0] for Putting Write Pointer */
#define	HDTX_PHY_FIFO_PTRS_HDTX_PHY_FIFO_WR_PTRS_3_0_MSK	(0xf)
#define	HDTX_PHY_FIFO_PTRS_HDTX_PHY_FIFO_WR_PTRS_3_0_BASE	0

/* HDTX_PRBS_CTRL0 0x0132 PRBS Control Register */
/* Bit(s) HDTX_PRBS_CTRL0_RSRV_7_3 reserved */
#define	HDTX_PRBS_CTRL0_PRBS_EN	(1 << 2)	/* PRBS Enable */
#define	HDTX_PRBS_CTRL0_PRBS_TYPE_SEL_1_0_MSK	(0x3)	/* PRBS Control */
#define	HDTX_PRBS_CTRL0_PRBS_TYPE_SEL_1_0_BASE	0

/* HDTX_HST_PKT_CTRL2 0x0133 Host Packet Control Register 2 */
/* Bit(s) HDTX_HST_PKT_CTRL2_RSRV_7_1 reserved */
/* Host Packet Control 2 */
#define	HDTX_HST_PKT_CTRL2_HOST_PKT_CTRL2	1

/* HDTX_HST_PKT_START 0x0134 Host Packet Transmission Control During VBI Register */
/* Bit(s) HDTX_HST_PKT_START_RSRV_7_1 reserved */
/* Host Packet Start */
#define	HDTX_HST_PKT_START_HOST_PKT_START	1

/* HDTX_AUD_CH1_SEL 0x0137 Audio Channel 1 Select Register */
/* Bit(s) HDTX_AUD_CH1_SEL_RSRV_7_3 reserved */
/* Audio Channel1 select */
#define	HDTX_AUD_CH1_SEL_AUD_CH1_SEL_MSK	(0x7)
#define	HDTX_AUD_CH1_SEL_AUD_CH1_SEL_BASE	0

/* HDTX_AUD_CH2_SEL 0x0138 Audio Channel 2 Select Register */
/* Bit(s) HDTX_AUD_CH2_SEL_RSRV_7_3 reserved */
/* Audio Channel2 select */
#define	HDTX_AUD_CH2_SEL_AUD_CH2_SEL_MSK	(0x7)
#define	HDTX_AUD_CH2_SEL_AUD_CH2_SEL_BASE	0

/* HDTX_AUD_CH3_SEL 0x0139 Audio Channel 3 Select Register */
/* Bit(s) HDTX_AUD_CH3_SEL_RSRV_7_3 reserved */
/* Audio Channel3 select */
#define	HDTX_AUD_CH3_SEL_AUD_CH3_SEL_MSK	(0x7)
#define	HDTX_AUD_CH3_SEL_AUD_CH3_SEL_BASE	0

/* HDTX_AUD_CH4_SEL 0x013A Audio Channel 4 Select Register */
/* Bit(s) HDTX_AUD_CH4_SEL_RSRV_7_3 reserved */
/* Audio Channel4 select */
#define	HDTX_AUD_CH4_SEL_AUD_CH4_SEL_MSK	(0x7)
#define	HDTX_AUD_CH4_SEL_AUD_CH4_SEL_BASE	0

/* HDTX_AUD_CH5_SEL 0x013B Audio Channel 5 Select Register */
/* Bit(s) HDTX_AUD_CH5_SEL_RSRV_7_3 reserved */
/* Audio Channel5 select */
#define	HDTX_AUD_CH5_SEL_AUD_CH5_SEL_MSK	(0x7)
#define	HDTX_AUD_CH5_SEL_AUD_CH5_SEL_BASE	0

/* HDTX_AUD_CH6_SEL 0x013C Audio Channel 6 Select Register */
/* Bit(s) HDTX_AUD_CH6_SEL_RSRV_7_3 reserved */
/* Audio Channel6 select */
#define	HDTX_AUD_CH6_SEL_AUD_CH6_SEL_MSK	(0x7)
#define	HDTX_AUD_CH6_SEL_AUD_CH6_SEL_BASE	0

/* HDTX_AUD_CH7_SEL 0x013D Audio Channel 7 Select Register */
/* Bit(s) HDTX_AUD_CH7_SEL_RSRV_7_3 reserved */
/* Audio Channel7 select */
#define	HDTX_AUD_CH7_SEL_AUD_CH7_SEL_MSK	(0x7)
#define	HDTX_AUD_CH7_SEL_AUD_CH7_SEL_BASE	0

/* HDTX_AUD_CH8_SEL 0x013E Audio Channel 8 Select Register */
/* Bit(s) HDTX_AUD_CH8_SEL_RSRV_7_3 reserved */
/* Audio Channel8 select */
#define	HDTX_AUD_CH8_SEL_AUD_CH8_SEL_MSK	(0x7)
#define	HDTX_AUD_CH8_SEL_AUD_CH8_SEL_BASE	0

/* TX_HDCP_AKEY0_BYTE_0 0x1200 AKEY0 [7_0] Register */
#define	TX_HDCP_AKEY0_BYTE_0_AKEY0_7_0_MSK	(0xff)	/* AKEY0 [7_0] */
#define	TX_HDCP_AKEY0_BYTE_0_AKEY0_7_0_BASE	0

/* TX_HDCP_AKEY0_BYTE_1 0x1201 AKEY0 [15_8] Register */
#define	TX_HDCP_AKEY0_BYTE_1_AKEY0_15_8_MSK	(0xff)	/* AKEY0 [15_8] */
#define	TX_HDCP_AKEY0_BYTE_1_AKEY0_15_8_BASE	0

/* TX_HDCP_AKEY0_BYTE_2	0x1202 AKEY0 [23_16] Register */
/* AKEY0 [23_16] */
#define	TX_HDCP_AKEY0_BYTE_2_AKEY0_23_16_MSK	(0xff)
#define	TX_HDCP_AKEY0_BYTE_2_AKEY0_23_16_BASE	0

/* TX_HDCP_AKEY0_BYTE_3	0x1203 AKEY0 [31_24] Register */
/* AKEY0 [31_24] */
#define	TX_HDCP_AKEY0_BYTE_3_AKEY0_31_24_MSK	(0xff)
#define	TX_HDCP_AKEY0_BYTE_3_AKEY0_31_24_BASE	0

/* TX_HDCP_AKEY0_BYTE_4	0x1204 AKEY0 [39_32] Register */
/* AKEY0 [39_32] */
#define	TX_HDCP_AKEY0_BYTE_4_AKEY0_39_32_MSK	(0xff)
#define	TX_HDCP_AKEY0_BYTE_4_AKEY0_39_32_BASE	0

/* TX_HDCP_AKEY0_BYTE_5	0x1205 AKEY0 [47_40] Register */
/* AKEY0 [47_40] */
#define	TX_HDCP_AKEY0_BYTE_5_AKEY0_47_40_MSK	(0xff)
#define	TX_HDCP_AKEY0_BYTE_5_AKEY0_47_40_BASE	0

/* TX_HDCP_AKEY0_BYTE_6	0x1206 AKEY0 [55_48] Register */
/* AKEY0 [55_48] */
#define	TX_HDCP_AKEY0_BYTE_6_AKEY0_55_48_MSK	(0xff)
#define	TX_HDCP_AKEY0_BYTE_6_AKEY0_55_48_BASE	0

/* TX_HDCP_AKSV_BYTE_0 0x1340 AKSV [7_0] Register */
#define	TX_HDCP_AKSV_BYTE_0_AKSV0_7_0_MSK	(0xff)	/* AKSV0 [7_0] */
#define	TX_HDCP_AKSV_BYTE_0_AKSV0_7_0_BASE	0

/* TX_HDCP_AKSV_BYTE_1 0x1341 AKSV [15_8] Register */
#define	TX_HDCP_AKSV_BYTE_1_AKSV0_15_8_MSK	(0xff)	/* AKSV0 [15_8] */
#define	TX_HDCP_AKSV_BYTE_1_AKSV0_15_8_BASE	0

/* TX_HDCP_AKSV_BYTE_2 0x1342 AKSV [23_16] Register */
/* AKSV0 [23_16] */
#define	TX_HDCP_AKSV_BYTE_2_AKSV0_23_16_MSK	(0xff)
#define	TX_HDCP_AKSV_BYTE_2_AKSV0_23_16_BASE	0

/* TX_HDCP_AKSV_BYTE_3 0x1343 AKSV [31_24] Register */
/* AKSV0 [31_24] */
#define	TX_HDCP_AKSV_BYTE_3_AKSV0_31_24_MSK	(0xff)
#define	TX_HDCP_AKSV_BYTE_3_AKSV0_31_24_BASE	0

/* TX_HDCP_AKSV_BYTE_4 0x1344 AKSV [39_32] Register */
/* AKSV0 [39_32] */
#define	TX_HDCP_AKSV_BYTE_4_AKSV0_39_32_MSK	(0xff)
#define	TX_HDCP_AKSV_BYTE_4_AKSV0_39_32_BASE	0

/* TX_HDCP_CONTROL 0x1350 HDCP Control Register */
#define	TX_HDCP_CONTROL_HDMI_MODE	(1 << 7)	/* HDMI Mode */
#define	TX_HDCP_CONTROL_EESS_EN	(1 << 6)	/* EESS Enable */
#define	TX_HDCP_CONTROL_REPEATER	(1 << 5)	/* Repeater */
#define	TX_HDCP_CONTROL_ADVANCE_CIPHER	(1 << 4)	/* Advance Cipher */
/* Enhanced Link Verification */
#define	TX_HDCP_CONTROL_ENH_LINK_VERIFICATION	(1 << 3)
#define	TX_HDCP_CONTROL_CIPHER_EN	(1 << 2)	/* Cipher Enable */
#define	TX_HDCP_CONTROL_READ_AKSV	(1 << 1)	/* Read AKSV */
/* Bit(s) TX_HDCP_CONTROL_RSRV_0 reserved */

/* TX_HDCP_STATUS_1 0x1351 HDCP Status Register 1 */
#define	TX_HDCP_STATUS_1_BKSV_READY	(1 << 7)	/* BKSV Ready */
/* R0 Authentication Passed */
#define	TX_HDCP_STATUS_1_R0_AUTH_PASS	(1 << 6)
/* KSV List Check Passed */
#define	TX_HDCP_STATUS_1_KSV_LIST_CHECK_PASS	(1 << 5)
#define	TX_HDCP_STATUS_1_ERR	(1 << 4)	/* Error */
#define	TX_HDCP_STATUS_1_FW_ENC_EN	(1 << 3)	/* FW Encryption Enable */
#define	TX_HDCP_STATUS_1_READY_KEYS	(1 << 2)	/* Ready Keys */
#define	TX_HDCP_STATUS_1_AKSV_SENT	(1 << 1)	/* AKSV Sent */
/* Bit(s) TX_HDCP_STATUS_1_RSRV_0 reserved */

/* TX_HDCP_STATUS_2 0x1352 HDCP Status Register 2 */
/* Video Mode [1_0] */
#define	TX_HDCP_STATUS_2_VIDEO_MODE_1_0_MSK	((0x3) << 6)
#define	TX_HDCP_STATUS_2_VIDEO_MODE_1_0_BASE	6
/* Bit(s) TX_HDCP_STATUS_2_RSRV_5_0 reserved */

/* TX_HDCP_INTR_0 0x1353 HDCP Interrupt Register */
#define	TX_HDCP_INTR_0_AKSV_READY_STATUS	(1 << 7)		/* AKSV Ready Status */
#define	TX_HDCP_INTR_0_TX_AN_READY_STATUS	(1 << 6)		/* Tx An Ready Status */
#define	TX_HDCP_INTR_0_TX_R0_READY_STATUS	(1 << 5)		/* Tx R0 Ready Status */
#define	TX_HDCP_INTR_0_TX_RI_READY_STATUS	(1 << 4)		/* Tx Ri Ready Status */
#define	TX_HDCP_INTR_0_TX_PJ_READY_STATUS	(1 << 3)		/* Tx Pj Ready Status */
/* Bit(s) TX_HDCP_INTR_0_RSRV_2_0 reserved */

/* TX_HDCP_TX_AKSV_0 0x1354 TX KSV Byte 0 Register */
/* TX_AKSV0 [7_0] */
#define	TX_HDCP_TX_AKSV_0_TX_AKSV0_7_0_MSK	(0xff)
#define	TX_HDCP_TX_AKSV_0_TX_AKSV0_7_0_BASE	0

/* TX_HDCP_TX_AKSV_1 0x1355 TX KSV Byte 1 Register */
/* TX_AKSV0 [15_8] */
#define	TX_HDCP_TX_AKSV_1_TX_AKSV0_15_8_MSK	(0xff)
#define	TX_HDCP_TX_AKSV_1_TX_AKSV0_15_8_BASE	0

/* TX_HDCP_TX_AKSV_2 0x1356 TX KSV Byte 2 Register */
/* TX_AKSV0 [23_16] */
#define	TX_HDCP_TX_AKSV_2_TX_AKSV0_23_16_MSK	(0xff)
#define	TX_HDCP_TX_AKSV_2_TX_AKSV0_23_16_BASE	0

/* TX_HDCP_TX_AKSV_3 0x1357 TX KSV Byte 3 Register */
/* TX_AKSV0 [31_24] */
#define	TX_HDCP_TX_AKSV_3_TX_AKSV0_31_24_MSK	(0xff)
#define	TX_HDCP_TX_AKSV_3_TX_AKSV0_31_24_BASE	0

/* TX_HDCP_TX_AKSV_4 0x1358 TX KSV Byte 4 Register */
/* TX_AKSV0 [39_32] */
#define	TX_HDCP_TX_AKSV_4_TX_AKSV0_39_32_MSK	(0xff)
#define	TX_HDCP_TX_AKSV_4_TX_AKSV0_39_32_BASE	0

/* TX_HDCP_RX_BKSV_0 0x135C RX KSV Byte 0 Register */
/* RX_BKSV0 [7_0] */
#define	TX_HDCP_RX_BKSV_0_RX_BKSV0_7_0_MSK	(0xff)
#define	TX_HDCP_RX_BKSV_0_RX_BKSV0_7_0_BASE	0

/* TX_HDCP_RX_BKSV_1 0x135D RX KSV Byte 1 Register */
/* RX_BKSV0 [15_8] */
#define	TX_HDCP_RX_BKSV_1_RX_BKSV0_15_8_MSK	(0xff)
#define	TX_HDCP_RX_BKSV_1_RX_BKSV0_15_8_BASE	0

/* TX_HDCP_RX_BKSV_2 0x135E RX KSV Byte 2 Register */
/* RX_BKSV0 [23_16] */
#define	TX_HDCP_RX_BKSV_2_RX_BKSV0_23_16_MSK	(0xff)
#define	TX_HDCP_RX_BKSV_2_RX_BKSV0_23_16_BASE	0

/* TX_HDCP_RX_BKSV_3 0x135F RX KSV Byte 3 Register */
/* RX_BKSV0 [31_24] */
#define	TX_HDCP_RX_BKSV_3_RX_BKSV0_31_24_MSK	(0xff)
#define	TX_HDCP_RX_BKSV_3_RX_BKSV0_31_24_BASE	0

/* TX_HDCP_RX_BKSV_4 0x1360 RX KSV Byte 4 Register */
/* RX_BKSV0 [39_32] */
#define	TX_HDCP_RX_BKSV_4_RX_BKSV0_39_32_MSK	(0xff)
#define	TX_HDCP_RX_BKSV_4_RX_BKSV0_39_32_BASE	0

/* TX_HDCP_TX_AINFO 0x1361 Tx AINFO Register */
#define	TX_HDCP_TX_AINFO_AINFO_7_2_MSK	((0x3f) << 2)	/* ainfo [7_2] */
#define	TX_HDCP_TX_AINFO_AINFO_7_2_BASE	2
/* ENABLE_1.1_FEATURES */
#define	TX_HDCP_TX_AINFO_AINFO_b1	(1 << 1)
#define	TX_HDCP_TX_AINFO_AINFO_b0	1			/* ainfo[0] */

/* TX_HDCP_RX_BCAPS 0x1362 BCAPS Read from HDMI Rx Register */
#define	TX_HDCP_RX_BCAPS_BCAPS_b7	(1 << 7)		/* bcaps[7] */
/* REPEATER, HDCP Repeater Capability */
#define	TX_HDCP_RX_BCAPS_BCAPS_b6	(1 << 6)
/* READY, KSV FIFO Ready */
#define	TX_HDCP_RX_BCAPS_BCAPS_b5	(1 << 5)
#define	TX_HDCP_RX_BCAPS_BCAPS_b4	(1 << 4)		/* FAST */
#define	TX_HDCP_RX_BCAPS_BCAPS_3_2_MSK	((0x3) << 2)	/* bcaps[3_2] */
#define	TX_HDCP_RX_BCAPS_BCAPS_3_2_BASE	2
#define	TX_HDCP_RX_BCAPS_BCAPS_b1	(1 << 1)		/* 1.1_FEATURES */
/* FAST_REAUTHENTICATION */
#define	TX_HDCP_RX_BCAPS_BCAPS_b0	1

/* TX_HDCP_RX_BSTATUS_0	0x1364 BSTATUS MSB Byte Read from HDMI Rx Register */
/* bstatus[15_14] */
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_15_14_MSK	((0x3) << 6)
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_15_14_BASE	6
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_b13	(1 << 5)		/* bstatus[13] */
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_b12	(1 << 4)		/* HDMI Mode */
/* Topology Error Indicator */
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_b11	(1 << 3)
/* Three-bit Repeater Cascade Depth */
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_10_8_MSK	(0x7)
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_10_8_BASE	0

/* TX_HDCP_RX_BSTATUS_1	0x1365 BSTATUS LSB Byte Read from HDMI Rx Register */
/* Topology Error Indicator */
#define	TX_HDCP_RX_BSTATUS_1_BSTATUS_b7	(1 << 7)
/* Total Number of Attached Downstream Devices */
#define	TX_HDCP_RX_BSTATUS_1_BSTATUS_6_0_MSK	(0x7f)
#define	TX_HDCP_RX_BSTATUS_1_BSTATUS_6_0_BASE	0

/* TX_HDCP_TX_AN_0 0x1368 HDCP Tx AN Value (Byte 0) Register */
/* HDCP Tx AN Value [7_0] */
#define	TX_HDCP_TX_AN_0_AN_7_0_MSK	(0xff)
#define	TX_HDCP_TX_AN_0_AN_7_0_BASE	0

/* TX_HDCP_TX_AN_1 0x1369 HDCP Tx AN Value (Byte 1) Register */
/* HDCP Tx AN Value [15_8] */
#define	TX_HDCP_TX_AN_1_AN_15_8_MSK	(0xff)
#define	TX_HDCP_TX_AN_1_AN_15_8_BASE	0

/* TX_HDCP_TX_AN_2 0x136A HDCP Tx AN Value (Byte 2) Register */
/* HDCP Tx AN Value [23_16] */
#define	TX_HDCP_TX_AN_2_AN_23_16_MSK	(0xff)
#define	TX_HDCP_TX_AN_2_AN_23_16_BASE	0

/* TX_HDCP_TX_AN_3 0x163B HDCP Tx AN Value (Byte 3) Register */
/* HDCP Tx AN Value [31_24] */
#define	TX_HDCP_TX_AN_3_AN_31_24_MSK	(0xff)
#define	TX_HDCP_TX_AN_3_AN_31_24_BASE	0

/* TX_HDCP_TX_AN_4 0x163C HDCP Tx AN Value (Byte 4) Register */
/* HDCP Tx AN Value [39_32] */
#define	TX_HDCP_TX_AN_4_AN_39_32_MSK	(0xff)
#define	TX_HDCP_TX_AN_4_AN_39_32_BASE	0

/* TX_HDCP_TX_AN_5 0x163D HDCP Tx AN Value (Byte 5) Register */
/* HDCP Tx AN Value [47_40] */
#define	TX_HDCP_TX_AN_5_AN_47_40_MSK	(0xff)
#define	TX_HDCP_TX_AN_5_AN_47_40_BASE	0

/* TX_HDCP_TX_AN_6 0x163E HDCP Tx AN Value (Byte 6) Register */
/* HDCP Tx AN Value [55_48] */
#define	TX_HDCP_TX_AN_6_AN_55_48_MSK	(0xff)
#define	TX_HDCP_TX_AN_6_AN_55_48_BASE	0

/* TX_HDCP_TX_AN_7 0x163F HDCP Tx AN Value (Byte 7) Register */
/* HDCP Tx AN Value [63_56] */
#define	TX_HDCP_TX_AN_7_AN_63_56_MSK	(0xff)
#define	TX_HDCP_TX_AN_7_AN_63_56_BASE	0

/* TX_HDCP_TX_M0_0 0x1370 HDCP Tx M0 Value (Byte 0) Register */
/* HDCP Tx M0 Value [7_0] */
#define	TX_HDCP_TX_M0_0_M0_7_0_MSK	(0xff)
#define	TX_HDCP_TX_M0_0_M0_7_0_BASE	0

/* TX_HDCP_TX_M0_1 0x1371 HDCP Tx M0 Value (Byte 1) Register */
/* HDCP Tx M0 Value [15_8] */
#define	TX_HDCP_TX_M0_1_M0_15_8_MSK	(0xff)
#define	TX_HDCP_TX_M0_1_M0_15_8_BASE	0

/* TX_HDCP_TX_M0_2 0x1372 HDCP Tx M0 Value (Byte 2) Register */
/* HDCP Tx M0 Value [23_16] */
#define	TX_HDCP_TX_M0_2_M0_23_16_MSK	(0xff)
#define	TX_HDCP_TX_M0_2_M0_23_16_BASE	0

/* TX_HDCP_TX_M0_3 0x1373 HDCP Tx M0 Value (Byte 3) Register */
/* HDCP Tx M0 Value [31_24] */
#define	TX_HDCP_TX_M0_3_M0_31_24_MSK	(0xff)
#define	TX_HDCP_TX_M0_3_M0_31_24_BASE	0

/* TX_HDCP_TX_M0_4 0x1374 HDCP Tx M0 Value (Byte 4) Register */
/* HDCP Tx M0 Value [39_32] */
#define	TX_HDCP_TX_M0_4_M0_39_32_MSK	(0xff)
#define	TX_HDCP_TX_M0_4_M0_39_32_BASE	0

/* TX_HDCP_TX_M0_5 0x1375 HDCP Tx M0 Value (Byte 5) Register */
/* HDCP Tx M0 Value [47_40] */
#define	TX_HDCP_TX_M0_5_M0_47_40_MSK	(0xff)
#define	TX_HDCP_TX_M0_5_M0_47_40_BASE	0

/* TX_HDCP_TX_M0_6 0x1376 HDCP Tx M0 Value (Byte 6) Register */
/* HDCP Tx M0 Value [55_48] */
#define	TX_HDCP_TX_M0_6_M0_55_48_MSK	(0xff)
#define	TX_HDCP_TX_M0_6_M0_55_48_BASE	0

/* TX_HDCP_TX_M0_7 0x1377 HDCP Tx M0 Value (Byte 7) Register */
/* HDCP Tx M0 Value [63_56] */
#define	TX_HDCP_TX_M0_7_M0_63_56_MSK	(0xff)
#define	TX_HDCP_TX_M0_7_M0_63_56_BASE	0

/* TX_HDCP_TX_R0_0 0x1378 Tx R0 (Byte 0) Register */
#define	TX_HDCP_TX_R0_0_TX_R0_7_0_MSK	(0xff)	/* Tx R0 [7_0] */
#define	TX_HDCP_TX_R0_0_TX_R0_7_0_BASE	0

/* TX_HDCP_TX_R0_1 0x1379 Tx R0 (Byte 1) Register */
#define	TX_HDCP_TX_R0_1_TX_R0_15_8_MSK	(0xff)	/* Tx R0 [15_8] */
#define	TX_HDCP_TX_R0_1_TX_R0_15_8_BASE	0

/* TX_HDCP_RX_R0_0 0x137A Rx R0 (Byte 0) Register */
#define	TX_HDCP_RX_R0_0_RX_R0_7_0_MSK	(0xff)	/* Rx R0 [7_0] */
#define	TX_HDCP_RX_R0_0_RX_R0_7_0_BASE	0

/* TX_HDCP_RX_R0_1 0x137B Rx R0 (Byte 1) Register */
#define	TX_HDCP_RX_R0_1_RX_R0_15_8_MSK	(0xff)	/* Rx R0 [15_8] */
#define	TX_HDCP_RX_R0_1_RX_R0_15_8_BASE	0

/* TX_HDCP_TX_RI_0 0x137C Tx RI (Byte 0) Register */
#define	TX_HDCP_TX_RI_0_TX_RI_7_0_MSK	(0xff)	/* Tx RI [7_0] */
#define	TX_HDCP_TX_RI_0_TX_RI_7_0_BASE	0

/* TX_HDCP_TX_RI_1 0x137D Rx RI (Byte 1) Register */
#define	TX_HDCP_TX_RI_1_TX_RI_15_8_MSK	(0xff)	/* Tx RI [15_8] */
#define	TX_HDCP_TX_RI_1_TX_RI_15_8_BASE	0

/* TX_HDCP_RX_RI_0 0x137E Rx RI (Byte 0) Register */
#define	TX_HDCP_RX_RI_0_RX_RI_7_0_MSK	(0xff)	/* Rx RI [7_0] */
#define	TX_HDCP_RX_RI_0_RX_RI_7_0_BASE	0

/* TX_HDCP_RX_RI_1 0x137F Rx RI (Byte 1) Register */
#define	TX_HDCP_RX_RI_1_RX_RI_15_8_MSK	(0xff)	/* Rx RI [15_8] */
#define	TX_HDCP_RX_RI_1_RX_RI_15_8_BASE	0

/* TX_HDCP_TX_PJ 0x1380	Tx PJ Register */
#define	TX_HDCP_TX_PJ_TX_PJ_MSK	(0xff)	/* Tx PJ */
#define	TX_HDCP_TX_PJ_TX_PJ_BASE	0

/* TX_HDCP_RX_PJ 0x1381	Rx PJ Register */
#define	TX_HDCP_RX_PJ_RX_PJ_MSK	(0xff)	/* Rx PJ */
#define	TX_HDCP_RX_PJ_RX_PJ_BASE	0

/* TX_HDCP_FIX_CLR_0 0x1384 Default Video Value (Byte 0) Register */
/* Default Video Value [7_0] */
#define	TX_HDCP_FIX_CLR_0_FIX_CLR_7_0_MSK	(0xff)
#define	TX_HDCP_FIX_CLR_0_FIX_CLR_7_0_BASE	0

/* TX_HDCP_FIX_CLR_1 0x1385 Default Video Value (Byte 1) Register */
/* Default Video Value [15_8] */
#define	TX_HDCP_FIX_CLR_1_FIX_CLR_15_8_MSK	(0xff)
#define	TX_HDCP_FIX_CLR_1_FIX_CLR_15_8_BASE	0

/* TX_HDCP_FIX_CLR_2 0x1386 Default Video Value (Byte 2) Register */
/* Default Video Value [23_16] */
#define	TX_HDCP_FIX_CLR_2_FIX_CLR_23_16_MSK	(0xff)
#define	TX_HDCP_FIX_CLR_2_FIX_CLR_23_16_BASE	0

/* TX_HDCP_KINIT_0 0x1388 KINIT (Byte 0) Register */
#define	TX_HDCP_KINIT_0_KINIT_7_0_MSK	(0xff)	/* KINIT [7_0] */
#define	TX_HDCP_KINIT_0_KINIT_7_0_BASE	0

/* TX_HDCP_KINIT_1 0x1389 KINIT (Byte 1) Register */
#define	TX_HDCP_KINIT_1_KINIT_15_8_MSK	(0xff)	/* KINIT [15_8] */
#define	TX_HDCP_KINIT_1_KINIT_15_8_BASE	0

/* TX_HDCP_KINIT_2 0x138A KINIT (Byte 2) Register */
#define	TX_HDCP_KINIT_2_KINIT_23_16_MSK	(0xff)	/* KINIT [23_16] */
#define	TX_HDCP_KINIT_2_KINIT_23_16_BASE	0

/* TX_HDCP_KINIT_3 0x138B KINIT (Byte 3) Register */
#define	TX_HDCP_KINIT_3_KINIT_31_24_MSK	(0xff)	/* KINIT [31_24] */
#define	TX_HDCP_KINIT_3_KINIT_31_24_BASE	0

/* TX_HDCP_KINIT_4 0x138C KINIT (Byte 4) Register */
#define	TX_HDCP_KINIT_4_KINIT_39_32_MSK	(0xff)	/* KINIT [39_32] */
#define	TX_HDCP_KINIT_4_KINIT_39_32_BASE	0
/* TX_HDCP_KINIT_5 0x138D KINIT (Byte 5) Register */
#define	TX_HDCP_KINIT_5_KINIT_47_40_MSK	(0xff)	/* KINIT [47_40] */
#define	TX_HDCP_KINIT_5_KINIT_47_40_BASE	0
/* TX_HDCP_KINIT_6 0x138E KINIT (Byte 6) Register */
#define	TX_HDCP_KINIT_6_KINIT_55_48_MSK	(0xff)	/* KINIT [55_48] */
#define	TX_HDCP_KINIT_6_KINIT_55_48_BASE	0

/* TX_HDCP_BINIT_0 0x1390 BINIT (Byte 0) Register */
#define	TX_HDCP_BINIT_0_BINIT_7_0_MSK		(0xff)	/* BINIT [7_0] */
#define	TX_HDCP_BINIT_0_BINIT_7_0_BASE		0

/* TX_HDCP_BINIT_1 0x1391 BINIT (Byte 1) Register */
#define	TX_HDCP_BINIT_1_BINIT_15_8_MSK	(0xff)	/* BINIT[15_8] */
#define	TX_HDCP_BINIT_1_BINIT_15_8_BASE	0

/* TX_HDCP_BINIT_2 0x1392 BINIT (Byte 2) Register */
#define	TX_HDCP_BINIT_2_BINIT_23_16_MSK	(0xff)	/* BINIT [23_16] */
#define	TX_HDCP_BINIT_2_BINIT_23_16_BASE	0

/* TX_HDCP_BINIT_3 0x1393 BINIT (Byte 3) Register */
#define	TX_HDCP_BINIT_3_BINIT_31_24_MSK	(0xff)	/* BINIT[31_24] */
#define	TX_HDCP_BINIT_3_BINIT_31_24_BASE	0

/* TX_HDCP_BINIT_4 0x1394 BINIT (Byte 4) Register */
#define	TX_HDCP_BINIT_4_BINIT_39_32_MSK	(0xff)	/* BINIT [39_32] */
#define	TX_HDCP_BINIT_4_BINIT_39_32_BASE	0

/* TX_HDCP_BINIT_5 0x1395 BINIT (Byte 5) Register */
#define	TX_HDCP_BINIT_5_BINIT_47_40_MSK	(0xff)	/* BINIT [47_40] */
#define	TX_HDCP_BINIT_5_BINIT_47_40_BASE	0

/* TX_HDCP_BINIT_6 0x1396 BINIT (Byte 6) Register */
#define	TX_HDCP_BINIT_6_BINIT_55_48_MSK	(0xff)	/* BINIT [55_48] */
#define	TX_HDCP_BINIT_6_BINIT_55_48_BASE	0

/* TX_HDCP_BINIT_7 0x1397 BINIT (Byte 7) Register */
#define	TX_HDCP_BINIT_7_BINIT_63_56_MSK	(0xff)	/* BINIT [63_56] */
#define	TX_HDCP_BINIT_7_BINIT_63_56_BASE	0

/* TX_HDCP_INTR_CLR 0x1398 Interrupt Clear Register */
#define	TX_HDCP_INTR_CLR_AKSV_READY_CLR		(1 << 7)	/* AKSV Ready Clear */
#define	TX_HDCP_INTR_CLR_TX_AN_READY_CLR	(1 << 6)	/* Tx An Ready Clear */
#define	TX_HDCP_INTR_CLR_TX_R0_READY_CLR	(1 << 5)	/* Tx R0 Ready Clear */
#define	TX_HDCP_INTR_CLR_TX_RI_READY_CLR	(1 << 4)	/* Tx Ri Ready Clear */
#define	TX_HDCP_INTR_CLR_TX_PJ_READY_CLR	(1 << 3)	/* Tx Pj Ready Clear */
/* Bit(s) TX_HDCP_INTR_CLR_RSRV_2_0 reserved */

/* TX_HDCP_INTR_MASK 0x1399 Interrupt Masking Register */
#define	TX_HDCP_INTR_MASK_AKSV_READY_MASK	(1 << 7)	/* AKSV Ready Mask */
#define	TX_HDCP_INTR_MASK_TX_AN_READY_MASK	(1 << 6)	/* Tx An Ready Mask */
#define	TX_HDCP_INTR_MASK_TX_R0_READY_MASK	(1 << 5)	/* Tx R0 Ready Mask */
#define	TX_HDCP_INTR_MASK_TX_RI_READY_MASK	(1 << 4)	/* Tx Ri Ready Mask */
#define	TX_HDCP_INTR_MASK_TX_PJ_READY_MASK	(1 << 3)	/* Tx Pj Ready Mask */

#define HDMI_PLL_FREQ_25MHz	25
#define HDMI_PLL_FREQ_27MHz	27
#define HDMI_PLL_FREQ_54MHz	54
#define HDMI_PLL_FREQ_74MHz	74
#define HDMI_PLL_FREQ_108MHz	108
#define HDMI_PLL_FREQ_148MHz	148
#define	HDMI_MODE	HDTX_HDMI_CTRL_HDMI_MODE
#define BASE_OFFSET 0xc00
#define HDMI_DATA 0x0
#define HDMI_ADDR 0x4

#define MAP_MEM_SIZE "/sys/class/uio/uio1/maps/map0/size"
#define HDMI_UIO "/dev/uio1"
extern unsigned *hdmi_base;
// HDMI-TX registers
//-----------AUDIO---------------
#define HDTX_ACR_N0            0x0000 //FF
#define HDTX_ACR_N1            0x0001 //FF
#define HDTX_ACR_N2            0x0002 //0F
#define HDTX_ACR_CTS0          0x0004 //FF
#define HDTX_ACR_CTS1          0x0005 //FF
#define HDTX_ACR_CTS2          0x0006 //0F
#define HDTX_ACR_CTRL          0x0007 //0F
#define HDTX_ACR_STS0          0x0008
#define HDTX_ACR_STS1          0x0009
#define HDTX_ACR_STS2          0x000A
#define HDTX_AUD_CTRL          0x000B //FF
#define HDTX_I2S_CTRL          0x000C //1F
#define HDTX_I2S_DLEN          0x000D //1F
#define HDTX_I2S_DBG_LFT0      0x0010 //FF
#define HDTX_I2S_DBG_LFT1      0x0011 //FF
#define HDTX_I2S_DBG_LFT2      0x0012 //FF
#define HDTX_I2S_DBG_LFT3      0x0013 //FF
#define HDTX_I2S_DBG_RIT0      0x0014 //FF
#define HDTX_I2S_DBG_RIT1      0x0015 //FF
#define HDTX_I2S_DBG_RIT2      0x0016 //FF
#define HDTX_I2S_DBG_RIT3      0x0017 //FF
#define HDTX_CHSTS_0           0x0018 //FF
#define HDTX_CHSTS_1           0x0019 //FF
#define HDTX_CHSTS_2           0x001A //FF
#define HDTX_CHSTS_3           0x001B //FF
#define HDTX_CHSTS_4           0x001C //FF
#define HDTX_FIFO_CTRL         0x001D //03
#define HDTX_MEMSIZE_L         0x001E //FF
#define HDTX_MEMSIZE_H         0x001F //01
#define HDTX_GCP_CFG0          0x0020 //0F
#define HDTX_GCP_CFG1          0x0021 //FF
#define HDTX_AUD_STS           0x0022
//-----------VIDEO --------------
#define HDTX_HTOT_L            0x0024 //FF
#define HDTX_HTOT_H            0x0025 //FF
#define HDTX_HBLANK_L          0x0026 //FF
#define HDTX_HBLANK_H          0x0027 //FF
#define HDTX_VTOT_L            0x0028 //FF
#define HDTX_VTOT_H            0x0029 //FF
#define HDTX_VRES_L            0x002A //FF
#define HDTX_VRES_H            0x002B //FF
#define HDTX_VSTART_L          0x002C //FF
#define HDTX_VSTART_H          0x002D //FF
#define HDTX_HTOT_STS_L        0x002E
#define HDTX_HTOT_STS_H        0x002F
#define HDTX_HBLANK_STS_L      0x0030
#define HDTX_HBLANK_STS_H      0x0031
#define HDTX_VTOT_STS_L        0x0032
#define HDTX_VTOT_STS_H        0x0033
#define HDTX_VRES_STS_L        0x0034
#define HDTX_VRES_STS_H        0x0035
#define HDTX_VSTART_STS_L      0x0036
#define HDTX_VSTART_STS_H      0x0037
#define HDTX_VIDEO_STS         0x0038
#define HDTX_VIDEO_CTRL        0x0039 //7F
#define HDTX_HDMI_CTRL         0x003A //FF
//-----------------------------
// DDC - NOT USED -CAN BE REMOVED
//------_-----------------------
//#define HDMI_EDDC_CTRL         0x103B //NOT NEEDED
//#define HDMI_EDDC_DEV_ID       0x103C //NOT NEEDED
//#define HDMI_EDDC_DEV_OFF      0x103D //NOT NEEDED
//#define HDMI_EDDC_SEG_ADDR     0x103E //NOT NEEDED
//#define HDMI_EDDC_CMD          0x103F //NOT NEEDED
//#define HDMI_EDDC_RW_CNT_L     0x1040 //NOT NEEDED
//#define HDMI_EDDC_RW_CNT_H     0x1041 //NOT NEEDED
//#define HDMI_EDDC_PERIOD       0x1042 //NOT NEEDED
//#define HDMI_EDDC_RW_DATA      0x1043 //NOT NEEDED
//#define HDMI_EDDC_STATUS       0x1044 //NOT NEEDED
//#define HDMI_EDDC_SEG_PTR      0x1045 //NOT NEEDED

#define HDTX_PP_HW             0x0046

//-----------------------------
// DEEP COLOR
//------_-----------------------
#define HDTX_DC_FIFO_SFT_RST   0x0047 //01
#define HDTX_DC_FIFO_WR_PTR    0x0048 //3F
#define HDTX_DC_FIFO_RD_PTR    0x0049 //3F
#define HDTX_DC_PP_CTRL        0x004A //01

//-------------------------------
// HDMI_TX PHY GLUE LOGIC
//-------------------------------
#define HDTX_TDATA0_0          0x004C //FF
#define HDTX_TDATA0_1          0x004D //FF
#define HDTX_TDATA0_2          0x004E //0F
#define HDTX_TDATA1_0          0x0050 //FF
#define HDTX_TDATA1_1          0x0051 //FF
#define HDTX_TDATA1_2          0x0052 //0F
#define HDTX_TDATA2_0          0x0054 //FF
#define HDTX_TDATA2_1          0x0055 //FF
#define HDTX_TDATA2_2          0x0056 //0F
#define HDTX_TDATA3_0          0x0058 //FF
#define HDTX_TDATA3_1          0x0059 //FF
#define HDTX_TDATA3_2          0x005A //0F
#define HDTX_TDATA_SEL         0x005B //7F
#define HDTX_SWAP_CTRL         0x005C //03

#define HDTX_AVMUTE_CTRL       0x005D //03

#define HDTX_HST_PKT_CTRL0     0x005E //3F
#define HDTX_HST_PKT_CTRL1     0x005F //3F
//-----------------------------
#define HDTX_PKT0_BYTE0        0x0060 //FF
#define HDTX_PKT0_BYTE1        0x0061 //FF
#define HDTX_PKT0_BYTE2        0x0062 //FF
#define HDTX_PKT0_BYTE3        0x0063 //FF
#define HDTX_PKT0_BYTE4        0x0064 //FF
#define HDTX_PKT0_BYTE5        0x0065 //FF
#define HDTX_PKT0_BYTE6        0x0066 //FF
#define HDTX_PKT0_BYTE7        0x0067 //FF
#define HDTX_PKT0_BYTE8        0x0068 //FF
#define HDTX_PKT0_BYTE9        0x0069 //FF
#define HDTX_PKT0_BYTE10       0x006A //FF
#define HDTX_PKT0_BYTE11       0x006B //FF
#define HDTX_PKT0_BYTE12       0x006C //FF
#define HDTX_PKT0_BYTE13       0x006D //FF
#define HDTX_PKT0_BYTE14       0x006E //FF
#define HDTX_PKT0_BYTE15       0x006F //FF
#define HDTX_PKT0_BYTE16       0x0070 //FF
#define HDTX_PKT0_BYTE17       0x0071 //FF
#define HDTX_PKT0_BYTE18       0x0072 //FF
#define HDTX_PKT0_BYTE19       0x0073 //FF
#define HDTX_PKT0_BYTE20       0x0074 //FF
#define HDTX_PKT0_BYTE21       0x0075 //FF
#define HDTX_PKT0_BYTE22       0x0076 //FF
#define HDTX_PKT0_BYTE23       0x0077 //FF
#define HDTX_PKT0_BYTE24       0x0078 //FF
#define HDTX_PKT0_BYTE25       0x0079 //FF
#define HDTX_PKT0_BYTE26       0x007A //FF
#define HDTX_PKT0_BYTE27       0x007B //FF
#define HDTX_PKT0_BYTE28       0x007C //FF
#define HDTX_PKT0_BYTE29       0x007D //FF
#define HDTX_PKT0_BYTE30       0x007E //FF
//-----------------------------
#define HDTX_PKT1_BYTE0        0x0080 //FF
#define HDTX_PKT1_BYTE1        0x0081 //FF
#define HDTX_PKT1_BYTE2        0x0082 //FF
#define HDTX_PKT1_BYTE3        0x0083 //FF
#define HDTX_PKT1_BYTE4        0x0084 //FF
#define HDTX_PKT1_BYTE5        0x0085 //FF
#define HDTX_PKT1_BYTE6        0x0086 //FF
#define HDTX_PKT1_BYTE7        0x0087 //FF
#define HDTX_PKT1_BYTE8        0x0088 //FF
#define HDTX_PKT1_BYTE9        0x0089 //FF
#define HDTX_PKT1_BYTE10       0x008A //FF
#define HDTX_PKT1_BYTE11       0x008B //FF
#define HDTX_PKT1_BYTE12       0x008C //FF
#define HDTX_PKT1_BYTE13       0x008D //FF
#define HDTX_PKT1_BYTE14       0x008E //FF
#define HDTX_PKT1_BYTE15       0x008F //FF
#define HDTX_PKT1_BYTE16       0x0090 //FF
#define HDTX_PKT1_BYTE17       0x0091 //FF
#define HDTX_PKT1_BYTE18       0x0092 //FF
#define HDTX_PKT1_BYTE19       0x0093 //FF
#define HDTX_PKT1_BYTE20       0x0094 //FF
#define HDTX_PKT1_BYTE21       0x0095 //FF
#define HDTX_PKT1_BYTE22       0x0096 //FF
#define HDTX_PKT1_BYTE23       0x0097 //FF
#define HDTX_PKT1_BYTE24       0x0098 //FF
#define HDTX_PKT1_BYTE25       0x0099 //FF
#define HDTX_PKT1_BYTE26       0x009A //FF
#define HDTX_PKT1_BYTE27       0x009B //FF
#define HDTX_PKT1_BYTE28       0x009C //FF
#define HDTX_PKT1_BYTE29       0x009D //FF
#define HDTX_PKT1_BYTE30       0x009E //FF
//-----------------------------
#define HDTX_PKT2_BYTE0        0x00A0 //FF
#define HDTX_PKT2_BYTE1        0x00A1 //FF
#define HDTX_PKT2_BYTE2        0x00A2 //FF
#define HDTX_PKT2_BYTE3        0x00A3 //FF
#define HDTX_PKT2_BYTE4        0x00A4 //FF
#define HDTX_PKT2_BYTE5        0x00A5 //FF
#define HDTX_PKT2_BYTE6        0x00A6 //FF
#define HDTX_PKT2_BYTE7        0x00A7 //FF
#define HDTX_PKT2_BYTE8        0x00A8 //FF
#define HDTX_PKT2_BYTE9        0x00A9 //FF
#define HDTX_PKT2_BYTE10       0x00AA //FF
#define HDTX_PKT2_BYTE11       0x00AB //FF
#define HDTX_PKT2_BYTE12       0x00AC //FF
#define HDTX_PKT2_BYTE13       0x00AD //FF
#define HDTX_PKT2_BYTE14       0x00AE //FF
#define HDTX_PKT2_BYTE15       0x00AF //FF
#define HDTX_PKT2_BYTE16       0x00B0 //FF
#define HDTX_PKT2_BYTE17       0x00B1 //FF
#define HDTX_PKT2_BYTE18       0x00B2 //FF
#define HDTX_PKT2_BYTE19       0x00B3 //FF
#define HDTX_PKT2_BYTE20       0x00B4 //FF
#define HDTX_PKT2_BYTE21       0x00B5 //FF
#define HDTX_PKT2_BYTE22       0x00B6 //FF
#define HDTX_PKT2_BYTE23       0x00B7 //FF
#define HDTX_PKT2_BYTE24       0x00B8 //FF
#define HDTX_PKT2_BYTE25       0x00B9 //FF
#define HDTX_PKT2_BYTE26       0x00BA //FF
#define HDTX_PKT2_BYTE27       0x00BB //FF
#define HDTX_PKT2_BYTE28       0x00BC //FF
#define HDTX_PKT2_BYTE29       0x00BD //FF
#define HDTX_PKT2_BYTE30       0x00BE //FF
//-----------------------------
#define HDTX_PKT3_BYTE0        0x00C0 //FF
#define HDTX_PKT3_BYTE1        0x00C1 //FF
#define HDTX_PKT3_BYTE2        0x00C2 //FF
#define HDTX_PKT3_BYTE3        0x00C3 //FF
#define HDTX_PKT3_BYTE4        0x00C4 //FF
#define HDTX_PKT3_BYTE5        0x00C5 //FF
#define HDTX_PKT3_BYTE6        0x00C6 //FF
#define HDTX_PKT3_BYTE7        0x00C7 //FF
#define HDTX_PKT3_BYTE8        0x00C8 //FF
#define HDTX_PKT3_BYTE9        0x00C9 //FF
#define HDTX_PKT3_BYTE10       0x00CA //FF
#define HDTX_PKT3_BYTE11       0x00CB //FF
#define HDTX_PKT3_BYTE12       0x00CC //FF
#define HDTX_PKT3_BYTE13       0x00CD //FF
#define HDTX_PKT3_BYTE14       0x00CE //FF
#define HDTX_PKT3_BYTE15       0x00CF //FF
#define HDTX_PKT3_BYTE16       0x00D0 //FF
#define HDTX_PKT3_BYTE17       0x00D1 //FF
#define HDTX_PKT3_BYTE18       0x00D2 //FF
#define HDTX_PKT3_BYTE19       0x00D3 //FF
#define HDTX_PKT3_BYTE20       0x00D4 //FF
#define HDTX_PKT3_BYTE21       0x00D5 //FF
#define HDTX_PKT3_BYTE22       0x00D6 //FF
#define HDTX_PKT3_BYTE23       0x00D7 //FF
#define HDTX_PKT3_BYTE24       0x00D8 //FF
#define HDTX_PKT3_BYTE25       0x00D9 //FF
#define HDTX_PKT3_BYTE26       0x00DA //FF
#define HDTX_PKT3_BYTE27       0x00DB //FF
#define HDTX_PKT3_BYTE28       0x00DC //FF
#define HDTX_PKT3_BYTE29       0x00DD //FF
#define HDTX_PKT3_BYTE30       0x00DE //FF

//-----------------------------
#define HDTX_PKT4_BYTE0        0x00E0 //FF
#define HDTX_PKT4_BYTE1        0x00E1 //FF
#define HDTX_PKT4_BYTE2        0x00E2 //FF
#define HDTX_PKT4_BYTE3        0x00E3 //FF
#define HDTX_PKT4_BYTE4        0x00E4 //FF
#define HDTX_PKT4_BYTE5        0x00E5 //FF
#define HDTX_PKT4_BYTE6        0x00E6 //FF
#define HDTX_PKT4_BYTE7        0x00E7 //FF
#define HDTX_PKT4_BYTE8        0x00E8 //FF
#define HDTX_PKT4_BYTE9        0x00E9 //FF
#define HDTX_PKT4_BYTE10       0x00EA //FF
#define HDTX_PKT4_BYTE11       0x00EB //FF
#define HDTX_PKT4_BYTE12       0x00EC //FF
#define HDTX_PKT4_BYTE13       0x00ED //FF
#define HDTX_PKT4_BYTE14       0x00EE //FF
#define HDTX_PKT4_BYTE15       0x00EF //FF
#define HDTX_PKT4_BYTE16       0x00F0 //FF
#define HDTX_PKT4_BYTE17       0x00F1 //FF
#define HDTX_PKT4_BYTE18       0x00F2 //FF
#define HDTX_PKT4_BYTE19       0x00F3 //FF
#define HDTX_PKT4_BYTE20       0x00F4 //FF
#define HDTX_PKT4_BYTE21       0x00F5 //FF
#define HDTX_PKT4_BYTE22       0x00F6 //FF
#define HDTX_PKT4_BYTE23       0x00F7 //FF
#define HDTX_PKT4_BYTE24       0x00F8 //FF
#define HDTX_PKT4_BYTE25       0x00F9 //FF
#define HDTX_PKT4_BYTE26       0x00FA //FF
#define HDTX_PKT4_BYTE27       0x00FB //FF
#define HDTX_PKT4_BYTE28       0x00FC //FF
#define HDTX_PKT4_BYTE29       0x00FD //FF
#define HDTX_PKT4_BYTE30       0x00FE //FF
//-----------------------------

#define HDTX_PKT5_BYTE0        0x0100 //FF
#define HDTX_PKT5_BYTE1        0x0101 //FF
#define HDTX_PKT5_BYTE2        0x0102 //FF
#define HDTX_PKT5_BYTE3        0x0103 //FF
#define HDTX_PKT5_BYTE4        0x0104 //FF
#define HDTX_PKT5_BYTE5        0x0105 //FF
#define HDTX_PKT5_BYTE6        0x0106 //FF
#define HDTX_PKT5_BYTE7        0x0107 //FF
#define HDTX_PKT5_BYTE8        0x0108 //FF
#define HDTX_PKT5_BYTE9        0x0109 //FF
#define HDTX_PKT5_BYTE10       0x010A //FF
#define HDTX_PKT5_BYTE11       0x010B //FF
#define HDTX_PKT5_BYTE12       0x010C //FF
#define HDTX_PKT5_BYTE13       0x010D //FF
#define HDTX_PKT5_BYTE14       0x010E //FF
#define HDTX_PKT5_BYTE15       0x010F //FF
#define HDTX_PKT5_BYTE16       0x0110 //FF
#define HDTX_PKT5_BYTE17       0x0111 //FF
#define HDTX_PKT5_BYTE18       0x0112 //FF
#define HDTX_PKT5_BYTE19       0x0113 //FF
#define HDTX_PKT5_BYTE20       0x0114 //FF
#define HDTX_PKT5_BYTE21       0x0115 //FF
#define HDTX_PKT5_BYTE22       0x0116 //FF
#define HDTX_PKT5_BYTE23       0x0117 //FF
#define HDTX_PKT5_BYTE24       0x0118 //FF
#define HDTX_PKT5_BYTE25       0x0119 //FF
#define HDTX_PKT5_BYTE26       0x011A //FF
#define HDTX_PKT5_BYTE27       0x011B //FF
#define HDTX_PKT5_BYTE28       0x011C //FF
#define HDTX_PKT5_BYTE29       0x011D //FF
#define HDTX_PKT5_BYTE30       0x011E //FF
#define HDTX_RSVD              0x011F

//-----------------------------
// NEW HDMI-TX REGISTERS
//-----------------------------
#define HDTX_UBITS_0           0x0120 //FF
#define HDTX_UBITS_1           0x0121 //FF
#define HDTX_UBITS_2           0x0122 //FF
#define HDTX_UBITS_3           0x0123 //FF
#define HDTX_UBITS_4           0x0124 //FF
#define HDTX_UBITS_5           0x0125 //FF
#define HDTX_UBITS_6           0x0126 //FF
#define HDTX_UBITS_7           0x0127 //FF
#define HDTX_UBITS_8           0x0128 //FF
#define HDTX_UBITS_9           0x0129 //FF
#define HDTX_UBITS_10          0x012A //FF
#define HDTX_UBITS_11          0x012B //FF
#define HDTX_UBITS_12          0x012C //FF
#define HDTX_UBITS_13          0x012D //0F
#define HDTX_HBR_PKT           0x012E //01

#define HDTX_PHY_FIFO_SOFT_RST 0x0130 //01
#define HDTX_PHY_FIFO_PTRS     0x0131 //FF
#define HDTX_PRBS_CTRL0        0x0132 //07
#define HDTX_HST_PKT_CTRL2     0x0133 //01
#define HDTX_HST_PKT_START     0x0134 //01
#define TX_HDCP_FRM_CNT        0x0135
#define TX_HDCP_HPD_STS        0x0136
#define HDTX_AUD_CH1_SEL       0x0137 //07
#define HDTX_AUD_CH2_SEL       0x0138 //07
#define HDTX_AUD_CH3_SEL       0x0139 //07
#define HDTX_AUD_CH4_SEL       0x013A //07
#define HDTX_AUD_CH5_SEL       0x013B //07
#define HDTX_AUD_CH6_SEL       0x013C //07
#define HDTX_AUD_CH7_SEL       0x013D //07
#define HDTX_AUD_CH8_SEL       0x013E //07
#define TX_HDCP_HW_STATUS      0x013F
#define TX_HDCP_HPD_SEL        0x0140 //01
#define TX_HDCP_SW_HPD         0x0141 //01

// --  CEC

#define CEC_BASE 0x200

#define CEC_TOGGLE_FOR_WRITE_REG_ADDR        CEC_BASE + 0x0000
#define CEC_TOGGLE_FOR_READ_REG_ADDR         CEC_BASE + 0x0004
#define CEC_RDY_ADDR                         CEC_BASE + 0x0008 //01
#define CEC_RX_RDY_ADDR                      CEC_BASE + 0x000c //01
#define CEC_TX_FIFO_RESET_ADDR               CEC_BASE + 0x0010 //03
#define CEC_RX_FIFO_RESET_ADDR               CEC_BASE + 0x0014 //03
#define CEC_PMODE_ADDR                       CEC_BASE + 0x0018 //01
//#define CEC_TX_RDY_ADDR                      CEC_BASE + 0x001c //01
#define CEC_TX_TYPE_ADDR                     CEC_BASE + 0x0020 //01
//#define CEC_TX_RESP_TIME_0_ADDR              CEC_BASE + 0x0024 //FF
//#define CEC_TX_RESP_TIME_1_ADDR              CEC_BASE + 0x0025 //FF
//#define CEC_TX_RESP_TIME_2_ADDR              CEC_BASE + 0x0026 //FF
//#define CEC_TX_RESP_TIME_3_ADDR              CEC_BASE + 0x0027 //FF
#define CEC_SIGNAL_FREE_TIME_0_ADDR          CEC_BASE + 0x0028 //FF
#define CEC_SIGNAL_FREE_TIME_1_ADDR          CEC_BASE + 0x0029 //FF
#define CEC_SIGNAL_FREE_TIME_2_ADDR          CEC_BASE + 0x002a //FF
#define CEC_SIGNAL_FREE_TIME_3_ADDR          CEC_BASE + 0x002b //FF
#define CEC_START_BIT_LO_THRESH_0_ADDR       CEC_BASE + 0x002c //FF
#define CEC_START_BIT_LO_THRESH_1_ADDR       CEC_BASE + 0x002d //FF
#define CEC_START_BIT_LO_THRESH_2_ADDR       CEC_BASE + 0x002e //FF
#define CEC_START_BIT_LO_THRESH_3_ADDR       CEC_BASE + 0x002f //FF
#define CEC_START_BIT_HI_THRESH_0_ADDR       CEC_BASE + 0x0030 //FF
#define CEC_START_BIT_HI_THRESH_1_ADDR       CEC_BASE + 0x0031 //FF
#define CEC_START_BIT_HI_THRESH_2_ADDR       CEC_BASE + 0x0032 //FF
#define CEC_START_BIT_HI_THRESH_3_ADDR       CEC_BASE + 0x0033 //FF
#define CEC_DATA_BIT_0_LO_THRESH_0_ADDR      CEC_BASE + 0x0034 //FF
#define CEC_DATA_BIT_0_LO_THRESH_1_ADDR      CEC_BASE + 0x0035 //FF
#define CEC_DATA_BIT_0_LO_THRESH_2_ADDR      CEC_BASE + 0x0036 //FF
#define CEC_DATA_BIT_0_LO_THRESH_3_ADDR      CEC_BASE + 0x0037 //FF
#define CEC_DATA_BIT_1_LO_THRESH_0_ADDR      CEC_BASE + 0x0038 //FF
#define CEC_DATA_BIT_1_LO_THRESH_1_ADDR      CEC_BASE + 0x0039 //FF
#define CEC_DATA_BIT_1_LO_THRESH_2_ADDR      CEC_BASE + 0x003a //FF
#define CEC_DATA_BIT_1_LO_THRESH_3_ADDR      CEC_BASE + 0x003b //FF
#define CEC_DATA_BIT_0_HI_THRESH_0_ADDR      CEC_BASE + 0x003c //FF
#define CEC_DATA_BIT_0_HI_THRESH_1_ADDR      CEC_BASE + 0x003d //FF
#define CEC_DATA_BIT_0_HI_THRESH_2_ADDR      CEC_BASE + 0x003e //FF
#define CEC_DATA_BIT_0_HI_THRESH_3_ADDR      CEC_BASE + 0x003f //FF
#define CEC_DATA_BIT_1_HI_THRESH_0_ADDR      CEC_BASE + 0x0040 //FF
#define CEC_DATA_BIT_1_HI_THRESH_1_ADDR      CEC_BASE + 0x0041 //FF
#define CEC_DATA_BIT_1_HI_THRESH_2_ADDR      CEC_BASE + 0x0042 //FF
#define CEC_DATA_BIT_1_HI_THRESH_3_ADDR      CEC_BASE + 0x0043 //FF
#define CEC_SSP_ACK_TIME_0_ADDR              CEC_BASE + 0x0044 //FF
#define CEC_SSP_ACK_TIME_1_ADDR              CEC_BASE + 0x0045 //FF
#define CEC_SSP_ACK_TIME_2_ADDR              CEC_BASE + 0x0046 //FF
#define CEC_SSP_ACK_TIME_3_ADDR              CEC_BASE + 0x0047 //FF
#define CEC_INTR_ENABLE_REG_ADDR             CEC_BASE + 0x0048 //FF
#define CEC_INTR_ENABLE1_REG_ADDR            CEC_BASE + 0x0049 //FF
#define CEC_DATA_REG_ADDR                    CEC_BASE + 0x004c //FF
#define CEC_EOM_REG_ADDR                     CEC_BASE + 0x0050 //01
#define CEC_FIFO_STATUS_REG_ADDR             CEC_BASE + 0x0054
#define CEC_INTR_STATUS_REG_ADDR             CEC_BASE + 0x0058
#define CEC_NOMINAL_SAMPLE_TIME_0_ADDR       CEC_BASE + 0x005c //FF
#define CEC_NOMINAL_SAMPLE_TIME_1_ADDR       CEC_BASE + 0x005d //FF
#define CEC_NOMINAL_SAMPLE_TIME_2_ADDR       CEC_BASE + 0x005e //FF
#define CEC_NOMINAL_SAMPLE_TIME_3_ADDR       CEC_BASE + 0x005f //FF
#define CEC_HYST_TIME_0_ADDR                 CEC_BASE + 0x0060 //FF
#define CEC_HYST_TIME_1_ADDR                 CEC_BASE + 0x0061 //FF
#define CEC_HYST_TIME_2_ADDR                 CEC_BASE + 0x0062 //FF
#define CEC_HYST_TIME_3_ADDR                 CEC_BASE + 0x0063 //FF
#define CEC_FOLLOWER_ACK_TIME_0_ADDR         CEC_BASE + 0x0064 //FF
#define CEC_FOLLOWER_ACK_TIME_1_ADDR         CEC_BASE + 0x0065 //FF
#define CEC_FOLLOWER_ACK_TIME_2_ADDR         CEC_BASE + 0x0066 //FF
#define CEC_FOLLOWER_ACK_TIME_3_ADDR         CEC_BASE + 0x0067 //FF
#define CEC_RX_BUF_READ_REG_ADDR             CEC_BASE + 0x0068
#define CEC_RX_EOM_READ_REG_ADDR             CEC_BASE + 0x0069
#define CEC_LOGICAL_ADDR0_REG_ADDR           CEC_BASE + 0x006a //1F
#define CEC_LOGICAL_ADDR1_REG_ADDR           CEC_BASE + 0x006b //1F
#define CEC_LOGICAL_ADDR2_REG_ADDR           CEC_BASE + 0x006c //1F
#define CEC_LOGICAL_ADDR3_REG_ADDR           CEC_BASE + 0x006d //1F
#define CEC_LOGICAL_ADDR4_REG_ADDR           CEC_BASE + 0x006e //1F
#define CEC_JITTER_CNT_0_ADDR                CEC_BASE + 0x0070 //FF
#define CEC_JITTER_CNT_1_ADDR                CEC_BASE + 0x0071 //FF
#define CEC_JITTER_CNT_2_ADDR                CEC_BASE + 0x0072 //FF
#define CEC_JITTER_CNT_3_ADDR                CEC_BASE + 0x0073 //FF
#define CEC_FAIL_INTR_STATUS_ADDR            CEC_BASE + 0x0074
#define CEC_TX_PRESENT_STATE_REG_ADDR        CEC_BASE + 0x0078
#define CEC_RX_PRESENT_STATE_REG_ADDR        CEC_BASE + 0x0079
#define CEC_COLL_CTRL_REG_ADDR               CEC_BASE + 0x007a //03
//#define CEC_GLITCH_FILTER_STAGES_ADDR        CEC_BASE + 0x007b //07
#define CEC_COLL_WINDOW_TIME_REG_0_ADDR      CEC_BASE + 0x007c //FF
#define CEC_COLL_WINDOW_TIME_REG_1_ADDR      CEC_BASE + 0x007d //FF
#define CEC_COLL_WINDOW_TIME_REG_2_ADDR      CEC_BASE + 0x007e //FF
#define CEC_COLL_WINDOW_TIME_REG_3_ADDR      CEC_BASE + 0x007f //FF
#define CEC_TX_FIFO_FULL_THRESH              CEC_BASE + 0x0080 //0F
#define CEC_TX_FIFO_WPTR                     CEC_BASE + 0x0081
#define CEC_TX_FIFO_RPTR                     CEC_BASE + 0x0082
#define CEC_TX_FIFO_DPTR                     CEC_BASE + 0x0083
#define CEC_RX_FIFO_FULL_THRESH              CEC_BASE + 0x0084 //0F
#define CEC_RX_FIFO_WPTR                     CEC_BASE + 0x0085
#define CEC_RX_FIFO_RPTR                     CEC_BASE + 0x0086
#define CEC_RX_FIFO_DPTR                     CEC_BASE + 0x0087
#define CEC_JITTER_CNT_SB_0                  CEC_BASE + 0x0088 //FF
#define CEC_JITTER_CNT_SB_1                  CEC_BASE + 0x0089 //FF
#define CEC_JITTER_CNT_SB_2                  CEC_BASE + 0x008a //FF
#define CEC_JITTER_CNT_SB_3                  CEC_BASE + 0x008b //FF
#define CEC_ERR_NOTIF_TIME_0                 CEC_BASE + 0x008c //FF
#define CEC_ERR_NOTIF_TIME_1                 CEC_BASE + 0x008d //FF
#define CEC_ERR_NOTIF_TIME_2                 CEC_BASE + 0x008e //FF
#define CEC_ERR_NOTIF_TIME_3                 CEC_BASE + 0x008f //FF
#define CEC_GLITCH_FILT_W_L                  CEC_BASE + 0x0090 //FF
#define CEC_GLITCH_FILT_W_H                  CEC_BASE + 0x0091 //FF

//--------------------------------
//HDCP
//--------------------------------

//ADDRESS OF AKEYS
#define TX_HDCP_AKEY0_BYTE_0     0x1200 //00
#define TX_HDCP_AKEY0_BYTE_1     0x1201 //00
#define TX_HDCP_AKEY0_BYTE_2     0x1202 //00
#define TX_HDCP_AKEY0_BYTE_3     0x1203 //00
#define TX_HDCP_AKEY0_BYTE_4     0x1204 //00
#define TX_HDCP_AKEY0_BYTE_5     0x1205 //00
#define TX_HDCP_AKEY0_BYTE_6     0x1206 //00

#define TX_HDCP_AKEY1_BYTE_0     0x1208 //00
#define TX_HDCP_AKEY1_BYTE_1     0x1209 //00
#define TX_HDCP_AKEY1_BYTE_2     0x120A //00
#define TX_HDCP_AKEY1_BYTE_3     0x120B //00
#define TX_HDCP_AKEY1_BYTE_4     0x120C //00
#define TX_HDCP_AKEY1_BYTE_5     0x120D //00
#define TX_HDCP_AKEY1_BYTE_6     0x120E //00

#define TX_HDCP_AKEY2_BYTE_0     0x1210 //00
#define TX_HDCP_AKEY2_BYTE_1     0x1211 //00
#define TX_HDCP_AKEY2_BYTE_2     0x1212 //00
#define TX_HDCP_AKEY2_BYTE_3     0x1213 //00
#define TX_HDCP_AKEY2_BYTE_4     0x1214 //00
#define TX_HDCP_AKEY2_BYTE_5     0x1215 //00
#define TX_HDCP_AKEY2_BYTE_6     0x1216 //00

#define TX_HDCP_AKEY3_BYTE_0     0x1218 //00
#define TX_HDCP_AKEY3_BYTE_1     0x1219 //00
#define TX_HDCP_AKEY3_BYTE_2     0x121A //00
#define TX_HDCP_AKEY3_BYTE_3     0x121B //00
#define TX_HDCP_AKEY3_BYTE_4     0x121C //00
#define TX_HDCP_AKEY3_BYTE_5     0x121D //00
#define TX_HDCP_AKEY3_BYTE_6     0x121E //00

#define TX_HDCP_AKEY4_BYTE_0     0x1220 //00
#define TX_HDCP_AKEY4_BYTE_1     0x1221 //00
#define TX_HDCP_AKEY4_BYTE_2     0x1222 //00
#define TX_HDCP_AKEY4_BYTE_3     0x1223 //00
#define TX_HDCP_AKEY4_BYTE_4     0x1224 //00
#define TX_HDCP_AKEY4_BYTE_5     0x1225 //00
#define TX_HDCP_AKEY4_BYTE_6     0x1226 //00

#define TX_HDCP_AKEY5_BYTE_0     0x1228 //00
#define TX_HDCP_AKEY5_BYTE_1     0x1229 //00
#define TX_HDCP_AKEY5_BYTE_2     0x122A //00
#define TX_HDCP_AKEY5_BYTE_3     0x122B //00
#define TX_HDCP_AKEY5_BYTE_4     0x122C //00
#define TX_HDCP_AKEY5_BYTE_5     0x122D //00
#define TX_HDCP_AKEY5_BYTE_6     0x122E //00

#define TX_HDCP_AKEY6_BYTE_0     0x1230 //00
#define TX_HDCP_AKEY6_BYTE_1     0x1231 //00
#define TX_HDCP_AKEY6_BYTE_2     0x1232 //00
#define TX_HDCP_AKEY6_BYTE_3     0x1233 //00
#define TX_HDCP_AKEY6_BYTE_4     0x1234 //00
#define TX_HDCP_AKEY6_BYTE_5     0x1235 //00
#define TX_HDCP_AKEY6_BYTE_6     0x1236 //00

#define TX_HDCP_AKEY7_BYTE_0     0x1238 //00
#define TX_HDCP_AKEY7_BYTE_1     0x1239 //00
#define TX_HDCP_AKEY7_BYTE_2     0x123A //00
#define TX_HDCP_AKEY7_BYTE_3     0x123B //00
#define TX_HDCP_AKEY7_BYTE_4     0x123C //00
#define TX_HDCP_AKEY7_BYTE_5     0x123D //00
#define TX_HDCP_AKEY7_BYTE_6     0x123E //00

#define TX_HDCP_AKEY8_BYTE_0     0x1240 //00
#define TX_HDCP_AKEY8_BYTE_1     0x1241 //00
#define TX_HDCP_AKEY8_BYTE_2     0x1242 //00
#define TX_HDCP_AKEY8_BYTE_3     0x1243 //00
#define TX_HDCP_AKEY8_BYTE_4     0x1244 //00
#define TX_HDCP_AKEY8_BYTE_5     0x1245 //00
#define TX_HDCP_AKEY8_BYTE_6     0x1246 //00

#define TX_HDCP_AKEY9_BYTE_0     0x1248 //00
#define TX_HDCP_AKEY9_BYTE_1     0x1249 //00
#define TX_HDCP_AKEY9_BYTE_2     0x124A //00
#define TX_HDCP_AKEY9_BYTE_3     0x124B //00
#define TX_HDCP_AKEY9_BYTE_4     0x124C //00
#define TX_HDCP_AKEY9_BYTE_5     0x124D //00
#define TX_HDCP_AKEY9_BYTE_6     0x124E //00

#define TX_HDCP_AKEY10_BYTE_0    0x1250 //00
#define TX_HDCP_AKEY10_BYTE_1    0x1251 //00
#define TX_HDCP_AKEY10_BYTE_2    0x1252 //00
#define TX_HDCP_AKEY10_BYTE_3    0x1253 //00
#define TX_HDCP_AKEY10_BYTE_4    0x1254 //00
#define TX_HDCP_AKEY10_BYTE_5    0x1255 //00
#define TX_HDCP_AKEY10_BYTE_6    0x1256 //00

#define TX_HDCP_AKEY11_BYTE_0    0x1258 //00
#define TX_HDCP_AKEY11_BYTE_1    0x1259 //00
#define TX_HDCP_AKEY11_BYTE_2    0x125A //00
#define TX_HDCP_AKEY11_BYTE_3    0x125B //00
#define TX_HDCP_AKEY11_BYTE_4    0x125C //00
#define TX_HDCP_AKEY11_BYTE_5    0x125D //00
#define TX_HDCP_AKEY11_BYTE_6    0x125E //00

#define TX_HDCP_AKEY12_BYTE_0    0x1260 //00
#define TX_HDCP_AKEY12_BYTE_1    0x1261 //00
#define TX_HDCP_AKEY12_BYTE_2    0x1262 //00
#define TX_HDCP_AKEY12_BYTE_3    0x1263 //00
#define TX_HDCP_AKEY12_BYTE_4    0x1264 //00
#define TX_HDCP_AKEY12_BYTE_5    0x1265 //00
#define TX_HDCP_AKEY12_BYTE_6    0x1266 //00

#define TX_HDCP_AKEY13_BYTE_0    0x1268 //00
#define TX_HDCP_AKEY13_BYTE_1    0x1269 //00
#define TX_HDCP_AKEY13_BYTE_2    0x126A //00
#define TX_HDCP_AKEY13_BYTE_3    0x126B //00
#define TX_HDCP_AKEY13_BYTE_4    0x126C //00
#define TX_HDCP_AKEY13_BYTE_5    0x126D //00
#define TX_HDCP_AKEY13_BYTE_6    0x126E //00

#define TX_HDCP_AKEY14_BYTE_0    0x1270 //00
#define TX_HDCP_AKEY14_BYTE_1    0x1271 //00
#define TX_HDCP_AKEY14_BYTE_2    0x1272 //00
#define TX_HDCP_AKEY14_BYTE_3    0x1273 //00
#define TX_HDCP_AKEY14_BYTE_4    0x1274 //00
#define TX_HDCP_AKEY14_BYTE_5    0x1275 //00
#define TX_HDCP_AKEY14_BYTE_6    0x1276 //00

#define TX_HDCP_AKEY15_BYTE_0    0x1278 //00
#define TX_HDCP_AKEY15_BYTE_1    0x1279 //00
#define TX_HDCP_AKEY15_BYTE_2    0x127A //00
#define TX_HDCP_AKEY15_BYTE_3    0x127B //00
#define TX_HDCP_AKEY15_BYTE_4    0x127C //00
#define TX_HDCP_AKEY15_BYTE_5    0x127D //00
#define TX_HDCP_AKEY15_BYTE_6    0x127E //00

#define TX_HDCP_AKEY16_BYTE_0    0x1280 //00
#define TX_HDCP_AKEY16_BYTE_1    0x1281 //00
#define TX_HDCP_AKEY16_BYTE_2    0x1282 //00
#define TX_HDCP_AKEY16_BYTE_3    0x1283 //00
#define TX_HDCP_AKEY16_BYTE_4    0x1284 //00
#define TX_HDCP_AKEY16_BYTE_5    0x1285 //00
#define TX_HDCP_AKEY16_BYTE_6    0x1286 //00

#define TX_HDCP_AKEY17_BYTE_0    0x1288 //00
#define TX_HDCP_AKEY17_BYTE_1    0x1289 //00
#define TX_HDCP_AKEY17_BYTE_2    0x128A //00
#define TX_HDCP_AKEY17_BYTE_3    0x128B //00
#define TX_HDCP_AKEY17_BYTE_4    0x128C //00
#define TX_HDCP_AKEY17_BYTE_5    0x128D //00
#define TX_HDCP_AKEY17_BYTE_6    0x128E //00

#define TX_HDCP_AKEY18_BYTE_0    0x1290 //00
#define TX_HDCP_AKEY18_BYTE_1    0x1291 //00
#define TX_HDCP_AKEY18_BYTE_2    0x1292 //00
#define TX_HDCP_AKEY18_BYTE_3    0x1293 //00
#define TX_HDCP_AKEY18_BYTE_4    0x1294 //00
#define TX_HDCP_AKEY18_BYTE_5    0x1295 //00
#define TX_HDCP_AKEY18_BYTE_6    0x1296 //00

#define TX_HDCP_AKEY19_BYTE_0    0x1298 //00
#define TX_HDCP_AKEY19_BYTE_1    0x1299 //00
#define TX_HDCP_AKEY19_BYTE_2    0x129A //00
#define TX_HDCP_AKEY19_BYTE_3    0x129B //00
#define TX_HDCP_AKEY19_BYTE_4    0x129C //00
#define TX_HDCP_AKEY19_BYTE_5    0x129D //00
#define TX_HDCP_AKEY19_BYTE_6    0x129E //00

#define TX_HDCP_AKEY20_BYTE_0    0x12A0 //00
#define TX_HDCP_AKEY20_BYTE_1    0x12A1 //00
#define TX_HDCP_AKEY20_BYTE_2    0x12A2 //00
#define TX_HDCP_AKEY20_BYTE_3    0x12A3 //00
#define TX_HDCP_AKEY20_BYTE_4    0x12A4 //00
#define TX_HDCP_AKEY20_BYTE_5    0x12A5 //00
#define TX_HDCP_AKEY20_BYTE_6    0x12A6 //00

#define TX_HDCP_AKEY21_BYTE_0    0x12A8 //00
#define TX_HDCP_AKEY21_BYTE_1    0x12A9 //00
#define TX_HDCP_AKEY21_BYTE_2    0x12AA //00
#define TX_HDCP_AKEY21_BYTE_3    0x12AB //00
#define TX_HDCP_AKEY21_BYTE_4    0x12AC //00
#define TX_HDCP_AKEY21_BYTE_5    0x12AD //00
#define TX_HDCP_AKEY21_BYTE_6    0x12AE //00

#define TX_HDCP_AKEY22_BYTE_0    0x12B0 //00
#define TX_HDCP_AKEY22_BYTE_1    0x12B1 //00
#define TX_HDCP_AKEY22_BYTE_2    0x12B2 //00
#define TX_HDCP_AKEY22_BYTE_3    0x12B3 //00
#define TX_HDCP_AKEY22_BYTE_4    0x12B4 //00
#define TX_HDCP_AKEY22_BYTE_5    0x12B5 //00
#define TX_HDCP_AKEY22_BYTE_6    0x12B6 //00

#define TX_HDCP_AKEY23_BYTE_0    0x12B8 //00
#define TX_HDCP_AKEY23_BYTE_1    0x12B9 //00
#define TX_HDCP_AKEY23_BYTE_2    0x12BA //00
#define TX_HDCP_AKEY23_BYTE_3    0x12BB //00
#define TX_HDCP_AKEY23_BYTE_4    0x12BC //00
#define TX_HDCP_AKEY23_BYTE_5    0x12BD //00
#define TX_HDCP_AKEY23_BYTE_6    0x12BE //00

#define TX_HDCP_AKEY24_BYTE_0    0x12C0 //00
#define TX_HDCP_AKEY24_BYTE_1    0x12C1 //00
#define TX_HDCP_AKEY24_BYTE_2    0x12C2 //00
#define TX_HDCP_AKEY24_BYTE_3    0x12C3 //00
#define TX_HDCP_AKEY24_BYTE_4    0x12C4 //00
#define TX_HDCP_AKEY24_BYTE_5    0x12C5 //00
#define TX_HDCP_AKEY24_BYTE_6    0x12C6 //00

#define TX_HDCP_AKEY25_BYTE_0    0x12C8 //00
#define TX_HDCP_AKEY25_BYTE_1    0x12C9 //00
#define TX_HDCP_AKEY25_BYTE_2    0x12CA //00
#define TX_HDCP_AKEY25_BYTE_3    0x12CB //00
#define TX_HDCP_AKEY25_BYTE_4    0x12CC //00
#define TX_HDCP_AKEY25_BYTE_5    0x12CD //00
#define TX_HDCP_AKEY25_BYTE_6    0x12CE //00

#define TX_HDCP_AKEY26_BYTE_0    0x12D0 //00
#define TX_HDCP_AKEY26_BYTE_1    0x12D1 //00
#define TX_HDCP_AKEY26_BYTE_2    0x12D2 //00
#define TX_HDCP_AKEY26_BYTE_3    0x12D3 //00
#define TX_HDCP_AKEY26_BYTE_4    0x12D4 //00
#define TX_HDCP_AKEY26_BYTE_5    0x12D5 //00
#define TX_HDCP_AKEY26_BYTE_6    0x12D6 //00

#define TX_HDCP_AKEY27_BYTE_0    0x12D8 //00
#define TX_HDCP_AKEY27_BYTE_1    0x12D9 //00
#define TX_HDCP_AKEY27_BYTE_2    0x12DA //00
#define TX_HDCP_AKEY27_BYTE_3    0x12DB //00
#define TX_HDCP_AKEY27_BYTE_4    0x12DC //00
#define TX_HDCP_AKEY27_BYTE_5    0x12DD //00
#define TX_HDCP_AKEY27_BYTE_6    0x12DE //00

#define TX_HDCP_AKEY28_BYTE_0    0x12E0 //00
#define TX_HDCP_AKEY28_BYTE_1    0x12E1 //00
#define TX_HDCP_AKEY28_BYTE_2    0x12E2 //00
#define TX_HDCP_AKEY28_BYTE_3    0x12E3 //00
#define TX_HDCP_AKEY28_BYTE_4    0x12E4 //00
#define TX_HDCP_AKEY28_BYTE_5    0x12E5 //00
#define TX_HDCP_AKEY28_BYTE_6    0x12E6 //00

#define TX_HDCP_AKEY29_BYTE_0    0x12E8 //00
#define TX_HDCP_AKEY29_BYTE_1    0x12E9 //00
#define TX_HDCP_AKEY29_BYTE_2    0x12EA //00
#define TX_HDCP_AKEY29_BYTE_3    0x12EB //00
#define TX_HDCP_AKEY29_BYTE_4    0x12EC //00
#define TX_HDCP_AKEY29_BYTE_5    0x12ED //00
#define TX_HDCP_AKEY29_BYTE_6    0x12EE //00

#define TX_HDCP_AKEY30_BYTE_0    0x12F0 //00
#define TX_HDCP_AKEY30_BYTE_1    0x12F1 //00
#define TX_HDCP_AKEY30_BYTE_2    0x12F2 //00
#define TX_HDCP_AKEY30_BYTE_3    0x12F3 //00
#define TX_HDCP_AKEY30_BYTE_4    0x12F4 //00
#define TX_HDCP_AKEY30_BYTE_5    0x12F5 //00
#define TX_HDCP_AKEY30_BYTE_6    0x12F6 //00

#define TX_HDCP_AKEY31_BYTE_0    0x12F8 //00
#define TX_HDCP_AKEY31_BYTE_1    0x12F9 //00
#define TX_HDCP_AKEY31_BYTE_2    0x12FA //00
#define TX_HDCP_AKEY31_BYTE_3    0x12FB //00
#define TX_HDCP_AKEY31_BYTE_4    0x12FC //00
#define TX_HDCP_AKEY31_BYTE_5    0x12FD //00
#define TX_HDCP_AKEY31_BYTE_6    0x12FE //00

#define TX_HDCP_AKEY32_BYTE_0    0x1300 //00
#define TX_HDCP_AKEY32_BYTE_1    0x1301 //00
#define TX_HDCP_AKEY32_BYTE_2    0x1302 //00
#define TX_HDCP_AKEY32_BYTE_3    0x1303 //00
#define TX_HDCP_AKEY32_BYTE_4    0x1304 //00
#define TX_HDCP_AKEY32_BYTE_5    0x1305 //00
#define TX_HDCP_AKEY32_BYTE_6    0x1306 //00

#define TX_HDCP_AKEY33_BYTE_0    0x1308 //00
#define TX_HDCP_AKEY33_BYTE_1    0x1309 //00
#define TX_HDCP_AKEY33_BYTE_2    0x130A //00
#define TX_HDCP_AKEY33_BYTE_3    0x130B //00
#define TX_HDCP_AKEY33_BYTE_4    0x130C //00
#define TX_HDCP_AKEY33_BYTE_5    0x130D //00
#define TX_HDCP_AKEY33_BYTE_6    0x130E //00

#define TX_HDCP_AKEY34_BYTE_0    0x1310 //00
#define TX_HDCP_AKEY34_BYTE_1    0x1311 //00
#define TX_HDCP_AKEY34_BYTE_2    0x1312 //00
#define TX_HDCP_AKEY34_BYTE_3    0x1313 //00
#define TX_HDCP_AKEY34_BYTE_4    0x1314 //00
#define TX_HDCP_AKEY34_BYTE_5    0x1315 //00
#define TX_HDCP_AKEY34_BYTE_6    0x1316 //00

#define TX_HDCP_AKEY35_BYTE_0    0x1318 //00
#define TX_HDCP_AKEY35_BYTE_1    0x1319 //00
#define TX_HDCP_AKEY35_BYTE_2    0x131A //00
#define TX_HDCP_AKEY35_BYTE_3    0x131B //00
#define TX_HDCP_AKEY35_BYTE_4    0x131C //00
#define TX_HDCP_AKEY35_BYTE_5    0x131D //00
#define TX_HDCP_AKEY35_BYTE_6    0x131E //00

#define TX_HDCP_AKEY36_BYTE_0    0x1320 //00
#define TX_HDCP_AKEY36_BYTE_1    0x1321 //00
#define TX_HDCP_AKEY36_BYTE_2    0x1322 //00
#define TX_HDCP_AKEY36_BYTE_3    0x1323 //00
#define TX_HDCP_AKEY36_BYTE_4    0x1324 //00
#define TX_HDCP_AKEY36_BYTE_5    0x1325 //00
#define TX_HDCP_AKEY36_BYTE_6    0x1326 //00

#define TX_HDCP_AKEY37_BYTE_0    0x1328 //00
#define TX_HDCP_AKEY37_BYTE_1    0x1329 //00
#define TX_HDCP_AKEY37_BYTE_2    0x132A //00
#define TX_HDCP_AKEY37_BYTE_3    0x132B //00
#define TX_HDCP_AKEY37_BYTE_4    0x132C //00
#define TX_HDCP_AKEY37_BYTE_5    0x132D //00
#define TX_HDCP_AKEY37_BYTE_6    0x132E //00

#define TX_HDCP_AKEY38_BYTE_0    0x1330 //00
#define TX_HDCP_AKEY38_BYTE_1    0x1331 //00
#define TX_HDCP_AKEY38_BYTE_2    0x1332 //00
#define TX_HDCP_AKEY38_BYTE_3    0x1333 //00
#define TX_HDCP_AKEY38_BYTE_4    0x1334 //00
#define TX_HDCP_AKEY38_BYTE_5    0x1335 //00
#define TX_HDCP_AKEY38_BYTE_6    0x1336 //00

#define TX_HDCP_AKEY39_BYTE_0    0x1338 //00
#define TX_HDCP_AKEY39_BYTE_1    0x1339 //00
#define TX_HDCP_AKEY39_BYTE_2    0x133A //00
#define TX_HDCP_AKEY39_BYTE_3    0x133B //00
#define TX_HDCP_AKEY39_BYTE_4    0x133C //00
#define TX_HDCP_AKEY39_BYTE_5    0x133D //00
#define TX_HDCP_AKEY39_BYTE_6    0x133E //00


#define TX_HDCP_AKSV_BYTE_0      0x1340
#define TX_HDCP_AKSV_BYTE_1      0x1341
#define TX_HDCP_AKSV_BYTE_2      0x1342
#define TX_HDCP_AKSV_BYTE_3      0x1343
#define TX_HDCP_AKSV_BYTE_4      0x1344

// HDCP REGS
#define TX_HDCP_CTRL             0x1350 //FE
#define TX_HDCP_STATUS_1         0x1351 //FE
#define TX_HDCP_STATUS_2         0x1352
#define TX_HDCP_INTR_0           0x1353
#define TX_HDCP_TX_AKSV_0        0x1354
#define TX_HDCP_TX_AKSV_1        0x1355
#define TX_HDCP_TX_AKSV_2        0x1356
#define TX_HDCP_TX_AKSV_3        0x1357
#define TX_HDCP_TX_AKSV_4        0x1358
#define TX_HDCP_RX_BKSV_0        0x135C //FF
#define TX_HDCP_RX_BKSV_1        0x135D //FF
#define TX_HDCP_RX_BKSV_2        0x135E //FF
#define TX_HDCP_RX_BKSV_3        0x135F //FF
#define TX_HDCP_RX_BKSV_4        0x1360 //FF
#define TX_HDCP_TX_AINFO         0x1361 //FF
#define TX_HDCP_RX_BCAPS         0x1362 //FF
#define TX_HDCP_RX_BSTATUS_0     0x1364 //FF
#define TX_HDCP_RX_BSTATUS_1     0x1365 //FF
#define TX_HDCP_TX_AN_0          0x1368
#define TX_HDCP_TX_AN_1          0x1369
#define TX_HDCP_TX_AN_2          0x136A
#define TX_HDCP_TX_AN_3          0x136B
#define TX_HDCP_TX_AN_4          0x136C
#define TX_HDCP_TX_AN_5          0x136D
#define TX_HDCP_TX_AN_6          0x136E
#define TX_HDCP_TX_AN_7          0x136F
#define TX_HDCP_TX_M0_0          0x1370 //M0 shouldn't be accessable by FW
#define TX_HDCP_TX_M0_1          0x1371
#define TX_HDCP_TX_M0_2          0x1372
#define TX_HDCP_TX_M0_3          0x1373
#define TX_HDCP_TX_M0_4          0x1374
#define TX_HDCP_TX_M0_5          0x1375
#define TX_HDCP_TX_M0_6          0x1376
#define TX_HDCP_TX_M0_7          0x1377
#define TX_HDCP_TX_R0_0          0x1378
#define TX_HDCP_TX_R0_1          0x1379
#define TX_HDCP_RX_R0_0          0x137A //FF
#define TX_HDCP_RX_R0_1          0x137B //FF
#define TX_HDCP_TX_RI_0          0x137C
#define TX_HDCP_TX_RI_1          0x137D
#define TX_HDCP_RX_RI_0          0x137E //FF
#define TX_HDCP_RX_RI_1          0x137F //FF
#define TX_HDCP_TX_PJ            0x1380
#define TX_HDCP_RX_PJ            0x1381 //FF
#define TX_HDCP_FIX_CLR_0        0x1384 //FF
#define TX_HDCP_FIX_CLR_1        0x1385 //FF
#define TX_HDCP_FIX_CLR_2        0x1386 //FF
#define TX_HDCP_KINIT_0          0x1388 //FF
#define TX_HDCP_KINIT_1          0x1389 //FF
#define TX_HDCP_KINIT_2          0x138A //FF
#define TX_HDCP_KINIT_3          0x138B //FF
#define TX_HDCP_KINIT_4          0x138C //FF
#define TX_HDCP_KINIT_5          0x138D //FF
#define TX_HDCP_KINIT_6          0x138E //FF
#define TX_HDCP_BINIT_0          0x1390 //FF
#define TX_HDCP_BINIT_1          0x1391 //FF
#define TX_HDCP_BINIT_2          0x1392 //FF
#define TX_HDCP_BINIT_3          0x1393 //FF
#define TX_HDCP_BINIT_4          0x1394 //FF
#define TX_HDCP_BINIT_5          0x1395 //FF
#define TX_HDCP_BINIT_6          0x1396 //FF
#define TX_HDCP_BINIT_7          0x1397 //FF
#define TX_HDCP_INTR_CLR         0x1398 //F8
#define TX_HDCP_INTR_MASK        0x1399 //F8



#define VPP_BE_HDMITX_MAX_PKT_INDEX 6
