/*
 * linux/include/video/dovefbreg.h -- Marvell frame buffer for DOVE
 *
 *
 * Copyright (C) Marvell Semiconductor Company.  All rights reserved.
 *
 * Written by Green Wan <gwan@marvell.com>
 *
 * Adapted from:  linux/drivers/video/skeletonfb.c
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 *
 */
#ifndef _PXA168FB_H_
#define _PXA168FB_H_

#define DISPLAY_CONTROLLER_BASE			0xD420B000
#define LCD_TOP_CTRL				0x01DC
#define VDMA_ENABLE				0xfff0

#define DSI1_REG_BASE				0xD420B800

/* DMA Control 0 Register */
#define LCD_SPU_DMA_CTRL0			0x0190
#define     CFG_ARBFAST_ENA(an)			((an)<<27)
/* for graphic part */
#define     CFG_GRA_HSMOOTH(smooth)		((smooth)<<14)

/* for video part */
#define     CFG_DMA_HSMOOTH(smooth)		((smooth)<<6)

/* DMA Control 1 Register */
#define LCD_SPU_DMA_CTRL1			0x0194

#define  CFG_ALPHA_MODE(amode)                  ((amode)<<16)
#define  CFG_ALPHA_MODE_MASK                    0x00030000
#define  CFG_ALPHA(alpha)                       ((alpha)<<8)
#define  CFG_ALPHA_MASK                         0x0000FF00

/* Smart or Dumb Panel Clock Divider */
#define LCD_CFG_SCLK_DIV			0x01A8
/* Dump LCD Panel Control Register */
#define LCD_SPU_DUMB_CTRL			0x01B8
/* LCD I/O Pads Control Register */
#define SPU_IOPAD_CONTROL			0x01BC
/* csc */
#define     CFG_CYC_BURST_LEN16			(1<<4)
#define     CFG_CYC_BURST_LEN8			(0<<4)

#define SPU_IRQ_ISR                     	0x01C4

/* LCD Interrupt Control Register */
#define     GRA_FRAME_IRQ0_ENA_MASK		0x08000000




#define     GRA_FRAME_IRQ1_ENA_MASK		0x04000000


#define     VSYNC_IRQ_ENA_MASK			0x00800000



#define     DUMB_FRAMEDONE_ENA_MASK		0x00400000

#define	    TVSYNC_IRQ_ENA_MASK			0x00001000


#define     TV_FRAME_IRQ0_ENA_MASK		0x00000800
#define     TV_FRAME_IRQ1_ENA_MASK              0x00000400
#define     TV_FRAMEDONE_ENA_MASK		0x00000100

#define  TV_DMA_FRAME_IRQ0_ENA_MASK             0x00008000
#define  TV_DMA_FRAME_IRQ1_ENA_MASK             0x00004000

/* FIXME - JUST GUESS */
#define	    PN2_DMA_FRAME_IRQ0_ENA_MASK		0x00000080
#define	    PN2_DMA_FRAME_IRQ1_ENA_MASK		0x00000040
#define     PN2_GRA_FRAME_IRQ0_ENA_MASK		0x00000008
#define     PN2_GRA_FRAME_IRQ1_ENA_MASK		0x04000004
#define     PN2_SYNC_IRQ_ENA_MASK               0x00000001

#define gf0_imask(id)	((id) ? (((id) & 1) ? TV_FRAME_IRQ0_ENA_MASK \
		: PN2_GRA_FRAME_IRQ0_ENA_MASK) : GRA_FRAME_IRQ0_ENA_MASK)
#define gf1_imask(id)	((id) ? (((id) & 1) ? TV_FRAME_IRQ1_ENA_MASK \
		: PN2_GRA_FRAME_IRQ1_ENA_MASK) : GRA_FRAME_IRQ1_ENA_MASK)
#define vsync_imask(id)	((id) ? (((id) & 1) ? TVSYNC_IRQ_ENA_MASK \
		: PN2_SYNC_IRQ_ENA_MASK) : VSYNC_IRQ_ENA_MASK)

#define display_done_imask(id)	((id) ? (((id) & 1) ? TV_FRAMEDONE_ENA_MASK\
	: (PN2_DMA_FRAME_IRQ0_ENA_MASK | PN2_DMA_FRAME_IRQ1_ENA_MASK))\
	: DUMB_FRAMEDONE_ENA_MASK)
#define LCD_PN2_CTRL0                           (0x02C8)
#define LCD_DUMB2_CTRL                          (0x02d8)
#define LCD_PN2_CTRL1                           (0x02DC)
#define LCD_PN2_SCLK_DIV                        (0x01EC)

#define LCD_TCLK_DIV		(0x009C)
#define clk_div(id)     ((id) ? ((id) & 1 ? LCD_TCLK_DIV : LCD_PN2_SCLK_DIV) \
		: LCD_CFG_SCLK_DIV)

#endif

