/*
 * This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

#include <asm/io.h>
#include <asm-generic/errno.h>
#include <hdmi.h>
#include <linux/ctype.h>
#include <linux/fb.h>
#include <malloc.h>
#include "hdmi-mmp.h"

struct hdmi_mmp_dev {
	struct hdmi_dev hdmi_dev;
	void *base;
};

unsigned *hdmi_base;

static struct hdmi_mmp_dev *mdp;
static struct hdmi_mmp_dev mdev;

void hdmi_direct_write(void *base, unsigned addr, unsigned data)
{
	writel(data, base + BASE_OFFSET + addr);
}

unsigned hdmi_direct_read(void *base, unsigned addr)
{
	return readl(base + BASE_OFFSET + addr);
}

unsigned hdmi_read(void *base, unsigned addr)
{
	writel(addr | 1 << 30, base + BASE_OFFSET + HDMI_ADDR);
	return readl((base + BASE_OFFSET + HDMI_DATA)) & 0xff;
}

void hdmi_write(void *base, unsigned addr, unsigned data)
{
	writel(data & 0xff, base + BASE_OFFSET + HDMI_DATA);
	writel(addr | 1 << 31, base + BASE_OFFSET + HDMI_ADDR);
}

static void hdmi_write_multi(struct hdmi_mmp_dev *md, u32 addr, u8 *buf,
		u32 length)
{
	u32 i;
	for (i = 0; i < length; i++)
		hdmi_write(md->base, addr + i, *(buf + i));
}

static int hdmi_mmp_init_controller(struct hdmi_dev *dev)
{
	struct hdmi_mmp_dev *md = (struct hdmi_mmp_dev *)dev;
	u32 i, cnt, tmp;

	printf("Init mmp hdmi ctroller...\n");
	hdmi_direct_write(md->base, HDMI_PHY_CFG_2, 1<<27);

	hdmi_direct_write(md->base, HDMI_PHY_CFG_0, 0X00249b6d);
	hdmi_direct_write(md->base, HDMI_PHY_CFG_1, 0x0249cccc);
	/* hdmi_direct_write(md->base, HDMI_PHY_CFG_2, 0X00003c30); */
	hdmi_direct_write(md->base, HDMI_PHY_CFG_2, 0X00003c10);
	tmp = hdmi_direct_read(md->base, HDMI_PHY_CFG_3);
	hdmi_direct_write(md->base, HDMI_PHY_CFG_3, (tmp | 0X0000001));
	udelay(50000);
	hdmi_direct_write(md->base, HDMI_PHY_CFG_3, (tmp & (~0X0000001)));
	/* temporatyly set the aduio cfg */
	/* hdmi_direct_write(md->base, HDMI_AUDIO_CFG, 0x869); */

	hdmi_write(md->base, HDTX_HST_PKT_CTRL0, 0);
	hdmi_write(md->base, HDTX_HST_PKT_CTRL1, 0);

	/* Set MemSize */
	hdmi_write(md->base, HDTX_MEMSIZE_L, 0xff);
	hdmi_write(md->base, HDTX_MEMSIZE_H, 0xff);

	/* Disable audio FIFO reset (H/W bug) */
	hdmi_write(md->base, HDTX_FIFO_CTRL, 0);

	/* Set deep color fifo read and write pointers */
	hdmi_write(md->base, HDTX_DC_FIFO_WR_PTR, 0);
	hdmi_write(md->base, HDTX_DC_FIFO_RD_PTR, 0x1f);

	/* Set BCH Rotate Flag, disable rotate */
	/* No register are affected ??? */

	hdmi_write(md->base, HDTX_HDMI_CTRL, 0);

	/* Set Swap Control, no TMDS channel swap */
	hdmi_write(md->base, HDTX_SWAP_CTRL, 0);

	/* Set Tmds Clock as 0x0F83E0(hard code value from DE) */
	hdmi_write(md->base, HDTX_TDATA3_0, 0xe0);
	hdmi_write(md->base, HDTX_TDATA3_1, 0x83);
	hdmi_write(md->base, HDTX_TDATA3_2, 0xf);

	/* Set I2S Audio Bus Parameters */
	hdmi_write(md->base, HDTX_AUD_CTRL, 0x4);
	hdmi_write(md->base, HDTX_I2S_CTRL, 0x8);

	/* Disable audio transmission */
	/*hdmi_write(md->base, HDTX_AUD_CTRL, 0x4); */
	hdmi_write(md->base, HDTX_ACR_CTRL, 0x2);
	hdmi_write(md->base, HDTX_ACR_N0, 0);
	hdmi_write(md->base, HDTX_ACR_N1, 0);
	hdmi_write(md->base, HDTX_ACR_N2, 0);

	hdmi_write(md->base, HDTX_HDMI_CTRL, 0);

	hdmi_write(md->base, HDTX_I2S_DLEN, 0);
	hdmi_write(md->base, HDTX_CHSTS_0, 0);
	hdmi_write(md->base, HDTX_CHSTS_1, 0);
	hdmi_write(md->base, HDTX_CHSTS_2, 0);
	hdmi_write(md->base, HDTX_CHSTS_3, 0);
	hdmi_write(md->base, HDTX_CHSTS_4, 0);

	/* Mute audio and video */
	hdmi_write(md->base, HDTX_AVMUTE_CTRL, 0x3);

	/* Clear all packet transmission */
	for (i = 0; i < VPP_BE_HDMITX_MAX_PKT_INDEX; i++)
	{
		tmp = HDTX_PKT0_BYTE0 + 0x20 * i;
		for (cnt = 0; cnt < 0x1f; cnt++)
			hdmi_write(md->base, tmp, 0);
	}
	hdmi_write(md->base, HDTX_HST_PKT_CTRL0, 0);
	hdmi_write(md->base, HDTX_HST_PKT_CTRL1, 0);

	/* ACR given more priority than audio sample packets */
	/* No register are affected ??? */

	/* Why twice from VG ??? */
	hdmi_write(md->base, HDTX_GCP_CFG0, 0);
	hdmi_write(md->base, HDTX_GCP_CFG0, 0);

	/* use program timing */
	hdmi_write(md->base, HDTX_VIDEO_CTRL, 0x8);
	hdmi_write(md->base, HDTX_HDMI_CTRL, 0x8);

	/* reset program timing */
	for (i = HDTX_HTOT_L; i <= HDTX_VSTART_H; i++)
		hdmi_write(md->base, i, 0x0);

	/* Clear UBits, user data of SPDIF header */
	tmp = HDTX_UBITS_0;
	for (i = 0; i < 14; i++, tmp++)
		hdmi_write(md->base, tmp, 0x0);

	/* Reset HBR Control. Spec doesn't have it!!! */
	hdmi_write(md->base, HDTX_HBR_PKT, 0x0);

	/* Clear Audio channel select. No swap */
	tmp = HDTX_AUD_CH1_SEL;
	for (i = 0; i < 8; i++, tmp++)
		hdmi_write(md->base, tmp, i);

	/* HPD Select. We do not have it !!! */
	hdmi_write(md->base, TX_HDCP_HPD_SEL, 0x0);

	/* Deep color control for pixel packing phase */
	hdmi_write(md->base, HDTX_DC_PP_CTRL, 0x0);

	/* host packets aligned with vsync rise edge timing */
	hdmi_write(md->base, HDTX_HST_PKT_CTRL2, 0x0);

	/* Enable PHY FIFO */
	hdmi_write(md->base, HDTX_PHY_FIFO_SOFT_RST, 0x1);
	hdmi_write(md->base, HDTX_PHY_FIFO_SOFT_RST, 0x0);

	/* Set HDCP Color */
	hdmi_write(md->base, TX_HDCP_FIX_CLR_0, 0x80);
	hdmi_write(md->base, TX_HDCP_FIX_CLR_1, 0x10);
	hdmi_write(md->base, TX_HDCP_FIX_CLR_2, 0x80);

	return 0;
}

void hdmi_3d_sync_view(void)
{
	u32 reg;

	/*
	reg = hdmi_direct_read(hdmi_base, HDMI_3D_CTRL);
	reg &= ~ ((1 << 1) | (1 << 2) | (1 << 3));
	reg |= 1 << 2;
	hdmi_direct_write(hdmi_base, HDMI_3D_CTRL, 0);
	*/
	reg = 0x5;
	hdmi_direct_write(hdmi_base, HDMI_3D_CTRL, reg);
}

static int hdmi_mmp_set_freq(struct hdmi_mmp_dev *md)
{
	int ret = 0;

	/* Fix the pll2refdiv to 1(+2), to get 8.66MHz ref clk
	 * Stable val recomended between 8-12MHz. To get the reqd
	 * freq val, just program the fbdiv
	 * freq takes effect during a fc req
	 */
	u32 residual;
	volatile unsigned int temp = 0;
	u32 freq;
	unsigned int postdiv;
	unsigned int freq_offset_inner;
	unsigned int hdmi_pll_fbdiv;
	unsigned int hdmi_pll_refdiv;
	unsigned int kvco;
	unsigned int freq_offset_inner_16;
	unsigned int VPLL_CALCLK_DIV = 1;
	unsigned int VDDM = 1;
	unsigned int VDDL = 0x9;
	unsigned int ICP = 0x9;
	unsigned int VREG_IVREF = 2;
	unsigned int INTPI = 5;
	unsigned int VTH_VPLL_CA = 2;
	int count = 0x10000;

	ret = hdmi_get_freq ((struct hdmi_dev *)md, &freq);
	if (ret)
		return ret;

	freq /= 1000;
	/* round to freq = N*100k Hz */
	residual = freq % 100;
	freq = residual? (freq + 100 - residual) : freq;
	freq /= 1000;

	switch (freq) {
	case 25:
		postdiv = 0x3;
		freq_offset_inner = 0x1C47;
		freq_offset_inner_16 = 1;
		hdmi_pll_fbdiv = 39;
		hdmi_pll_refdiv = 0;
		kvco = 0x4;
		break;
	case 27:
		postdiv = 0x3;
		freq_offset_inner = 0x2d03;
		freq_offset_inner_16 = 1;
		hdmi_pll_fbdiv = 42;
		hdmi_pll_refdiv = 0;
		kvco = 0x4;
		break;
	case 54:
		postdiv = 0x2;
		freq_offset_inner = 0x2d03;
		freq_offset_inner_16 = 1;
		hdmi_pll_fbdiv = 42;
		hdmi_pll_refdiv = 0;
		kvco = 0x4;
		break;
	case 74:
		postdiv = 0x2;
		freq_offset_inner = 0x084B;
		freq_offset_inner_16 = 0;
		hdmi_pll_fbdiv = 57;
		hdmi_pll_refdiv = 0;
		kvco = 0x4;
		break;
	case 108:
		postdiv = 0x1;
		freq_offset_inner = 0x2d03;
		freq_offset_inner_16 = 1;
		hdmi_pll_fbdiv = 42;
		hdmi_pll_refdiv = 0;
		kvco = 0x4;
		break;
	case 148:
		postdiv = 0x1;
		freq_offset_inner = 0x084B;
		freq_offset_inner_16 = 0;
		hdmi_pll_fbdiv = 57;
		hdmi_pll_refdiv = 0;
		kvco = 0x4;
		break;
	default:
		printf("not supported freq %d\n", freq);
		return -EINVAL;
	}

	hdmi_direct_write(md->base, HDMI_CLOCK_CFG, 1|1<<2|1<<4|4<<5|5<<9|5<<13);
	/*hdmi_direct_write(md->base, HDMI_CLOCK_CFG, 1|1<<2|4<<5|5<<9|5<<13); */

	/* power up the pll */
	temp |= HDMI_PLL_CFG_3_HDMI_PLL_ON;
	hdmi_direct_write(md->base, HDMI_PLL_CFG_3, temp);

	temp = ((INTPI << HDMI_PLL_CFG_0_INTPI_BASE)	|
			(HDMI_PLL_CFG_0_CLK_DET_EN)	|
			(VDDM << HDMI_PLL_CFG_0_VDDM_BASE)	|
			(VDDL << HDMI_PLL_CFG_0_VDDL_BASE)	|
			(ICP << HDMI_PLL_CFG_0_ICP_BASE)	|
			(kvco << HDMI_PLL_CFG_0_KVCO_BASE)	|
			(VREG_IVREF << HDMI_PLL_CFG_0_VREG_IVREF_BASE)	|
			(VPLL_CALCLK_DIV << HDMI_PLL_CFG_0_VPLL_CALCLK_DIV_BASE) |
			(postdiv << HDMI_PLL_CFG_0_POSTDIV_BASE)	|
			(ICP  << HDMI_PLL_CFG_0_ICP_BASE));
	hdmi_direct_write(md->base, HDMI_PLL_CFG_0, temp);

	temp = ((HDMI_PLL_CFG_1_MODE) |
			(HDMI_PLL_CFG_1_EN_PANNEL)|
			(HDMI_PLL_CFG_1_EN_HDMI)|
			(VTH_VPLL_CA << HDMI_PLL_CFG_1_VTH_VPLL_CA_BASE)|
			(freq_offset_inner << HDMI_PLL_CFG_1_FREQ_OFFSET_INNER_BASE)|
			(freq_offset_inner_16 << 20));
	hdmi_direct_write(md->base, HDMI_PLL_CFG_1, temp);

	temp = 0;
	hdmi_direct_write(md->base, HDMI_PLL_CFG_2, temp);

	/* Power Down the pll and set the divider ratios*/
	temp = (hdmi_pll_fbdiv << HDMI_PLL_CFG_3_HDMI_PLL_FBDIV_BASE)|
		(hdmi_pll_refdiv << HDMI_PLL_CFG_3_HDMI_PLL_REFDIV_BASE);
	hdmi_direct_write(md->base, HDMI_PLL_CFG_3, temp);

	/* power up the pll */
	temp |= HDMI_PLL_CFG_3_HDMI_PLL_ON;
	hdmi_direct_write(md->base, HDMI_PLL_CFG_3, temp);

	/*wait 10us : just an estimate*/
	udelay(100);

	/* release the pll out of reset*/
	temp |= HDMI_PLL_CFG_3_HDMI_PLL_RSTB;
	hdmi_direct_write(md->base, HDMI_PLL_CFG_3, temp);

	/* PLL_LOCK should be 1, */
	temp = hdmi_direct_read(md->base, HDMI_PLL_CFG_3);
	temp &= 0x400000;
	while ( !temp )	{
		temp = hdmi_direct_read(md->base, HDMI_PLL_CFG_3);
		temp &= 0x400000;
		udelay(10);
		count--;
		if (count <= 0) {
			printf("%s PLL lock error, PLL_CFG_3 %x\n",
				__func__, temp);
			return -EIO;
		}
	}
	/* pll stablize in 10ms */
	udelay(10000);
	return ret;
}

static int hdmi_send_packet (struct hdmi_mmp_dev *md, char *buf, int idx)
{
	u32 v;

	hdmi_write_multi (md->base, HDTX_PKT0_BYTE0 + 0x20 * idx, buf, 32);
	printf("packet addr 0x%x type 0x%x\n", HDTX_PKT0_BYTE0 + 0x20 * idx, *buf);

	v = hdmi_read(md->base, HDTX_HST_PKT_CTRL0);
	printf("ctrl0 0x%x\n", v);
	hdmi_write(md->base, HDTX_HST_PKT_CTRL0, v | (1 << idx));
	/* HW bug */
	v &= ~(1 << idx);
	hdmi_write(md->base, HDTX_HST_PKT_CTRL0, v);
	v = hdmi_read(md->base, HDTX_HST_PKT_CTRL1);
	hdmi_write(md->base, HDTX_HST_PKT_CTRL1, v | (1 << idx));
}

static int hdmi_mmp_get_reg (struct hdmi_dev *dev)
{
	struct hdmi_mmp_dev *md = (struct hdmi_mmp_dev *)dev;
	int i;

	printf("************direct register*******************\n");
	for (i = 0x8; i <= 0x30; i += 4)
		printf("direct offset 0x%x is 0x%x\n", i, hdmi_direct_read(md->base, i));
	printf("************indirect register*******************\n");
	for (i = 0; i < 0x13e; i++)
		printf("offset 0x%x is 0x%x\n", i, hdmi_read(md->base, i));
}

static int hdmi_mmp_set_reg (struct hdmi_dev *dev)
{
	struct hdmi_mmp_dev *md = (struct hdmi_mmp_dev *)dev;

	return 0;
}

static int hdmi_mmp_set_mode (struct hdmi_dev *dev, cea_mode_id id)
{
	char *buf;
	u32 v;
	struct hdmi_mmp_dev *md = (struct hdmi_mmp_dev *)dev;

	dev->mode_id = id;
	hdmi_mmp_set_freq (md);
	buf = malloc (32);

	hdmi_write(md->base, HDTX_PHY_FIFO_SOFT_RST, 0x0);
	hdmi_write(md->base, HDTX_PHY_FIFO_SOFT_RST, 0x1);
	hdmi_write(md->base, HDTX_PHY_FIFO_SOFT_RST, 0x0);
	/* get proper avi packet */
	hdmi_avi_info_frame (dev, buf);
	/* write avi packet */
	hdmi_send_packet (dev, buf, 0);
	memset (buf, 0, 32);
	hdmi_vender_info_frame (dev, buf);
	hdmi_send_packet (dev, buf, 2);
	free(buf);

	v = hdmi_read(md->base, HDTX_HDMI_CTRL);
	v &= ~0x78;
	hdmi_write(md->base, HDTX_HDMI_CTRL, v | 0x1 | (dev->pixel_rept));
	/* auto detect lcd timing */
	hdmi_write(md->base, HDTX_VIDEO_CTRL, 0x58);
	if (dev->mode_3d)
		hdmi_direct_write(md->base, HDMI_3D_CTRL, 0x1);
	else
		hdmi_direct_write(md->base, HDMI_3D_CTRL, 0x0);
	return 0;
}

static int hdmi_mmp_enable(struct hdmi_dev *dev, int enable)
{
	struct hdmi_mmp_dev *md = (struct hdmi_mmp_dev *)dev;
	u32 v;

	v = hdmi_direct_read(md->base, HDMI_CLOCK_CFG);
	v = enable ? (v | (1<<4)) : (v & ~(1<<4));
	hdmi_direct_write(md->base, HDMI_CLOCK_CFG, v);
	hdmi_write(md->base, HDTX_AVMUTE_CTRL, 0x1);
	hdmi_write(md->base, HDTX_GCP_CFG0, 1);

	/* magic register */
	hdmi_write(md->base, HDTX_TDATA3_1, 0x83);
	hdmi_direct_write(md->base, HDMI_PHY_CFG_2, 0x3c10);
	/*hdmi_direct_write(md->base,0x1c, 0xaa95); */
	v = hdmi_read(md->base, HDTX_VIDEO_CTRL);
	hdmi_write(md->base, HDTX_VIDEO_CTRL, v & (~0x40));
	hdmi_write(md->base, HDTX_VIDEO_CTRL, v);
	return 0;
}

void hdmi_init(struct fb_var_screeninfo *var, u32 id, u32 enable_3d)
{
	struct hdmi_mmp_dev *md = &mdev;
	struct hdmi_dev *hd = (struct hdmi_dev *)md;

#define DISPLAY_CONTROLLER_BASE                 0xD420B000
	md->base = (void *)DISPLAY_CONTROLLER_BASE;
	mdp = md;
	hd->mode_3d = enable_3d;
	//hd->mode_3d = 1;

	hdmi_mmp_init_controller(hd);
	hdmi_mmp_set_mode(hd, id);
	hdmi_mmp_enable(hd, 1);
	hdmi_base = md->base;
}
