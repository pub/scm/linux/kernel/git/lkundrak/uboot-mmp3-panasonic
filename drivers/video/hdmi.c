/*
 * HDMI Control Abstraction
 *
 * This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

#include <hdmi.h>

#define HDMI_DEBUG
static int hdmi_core_debug = 5;
#define hdmi_dprintk(level, fmt, arg...)	if (hdmi_core_debug >= level) \
	printf("%s: " fmt , __func__, ## arg)

const struct fb_videomode cea_modes[65] = {
	/* #1: 640x480p@59.94/60Hz */
	[1] = {
		NULL, 60, 640, 480, 39722, 48, 16, 33, 10, 96, 2, 0,
		FB_VMODE_NONINTERLACED, FB_MODE_IS_CEA,
	},
	/* #2: 720x480p@59.94/60Hz ratio 4:3 */
	/* #3: 720x480p@59.94/60Hz */
	[3] = {
		NULL, 60, 720, 480, 37037, 60, 16, 30, 9, 62, 6, 0,
		FB_VMODE_NONINTERLACED, FB_MODE_IS_CEA,
	},
	/* #4: 1280x720p@59.94/60Hz */
	[4] = {
		NULL, 60, 1280, 720, 13468, 220, 110, 20, 5, 40, 5,
		FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
		FB_VMODE_NONINTERLACED, FB_MODE_IS_CEA,
	},
	/* #5: 1920x1080i@59.94/60Hz */
	[5] = {
		NULL, 60, 1920, 1080, 13763, 148, 88, 15, 2, 44, 5,
		FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
		FB_VMODE_INTERLACED, FB_MODE_IS_CEA,
	},
	/* #6: 720(1440)x480iH@59.94/60Hz ratio 4:3 */
	/* #7: 720(1440)x480iH@59.94/60Hz */
	[7] = {
		NULL, 60, 1440, 480, 18554/*37108*/, 114, 38, 15, 4, 124, 3, 0,
		FB_VMODE_INTERLACED, FB_MODE_IS_CEA,
	},
	/* #8: 720(1440)x240pH@59.94/60Hz ratio 4:3 */
	/* #9: 720(1440)x240pH@59.94/60Hz */
	[9] = {
		NULL, 60, 1440, 240, 18554, 114, 38, 15, 4, 124, 3, 0,
		FB_VMODE_NONINTERLACED, FB_MODE_IS_CEA,
	},
	/* #16: 1920x1080p@59.94/60Hz */
	[16] = {
		NULL, 60, 1920, 1080, 6734, 148, 88, 36, 4, 44, 5,
		FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
		FB_VMODE_NONINTERLACED, FB_MODE_IS_CEA,
	},
	/* #17: 720x576pH@50Hz ratio 4:3 */
	/* #18: 720x576pH@50Hz */
	[18] = {
		NULL, 50, 720, 576, 37037, 68, 12, 39, 5, 64, 5, 0,
		FB_VMODE_NONINTERLACED, FB_MODE_IS_CEA,
	},
	/* #19: 1280x720p@50Hz */
	[19] = {
		NULL, 50, 1280, 720, 13468, 220, 440, 20, 5, 40, 5,
		FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
		FB_VMODE_NONINTERLACED, FB_MODE_IS_CEA,
	},
	/* #20: 1920x1080i@50Hz */
	[20] = {
		NULL, 50, 1920, 1080, 13480, 148, 528, 15, 5, 528, 5,
		FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
		FB_VMODE_INTERLACED, FB_MODE_IS_CEA,
	},
	/* #21: 720x576i@50Hz ratio 4:3 */
	/* #22: 720x576i@50Hz */
	[22] = {
		NULL, 50, 720, 576, 37037, 138, 24, 19, 2, 24, 3, 0,
		FB_VMODE_INTERLACED, FB_MODE_IS_CEA,
	},
	/* #31: 1920x1080p@50Hz */
	[31] = {
		NULL, 50, 1920, 1080, 6734, 148, 528, 36, 4, 44, 5,
		FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
		FB_VMODE_NONINTERLACED, FB_MODE_IS_CEA,
	},
	/* #32: 1920x1080p@23.98/24Hz */
	[32] = {
		NULL, 24, 1920, 1080, 13480, 148, 638, 36, 4, 44, 5,
		FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
		FB_VMODE_NONINTERLACED, FB_MODE_IS_CEA,
	},
	/* #33: 1920x1080p@25Hz */
	[33] = {
		NULL, 25, 1920, 1080, 13480, 148, 638, 36, 4, 44, 5,
		FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
		FB_VMODE_NONINTERLACED, FB_MODE_IS_CEA,
	},
	/* #34: 1920x1080p@30Hz */
	[34] = {
		NULL, 30, 1920, 1080, 13480, 148, 88, 36, 4, 44, 5,
		FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
		FB_VMODE_NONINTERLACED, FB_MODE_IS_CEA,
	},
	/* #35: (2880)x480p4x@59.94/60Hz */
	[35] = {
		NULL, 60, 2880, 480, 9250, 240, 64, 30, 9, 248, 6, 0,
		FB_VMODE_NONINTERLACED, FB_MODE_IS_CEA,
	},
};
/* ==================== info frame utility ===================== */

int hdmi_vender_info_frame (struct hdmi_dev *hd, char buf[])
{
	int count;
	char check_sum = 0;

	buf[0] = 0x81;
	buf[1] = 0x1;
	buf[2] = 0x5;
	buf[3] = 0;
	buf[4] = 0x3;
	buf[5] = 0xc;
	buf[6] = 0x0;
	if (hd->mode_3d)
		buf[7] = 0x40;
	else
		buf[7] = 0x0;
	buf[8] = 0x0; /* only support frame packing currently */

	for (count = 0; count < 14; count++)
		check_sum += buf[count];
	buf[3] = 0x100 - check_sum;
	return 0;
}

int hdmi_avi_info_frame (struct hdmi_dev *hd, char buf[])
{
	int count;
	char check_sum = 0;

	hdmi_dprintk(3, "pack avi infoframe\n");
	/* packet header for AVI Packet in pkt1 */
	/* Fix me. we should set bits according to format */
	buf[0] = 0x82; /* InfoFrame Type, AVI frame */
	buf[1] = 0x2;  /* Version = 02 */
	buf[2] = 0xd;  /* Length = 13 */
	buf[3] = 0x0;  /* checksum */
	buf[4] = 0x0; /* Fix to RGB format currently */
	/*
	buf[4] = 0x10; // RGB, Active Format(r0,r1,r2,r3) valid, no bar, no scan
	buf[4] = 0x30; // HDMI_YUV422, Active Format(r0,r1,r2,r3) valid, no bar, no scan
	buf[4] = 0x50; // HDMI_YUV444, Active Format(r0,r1,r2,r3) valid, no bar, no scan
	*/

	buf[5] = 0xa8;  /* ITU-R 709, 16:9 */
	buf[6] = 0x20;
	/* buf[6] = 0x0;   // No IT content, xvYCC601, no scaling */
	buf[7] = hd->mode_id;
	/* limited YCC range, graphics, no repeat  Pixel repitition factor */
	buf[8] = hd->pixel_rept;

	buf[9] = 0x0;
	buf[10] = 0x0;
	buf[11] = 0x0;
	buf[12] = 0x0;
	buf[13] = 0x0;

	for (count = 0; count < 14; count++)
		check_sum += buf[count];
	buf[3] = 0x100 - check_sum;
	return 0;
}

int hdmi_get_freq (struct hdmi_dev *hd, u32 *need_freq)
{
	int freq;

	/* Calc divider according to pclk in pico second */
	freq = 1000000000000LL / cea_modes[hd->mode_id].pixclock;

	hdmi_dprintk(3, "get base pclk %d\n", freq);

	if (FB_VMODE_INTERLACED & cea_modes[hd->mode_id].vmode)
		freq >>= 1;

	/* We only support repeat=2/4 currently. Add more later */
	if (!(hd->pixel_rept == 0 || hd->pixel_rept == 1
				|| hd->pixel_rept == 3)) {
		hdmi_dprintk(3, "Not support pix repeat %d\n", hd->pixel_rept);
		return -1;
	}

	if (1 == hd->pixel_rept)
	{
		if (!(FB_VMODE_INTERLACED & cea_modes[hd->mode_id].vmode))
			freq <<= 1;
	} else if (3 == hd->pixel_rept) {
		if (!(FB_VMODE_INTERLACED & cea_modes[hd->mode_id].vmode))
			freq <<= 2;
		else
			freq <<= 1;
	}
	if (hd->mode_3d)
			freq <<= 1;

	hdmi_dprintk(3, "get hdmi pclk %d\n", freq);
	*need_freq = freq;
	return 0;
}
