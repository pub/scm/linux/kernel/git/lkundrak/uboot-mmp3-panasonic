/*
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */
#include <common.h>
#include <asm/io.h>
#include <asm/arch/cpu.h>
#include <asm/arch/armada610.h>

#define UARTCLK14745KHZ	(APBC_APBCLK | APBC_FNCLK | APBC_FNCLKSEL(1))
#define CPUID_ID	        0
extern unsigned int mv_stepping;

int cpu_is_armada610_z0(void)
{
    unsigned int chip_id;
    unsigned int id;
    if (mv_stepping)
        return 0;
    chip_id = __raw_readl(0xd4282c00);
	id = read_cpuid(CPUID_ID);
	if (__cpu_is_armada610(id) && ((chip_id & 0x00ff0000) == 0x00f00000))
        return 1;
	else
        return 0;
}

int cpu_is_armada610_z1(void)
{
    unsigned int chip_id;
    unsigned int id;
    if (mv_stepping)
        return 0;
	chip_id = __raw_readl(0xd4282c00);
	id = read_cpuid(CPUID_ID);
	if (__cpu_is_armada610(id) && ((chip_id & 0x00ff0000) == 0x00e00000))
		return 1;
	else
		return 0;
}

int cpu_is_armada610(void)
{
	unsigned int id = read_cpuid(CPUID_ID);
	return __cpu_is_armada610(id);
}

int cpu_is_armada610_a0(void)
{
	if (mv_stepping == 0xa0)
		return 1;
	else
		return 0;
}

int cpu_is_armada610_a1(void)
{
	if (mv_stepping == 0xa1)
		return 1;
	else
		return 0;
}

int cpu_is_armada610_a2(void)
{
	if (mv_stepping == 0xa2)
		return 1;
	else
		return 0;
}

int arch_cpu_init(void)
{
	struct armd6mpmu_registers *mpmu =
		(struct armd6mpmu_registers *) AMAD6_MPMU_BASE;

	struct armd6apbc_registers *apbc =
		(struct armd6apbc_registers *) AMAD6_APBC_BASE;

	struct armd6apmu_registers *apmu =
		(struct armd6apmu_registers *) AMAD6_APMU_BASE;

	/* Turn on clock gating (PMUM_CGR_PJ) */
	writel(0xFFFFFFFF, &mpmu->acgr);

	/* Turn on APB, PLL1, PLL2 clock */
	writel(0x1FFFF, &apmu->gbl_clkctrl);

	/* Turn on AIB clock */
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->aib);

	/* Turn on uart3 clock */
	writel(UARTCLK14745KHZ, &apbc->uart3);

	/* Turn on timer clock */
	writel(APBC_APBCLK | APBC_FNCLK | APBC_FNCLKSEL(0x3), &apbc->timers);

	/* Enable GPIO clock */
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->gpio);

	/* Enable twsi1,5,6 clock */
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->twsi1);
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->twsi5);
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->twsi6);

	icache_enable();

#ifdef CONFIG_MV_SDHCI
	/* Enable SD1 clock */
	writel(APMU_PERIPH_CLK_EN | APMU_AXI_CLK_EN | APMU_PERIPH_RESET | \
		APMU_AXI_RESET | (1 << 10), &apmu->sd1);

	/* Enable SD3 clock */
	writel(APMU_PERIPH_CLK_EN | APMU_AXI_CLK_EN | APMU_PERIPH_RESET | \
		APMU_AXI_RESET, &apmu->sd3);
#endif

	/* Enable USB clock */
	writel(APMU_AXI_CLK_EN | APMU_AXI_RESET, &apmu->usb);
	writel(0xffff, &apmu->usb_dyn_gate);

#ifdef CONFIG_PXA168_FB
	/* Enable display clock */
	writel(APMU_PERIPH_CLK_EN | APMU_AXI_CLK_EN | APMU_PERIPH_RESET | \
		APMU_AXI_RESET | CLK_DIV_SEL(2) | CLK_SEL(2) | DSI_ESCCLK_EN |\
		DIS_CLK_PLL_SEL | DSI_PHY_SLOW_CLK_EN, &apmu->display1);
	writel(PLL1_1P5M_EN, &mpmu->pll1);
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->pwm3);
#endif

	return 0;
}

#if defined(CONFIG_DISPLAY_CPUINFO)
int print_cpuinfo(void)
{
	u32 id;
	struct armd6cpu_registers *cpuregs =
		(struct armd6cpu_registers *) AMAD6_CPU_BASE;

	id = readl(&cpuregs->chip_id);
	printf("SoC:   ARMADA610 88AP%X-%X\n", (id & 0xFFF), (id >> 0x10));
	return 0;
}
#endif

#ifdef CONFIG_I2C_MV
void i2c_clk_enable(void)
{
}
#endif

#ifndef CONFIG_SYS_DCACHE_OFF
void enable_caches(void)
{
	/* Enable D-cache. I-cache is already enabled in start.S */
	dcache_enable();
}
#endif
