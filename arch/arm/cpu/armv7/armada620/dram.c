/*
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Lei Wen <leiwen@marvell.com>,
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#include <common.h>
#include <asm/arch/cpu.h>
#include <asm/arch/armada620.h>

DECLARE_GLOBAL_DATA_PTR;

/*
 * armd6_sdram_base - reads SDRAM Base Address Register
 */
u32 armd6_sdram_base(int chip_sel, u32 base)
{
	struct armd6ddr_registers *ddr_regs;
	u32 result = 0;
	u32 CS_valid;

	ddr_regs = (struct armd6ddr_registers *)base;
	CS_valid = 0x01 & readl(&ddr_regs->mmap[chip_sel].cs);
	if (!CS_valid)
		return 0;

	result = readl(&ddr_regs->mmap[chip_sel].cs) & 0xFF800000;
	return result;
}

/*
 * armd6_sdram_size - reads SDRAM size
 */
u32 armd6_sdram_size(int chip_sel, u32 base)
{
	struct armd6ddr_registers *ddr_regs;
	u32 result = 0;
	u32 CS_valid;

	ddr_regs = (struct armd6ddr_registers *)base;
	CS_valid = 0x01 & readl(&ddr_regs->mmap[chip_sel].cs);
	if (!CS_valid)
		return 0;

	result = readl(&ddr_regs->mmap[chip_sel].cs);
	result = (result >> 16) & 0xF;
	if (result < 0x7) {
		printf("Unknown DRAM Size\n");
		return -1;
	} else {
		return (0x8 << (result - 0x7)) * 1024 * 1024;
	}
}

#ifndef CONFIG_SYS_BOARD_DRAM_INIT
int dram_init(void)
{
	/* This confine uboot relocation final range */
	gd->ram_size = CONFIG_FIX_RAMSIZE;

	return 0;
}

u32 dram_interleave_size(void)
{
	if (cpu_is_ax()) {
		switch ((readl(0xd4282984) & 0x3f0000) >> 16) {
		case 0x1:	return 0x00001000;
		case 0x2:	return 0x00004000;
		case 0x4:	return 0x00010000;
		case 0x8:	return 0x00040000;
		case 0x10:	return 0x00100000;
		case 0x20:	return 0x40000000;
		}
	} else {
		switch (readl(0xd4282ca0) & 0x7f) {
		case 0x1:	return 0x00001000;
		case 0x2:	return 0x00004000;
		case 0x4:	return 0x00010000;
		case 0x8:	return 0x00040000;
		case 0x10:	return 0x00100000;
		case 0x20:	return 0x20000000;
		case 0x40:	return 0x40000000;
		}
	}
	return 0;
}

/*
 * If this function is not defined here,
 * board.c alters dram bank zero configuration defined above.
 */
void dram_init_banksize(void)
{
	int i;
	u32 itlvsize, base, high_edge;

	/**
	 *
	 *  some assumptions on the working MC configuration to simplify things
	 *  1. lower CS on lower address
	 *  2. no space overlap
	 *  3. multiple CS address mapping in to one contiguous space
	 *  4. always using lower CS
	 *  5. in dual MC configuration, both MC use the same space setup
	 *  6. DDR reserved space is 0x00000000-0x80000000
	 *  7. no CS size smaller than 8MB
	 *  8. CS starting address aligned to 8MB
	 *  9. first CS address starts at 0x0, required by CPU reset vector
	 *
	 *  With above assumption we can only read the first address and
	 *  add up all size.
	 */
	for (i = 0; i < CONFIG_NR_DRAM_BANKS; i++) {
		gd->bd->bi_dram[i].start = 0;
		gd->bd->bi_dram[i].size = 0;
	}


	/* read the first MC configuration */
	base = AMAD6_DRAM_BASE;
	gd->bd->bi_dram[0].start = armd6_sdram_base(0, base);
	gd->bd->bi_dram[0].size = 0;
	for (i = 0; i < CONFIG_DRAM_CSNUM_MAX; i++) {
		/* since we assume all CS will be configured to be one range */
		gd->bd->bi_dram[0].size += armd6_sdram_size(i, base);
	}

	/* now process interleave */
	itlvsize = dram_interleave_size();
	if (itlvsize != 0) {
		if (gd->bd->bi_dram[0].size > itlvsize) {
			/**
			 *  there is no holes in the space, just double it
			 *  Ax has a bug that lead to loss of half memory
			 */
			if (!cpu_is_ax())
				gd->bd->bi_dram[0].size *= 2;
		} else {
			/* hole */
			if (CONFIG_NR_DRAM_BANKS > 1) {
				gd->bd->bi_dram[1].start =
					gd->bd->bi_dram[0].start + itlvsize;
				gd->bd->bi_dram[1].size =
					gd->bd->bi_dram[0].size;
			}
		}
	}


	/*

	high_edge = 0x80000000;

	 * workaround for Ax and B0
	 * According to JIRA MMP3-1713, the 0x7FF00000~0x7FFFFFFF memory space
	 * is mapped to ROM space, no for DDR. Remove the range
	 */
	high_edge = 0x7FF00000;

	for (i = 0; i < CONFIG_NR_DRAM_BANKS; i++) {
		if (gd->bd->bi_dram[i].start < high_edge) {
			/* start not in range */
			if ((gd->bd->bi_dram[i].start
				+ gd->bd->bi_dram[i].size) >= high_edge) {
				gd->bd->bi_dram[i].size =
					high_edge - gd->bd->bi_dram[i].start;
				/* space beyong 0x80000000 not in DDR space */
			}
		} else {
			gd->bd->bi_dram[i].start = 0;
			gd->bd->bi_dram[i].size = 0;
		}
	}


	/* space always starts @ 0x0 */
	if (gd->bd->bi_dram[0].size < CONFIG_TZ_HYPERVISOR_SIZE) {
		/* should not happen, TZ MAX to 128MB */
		printf("Cannot meet requirement for trustzone hypervisor\n");
	} else {
		gd->bd->bi_dram[0].start += CONFIG_TZ_HYPERVISOR_SIZE;
		gd->bd->bi_dram[0].size  -= CONFIG_TZ_HYPERVISOR_SIZE;
	}

	for (i = 0; i < CONFIG_NR_DRAM_BANKS; i++) {
		if (gd->bd->bi_dram[i].size > 0) {
			printf("DRAM_BANKS[%d]: 0x%08x, 0x%08x\n"
				, i
				, (unsigned int)gd->bd->bi_dram[i].start
				, (unsigned int)gd->bd->bi_dram[i].size
				);
		}
	}

	return;
}
#endif /* CONFIG_SYS_BOARD_DRAM_INIT */
