/*
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/cpu.h>
#include <asm/arch/armada620.h>

#define TIMER			0	/* Use TIMER 0 */
/* Each timer has 3 match registers */
#define MATCH_CMP(x)		((3 * TIMER) + x)
#define TIMER_LOAD_VAL		0xffffffff
#define	COUNT_RD_REQ		0x1

DECLARE_GLOBAL_DATA_PTR;

/*
 * For preventing risk of instability in reading counter value,
 * first set read request to register cvwr and then read same
 * register after it captures counter value.
 */
ulong read_timer(void)
{
	struct armd6timer_registers *armd6timers =
		(struct armd6timer_registers *) AMAD6_TIMER_BASE;
	int loop = 100;
	ulong val;

	writel(COUNT_RD_REQ, &armd6timers->cvwr);
	while (loop--)
		val = readl(&armd6timers->cvwr);

	/*
	 * This stop gcc complain and prevent loop mistake init to 0
	 */
	val = readl(&armd6timers->cvwr);

	return val;
}

void reset_timer_masked(void)
{
	/* reset time */
	gd->tbl = read_timer();
	gd->tbu = 0;
}

ulong get_timer_masked(void)
{
	ulong now = read_timer();

	if (now >= gd->tbl) {
		/* normal mode */
		gd->tbu += now - gd->tbl;
	} else {
		/* we have an overflow ... */
		gd->tbu += now + TIMER_LOAD_VAL - gd->tbl;
	}
	gd->tbl = now;

	return gd->tbu;
}

void reset_timer(void)
{
	reset_timer_masked();
}

ulong get_timer(ulong base)
{
	return (get_timer_masked() / (CONFIG_SYS_HZ_CLOCK / 1000)) - base;
}

void set_timer(ulong t)
{
	gd->tbu = t;
}

void __udelay(unsigned long usec)
{
	ulong delayticks;
	ulong endtime;

	delayticks = (usec * (CONFIG_SYS_HZ_CLOCK / 1000000));
	endtime = get_timer_masked() + delayticks;

	while (get_timer_masked() < endtime)
		;
}

/*
 * init the Timer
 */
int timer_init(void)
{
	struct armd6apbc_registers *apb1clkres =
		(struct armd6apbc_registers *) AMAD6_APBC_BASE;
	struct armd6timer_registers *armd6timers =
		(struct armd6timer_registers *) AMAD6_TIMER_BASE;

	/* Enable Timer clock at 6.5 MHZ */
	writel(APBC_APBCLK | APBC_FNCLK | APBC_FNCLKSEL(1),
			&apb1clkres->timers);

	/* load value into timer */
	writel(0x0, &armd6timers->clk_ctrl);
	/* Use Timer 0 Match Resiger 0 */
	writel(TIMER_LOAD_VAL, &armd6timers->match[MATCH_CMP(0)]);
	/* Preload value is 0 */
	writel(0x0, &armd6timers->preload[TIMER]);
	/* Enable match comparator 0 for Timer 0 */
	writel(0x0, &armd6timers->preload_ctrl[TIMER]);

	/* Enable count 0 */
	writel(0x1, &armd6timers->cer);
	/* Set free run mode */
	writel(0x1, &armd6timers->cmr);

	/* init the gd->tbu and gd->tbl value */
	reset_timer_masked();

	return 0;
}
