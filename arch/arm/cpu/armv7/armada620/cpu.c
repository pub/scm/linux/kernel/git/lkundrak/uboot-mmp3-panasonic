/*
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */
#include <common.h>
#include <asm/io.h>
#include <asm/arch/cpu.h>
#include <asm/arch/armada620.h>

#define UARTCLK14745KHZ	(APBC_APBCLK | APBC_FNCLK | APBC_FNCLKSEL(1))
int arch_cpu_init(void)
{
	struct armd6mpmu_registers *mpmu =
		(struct armd6mpmu_registers *) AMAD6_MPMU_BASE;

	struct armd6apbc_registers *apbc =
		(struct armd6apbc_registers *) AMAD6_APBC_BASE;

	struct armd6apmu_registers *apmu =
		(struct armd6apmu_registers *) AMAD6_APMU_BASE;

	struct armd6cpu_registers *cpuregs =
		(struct armd6cpu_registers *) AMAD6_CPU_BASE;

	/* Start of day */
	writel(readl(&apmu->genctrl) | 0x70, &apmu->genctrl);
	writel(readl(&mpmu->sccr) | 0x1, &mpmu->sccr);
	writel(0x0, &apmu->sram_pd);

	writel(readl(&cpuregs->mp1_pd) & ~(0xfc00) | 0x5400, &cpuregs->mp1_pd);
	writel(readl(&cpuregs->mp2_pd) & ~(0xfc00) | 0x5400, &cpuregs->mp2_pd);
	writel(readl(&cpuregs->mm_pd) & ~(0xfc00) | 0x5400, &cpuregs->mm_pd);

	/* Turn off NF clock */
	writel(readl(&apmu->nf) & ~(0x3f), &apmu->nf);

	/* Turn off SMC clock */
	writel(readl(&apmu->smc) & ~(0x1f), &apmu->smc);

	/* Turn on clock gating (PMUM_CGR_PJ) */
	writel(0xFFFFFFFF, &mpmu->acgr);

	/* Turn on APB, PLL1, PLL2 clock */
	writel(0x1FFFF, &apmu->gbl_clkctrl);

	/* Turn on AIB clock */
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->aib);

	/* Turn on uart3 clock */
	writel(UARTCLK14745KHZ, &apbc->uart3);

	/* Turn on timer clock */
	writel(APBC_APBCLK | APBC_FNCLK | APBC_FNCLKSEL(0x3), &apbc->timers);

	/* Enable GPIO clock */
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->gpio);

	/* Enable twsi1,3,5,6 clock */
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->twsi1);
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->twsi3);
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->twsi5);
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->twsi6);

	icache_enable();

#ifdef CONFIG_MV_SDHCI
	/* Enable SD1 clock */
	writel(APMU_PERIPH_CLK_EN | APMU_AXI_CLK_EN | APMU_PERIPH_RESET | \
	       APMU_AXI_RESET | (1 << 10), &apmu->sd1);

	/* Enable SD3 clock */
	writel(APMU_PERIPH_CLK_EN | APMU_AXI_CLK_EN | APMU_PERIPH_RESET | \
	       APMU_AXI_RESET, &apmu->sd3);
#endif

	/* Enable USB clock */
	writel(USB_26M_SEL | APMU_PERIPH_CLK_EN | APMU_AXI_CLK_EN | \
	       APMU_PERIPH_RESET | APMU_AXI_RESET, &apmu->fsic3);
	writel(APMU_AXI_CLK_EN | APMU_AXI_RESET, &apmu->usb);
	writel(0xffff, &apmu->usb_dyn_gate);

#ifdef CONFIG_PXA168_FB
	/* Enable display clock */
	writel(APMU_PERIPH_CLK_EN | APMU_AXI_CLK_EN | APMU_PERIPH_RESET | \
		APMU_AXI_RESET | CLK_DIV_SEL(1) | CLK_SEL(2) | DSI_ESCCLK_EN |\
		DSI_PHY_SLOW_CLK_EN, &apmu->display1);
	writel(APBC_APBCLK | APBC_FNCLK, &apbc->pwm3);
	/*Enable PLL3 for DSI PLL as 1GHz*/
	writel(readl(&mpmu->pll3_ctl2) | SEL_VCO_CLK_SE, &mpmu->pll3_ctl2);
	writel(VCODIV_SEL_SE(0x2) | ICP(0x4) | KVCO(0x5) | CTUNE(0x2)
	      | VCO_VRNG(0x4) | VREG_IVREF(0x2) | VDDM(0x1) | VDDL(0x9),
	      &mpmu->pll3_ctl1);
	/*MPMU_PLL3CR: Program PLL3 VCO for 2.0Ghz -REFD = 3;*/
	if ((readl(&apmu->fsic3) & USB_PHY_REF_MASK) == USB_PHY_26M)
		writel(PLL3_REFDIV(0x3) | FBDIV(0xe6) | PLL3_ACTV_CNTRL,
		       &mpmu->pll3_cr);
	else
		writel(PLL3_REFDIV(0x3) | FBDIV(0xf6) | PLL3_ACTV_CNTRL,
		       &mpmu->pll3_cr);

	/* PLL3 Control register -Enable SW PLL3*/
	writel(readl(&mpmu->pll3_cr) | PLL3_SW_EN, &mpmu->pll3_cr);

	/* wait for PLLs to lock*/
	udelay(500);

	/* PMUM_PLL3_CTRL1: take PLL3 out of reset*/
	writel(readl(&mpmu->pll3_ctl1) | PLL_RST, &mpmu->pll3_ctl1);
	udelay(500);
#endif

#ifdef CONFIG_ARMADA100_FEC
	/* Enable Fast Ethernet clock */
	writel(FE_CLK_SRC_SEL | FE_CLKEN | FE_ACLKEN, &apmu->fastenet);
	udelay(500);
	writel(FE_CLK_SRC_SEL | FE_CLKEN | FE_ACLKEN | FE_SW_RSTN | FE_AXI_SW_RSTN,
		&apmu->fastenet);
#endif
	return 0;
}

int cpu_is_ax(void)
{
	struct armd6cpu_registers *cpuregs =
		(struct armd6cpu_registers *) AMAD6_CPU_BASE;
	u32 id;

	id = readl(&cpuregs->chip_id);
	return ((id >> 20) & 0xf) == 0xa;
}

int cpu_is_bx(void)
{
	struct armd6cpu_registers *cpuregs =
		(struct armd6cpu_registers *) AMAD6_CPU_BASE;
	u32 id;

	id = readl(&cpuregs->chip_id);
	return ((id >> 20) & 0xf) == 0xb;
}

#if defined(CONFIG_DISPLAY_CPUINFO)
extern void wtm_read_stepping(void);
extern unsigned int mv_soc_stepping;
int print_cpuinfo(void)
{
	u32 id;
	char *stepping;
	struct armd6cpu_registers *cpuregs =
		(struct armd6cpu_registers *) AMAD6_CPU_BASE;

	wtm_read_stepping();
	switch (mv_soc_stepping) {
	case 0x4130:
		stepping = "A0"; break;
	case 0x4131:
		stepping = "A1"; break;
	case 0x4132:
		stepping = "A2"; break;
	case 0x4230:
		stepping = "B0"; break;
	case 0x423050:
		stepping = "B0P"; break;
	default:
		stepping = "unknown"; break;
	}
	id = readl(&cpuregs->chip_id);
	printf("SoC:   ARMADA620 88AP%X-%s\n", (id & 0xFFFF), stepping);
	return 0;
}
#endif

#ifdef CONFIG_I2C_MV
void i2c_clk_enable(void)
{
}
#endif

#ifndef CONFIG_SYS_DCACHE_OFF
void enable_caches(void)
{
	/* Enable D-cache. I-cache is already enabled in start.S */
	dcache_enable();
}
#endif
