/*
 * (C) Copyright 2008 Texas Insturments
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * (C) Copyright 2002
 * Gary Jennejohn, DENX Software Engineering, <garyj@denx.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * CPU specific code
 */

#include <common.h>
#include <command.h>
#include <asm/system.h>
#include <asm/cache.h>
#include <asm/armv7.h>

void save_boot_params_default(u32 r0, u32 r1, u32 r2, u32 r3)
{
}

void save_boot_params(u32 r0, u32 r1, u32 r2, u32 r3)
	__attribute__((weak, alias("save_boot_params_default")));

int cleanup_before_linux(void)
{
	/*
	 * this function is called just before we call linux
	 * it prepares the processor for linux
	 *
	 * we turn off caches etc ...
	 */
	disable_interrupts();

	/*
	 * Turn off I-cache and invalidate it
	 */
	icache_disable();
	invalidate_icache_all();

	/*
	 * turn off D-cache
	 * dcache_disable() in turn flushes the d-cache and disables MMU
	 */
	dcache_disable();

	/*
	 * After D-cache is flushed and before it is disabled there may
	 * be some new valid entries brought into the cache. We are sure
	 * that these lines are not dirty and will not affect our execution.
	 * (because unwinding the call-stack and setting a bit in CP15 SCTRL
	 * is all we did during this. We have not pushed anything on to the
	 * stack. Neither have we affected any static data)
	 * So just invalidate the entire d-cache again to avoid coherency
	 * problems for kernel
	 */
	invalidate_dcache_all();

	return 0;
}


#ifdef CONFIG_CMD_MIPS

static unsigned long loops_per_sec;
#define PMNC_MASK	0x3f		/* Mask for writable bits */
#define PMNC_E		(1 << 0)	/* Enable all counters */
#define PMNC_P		(1 << 1)	/* Reset all counters */
#define PMNC_C		(1 << 2)	/* Cycle counter reset */
#define PMNC_D		(1 << 3)	/* CCNT counts every 64th cpu cycle */
#define PMNC_X		(1 << 4)	/* Export to ETM */
#define PMNC_DP		(1 << 5)	/* Disable CCNT if non-invasive debug*/

#define FLAG_C		(1 << 31)
#define FLAG_MASK	0x8000000f	/* Mask for writable bits */

#define CNTENC_C	(1 << 31)
#define CNTENC_MASK	0x8000000f	/* Mask for writable bits */

#define CNTENS_C	(1 << 31)
#define CNTENS_MASK	0x8000000f	/* Mask for writable bits */

static inline u32 armv7_pmnc_read(void)
{
	u32 val;

	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r" (val));
	return val;
}

static inline void armv7_pmnc_write(u32 val)
{
	val &= PMNC_MASK;
	asm volatile("mcr p15, 0, %0, c9, c12, 0" : : "r" (val));
}

static inline void armv7_pmnc_disable_counter(void)
{
	u32 val;

	val = CNTENC_C;

	val &= CNTENC_MASK;
	asm volatile("mcr p15, 0, %0, c9, c12, 2" : : "r" (val));
}

static void armv7_pmnc_reset_counter(void)
{
	u32 val = 0;

	asm volatile("mcr p15, 0, %0, c9, c13, 0" : : "r" (val));
}


static inline void armv7_pmnc_enable_counter(void)
{
	u32 val;

	val = CNTENS_C;
	val &= CNTENS_MASK;
	asm volatile("mcr p15, 0, %0, c9, c12, 1" : : "r" (val));
}

static inline void armv7_start_pmnc(void)
{
	armv7_pmnc_write(armv7_pmnc_read() | PMNC_E);
}

static inline void armv7_stop_pmnc(void)
{
	armv7_pmnc_write(armv7_pmnc_read() & ~PMNC_E);
}

static inline u32 armv7_counter_read(void)
{
	u32 val;

	asm volatile("mrc p15, 0, %0, c9, c13, 0" : "=r" (val));
	return val;
}

static inline u32 armv7_pmnc_getreset_flags(void)
{
	u32 val;

	/* Read */
	asm volatile("mrc p15, 0, %0, c9, c12, 3" : "=r" (val));

	/* Write to clear flags */
	val &= FLAG_MASK;
	asm volatile("mcr p15, 0, %0, c9, c12, 3" : : "r" (val));

	return val;
}

int do_calibrate_delay(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	ulong oldtimer, timer;
	unsigned int val, val_new;
	unsigned int flags;

	loops_per_sec = 0;
	printf("Calibrating delay loop.. ");

	armv7_pmnc_write(PMNC_P | PMNC_C);
	armv7_pmnc_disable_counter();
	armv7_pmnc_reset_counter();
	armv7_pmnc_enable_counter();
	val = armv7_counter_read();
	armv7_start_pmnc();

	oldtimer = get_timer_masked();
	while (1) {
		timer = get_timer_masked();
		timer = timer - oldtimer;
		if (timer >= CONFIG_SYS_HZ_CLOCK)
			break;
	}

	armv7_stop_pmnc();
	val_new = armv7_counter_read();
	flags = armv7_pmnc_getreset_flags();
	if (flags & FLAG_C)
		printf("counter overflow\n");
	loops_per_sec = val_new - val;

	printf("ok - %lu.%02lu BogoMips\n",
			loops_per_sec/1000000,
			(loops_per_sec/10000) % 100);

	return 0;
}

U_BOOT_CMD(
		mips,	6,	1,	do_calibrate_delay,
		"mips	- calculating BogoMips\n",
		" - calculating BogoMips\n"
	  );

#endif
