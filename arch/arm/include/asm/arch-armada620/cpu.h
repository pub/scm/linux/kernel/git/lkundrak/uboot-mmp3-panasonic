/*
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#ifndef _AMAD6_CPU_H
#define _AMAD6_CPU_H

#include <asm/io.h>
#include <asm/system.h>

/*
 * Main Power Management (MPMU) Registers
 * Refer Register Datasheet 9.1
 */
struct armd6mpmu_registers {
	u8 pad0[0x38];
	u32 sccr;	/*0x0038*/
	u8 pad1[0x50 - 0x38 - 4];
#define PLL3_REFDIV(x)		(((x) & 0x1f) << 19)
#define FBDIV(x)		(((x) & 0x1ff) << 10)
#define PLL3_ACTV_CNTRL		(0x1 << 9)
#define PLL3_SW_EN		(0x1 << 8)
	u32 pll3_cr;	/*0x0050*/
	u8 pad2[0x58 - 0x50 - 4];
#define PLL_RST			(0x1 << 29)
#define VCODIV_SEL_SE(x)	(((x) & 0x7) << 25)
#define ICP(x)			(((x) & 0x7) << 22)
#define KVCO(x)			(((x) & 0x7) << 19)
#define CTUNE(x)		(((x) & 0x3) << 15)
#define VCO_VRNG(x)		(((x) & 0x7) << 8)
#define VREG_IVREF(x)		(((x) & 0x3) << 6)
#define VDDM(x)			(((x) & 0x3) << 4)
#define VDDL(x)			(x & 0xf)
	u32 pll3_ctl1;	/*0x0058*/
	u8 pad3[0x60 - 0x58 - 4];
#define SEL_VCO_CLK_SE		0x1
	u32 pll3_ctl2;	/*0x0060*/
	u8 pad4[0x1024 - 0x60 - 4];
	u32 acgr;	/*0x1024*/
};

/*
 * APB Clock Reset/Control Registers
 * Refer Register Datasheet 6.14
 */
struct armd6apbc_registers {
 	u8 pad0[0x004];
 	u32 twsi1;	/*0x004*/
	u8 pad1[4];
	u32 twsi3;	/*0x00c*/
	u8 pad2[0x024 - 0x0c - 4];
 	u32 timers;	/*0x024*/
	u8 pad3[0x034 - 0x24 - 4];
 	u32 uart3;	/*0x034*/
 	u32 gpio;	/*0x038*/
	u8 pad4[0x044 - 0x38 - 4];
 	u32 pwm3;	/*0x044*/
	u8 pad5[0x064 - 0x44 - 4];
 	u32 aib;	/*0x064*/
	u8 pad6[0x07c - 0x64 - 4];
 	u32 twsi5;	/*0x07c*/
 	u32 twsi6;	/*0x080*/
};

/*
 * Application Subsystem PMU(APMU) Registers
 * Refer Register Datasheet Appendix A.24
 */
struct armd6apmu_registers {
	u8 pad0[0x034];
	u32 usb_dyn_gate;	/*0x034*/
	u8 pad1[0x04c - 0x34 - 4];
	u32 display1;		/*0x04c*/
	u8 pad2[0x054 - 0x4c - 4];
	u32 sd1;		/*0x054*/
	u8 pad3[0x05c - 0x54 - 4];
	u32 usb;		/*0x05c*/
	u8 pad4[0x060 - 0x5c - 4];
	u32 nf;			/*0x060*/
	u8 pad5[0x08c - 0x60 - 4];
	u32 sram_pd;		/*0x08c*/
	u8 pad6[0x0d4 - 0x8c - 4];
	u32 smc;		/*0x0d4*/
	u32 mspro;
	u32 gbl_clkctrl;	/*0x0dc*/
	u8 pad7[0x0e8 - 0xdc - 4];
	u32 sd3;		/*0x0e8*/
	u8 pad8[0x100 - 0xe8 - 4];
#define USB_PHY_REFCLK_SEL(x)	((x & 0xf) << 8)  /* select as 26m ref clock*/
#define USB_PHY_REF_MASK	(0xf << 8)
#define USB_PHY_26M		USB_PHY_REFCLK_SEL(0xd)
	u32 fsic3;		/*0x100*/
	u8 pad9[0x210 - 0x100 - 4];
	u32 fastenet;		/*0x210*/
	u8 pad10[0x244 - 0x210 - 4];
	u32 genctrl;		/*0x244*/
};

/*
 * Timer registers
 * Refer 6.2.9 in Datasheet
 */
struct armd6timer_registers {
	u32 clk_ctrl;	/* Timer clk control reg */
	u32 match[9];	/* Timer match registers */
	u32 count[3];	/* Timer count registers */
	u32 status[3];
	u32 ie[3];
	u32 preload[3];	/* Timer preload value */
	u32 preload_ctrl[3];
	u32 wdt_match_en;
	u32 wdt_match_r;
	u32 wdt_val;
	u32 wdt_sts;
	u32 icr[3];
	u32 wdt_icr;
	u32 cer;	/* Timer count enable reg */
	u32 cmr;
	u32 ilr[3];
	u32 wcr;
	u32 wfar;
	u32 wsar;
	u32 cvwr[3];
};

struct armd6cpu_registers {
	u32 chip_id;            /* Chip Id Reg */
	u8 pad0[0x7c - 4];
	u32 mp1_pd;		/*0x7c*/
	u32 mp2_pd;		/*0x80*/
	u32 mm_pd;		/*0x84*/
};

/*
 * DDR Memory Control Registers
 * Refer Datasheet 4.4
 */
struct armd6ddr_map_registers {
	u32	cs;	/* Memory Address Map Register -CS */
};

struct armd6ddr_registers {
	u8	pad[0x10];
	struct armd6ddr_map_registers mmap[4];
};

/*
 * Functions
 */
u32 armd6_sdram_base(int, u32);
u32 armd6_sdram_size(int, u32);
u32 dram_interleave_size(void);

int mv_sdh_init(u32 regbase, u32 max_clk, u32 min_clk, u32 quirks);
int cpu_is_ax(void);
int cpu_is_bx(void);

/* armada620 special mmc tuning register */
#define SD_FIFO_PARAM			0x104
#define  DIS_PAD_SD_CLK_GATE	(1 << 10)
#define  CLK_GATE_ON		(1 << 9)
#define  CLK_GATE_CTL		(1 << 8)
#define  WTC_DEF		0x1
#define  WTC(x)			((x & 0x3) << 2)
#define  RTC_DEF		0x1
#define  RTC(x)			(x & 0x3)

#define SD_CLOCK_AND_BURST_SIZE_SETUP	0x10a
#define  SDCLK_DELAY(x)		((x & 0x1f) << 9)
#define  SDCLK_SEL		(1 << 8)
#define  WR_ENDIAN		(1 << 7)
#define  RD_ENDIAN		(1 << 6)
#define  DMA_FIFO_128		1
#define  DMA_SIZE(x)		((x & 0x3) << 2)
#define  BURST_64		1
#define  BURST_SIZE(x)		(x & 0x3)

#define RX_CFG_REG              0x114
#define TUNING_DLY_INC(x)       ((x & 0x1ff) << 17)
#define SDCLK_DELAY(x)          ((x & 0x1ff) << 8)
#define SDCLK_SEL0(x)           ((x & 0x3) << 0)
#define SDCLK_SEL1(x)           ((x & 0x3) << 2)

#define TX_CFG_REG              0x118
#define TX_HOLD_DELAY0(x)       ((x & 0x1ff) << 0)
#define TX_HOLD_DELAY1(x)       ((x & 0x1ff) << 16)
#endif /* _AMAD6_CPU_H */
