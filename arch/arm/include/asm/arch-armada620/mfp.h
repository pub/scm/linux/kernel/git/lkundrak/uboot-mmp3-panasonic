/*
 * Based on arch/arm/include/asm/arch-armada100/mfp.h
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#ifndef __AMAD6_MFP_H
#define __AMAD6_MFP_H

/*
 * Frequently used MFP Configuration macros for all PANTHEON family of SoCs
 *
 * offset, pull,pF, drv,dF, edge,eF ,afn,aF
 */
/* UART3 */
#define UART3_RXD		(MFP_REG(0x120) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define UART3_TXD		(MFP_REG(0x124) | MFP_AF1 | MFP_DRIVE_MEDIUM)

/* TWSI1 */
#define TWSI1_SCL		(MFP_REG(0x140) | MFP_AF0 | MFP_DRIVE_SLOW)
#define TWSI1_SDA		(MFP_REG(0x144) | MFP_AF0 | MFP_DRIVE_SLOW)

/* TWSI3 */
#define TWSI3_SCL		(MFP_REG(0x2B0) | MFP_AF1 | MFP_DRIVE_SLOW)
#define TWSI3_SDA		(MFP_REG(0x2B4) | MFP_AF1 | MFP_DRIVE_SLOW)


/* TWSI53 */
#define TWSI3_SCL		(MFP_REG(0x2B0) | MFP_AF1 | MFP_DRIVE_SLOW)
#define TWSI3_SDA		(MFP_REG(0x2B4) | MFP_AF1 | MFP_DRIVE_SLOW)

/* TWSI5 */
#define TWSI5_SCL		(MFP_REG(0x1D4) | MFP_AF4 | MFP_DRIVE_SLOW)
#define TWSI5_SDA		(MFP_REG(0x1D8) | MFP_AF4 | MFP_DRIVE_SLOW)

/* TWSI6 */
#define TWSI6_SCL		(MFP_REG(0x1CC) | MFP_AF2 | MFP_DRIVE_SLOW)
#define TWSI6_SDA		(MFP_REG(0x1D0) | MFP_AF2 | MFP_DRIVE_SLOW)

/* MMC1 */
#define MMC1_DATA3		(MFP_REG(0x28) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define MMC1_DATA2		(MFP_REG(0x2c) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define MMC1_DATA1		(MFP_REG(0x30) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define MMC1_DATA0		(MFP_REG(0x34) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define MMC1_CLK		(MFP_REG(0x38) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define MMC1_CMD		(MFP_REG(0x3c) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define MMC1_CD			(MFP_REG(0x4c) | MFP_AF1 | MFP_PULL_HIGH)
#define MMC1_WP			(MFP_REG(0x50) | MFP_AF1 | MFP_PULL_HIGH)
#define MMC1_GPIO13		(MFP_REG(0x80) | MFP_AF0 | MFP_PULL_LOW)
#define MMC1_GPIO138		(MFP_REG(0x44) | MFP_AF0 | MFP_PULL_LOW)
/* MMC3 */
#define MMC3_DATA7		(MFP_REG(0x21c) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA6		(MFP_REG(0x218) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA5		(MFP_REG(0x210) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA4		(MFP_REG(0x208) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA3		(MFP_REG(0x200) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA2		(MFP_REG(0x214) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA1		(MFP_REG(0x20c) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA0		(MFP_REG(0x204) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_CLK		(MFP_REG(0x228) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_CMD		(MFP_REG(0x22c) | MFP_AF2 | MFP_DRIVE_MEDIUM)

#define MMC3_DATA7_NDIO15	(MFP_REG(0x21c) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA6_NDIO14	(MFP_REG(0x218) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA5_NDIO12	(MFP_REG(0x210) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA4_NDIO10	(MFP_REG(0x208) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA3_NDIO8	(MFP_REG(0x200) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA2_NDIO13	(MFP_REG(0x214) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA1_NDIO11	(MFP_REG(0x20c) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA0_NDIO9	(MFP_REG(0x204) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_CLK_SMNCS1		(MFP_REG(0x22c) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_CMD_SMNCS0		(MFP_REG(0x228) | MFP_AF2 | MFP_DRIVE_MEDIUM)

/* Fast Ethernet */
#define FE_LED0_N               (MFP_REG(0x020) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define FE_LED1_N               (MFP_REG(0x024) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define FE_LED2_N               (MFP_REG(0x048) | MFP_AF1 | MFP_DRIVE_MEDIUM)

/* LCD */
#define LCD_RESET		(MFP_REG(0x01C) | MFP_AF0 | MFP_DRIVE_FAST)
#define BACK_LIGHT_PWM3    (MFP_REG(0x128) | MFP_AF5 | MFP_DRIVE_MEDIUM)
#define BL_POWER_EN		(MFP_REG(0x98) | MFP_AF0 | MFP_DRIVE_FAST)
#define GPIO152_VLCD_3V3		(MFP_REG(0x248) | MFP_AF1 | MFP_DRIVE_FAST | MFP_PULL_LOW)

/* HDMI 5v Power */
#define GPIO_160                (MFP_REG(0x250) | MFP_AF1 | MFP_PULL_HIGH)

/* Backlight  enable*/
#define GPIO_17                 (MFP_REG(0x98) | MFP_AF0 | MFP_PULL_HIGH)

/* LVDS mode detection*/
#define GPIO_19                 (MFP_REG(0xA0) | MFP_AF0 | MFP_PULL_HIGH)

/* MENU Key*/
#define GPIO_147		(MFP_REG(0x230) | MFP_AF1 | MFP_PULL_HIGH) 

/*PWM3*/
#define GPIO53_PWM3             (MFP_REG(0x128) | MFP_AF5 | MFP_DRIVE_MEDIUM) //MFP_CFG_X(GPIO53, AF5, SLOW, PULL_LOW)
#define GPIO53_GPIO             (MFP_REG(0x128) | MFP_AF0 | MFP_PULL_HIGH) 

/*MODEM power control pins, William Liu*/
#define GPIO93_GPIO (MFP_REG(0x1bc) | MFP_AF0 | MFP_PULL_NONE)
#define GPIO94_GPIO (MFP_REG(0x1c0) | MFP_AF0 | MFP_PULL_HIGH)
#define GPIO95_GPIO (MFP_REG(0x1c4) | MFP_AF0 | MFP_PULL_HIGH)
#define GPIO129_GPIO (MFP_REG(0x020) | MFP_AF0 | MFP_PULL_HIGH)

/* More macros can be defined here... */

#define MFP_GPIO150		(MFP_REG(0x23C) | MFP_AF1 | MFP_PULL_LOW)

#define MFP_PIN_MAX	117
#endif /* __AMAD6_MFP_H */
