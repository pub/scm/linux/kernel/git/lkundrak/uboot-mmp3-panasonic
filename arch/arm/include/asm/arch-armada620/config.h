/*
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#ifndef _AMAD6_CONFIG_H
#define _AMAD6_CONFIG_H

#include <asm/arch/armada620.h>
#undef CONFIG_SYS_LOAD_ADDR

#define CONFIG_SYS_TCLK		(26000000)	/* NS16550 clk config */
#define CONFIG_SYS_HZ_CLOCK	(6500000)	/* Timer Freq. 6.5MHZ */
#ifdef CONFIG_TZ_HYPERVISOR
#define CONFIG_LOADADDR		0x207fc0	/* Default load place */
#else
#define CONFIG_LOADADDR		0x7fc0		/* Default load place */
#endif
#define CONFIG_SYS_LOAD_ADDR	CONFIG_LOADADDR
#define CONFIG_MARVELL_MFP			/* Enable mvmfp driver */
#define CONFIG_SYS_TIMERBASE	AMAD6_TIMER_BASE
#define MV_MFPR_BASE		AMAD6_MFPR_BASE
#define MV_UART_CONSOLE_BASE	AMAD6_UART3_BASE
/* The max cs num in one controller*/
#define CONFIG_DRAM_CSNUM_MAX	2
#define CONFIG_SYS_NS16550_IER	(1 << 6)	/* Bit 6 in UART_IER register
						represents UART Unit Enable */

#ifndef CONFIG_FIX_RAMSIZE
#define CONFIG_FIX_RAMSIZE	(384 * 1024 * 1024)
#endif
/*
 * I2C definition
 */
#ifdef CONFIG_CMD_I2C
#define CONFIG_I2C_MV		1
//#define CONFIG_MV_I2C_NUM	4
#define CONFIG_MV_I2C_NUM	6
#define CONFIG_I2C_MULTI_BUS	1
//#define CONFIG_MV_I2C_REG	{0xd4011000, 0xd4032000, 0xd4033800, 0xd4034000}
#define CONFIG_MV_I2C_REG	{0xd4011000, 0xd4031000, 0xd4032000,\
	0xd4033000, 0xd4033800, 0xd4034000}
//i2c 1->2
//    2->4
//    3->5

#define CONFIG_HARD_I2C		1
#define CONFIG_SYS_I2C_SPEED	0
#define CONFIG_SYS_I2C_SLAVE	0xfe
#endif

/*
 * MMC definition
 */
#ifdef CONFIG_CMD_MMC
#define CONFIG_CMD_FAT		1
#define CONFIG_MMC              1
#define CONFIG_GENERIC_MMC      1
#define CONFIG_SDHCI		1
#define CONFIG_MMC_SDMA		1
#define CONFIG_MV_SDHCI		1
#define CONFIG_DOS_PARTITION    1
#define CONFIG_EFI_PARTITION	1
#define CONFIG_SYS_MMC_NUM	2
#define CONFIG_SYS_MMC_BASE     {0xd4281000, 0xD4280000}
#define CONFIG_SYS_MMC_MAX_BLK_COUNT   100
#endif

#ifdef CONFIG_USB_ETHER
#define CONFIG_USB_GADGET_MV	1
#define CONFIG_USB_GADGET_DUALSPEED	1
#define CONFIG_CMD_NET		1
#define CONFIG_IPADDR		192.168.1.101
#define CONFIG_SERVERIP		192.168.1.100
#define CONFIG_ETHADDR		08:00:3e:26:0a:5b
#define CONFIG_NET_MULTI	1
#define CONFIG_USBNET_DEV_ADDR 	"00:0a:fa:63:8b:e8"
#define CONFIG_USBNET_HOST_ADDR	"0a:fa:63:8b:e8:0a"
#define CONFIG_MV_UDC		1
#define CONFIG_URB_BUF_SIZE	256
#define CONFIG_USB_REG_BASE	0xd4208000
#define CONFIG_USB_PHY_BASE	0xd4207000
#endif

#ifdef CONFIG_ARMADA100_FEC
#ifdef CONFIG_USB_ETHER
#define CONFIG_CMD_PING
#define CONFIG_MII
#define CONFIG_CMD_MII
#define CONFIG_SYS_FAULT_ECHO_LINK_DOWN
#else
#define CONFIG_CMD_NET		1
#define CONFIG_IPADDR           192.168.1.101
#define CONFIG_SERVERIP         192.168.1.100
#define CONFIG_ETHADDR          08:00:3e:26:0a:5b
#define CONFIG_NET_MULTI        1
#define CONFIG_CMD_PING
#define CONFIG_MII
#define CONFIG_CMD_MII
#define CONFIG_SYS_FAULT_ECHO_LINK_DOWN
#endif
#endif

#ifdef CONFIG_CMD_GPIO
#define CONFIG_MARVELL_GPIO    1
#endif

#ifdef CONFIG_CMD_FASTBOOT
#define CONFIG_USBD_VENDORID		0x18d1
#define CONFIG_USBD_PRODUCTID		0x4e11
#define CONFIG_USBD_MANUFACTURER	"Marvell Inc."
#define CONFIG_USBD_PRODUCT_NAME	"Android 2.1"
#define CONFIG_SERIAL_NUM		"MRUUUVLs001"
#define CONFIG_USBD_CONFIGURATION_STR	"fastboot"
#define CONFIG_SYS_FASTBOOT_ONFLY_SZ	0x40000
#define USB_LOADADDR			0x100000
#define CONFIG_FB_RESV			256
#endif

#define CONFIG_EXTRA_ENV_SETTINGS			\
	"autostart=yes\0"				\
	"verify=yes\0"					\
	"cdc_connect_timeout=60\0"

/*
 * LCD definition
 */
#ifdef CONFIG_PXA168_FB
#define FB_XRES			1024
#define FB_YRES			768
#define CONFIG_CMD_BMP		1
#define CONFIG_VIDEO		1
#define CONFIG_CFB_CONSOLE	1
#define VIDEO_KBD_INIT_FCT	-1
#define VIDEO_TSTC_FCT		serial_tstc
#define VIDEO_GETC_FCT		serial_getc
#define LCD_RST_GPIO		128
#define CONFIG_VIDEO_BMP_GZIP
#define CONFIG_SYS_VIDEO_LOGO_MAX_SIZE	(262144)
#endif

#endif /* _AMAD6_CONFIG_H */
