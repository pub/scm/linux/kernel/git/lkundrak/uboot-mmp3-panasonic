/*
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#ifndef _AMAD6_H
#define _AMAD6_H

/* Common APB clock register bit definitions */
#define APBC_APBCLK		(1<<0)  /* APB Bus Clock Enable */
#define APBC_FNCLK		(1<<1)  /* Functional Clock Enable */
#define APBC_RST		(1<<2)  /* Reset Generation */
/* Functional Clock Selection Mask */
#define APBC_FNCLKSEL(x)        (((x) & 0xf) << 4)

/* Common APMU clock register bit definitions */
#define APMU_PERIPH_CLK_EN	(1<<4)	/* Peripheral clock enable */
#define APMU_AXI_CLK_EN		(1<<3)	/* AXI clock enable */
#define APMU_PERIPH_RESET	(1<<1)	/* Peripheral reset */
#define APMU_AXI_RESET		(1<<0)	/* AXI BUS reset */
#define USB_26M_SEL		(0xd << 8)	/* select as 26m ref clock*/

/* Fast Ethernet Clock register bit definitions*/
#define FE_CLK_SRC_SEL		(1<<6) /* Fast Ethernet clock option */
#define FE_CLKEN		(1<<4) /* Fast Ethernet clock enable */
#define FE_ACLKEN		(1<<3) /* Fast Ethernet AXI clock enable */
#define FE_SW_RSTN		(1<<1) /* Fast Ethernet software reset */
#define FE_AXI_SW_RSTN		(1<<0) /* Fast Ethernet AXI interface software reset */

/* Display Control 1 Clock/Rest Control Register */
#define CLK_DIV_SEL(x)		((x)<<8)
#define CLK_SEL(x)		((x)<<6)
#define DIS_CLK_PLL_SEL		(1<<20)
#define DSI_ESCCLK_EN		(1<<12)
#define DSI_PHY_SLOW_CLK_EN	(1<<5)

/* Register Base Addresses */
#define AMAD6_DRAM_BASE		0xD0000000
#define AMAD6_FEC_BASE		0xD427F000
#define AMAD6_TIMER_BASE	0xD4014000
#define AMAD6_APBC_BASE		0xD4015000
#define AMAD6_UART3_BASE	0xD4018000
#define AMAD6_GPIO_BASE		0xD4019000
#define AMAD6_MFPR_BASE		0xD401E000
#define AMAD6_MPMU_BASE		0xD4050000
#define AMAD6_APMU_BASE		0xD4282800
#define AMAD6_CPU_BASE		0xD4282C00
#define ARAD6_GPIO_BASE		0xD4019000

/* pxa2128 has upto 169 gpio control bit */
#define MV_MAX_GPIO            169
#endif /* _AMAD6_H */
