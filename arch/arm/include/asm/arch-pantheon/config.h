/*
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#ifndef _PANTHEON_CONFIG_H
#define _PANTHEON_CONFIG_H

#include <asm/arch/pantheon.h>

#define CONFIG_ARM926EJS	1	/* Basic Architecture */

#define CONFIG_SYS_TCLK		(14745600)	/* NS16550 clk config */
#define CONFIG_SYS_HZ_CLOCK	(3250000)	/* Timer Freq. 3.25MHZ */
#define CONFIG_MARVELL_MFP			/* Enable mvmfp driver */
#define MV_MFPR_BASE		PANTHEON_MFPR_BASE
#define MV_UART_CONSOLE_BASE	PANTHEON_UART1_BASE
#define CONFIG_SYS_NS16550_IER	(1 << 6)	/* Bit 6 in UART_IER register
						represents UART Unit Enable */
/*
 * I2C definition
 */
#ifdef CONFIG_CMD_I2C
#define CONFIG_I2C_MV			1
#define CONFIG_MV_I2C_REG		0xd4011000
#define CONFIG_HARD_I2C			1
#define CONFIG_SYS_I2C_SPEED		0
#define CONFIG_SYS_I2C_SLAVE		0xfe
#endif

/*
 * MMC definition
 */
#ifdef CONFIG_CMD_MMC
#define CONFIG_CMD_FAT			1
#define CONFIG_MMC			1
#define CONFIG_GENERIC_MMC		1
#define CONFIG_SDHCI			1
#define CONFIG_MMC_SDHCI_IO_ACCESSORS	1
#define CONFIG_SYS_MMC_MAX_BLK_COUNT	0x1000
#define CONFIG_MMC_SDMA			1
#define CONFIG_MV_SDHCI			1
#define CONFIG_DOS_PARTITION		1
#define CONFIG_EFI_PARTITION		1
#define CONFIG_SYS_MMC_NUM		2
#define CONFIG_SYS_MMC_BASE		{0xD4280000, 0xd4281000}
#endif

/*
 * MMC definition
 */
#ifdef CONFIG_USB_ETHER
#define CONFIG_USB_GADGET_MV	1
#define CONFIG_USB_GADGET_DUALSPEED	1
#define CONFIG_CMD_NET		1
#define CONFIG_IPADDR		192.168.1.101
#define CONFIG_SERVERIP		192.168.1.100
#define CONFIG_ETHADDR		08:00:3e:26:0a:5b
#define CONFIG_NET_MULTI	1
#define CONFIG_USBNET_DEV_ADDR 	"00:0a:fa:63:8b:e8"
#define CONFIG_USBNET_HOST_ADDR	"0a:fa:63:8b:e8:0a"
#define CONFIG_MV_UDC		1
#define CONFIG_URB_BUF_SIZE	256
#define CONFIG_USB_REG_BASE	0xd4208000
#define CONFIG_USB_PHY_BASE	0xd4207000
#endif

#ifdef CONFIG_CMD_FASTBOOT
#define CONFIG_USBD_VENDORID		0x18d1
#define CONFIG_USBD_PRODUCTID		0x4e11
#define CONFIG_USBD_MANUFACTURER	"Marvell Inc."
#define CONFIG_USBD_PRODUCT_NAME	"Android 2.1"
#define CONFIG_SERIAL_NUM		"MRUUUVLs001"
#define CONFIG_USBD_CONFIGURATION_STR	"fastboot"
#define CONFIG_SYS_FB_YAFFS		{"cache", "system", "userdata", "telephony"}
#define CONFIG_SYS_FASTBOOT_ONFLY_SZ	0x40000
#define USB_LOADADDR			0x100000
#endif

#define CONFIG_EXTRA_ENV_SETTINGS			\
	"autostart=yes\0"				\
	"verify=no\0"					\
	"cdc_connect_timeout=60\0"

#endif /* _PANTHEON_CONFIG_H */
