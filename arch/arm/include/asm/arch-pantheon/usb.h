/*
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#ifndef _PANTHEON_USB_H
#define _PANTHEON_USB_H

/*
 * USB OTG PHY(UTMI) Registers
 * Refer Register Datasheet A.38
 */
struct usb_file {
	u32 pad0;		/* 0x00 */
#define BIT160			0x1
#define USB_CTL_29_28(x)	((x & 0x3) << 28)
#define PU_REF			(1 << 20)
#define REG_ARC_DPDM_MODE	(1 << 12)
#define PU_PLL			(1 << 1)
#define PU			(1 << 0)
	u32 UTMI_CTRL;		/* 0x04 */
#define ALLSET			0x3
#define PLLCALI12(x)		((x & 0x3) << 29)
#define PLLVDD18(x)		((x & 0x3) << 27)
#define PLLVDD12(x)		((x & 0x3) << 25)
#define PLLREADY		(1 << 23)
#define MIDKVCO			0x3
#define KVCO(x)			((x & 0x7) << 15)
#define MUA10			1
#define ICP(x)			((x & 0x7) << 12)
#define DIV240			0xee
#define FBDIV(x)		((x & 0xff) << 4)
#define DIV13			0xb
#define REFDIV(x)		(x & 0xf)
	u32 UTMI_PLL;		/* 0x08 */
#define FS_DEF			0x8
#define REG_EXT_FS_RCAL(x)	((x & 0xf) << 27)
#define VDD_DEF			0x1
#define VDD_ALL			0x3
#define TXVDD18(x)		((x & 0x3) << 24)
#define TXVDD12(x)		((x & 0x3) << 22)
#define TXDATA_BLOCK_EN		(1 << 21)
#define CLK60VAL		0x4
#define CK60_PHSEL(x)		((x & 0xf) << 17)
#define VTH45OHM		0x4
#define IMPCAL_VTH(x)		((x & 0x7) << 14)
#define HSVAL			0xf
#define HSDRV_EN(x)		((x & 0xf) << 7)
#define HS45OHM			0x8
#define REG_EXT_HS_RCAL(x)	((x & 0xf) << 3)
#define MA18			0x3
#define AMP(x)			(x & 0x7)
	u32 UTMI_TX;		/* 0x0c */
#define EARLY_VOS_ON_EN		(1 << 31)
#define RXDATA_BLOCK_EN		(1 << 30)
#define DEFLEN			0x2
#define RXDATA_BLOCK_LENGTH(x)	((x & 0x3) << 28)
#define EDGE_DET_EN		(1 << 27)
#define CLK_16CYCLE		0x2
#define S2TO3_DLY_SEL(x)	((x & 0x3) << 25)
#define PHASE_FREEZE_DLY	(1 << 20)
#define REG_USQ_LENGTH		(1 << 19)
#define REG_ACQ_LENGTH(x)	((x & 0x3) << 17)
#define SQUELCH_9HIGH		0x2
#define REQ_SQ_LENGTH(x)	((x & 0x3) << 15)
#define VL2425_VH2925		0x2
#define DISCON_THRESH(x)	((x & 0x3) << 8)
#define VOSL360_V100H450_V150H583	0x7
#define SQ_THRESH(x)		((x & 0xf) << 4)
#define MU20			1
#define INTPI(x)		(x & 0x3)
	u32 UTMI_RX;		/* 0x10 */
#define V145			0x1
#define RXVDD12(x)		((x & 0x3) << 6)
#define ALLEN			0xf
#define FSDRV_EN(x)		((x & 0xf) << 2)
#define REGDEF			0x2
#define REG_IMP_CAL_DLY(x)	(x & 0x3)
	u32 UTMI_IVREF;		/* 0x14 */
	u32 pad1[(0x30 - 0x14 - 4) / 4];
	u32 UTMI_CTL1;		/* 0x30 */
	u32 pad2[(0x3c - 0x30 - 4) / 4];
#define TEST_GRP_5_VALUE	0xfedcba98
	u32 UTMI_TEST_GRP_5;	/* 0x3c */
};

#endif /* _PANTHEON_USB_H */
