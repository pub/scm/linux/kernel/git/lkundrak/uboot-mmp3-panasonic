/*
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#ifndef _AMAD6_USB_H
#define _AMAD6_USB_H

/*
 * Main Power Management (MPMU) Registers
 * Refer Register Datasheet 9.1
 */
#ifdef CONFIG_MACH_BROWNSTONE
struct usb_file {
	u32 pad0;		/* 0x00 */
	u32 ctrl_reg;		/* 0x04 */
#define POWER_UP	(1)
#define PLL_POWER_UP	(1 << 1)
#define PU_REF		(1 << 20)
#define USB_CTL_29_28(x)	((x) << 28)
#define USB_CTL_29_28_MASK	0x30000000
	u32 pll_reg;		/* 0x08 */
#define REFDIV_MASK	0xf
#define REFDIV(x)	((x) - 2)
#define FBDIV_MASK	0xff0
#define FBDIV(x)	(((x) - 2) << 4)
#define ICP(x)		(((x)/5 - 1) << 12)
#define ICP_MASK	0x7000
#define PLL_READY	(1 << 23)
#define PLLVDD12_MASK	0x6000000
#define PLLVDD12(x)	((x) << 25)
#define PLLVDD18_MASK	0x18000000
#define PLLVDD18(x)	((x) << 27)
#define PLLCALLI12_MASK	0x60000000
#define PLLCALLI12(x)	((x) << 29)
#define VCOCAL_START	(1 << 21)
	u32 tx_reg;		/* 0x0c */
#define IMPCAL_VTH_MASK	0x1c000
#define IMPCAL_VTH(x)	((x) << 14)
#define	CK60_PHSEL_Mask	0x1e0000
#define	CK60_PHSEL(x)	((x) << 17)
#define TXVDD12_MASK	0xc00000
#define TXVDD12(x)	((x) << 22)
#define TXDATA_BLOCK_EN (1 << 21)
#define REG_RCAL_START	(1 << 12)
	u32 rx_reg;		/* 0x10 */
	u32 ivref_reg;		/* 0x14 */
	u32 test_group_0;	/* 0x18 */
	u32 test_group_1;	/* 0x1c */
	u32 test_group_2;	/* 0x20 */
	u32 id_grp;		/* 0x24 */
	u32 usb_int;		/* 0x28 */
	u32 dbg_ctl;		/* 0x2c */
	u32 ctl1;		/* 0x30 */
	u32 test_group_3;	/* 0x34 */
	u32 test_group_4;	/* 0x38 */
	u32 test_group_5;	/* 0x3c */
};
#else
struct usb_file {
	u32 usb_id;		/* 0x00 */
#define REFDIV_MASK	0x0f00
#define FB_DIV_MASK	0x00ff
#define VDD18(x)	((x & 0x3) << 14)
#define VDD12(x)	((x & 0x3) << 12)
#define REFDIV(x)	((x << 8) & REFDIV_MASK)
#define FB_DIV(x)	(x & FB_DIV_MASK)
	u32 pll_reg0;		/* 0x04 */
#define PLL_READY	0x8000
#define PLL_ICP_MASK	0x0700
#define PLL_KVCO_MASK	0x0070
#define PLL_CALI12_MASK	0x0003
#define UTMI_PLL_PU	0x2000
#define PLL_ICP(x)	((x << 8) & PLL_ICP_MASK)
#define PLL_KVCO(x)	((x << 4) & PLL_KVCO_MASK)
#define VCOCAL_START	0x0004
#define PLL_CALI12(x)	(x & PLL_CALI12_MASK)
	u32 pll_reg1;		/* 0x08 */
	u32 pad0;
#define RCAL_START	0x2000
#define IMPCAL_VTH_MASK	0x0700
#define IMPCAL_VTH(x)	((x << 8) & IMPCAL_VTH_MASK)
	u32 tx_reg0;		/* 0x10 */
#define CK60_PHSEL_MASK	0x000f
#define AMP_MASK	0x0070
#define TX_VDD12_MASK	0x0300
#define CK60_PHSEL(x)	(x & CK60_PHSEL_MASK)
#define AMP(x)		((x << 4) & AMP_MASK)
#define TX_VDD12(x)	((x << 8) & TX_VDD12_MASK)
	u32 tx_reg1;		/* 0x14 */
#define DRV_SLEWRATE(x)	((x & 0x3) << 10)
	u32 tx_reg2;		/* 0x18 */
	u32 pad1;
#define SQ_LENGTH_MASK	0x0c00
#define SQ_THRESH_MASK	0x00f0
#define SQ_LENGTH(x)	((x << 10) & SQ_LENGTH_MASK)
#define SQ_THRESH(x)	((x << 4) & SQ_THRESH_MASK)
	u32 rx_reg0;		/* 0x20 */
	u32 pad2[4];
#define ANA_PU		0x4000
	u32 ana_reg1;		/* 0x34 */
	u32 pad3[9];
#define PU_OTG		0x0008
	u32 otg_reg0;		/* 0x5c */
};
#endif
#endif /* _AMAD6_CPU_H */
