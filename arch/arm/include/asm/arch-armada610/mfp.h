/*
 * Based on arch/arm/include/asm/arch-armada100/mfp.h
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#ifndef __AMAD6_MFP_H
#define __AMAD6_MFP_H

/*
 * Frequently used MFP Configuration macros for all PANTHEON family of SoCs
 *
 * offset, pull,pF, drv,dF, edge,eF ,afn,aF
 */
/* UART3 */
#define UART3_RXD		(MFP_REG(0x120) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define UART3_TXD		(MFP_REG(0x124) | MFP_AF1 | MFP_DRIVE_MEDIUM)

/* TWSI5 */
#define TWSI5_SCL              (MFP_REG(0x1D4) | MFP_AF4 | MFP_DRIVE_SLOW)
#define TWSI5_SDA              (MFP_REG(0x1D8) | MFP_AF4 | MFP_DRIVE_SLOW)

/* TWSI6 */
#define TWSI6_SCL              (MFP_REG(0x1CC) | MFP_AF2 | MFP_DRIVE_SLOW)
#define TWSI6_SDA              (MFP_REG(0x1D0) | MFP_AF2 | MFP_DRIVE_SLOW)

/* MMC1 */
#define MMC1_DATA3             (MFP_REG(0x28) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define MMC1_DATA2             (MFP_REG(0x2c) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define MMC1_DATA1             (MFP_REG(0x30) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define MMC1_DATA0             (MFP_REG(0x34) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define MMC1_CLK               (MFP_REG(0x48) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define MMC1_CMD               (MFP_REG(0x3c) | MFP_AF1 | MFP_DRIVE_MEDIUM)
#define MMC1_CD                (MFP_REG(0x4c) | MFP_AF1 | MFP_PULL_HIGH)
#define MMC1_WP                (MFP_REG(0x50) | MFP_AF1 | MFP_PULL_HIGH)

/* MMC3 */
#define MMC3_DATA7             (MFP_REG(0x1ec) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA6             (MFP_REG(0x20c) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA5             (MFP_REG(0x1e8) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA4             (MFP_REG(0x208) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA3             (MFP_REG(0x1e4) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA2             (MFP_REG(0x204) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA1             (MFP_REG(0x1e0) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_DATA0             (MFP_REG(0x200) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_CLK               (MFP_REG(0x240) | MFP_AF2 | MFP_DRIVE_MEDIUM)
#define MMC3_CMD               (MFP_REG(0x244) | MFP_AF2 | MFP_DRIVE_MEDIUM)

/* LCD */
#define BACK_LIGHT_PWM3		(MFP_REG(0x128) | MFP_AF5 | MFP_DRIVE_MEDIUM)

#define VOLUME_UP		(MFP_REG(0x94) | MFP_AF0 | MFP_DRIVE_MEDIUM | MFP_PULL_HIGH)
#define VOLUME_DOWN		(MFP_REG(0x98) | MFP_AF0 | MFP_DRIVE_MEDIUM | MFP_PULL_HIGH)

/* VERS */
#define PLAT_VERS_PIN0		(MFP_REG(0x010) | MFP_AF0 | MFP_PULL_HIGH)
#define PLAT_VERS_PIN1		(MFP_REG(0x014) | MFP_AF0 | MFP_PULL_HIGH)
#define PLAT_VERS_PIN2		(MFP_REG(0x018) | MFP_AF0 | MFP_PULL_HIGH)
#define PLAT_VERS_PIN3		(MFP_REG(0x01c) | MFP_AF0 | MFP_PULL_HIGH)

/* More macros can be defined here... */

#define MFP_PIN_MAX	117
#endif /* __AMAD6_MFP_H */
