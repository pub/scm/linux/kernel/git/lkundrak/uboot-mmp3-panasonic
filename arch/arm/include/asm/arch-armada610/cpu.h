/*
 * (C) Copyright 2011
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#ifndef _AMAD6_CPU_H
#define _AMAD6_CPU_H

#include <asm/io.h>
#include <asm/system.h>

/*
 * Main Power Management (MPMU) Registers
 * Refer Register Datasheet 9.1
 */
struct armd6mpmu_registers {
	u8 pad0[0x0418];
	u32 pll1;	/*0x0418*/
	u8 pad1[0x1024 - 0x0418 - 4];
	u32 acgr;	/*0x1024*/
};

/*
 * APB Clock Reset/Control Registers
 * Refer Register Datasheet 6.14
 */
struct armd6apbc_registers {
	u8 pad0[0x004];
	u32 twsi1;	/*0x004*/
	u8 pad1[0x024 - 0x04 - 4];
	u32 timers;	/*0x024*/
	u8 pad2[0x034 - 0x24 - 4];
	u32 uart3;	/*0x034*/
	u32 gpio;	/*0x038*/
	u8 pad3[0x044 - 0x38 - 4];
	u32 pwm3;	/*0x044*/
	u8 pad4[0x064 - 0x44 - 4];
	u32 aib;	/*0x064*/
	u8 pad5[0x07c - 0x64 - 4];
	u32 twsi5;	/*0x07c*/
	u32 twsi6;	/*0x080*/
};

/*
 * Application Subsystem PMU(APMU) Registers
 * Refer Register Datasheet Appendix A.24
 */
struct armd6apmu_registers {
	u8 pad0[0x034];
	u32 usb_dyn_gate;	/*0x034*/
	u8 pad1[0x04c - 0x34 - 4];
	u32 display1;		/*0x04c*/
	u8 pad2[0x054 - 0x4c - 4];
	u32 sd1;		/*0x054*/
	u8 pad3[0x05c - 0x54 - 4];
	u32 usb;		/*0x05c*/
	u8 pad4[0x0dc - 0x5c - 4];
	u32 gbl_clkctrl;	/*0x0dc*/
	u8 pad5[0x0e8 - 0xdc - 4];
	u32 sd2;		/*0x0e8*/
	u32 sd3;		/*0x0ec*/
};

/*
 * Timer registers
 * Refer 6.2.9 in Datasheet
 */
struct armd6timer_registers {
	u32 clk_ctrl;	/* Timer clk control reg */
	u32 match[9];	/* Timer match registers */
	u32 count[3];	/* Timer count registers */
	u32 status[3];
	u32 ie[3];
	u32 preload[3];	/* Timer preload value */
	u32 preload_ctrl[3];
	u32 wdt_match_en;
	u32 wdt_match_r;
	u32 wdt_val;
	u32 wdt_sts;
	u32 icr[3];
	u32 wdt_icr;
	u32 cer;	/* Timer count enable reg */
	u32 cmr;
	u32 ilr[3];
	u32 wcr;
	u32 wfar;
	u32 wsar;
	u32 cvwr[3];
};

struct armd6cpu_registers {
	u32 chip_id;            /* Chip Id Reg */
};

/*
 * DDR Memory Control Registers
 * Refer Datasheet 4.4
 */
struct armd6ddr_map_registers {
	u32	cs;	/* Memory Address Map Register -CS */
	u32	pad[3];
};

struct armd6ddr_registers {
	u8	pad[0x100];
	struct armd6ddr_map_registers mmap[2];
};

/*
 * Functions
 */
u32 armd6_sdram_base(int);
u32 armd6_sdram_size(int);
int mv_sdh_init(u32 regbase, u32 max_clk, u32 min_clk, u32 quirks);
#define __stringify_1(x)        #x
#define __stringify(x)          __stringify_1(x)

#define read_cpuid(reg)							\
	({								\
		unsigned int __val;					\
		asm("mrc	p15, 0, %0, c0, c0, " __stringify(reg)	\
		    : "=r" (__val)					\
		    :							\
		    : "cc");						\
		__val;							\
	})

#ifdef CONFIG_ARMADA610
static unsigned int __cpu_is_armada610(unsigned int id)
{
		unsigned int _id = ((id) >> 8) & 0xff;
		return (_id == 0xb7 || _id == 0xc0  || _id == 0x58);
}
#else
static unsigned int __cpu_is_armada610(unsigned int id)
{
		return 0;
}
#endif
int cpu_is_armada610(void);
int cpu_is_armada610_a0(void);
int cpu_is_armada610_a1(void);
int cpu_is_armada610_a2(void);
int cpu_is_armada610_z0(void);
int cpu_is_armada610_z1(void);
/* armada620 special mmc tuning register */
#define SD_FIFO_PARAM                  0x104
#define  DIS_PAD_SD_CLK_GATE   (1 << 10)
#define  CLK_GATE_ON           (1 << 9)
#define  CLK_GATE_CTL          (1 << 8)
#define  WTC_DEF               0x1
#define  WTC(x)                        ((x & 0x3) << 2)
#define  RTC_DEF               0x1
#define  RTC(x)                        (x & 0x3)

#define SD_CLOCK_AND_BURST_SIZE_SETUP  0x10a
#define  SDCLK_DELAY(x)                ((x & 0x1f) << 9)
#define  SDCLK_SEL             (1 << 8)
#define  WR_ENDIAN             (1 << 7)
#define  RD_ENDIAN             (1 << 6)
#define  DMA_FIFO_128          1
#define  DMA_SIZE(x)           ((x & 0x3) << 2)
#define  BURST_64              1
#define  BURST_SIZE(x)         (x & 0x3)
#endif /* _AMAD6_CPU_H */
